#!/bin/sh
# Copyright (c) 2018-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

python3 embed_support_poses_taxonomy.py \
       -dim 2 \
       -lr 0.3 \
       -epochs 1000 \
       -negs 10 \
       -burnin 20 \
       -ndproc 4 \
       -model distorsion \
       -manifold poincare \
       -dset support_poses_closure.csv \
       -checkpoint support_poses_distance.pth \
#       -checkpoint support_poses_distorsion.pth \
       -batchsize 10 \
       -eval_each 1 \
       -fresh \
       -sparse \
       -train_threads 2 \
       -init True \
#       -initembedding support_poses_distance.pth \
