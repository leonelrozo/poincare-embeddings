import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
from HyperbolicEmbeddings.hyperbolic_manifold.poincare_functions import poincare_expmap, poincare_logmap
from hype.graph import load_edge_list

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))


def visualize_poincare_grid(grid_embeddings_coord, grid_embeddings_objects, edges_list=None):
    """
    Plots a 2D grid composed of 'num_nodes' nodes, represented by points embedded in the Hyperbolic manifold.


    Parameters
    ----------
    :param grid_embeddings_coord:
    :param grid_embeddings_objects

    """
    # Plot Poincaré disk
    ax = plt.gca()
    circle = plt.Circle(np.array([0, 0]), radius=1., color='black', fill=False)
    ax.add_patch(circle)

    # Plots grid nodes
    fontdict = {"fontsize": 20}
    node = 0
    for poincare_node in grid_embeddings_coord:
        color = color_function_support_poses(grid_embeddings_objects[node])
        plt.scatter(poincare_node[0], poincare_node[1], s=100, alpha=1.0, color=color)
        plt.text(poincare_node[0], poincare_node[1], grid_embeddings_objects[node], fontdict=fontdict)
        node += 1

    if edges_list is not None:
        nb_points = 10
        t = np.linspace(0., 1., nb_points)
        for edge in edges_list:
            x = grid_embeddings_coord[edge[0]]
            y = grid_embeddings_coord[edge[1]]
            u = poincare_logmap(x, y)
            geodesic = np.array([poincare_expmap(x, u, t[idx]) for idx in range(nb_points)])
            plt.plot(geodesic[:, 0], geodesic[:, 1], color='grey', linestyle='--')

    plt.xticks([])
    plt.yticks([])

    plt.show(block=False)


def color_function_support_poses(support_pose_name):
    single_foot_color = "orange"
    double_foot_color = "lightcoral"
    single_knee_color = "skyblue"
    double_knee_color = "lightgreen"
    combined_double_color = "lightsteelblue"

    if 'f2' in support_pose_name:
        color = double_foot_color
    elif 'k2' in support_pose_name:
        color = double_knee_color
    elif 'fk' in support_pose_name or 'kf' in support_pose_name:
        color = combined_double_color
    elif 'k' in support_pose_name:
        color = single_knee_color
    else:
        color = single_foot_color

    return color


if __name__ == '__main__':
    # Load grid Poincare embeddings
    # embed_grid_file = 'support_poses_distance.pth.best'
    embed_grid_file = 'support_poses_distorsion.pth.best'
    # embed_grid_file = 'support_poses.pth.0'
    DATA_PATH = ''
    # DATA_PATH = '../'
    grid_embeddings_info = torch.load(os.path.join(DATA_PATH, embed_grid_file))
    grid_embeddings_coord = grid_embeddings_info['embeddings'].cpu().numpy()  # Poincare coords of embedded grid nodes
    grid_embeddings_objects = grid_embeddings_info['objects']  # Embedded objects (name of grid nodes)
    num_nodes = grid_embeddings_coord.shape[0]  # Num of nodes in the grid

    dset = os.path.join(CURRENT_DIR, 'support_poses_closure.csv')
    idx, objects, weights = load_edge_list(dset, symmetrize=False)

    ax_embedding = None
    plt.figure(figsize=(10, 10))
    ax_embedding = plt.gca()
    visualize_poincare_grid(grid_embeddings_coord, grid_embeddings_objects, edges_list=idx)