import os
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser

from HyperbolicEmbeddings.ml_datasets_utils.data_loaders import MnistDataset, mnist_color_function
from HyperbolicEmbeddings.model_loading_utils.load_gplvm import load_trained_gplvm_model
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_geodesic, \
    lorentz_distance_torch
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d, \
    plot_gplvm_pullback_metric
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.normalization import centering
from HyperbolicEmbeddings.gplvm.gplvm_predict_pullback_metric import get_pullback_metric_function
from HyperbolicEmbeddings.utils.geodesics import get_geodesic_on_learned_manifold

import matplotlib.colors as pltc
import matplotlib.pyplot as plt
from mayavi import mlab


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()


def main(manifold, latent_dim, model_type, dataset, loss_type, loss_scale, pullback_metric=False):

    # Check that the pullback metric does not come with additional loss
    if pullback_metric and loss_type != 'Zero':
        raise NotImplementedError
    # Check that the pullback metric is applied only on Euclidean manifolds
    if pullback_metric and manifold != 'euclidean':
        raise NotImplementedError

    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    data_folder_path = ROOT_DIR / 'data'
    # Load data
    batch_size = 100
    dataset_loader = MnistDataset(batch_size)
    classes = [0, 1, 2, 3, 6, 9]
    # classes = [0, 1, 3, 6, 7, 9]
    # classes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    nb_classes = len(classes)
    data_per_class = 100
    training_data, nodes_names = dataset_loader.get_dynamic_binary_mnist_subset(train=True, data_per_class=data_per_class, classes=classes)
    color_function = mnist_color_function
    nodes_names_np = nodes_names.detach().numpy()
    nodes_legend_for_plot = [str(nodes_names_np[i]) for i in range(nodes_names_np.shape[0])]

    geodesic_idx = []

    # Center data
    training_data, data_mean = centering(training_data)

    # Rescale distance
    # if max_manifold_distance:
    #     max_graph_distance = np.max(graph_distances.detach().numpy())
    #     graph_distances = graph_distances / max_graph_distance * max_manifold_distance

    # Model parameters
    N = len(training_data)

    # Model name
    model_name = 'hyperbolic_gplvm_' + model_type + '_dim' + str(latent_dim) + \
        '_' + dataset + '_' + ' '.join(str(c) for c in classes).replace(" ", "") + \
        '_' + str(data_per_class) + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    # if max_manifold_distance:
        # model_name += '_maxdist' + str(max_manifold_distance)
    if pullback_metric:
        model_name += '_pullback'

    # Load model parameters
    outputscale_prior = (5.0, 0.8)
    mll = load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, model_type, dataset, training_data,
                                   None, None, None, loss_type, loss_scale, rejection_sampling_kernel=True,
                                   outputscale_prior=outputscale_prior)

    # Test evaluation
    posterior = mll.model(mll.model.X())
    error = (posterior.mean.T - training_data).detach().numpy()

    fig, axes = plt.subplots(nb_classes, 5, figsize=(10, 10))
    for i in range(nb_classes):
        for j in range(5):
            axes[i,j].imshow(training_data[i*data_per_class + j].detach().numpy().reshape(28,28), cmap='gray')
            axes[i,j].axis('off')
    fig, axes = plt.subplots(nb_classes, 5, figsize=(10, 10))
    for i in range(nb_classes):
        for j in range(5):
            axes[i,j].imshow(posterior.mean.T[i*data_per_class + j].detach().numpy().reshape(28,28), cmap='gray')
            axes[i,j].axis('off')
    plt.show()

    # Latent variables
    x_latent = mll.model.X().detach()

    # Get colors
    x_colors = []
    for n in range(N):
        x_colors.append(color_function(nodes_names[n]))

    # Compute pullback metric
    if pullback_metric:
        pullback_metric_function = get_pullback_metric_function(x_latent, training_data, mll.model.likelihood,
                                                                mll.model.covar_module)

    # Compute geodesics
    geodesics = []
    geodesics_task_space_mean = []
    geodesics_task_space_variance = []
    geodesics_closest_class_names = []
    geodesics_closest_class_idx = []
    geodesics_color = []
    nb_points_geodesic = 50
    for idx in geodesic_idx:
        if "hyperbolic" in model_name:
            # Compute geodesic in Lorentz and project to Poincare
            geodesic = lorentz_geodesic(x_latent[idx[0]], x_latent[idx[1]], nb_points=nb_points_geodesic)
            geodesic_poincare = lorentz_to_poincare(geodesic)
            geodesics.append(geodesic_poincare.detach().numpy())
        else:
            t = torch.linspace(0., 1., nb_points_geodesic)[:, None]
            geodesic = x_latent[idx[0]] + t * (x_latent[idx[1]] - x_latent[idx[0]])
            if pullback_metric:
                # Computes geodesic path with gradient-based energy minimization
                geodesic = get_geodesic_on_learned_manifold(geodesic, pullback_metric_function,
                                                            n_eval=nb_points_geodesic,
                                                            num_nodes_to_optimize=nb_points_geodesic)
            geodesics.append(geodesic.detach().numpy())

        # For each geodesic check closest class (=class of closest data point in the latent space) along the way
        if "hyperbolic" in model_name:
            distance_matrix = lorentz_distance_torch(geodesic, x_latent)
        else:
            distance_matrix = mll.model.covar_module.covar_dist(geodesic, x_latent)

        closest_point_idx = torch.argmin(distance_matrix, dim=1).detach().numpy()
        # closest_class_idx = indices_in_graph[closest_point_idx]
        closest_class_name = [nodes_names[i] for i in closest_point_idx.tolist()]
        # geodesics_closest_class_idx.append(closest_class_idx)
        geodesics_closest_class_names.append(closest_class_name)

        # Trajectories in observation / task space
        posterior = mll.model(geodesic)
        geodesics_task_space_mean.append(posterior.mean.T.detach().numpy())
        geodesics_task_space_variance.append(posterior.variance.T.detach().numpy())
        del posterior  # clear up memory

        # Get colors
        geodesic_color = []
        for j in range(nb_points_geodesic):
            geodesic_color.append(color_function(closest_class_name[j]))
        geodesics_color.append(geodesic_color)

        print(closest_class_name)

    # np.savez(MODEL_PATH / ('geodesics/' + model_name + '_geodesics.npz'), geodesics=geodesics,
    #          geodesic_idx=geodesic_idx,
    #          geodesics_closest_class_names=geodesics_closest_class_names,
    #          geodesics_task_space_mean=geodesics_task_space_mean,
    #          geodesics_task_space_variance=geodesics_task_space_variance)

    # Plot geodesics in latent space
    if "hyperbolic" in model_name:
        # From Lorentz to Poincaré
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()

    # If the latent space is H2, we plot the embedding in the Poincaré disk
    if latent_dim == 2:
        if "hyperbolic" in model_name:
            if "mnist" in model_name:
                if "MAP" in model_name:
                    max_magnification = 2.5

            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, geodesics=geodesics, geodesics_colors=geodesics_color,
                                     model=mll.model,
                                     model_magnification_path=MODEL_PATH / (model_name + '_magfac.npz'),
                                    #  max_magnification=max_magnification,
                                     save_path=VIZ_DIR / (model_name + '_latent.png'), show=True)
        elif pullback_metric:
            # TODO: now, we approximate the closest training latent point along each geodesic with Euclidean distance
            plot_gplvm_pullback_metric(x_latent, training_data, mll.model.likelihood, mll.model.covar_module,
                                       x_colors=x_colors, geodesics=geodesics, geodesics_colors=geodesics_color,
                                       grid_size=128, save_path=VIZ_DIR / (model_name + '_latent.png'), show=True)

        else:
            plot_euclidean_gplvm_2d(x_latent.detach().numpy(), x_colors, geodesics=geodesics,
                                    geodesics_colors=geodesics_color, model=mll.model,
                                    save_path=VIZ_DIR / (model_name + '_latent.png'))

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    elif latent_dim == 3:
        if "hyperbolic" in model_name:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors, geodesics=geodesics, geodesics_colors=geodesics_color,
                                     save_path=VIZ_DIR / (model_name + '_latent.png'), show=True)
        else:
            plot_euclidean_gplvm_3d(x_latent.detach().numpy(), x_colors, geodesics=geodesics,
                                    geodesics_colors=geodesics_color,
                                    save_path=VIZ_DIR / (model_name + '_latent.png'), show=True)

    # Plot distances in the latent space
    if "hyperbolic" in model_name:
        distances_latent = lorentz_distance_torch(x_latent, x_latent).detach().numpy()
    elif pullback_metric:
        distances_latent = np.load(MODEL_PATH / (model_name + '_geodesic_distances.npz'))['geodesic_distances']
    else:
        distances_latent = mll.model.covar_module.covar_dist(x_latent, x_latent).detach().numpy()

    plot_distance_matrix(distances_latent, max_distance=None, x_colors=x_colors,
                         save_path=VIZ_DIR / (model_name + '_distances.png'), show=True)

    print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--manifold", dest="manifold", default="hyperbolic",
                        help="Latent manifold. Options: hyperbolic, euclidean")
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=3,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_type", dest="model_type", default="MAP",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="mnist",
                        help="Set the dataset. Options: mnist.")
    parser.add_argument("--loss_type", dest="loss_type", default="Zero",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=0.0,
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="PCA",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--pullback_metric", dest="pullback_metric", type=bool, default=False,
                        help="If True, compute and plot pullback metric and geodesics.")


    args = parser.parse_args()

    manifold = args.manifold
    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    pullback_metric = args.pullback_metric

    main(manifold, latent_dim, model_type, dataset, loss_type, loss_scale, pullback_metric)

