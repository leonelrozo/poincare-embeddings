import numpy as np
import torch
from matplotlib import pyplot as plt
import matplotlib.colors as pltc
from mayavi import mlab
from tqdm import trange
import urllib.request
import tarfile

import gpytorch
from gpytorch.mlls import VariationalELBO
from gpytorch.likelihoods import GaussianLikelihood
import gpytorch.priors.torch_priors as torch_priors

from geoopt.optim.radam import RiemannianAdam

from HyperbolicEmbeddings.hyperbolic_distributions.hyperbolic_wrapped_normal import LorentzWrappedNormalPrior
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import MapExactHyperbolicGPLVM
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_bayesian_gplvm_models import HyperbolicVariationalBayesianGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch

torch.set_default_dtype(torch.float64)
# gpytorch.settings.cholesky_jitter._global_double_value = 1e-4
# gpytorch.settings.cholesky_jitter._global_float_value = 1e-4


def generate_data(nb_data, latent_dim):
    # Prior on latent variable
    latent_prior_mean = torch.zeros(nb_data, latent_dim + 1)  # Mean on the Lorentz model -> N x Q+1
    latent_prior_mean[:, 0] = 1.
    latent_prior_scale = 2.0 * torch.ones(N, latent_dim)  # On the tangent space of the mean on the Lorentz model -> N x Q
    latent_prior = LorentzWrappedNormalPrior(latent_prior_mean, latent_prior_scale)

    # Latent data
    latent_data = latent_prior.sample()

    # Generate observations
    observation_data = torch.sin(latent_data * (2 * torch.pi)) + torch.randn(latent_data.size()) * 0.2
    observation_data2 = torch.cos(latent_data * (2 * torch.pi)) + torch.randn(latent_data.size()) * 0.2
    observation_data = torch.hstack((observation_data, observation_data2))

    weights = torch.randn(latent_dim+1, latent_dim*2)
    observation_data = torch.mm(latent_data, weights) / 1000.

    return observation_data, latent_data


if __name__ == '__main__':
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Model parameters
    N = 200
    latent_dim = 2  # H2
    pca = False
    n_inducing = 25
    gplvm_type = "exactH"  # exactH, bayesianH, exactE, bayesianE

    # Generate artificial data
    Y, latent_data = generate_data(N, latent_dim)
    data_dim = Y.shape[1]
    latent_data_poincare = lorentz_to_poincare(latent_data).detach().numpy()

    # Prior on latent variable
    latent_prior_mean = torch.zeros(N, latent_dim + 1)  # Mean on the Lorentz model -> N x Q+1
    latent_prior_mean[:, 0] = 1.
    latent_prior_scale = 2.0 * torch.ones(N, latent_dim)  # On the tangent space of the mean on the Lorentz model -> N x Q
    latent_prior = LorentzWrappedNormalPrior(latent_prior_mean, latent_prior_scale)

    # Prior on lengthscale
    # hyperbolic_kernel_lengthscale_prior = None
    hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, .1)

    # Model
    if gplvm_type == "exactH":
        # Model
        model = MapExactHyperbolicGPLVM(Y, latent_dim, pca=pca, latent_prior=latent_prior,
                                        kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior)
        # Declaring the objective to be optimised along with optimiser
        mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)
        options = {"maxiter": 500, "disp": True, "lr": 0.1}
    elif gplvm_type == "bayesianH":
        model = HyperbolicVariationalBayesianGPLVM(Y, latent_dim, n_inducing, pca=pca, latent_prior=latent_prior,
                                        kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior)
        # Likelihood
        likelihood = GaussianLikelihood(batch_shape=model.batch_shape)

        # Declaring the objective to be optimised along with optimiser
        # (see models/latent_variable.py for how the additional loss terms are accounted for)
        mll = VariationalELBO(likelihood, model, num_data=len(Y))
        options = {"maxiter": 500, "disp": True, "lr": 0.05}


    # Plot initial latent variables
    # From Lorentz to Poincaré
    if gplvm_type == "exactH":
        x_poincare = lorentz_to_poincare(model.X.X).detach().numpy()
    elif gplvm_type == "bayesianH":
        x_poincare = lorentz_to_poincare(model.X.q_mu).detach().numpy()

    if latent_dim == 2:
        plt.figure(figsize=(8, 8))
        # Plot Poincaré disk
        ax = plt.gca()
        circle = plt.Circle(np.array([0, 0]), radius=1., color='black', fill=False)
        ax.add_patch(circle)

        # Plot points
        # colors = ['r', 'b', 'g']
        # for i, label in enumerate(np.unique(labels)):
        #     X_i = x_poincare[labels == label]
        #     plt.scatter(X_i[:, 0], X_i[:, 1], c=[colors[i]], label=label)
        plt.scatter(x_poincare[:, 0], x_poincare[:, 1], c='r')
        plt.scatter(latent_data_poincare[:, 0], latent_data_poincare[:, 1], c='b')
        plt.show()

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    elif latent_dim == 3:
        # Mayavi plot of the Poincare ball
        num_pts = 200
        u = np.linspace(0, 2 * np.pi, num_pts)
        v = np.linspace(0, np.pi, num_pts)
        x = 1 * np.outer(np.cos(u), np.sin(v))
        y = 1 * np.outer(np.sin(u), np.sin(v))
        z = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
        mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
        mlab.clf()
        mlab.mesh(x, y, z, color=(0.7, 0.7, 0.7), opacity=0.1)
        mlab.points3d(0, 0, 0, color=pltc.to_rgb('black'), scale_factor=0.05)  # Plot center

        # Plot points
        # colors = ['r', 'b', 'g']
        # for i, label in enumerate(np.unique(labels)):
        #     X_i = x_poincare[labels == label]
        #     mlab.points3d(X_i[:, 0], X_i[:, 1], X_i[:, 2], color=pltc.to_rgb(colors[i]), scale_factor=0.05)
        mlab.points3d(x_poincare[:, 0], x_poincare[:, 1], x_poincare[:, 2], color=pltc.to_rgb('r'), scale_factor=0.05)
        mlab.points3d(latent_data_poincare[:, 0], latent_data_poincare[:, 1], latent_data_poincare[:, 2],
                      color=pltc.to_rgb('b'), scale_factor=0.05)

        mlab.show()

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, options=options)
    mll.eval()

    # Test evaluation
    model.eval()
    if "exact" in gplvm_type:
        posterior = model(model.X.X)
    elif "bayesian" in gplvm_type:
        posterior = model(model.X.q_mu)
    error = (posterior.mean.T - Y).detach().numpy()

    # Plot results
    # From Lorentz to Poincaré
    if gplvm_type == "exactH":
        x_poincare = lorentz_to_poincare(model.X.X).detach().numpy()
    elif gplvm_type == "bayesianH":
        x_poincare = lorentz_to_poincare(model.X.q_mu).detach().numpy()
    # If the latent space is H2, we plot the embedding in the Poincaré disk
    if latent_dim == 2:
        plt.figure(figsize=(8, 8))
        # Plot Poincaré disk
        ax = plt.gca()
        circle = plt.Circle(np.array([0, 0]), radius=1., color='black', fill=False)
        ax.add_patch(circle)

        # Plot points
        # colors = ['r', 'b', 'g']
        # for i, label in enumerate(np.unique(labels)):
        #     X_i = x_poincare[labels == label]
        #     plt.scatter(X_i[:, 0], X_i[:, 1], c=[colors[i]], label=label)
        plt.scatter(x_poincare[:, 0], x_poincare[:, 1], c='r')
        plt.scatter(latent_data_poincare[:, 0], latent_data_poincare[:, 1], c='b')

        plt.show()

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    elif latent_dim == 3:
        # Mayavi plot of the Poincare ball
        num_pts = 200
        u = np.linspace(0, 2 * np.pi, num_pts)
        v = np.linspace(0, np.pi, num_pts)
        x = 1 * np.outer(np.cos(u), np.sin(v))
        y = 1 * np.outer(np.sin(u), np.sin(v))
        z = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
        mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
        mlab.clf()
        mlab.mesh(x, y, z, color=(0.7, 0.7, 0.7), opacity=0.1)
        mlab.points3d(0, 0, 0, color=pltc.to_rgb('black'), scale_factor=0.05)  # Plot center

        # Plot points
        # colors = ['r', 'b', 'g']
        # for i, label in enumerate(np.unique(labels)):
        #     X_i = x_poincare[labels == label]
        #     mlab.points3d(X_i[:, 0], X_i[:, 1], X_i[:, 2], color=pltc.to_rgb(colors[i]), scale_factor=0.05)
        mlab.points3d(x_poincare[:, 0], x_poincare[:, 1], x_poincare[:, 2], color=pltc.to_rgb('r'), scale_factor=0.05)
        mlab.points3d(latent_data_poincare[:, 0], latent_data_poincare[:, 1], latent_data_poincare[:, 2],
                      color=pltc.to_rgb('b'), scale_factor=0.05)

        mlab.show()

    print('End')
