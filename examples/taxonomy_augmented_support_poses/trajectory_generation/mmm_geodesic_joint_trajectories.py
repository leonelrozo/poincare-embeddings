import os
import gym
from pathlib import Path
import numpy as np
from robot_env import mjc_env
import mujoco

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import get_semifull_dataset
from HyperbolicEmbeddings.mmm_mujoco_utils.mmm_controllers import create_controllers, inverse_kinematics

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, '../../final_models/geodesics/')
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../final_models/vae_baselines/geodesics/')
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../saved_models/geodesics/')


if __name__ == '__main__':
    # Load mujoco environment and controller
    env = gym.make('MMMFullEmptyWorldEnv-v0')
    env.reset()

    # Create controllers in the order left leg, right leg, left arm, right arm
    controllers = create_controllers(env)

    # Load data (for initial joint configuration and joint names)
    base_path = Path(__file__).parent.parent.parent.resolve()
    data_folder_path = base_path / "data/support_poses"
    data_path = str(data_folder_path / 'keyPoseShapesXPose.xml')  # Path of XML file including Shape poses dataset
    pose_data, joint_data, _, joint_names = get_semifull_dataset(data_path)

    # Get joints ids
    joint_ids, _, _, _, _, _ = controllers[0].get_joint_ids(joint_names)

    # Load trajectories
    # model_name = 'hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5'
    model_name = 'euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5'
    # model_name = 'hyperbolic_vae_augmented_joints_Stress1.5'
    geodesic_data = np.load(MODEL_PATH + model_name + '_geodesics.npz')
    geodesics = geodesic_data["geodesics"]
    geodesic_idx = geodesic_data["geodesic_idx"]
    geodesics_closest_class_names = geodesic_data["geodesics_closest_class_names"]
    geodesics_task_space_mean = geodesic_data["geodesics_task_space_mean"]
    # geodesics_task_space_variance = geodesic_data["geodesics_task_space_variance"]

    for g in range(len(geodesics_task_space_mean)):
        print('Geodesic: ', g)
        geodesic = geodesics_task_space_mean[g]
        joint_position_log = []
        xpos_log = []

        # Set initial joint angle set
        env.data.qpos[joint_ids] = joint_data[geodesic_idx[g][0]]
        mujoco.mj_step(controllers[0].model, controllers[0].data, nstep=1)  # 1 step to actualize the data (any controller)
        for _ in range(100):
            env.render()

        for n in range(geodesic.shape[0]):
            # Set initial joint angle set
            env.data.qpos[joint_ids] = geodesic[n]
            mujoco.mj_step(controllers[0].model, controllers[0].data,
                           nstep=1)  # 1 step to actualize the data (any controller)
            for _ in range(10):
                env.render()

            # joint_position_log.append(joint_position)
            joint_position_log.append(env.data.qpos[joint_ids])

        for _ in range(100):
            env.render()

        path = MODEL_PATH + model_name + '_joints_geodesic' + str(g) + '.npz'
        np.savez(path, joint_positions=np.array(joint_position_log), #xpositions=np.array(xpos_log),
                 geodesic_idx=geodesic_idx[g], joint_names=joint_names)
