import json
import os
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser

import gpytorch
from gpytorch.kernels import ScaleKernel, RBFKernel
import gpytorch.priors.torch_priors as torch_priors

from geoopt.optim.radam import RiemannianAdam

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, color_function_shape_poses, text_function_shape_poses, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_semifull_dataset, get_missing_data_semifull_dataset, get_missing_data_reduced_dataset_2poses, \
    get_indices_shape_poses_graph, NAME_CLASS_MAPPING
from HyperbolicEmbeddings.gplvm.gplvm_models import BackConstrainedExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_initializations import \
    hyperbolic_tangent_pca_initialization, hyperbolic_stress_loss_initialization
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import BackConstrainedHyperbolicExactGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_distance_torch
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphGaussianKernel, GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix

from analysis.taxonomy_support_poses.plots_with_added_poses_and_classes import _inner_plotting_function_hyperbolic3d, \
    _inner_plot_distance_matrix


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../final_models/')
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()
MODEL_PATH = ROOT_DIR / 'saved_models'
MODEL_PATH.mkdir(exist_ok=True)
VIZ_DIR = ROOT_DIR / "model_viz"
VIZ_DIR.mkdir(exist_ok=True)
TRAINED_MODELS_PATH = ROOT_DIR / "final_models"
FIGURE_PATH = os.path.join(CURRENT_DIR, '../../../images/')


def load_trained_hyperbolic_gplvm_model(model_name, training_data, adjacency_matrix, shape_poses_indices,
                                        shape_pose_graph_distances, loss_type, loss_scale):
    # Prior on latent variable
    kernel_lengthscale_prior = None
    kernel_outputscale_prior = None

    # Model
    data_kernel = ScaleKernel(RBFKernel())
    classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))
    if "hyperbolic" in model_name:
        model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                                    data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                    kernel_lengthscale_prior=kernel_lengthscale_prior,
                                                    kernel_outputscale_prior=kernel_outputscale_prior,
                                                    taxonomy_based_back_constraints=True)
    else:
        model = BackConstrainedExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                          data_kernel=data_kernel, classes_kernel=classes_kernel,
                                          kernel_lengthscale_prior=kernel_lengthscale_prior,
                                          kernel_outputscale_prior=kernel_outputscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    mll.load_state_dict(torch.load(MODEL_PATH + model_name))  # load the model from file
    mll.eval()

    return mll


def main(
    latent_dim,
    model_name,
    dataset='semifull',
    loss_type='Zero',
    class_to_skip=(1, 1, 0),
    init_type="PCA",
    loss_scale=0.05,
    plot_on=False
):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        pose_data, joint_data, shape_pose_names, _ = parse_xml_pose_shape_data(data_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, _ = get_semifull_dataset(data_path)
    elif dataset == 'reduced':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == 'knees2':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_knees_2poses(data_path)

    # TODO: remove a class here, but save it for later encoding.
    ## Make sure that the classes are well encoded in tuples (F, H, K)
    for pose in shape_pose_names:
        n_feet, n_hands, n_knees = NAME_CLASS_MAPPING[pose]
        assert (
            pose.count("Foot") == n_feet and pose.count("Hand") == n_hands and pose.count("Knee") == n_knees
        )

    ## Building the reduced dataset, and saving the skipped data.
    # class_to_skip = (1, 1, 0)  # 1F-1H, for example.
    reduced_shape_pose_names = []
    reduced_pose_data = []
    reduced_joint_data = []
    skipped_pose_data = []
    skipped_joint_data = []
    skipped_shape_pose_names = [] # this one will be n copies of the same, but we still need it.

    for (pose, joints, pose_name) in zip(pose_data, joint_data, shape_pose_names):
        if NAME_CLASS_MAPPING[pose_name] != class_to_skip:
            reduced_shape_pose_names.append(pose_name)
            reduced_pose_data.append(pose)
            reduced_joint_data.append(joints)
        else:
            skipped_pose_data.append(pose)
            skipped_joint_data.append(joints)
            skipped_shape_pose_names.append(pose_name)

    reduced_data = reduced_joint_data
    skipped_data = skipped_joint_data

    graph_file_path = data_folder_path / 'support_poses_augmented_closure.csv'
    adjacency_matrix, reduced_shape_poses_indices = get_indices_shape_poses_graph(graph_file_path,
                                                                                  reduced_shape_pose_names,
                                                                                  augmented_taxonomy=True)
    reduced_shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, reduced_shape_pose_names,
                                                                            augmented_taxonomy=True)
    reduced_shape_poses_legend_for_plot = text_function_shape_poses(reduced_shape_pose_names)

    _, skipped_shape_poses_indices = get_indices_shape_poses_graph(graph_file_path, skipped_shape_pose_names,
                                                                   augmented_taxonomy=True)
    
    # Set training data
    reduced_data = torch.from_numpy(np.array(reduced_data))
    reduced_shape_poses_indices = torch.from_numpy(reduced_shape_poses_indices)

    skipped_data = torch.from_numpy(np.array(skipped_data))
    skipped_shape_poses_indices = torch.from_numpy(skipped_shape_poses_indices)

    training_data = reduced_data

    # Define the model (using the same hyperparameters as before)

    hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)
    hyperbolic_kernel_outputscale_prior = None

    data_kernel = ScaleKernel(RBFKernel())
    classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

    if loss_type == 'Stress':
        data_kernel.base_kernel.lengthscale = 2.0
        classes_kernel.base_kernel.lengthscale = 0.8
        # classes_kernel.base_kernel.lengthscale = 0.5  # graph approx kernel
        data_kernel.outputscale = 2.0
        classes_kernel.outputscale = 1.0
    elif loss_type == 'Zero':
        data_kernel.base_kernel.lengthscale = 1.5  # TODO
        classes_kernel.base_kernel.lengthscale = 0.6
        data_kernel.outputscale = 1.0
        classes_kernel.outputscale = 1.0
        # data_kernel.base_kernel.lengthscale = 2.0
        # classes_kernel.base_kernel.lengthscale = 0.5
        # data_kernel.outputscale = 1.0
        # classes_kernel.outputscale = 1.0

    # Training the one-skipped_class model
    # Initialization
    if init_type == 'Stress':
        X_init = hyperbolic_stress_loss_initialization(training_data, latent_dim, reduced_shape_pose_graph_distances)
    elif init_type == 'PCA':
        X_init = hyperbolic_tangent_pca_initialization(training_data, latent_dim)
    else:
        X_init = None
    # Model
    model_skipped = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, reduced_shape_poses_indices,
                                                        data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                        kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                                        kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                                        X_init=X_init, taxonomy_based_back_constraints=True)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(reduced_shape_pose_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(reduced_shape_pose_graph_distances, loss_scale)
    model_skipped.add_loss_term(added_loss)

    # Train the model.
    # Declaring the objective to be optimised along with optimiser
    mll_skipped = GPLVMExactMarginalLogLikelihood(model_skipped.likelihood, model_skipped)
    mll_skipped.train()
    fit_gplvm_torch(mll_skipped, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH / model_name, options={"maxiter": 1000})
    # mll.load_state_dict(torch.load(MODEL_PATH + model_name))  # load the model from file
    mll_skipped.eval()

    model_skipped.eval()

    # Evaluating the skipped model on held-out data.
    all_shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path,
                                                                        shape_pose_names + skipped_shape_pose_names,
                                                                        augmented_taxonomy=True)
    skipped_data = torch.from_numpy(np.array(skipped_data))
    x_latent_skipped = mll_skipped.model.X.back_constraint_function(skipped_data, skipped_shape_poses_indices)
    x_latent_and_embedded = torch.cat((model_skipped.X(), x_latent_skipped))

    # Evaluating the full model on all its data
    # x_latent_of_full_model = model_full.X()

    # ...but this data is not ordered in the same way as x_latent_and_embedded,
    # so we will re-order, moving all of the class to the last part.
    # x_latent_of_full_model_reduced = []
    # x_latent_of_full_model_skipped = []
    rearranged_graph_labels = reduced_shape_pose_names + skipped_shape_pose_names

    # # This puts the first "half" on reduced, and the second "half" on skipped
    # # in exactly the same order as x_latent_and_embedded.
    # for xi, pose_name in zip(x_latent_of_full_model, shape_pose_names):
    #     if NAME_CLASS_MAPPING[pose_name] != class_to_skip:
    #         x_latent_of_full_model_reduced.append(xi)
    #     else:
    #         x_latent_of_full_model_skipped.append(xi)

    # # Then we just concatenate them together
    # x_latent_of_full_model = torch.cat(
    #     (torch.vstack(x_latent_of_full_model_reduced), torch.vstack(x_latent_of_full_model_skipped))
    # )

    rearranged_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, rearranged_graph_labels,
                                                                    augmented_taxonomy=True)

    # Plot results
    if plot_on:
        # From Lorentz to Poincaré
        x_latent = model_skipped.X()
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()
        x_poincare_skipped = lorentz_to_poincare(x_latent_skipped).detach().numpy()
        x_poincare_all = np.concatenate((x_poincare, x_poincare_skipped))
        # x_poincare_of_full_model = lorentz_to_poincare(x_latent_of_full_model).detach().numpy()
        # Get colors
        x_colors = []
        for pose_name in reduced_shape_pose_names:
            x_colors.append(color_function_shape_poses(pose_name))
        
        x_colors_skipped = ["green" for _ in x_poincare_skipped]
        x_colors_all = x_colors + x_colors_skipped
        
        skipped_legends = text_function_shape_poses(skipped_shape_pose_names)
        all_legends = reduced_shape_poses_legend_for_plot + skipped_legends

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            # plot_hyperbolic_gplvm_2d(x_poincare, x_colors, reduced_shape_poses_legend_for_plot)
            # plot_hyperbolic_gplvm_2d(x_poincare_skipped, x_colors_skipped, skipped_legends)
            plot_hyperbolic_gplvm_2d(x_poincare_all, x_colors_all, all_legends, save_path=VIZ_DIR / f"latent_space_held_{model_name}.png")
            # plot_hyperbolic_gplvm_2d(x_poincare_of_full_model, x_colors_all, all_legends, save_path=VIZ_DIR / f"latent_space_full_model_{model_name}.png")

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            fig = _inner_plotting_function_hyperbolic3d(
                x_latent,
                reduced_shape_pose_names,
                x_latent_skipped,
                skipped_shape_pose_names
            )
            # plot_hyperbolic_gplvm_3d(x_poincare_all, x_colors_all, all_legends,
            #                          save_path=VIZ_DIR / f"latent_space_held_{model_name}.png")

        # Plot distances in the latent space
        _inner_plot_distance_matrix(
            x_latent,
            reduced_shape_pose_names,
            x_latent_skipped,
            skipped_shape_pose_names,
            model_name,
            FIGURE_PATH + 'distance_matrix_for_added_classes_' +model_name + '_with_highlighting.png',
            augmented_taxonomy=True,
        )

        # # distances_latent_full = lorentz_distance_torch(x_latent_of_full_model, x_latent_of_full_model).detach().numpy()
        # distances_latent_skipped_and_embedded = lorentz_distance_torch(x_latent_and_embedded, x_latent_and_embedded).detach().numpy()
        #
        # # max_distance = max(
        #     # all_shape_pose_graph_distances.max(),
        #     # distances_latent_skipped_and_embedded.max(),
        #     # distances_latent_full.max()
        # # )
        # max_distance = all_shape_pose_graph_distances.max()
        #
        # # plot_distance_matrix(distances_latent_full, max_distance=max_distance, save_path=VIZ_DIR / f"distances_latent_full_model_{model_name}.png")
        # plot_distance_matrix(distances_latent_skipped_and_embedded, max_distance=max_distance, save_path=VIZ_DIR / f"distances_latent_held_model_{model_name}.png")
        # # Plot distances between classes
        # plot_distance_matrix(rearranged_graph_distances, max_distance=max_distance, save_path=VIZ_DIR / f"distances_graph_{model_name}.png")

    print('End')

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=3,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_name", dest="model_name", default="hyperbolic_egplvm_augmented_joints_backconstrained",
                        help="Set the path to save the model.")
    parser.add_argument("--dataset", dest="dataset", default="feet5",
                        help="Set the dataset. Options: full, semifull, reduced, feet5, knees2.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--class_to_skip", dest="class_to_skip", type=str, default="1F1H0K",
                        help="Specifies the class to skip as a string nFmHkK where n, m, and k are integers.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_name = args.model_name
    dataset = args.dataset
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    init_type = args.init_type
    plot_on = args.plot_on
    class_str = args.class_to_skip

    # class_str is expected to be e.g. 1F1H0K.
    class_to_skip = (int(class_str[0]), int(class_str[2]), int(class_str[4]))

    if loss_type == "Stress":
        if latent_dim == 3:
            if dataset == "feet5":
                loss_scale = 1.5

    model_name = model_name + '_dim' + str(latent_dim) + '_' + dataset + '_' + loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)

    n_feet, n_hands, n_knees = class_to_skip
    model_name += f"_leaves_out_{n_feet}F_{n_hands}H_{n_knees}K"

    print(f"Training model: {model_name}")

    main(latent_dim, model_name,
        dataset=dataset, loss_type=loss_type, class_to_skip=class_to_skip,
        init_type=init_type, loss_scale=loss_scale, plot_on=plot_on
    )
