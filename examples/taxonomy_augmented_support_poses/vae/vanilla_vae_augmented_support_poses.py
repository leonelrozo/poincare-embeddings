"""
This script trains a vanilla VAE with latent dimension 3 on
the augmented taxonomy.
"""
from typing import Tuple, List
from pathlib import Path

import torch
import numpy as np

from gpytorch.kernels import RBFKernel

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import (
    parse_xml_pose_shape_data,
    shape_poses_graph_distance_mapping,
    color_function_shape_poses,
    text_function_shape_poses,
    get_reduced_dataset_2poses,
    get_reduced_dataset_knees_2poses,
    get_reduced_dataset_feet_5poses,
    get_semifull_dataset,
    get_indices_shape_poses_graph,
    augment_pose_data_with_contacts,
)
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import (
    plot_euclidean_gplvm_3d,
)
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import (
    lorentz_distance_torch,
    lorentz_to_poincare,
)
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix


from HyperbolicEmbeddings.vae_baselines.vanilla_vae import VanillaVAEBaseline

torch.set_default_dtype(torch.float64)

CURRENT_DIR = Path(__file__).parent.resolve()
ROOT_DIR = CURRENT_DIR.parent.parent.parent.resolve()
data_folder_path = ROOT_DIR / "data/support_poses"
saved_models_folder = ROOT_DIR / "final_models" / "vae_baselines"


def load_data(
    dataset: str,
) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, List[str]]:
    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/support_poses"
    data_path = (
        data_folder_path / "keyPoseShapesXPose.xml"
    )  # Path of XML file including Shape poses dataset
    if dataset == "full":
        (
            pose_data,
            joint_data,
            shape_pose_names,
            joint_names,
        ) = parse_xml_pose_shape_data(data_path)
    elif dataset == "semifull":
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(
            data_path
        )
    elif dataset == "reduced":
        (
            pose_data,
            joint_data,
            shape_pose_names,
            joint_names,
        ) = get_reduced_dataset_2poses(data_path)
    elif dataset == "feet5":
        (
            pose_data,
            joint_data,
            shape_pose_names,
            joint_names,
        ) = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == "knees2":
        (
            pose_data,
            joint_data,
            shape_pose_names,
            joint_names,
        ) = get_reduced_dataset_knees_2poses(data_path)

    graph_file_path = data_folder_path / "support_poses_augmented_closure.csv"
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path, shape_pose_names, augmented_taxonomy=True
    )
    adjacency_matrix, shape_poses_indices = get_indices_shape_poses_graph(
        graph_file_path, shape_pose_names, augmented_taxonomy=True
    )
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)
    pose_data = augment_pose_data_with_contacts(pose_data, shape_pose_names)

    # Transforming to torch
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    shape_poses_indices = torch.from_numpy(shape_poses_indices)

    return (pose_data, joint_data, shape_poses_indices, shape_pose_names)


def train_vanilla_vae_on_augmented(
    max_epochs: int = 1000,
    latent_dim: int = 3,
    hidden_dim: int = 22,
    dataset: str = "feet5",
    loss_type: str = "Zero",
    loss_scale: float = 0.1,
    learning_rate: float = 0.05,
) -> VanillaVAEBaseline:
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Get the training data
    _, training_data, _, shape_pose_names = load_data(dataset)

    # Get the distance matrices and other stuff we need to define the model.
    graph_file_path = data_folder_path / "support_poses_augmented_closure.csv"
    graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path, shape_pose_names, augmented_taxonomy=True
    )

    # Setting up the model and optimizer
    vae = VanillaVAEBaseline(
        input_dim=training_data.shape[1],
        latent_dim=latent_dim,
        hidden_dim=hidden_dim,
        loss_type=loss_type,
        graph_distance_matrix=graph_distances,
        loss_scale=loss_scale,
    )
    optimizer = torch.optim.Adam(vae.parameters(), lr=learning_rate)

    # Training full batch and without splitting train/test.
    for iteration in range(max_epochs):
        optimizer.zero_grad()
        loss, rec_loss, kld, added_loss = vae.elbo_loss(training_data)
        loss.backward()
        optimizer.step()

        print(
            f"Step: {iteration:05d}, loss: {loss:.3f}, rec loss: {rec_loss:.3f}, kld: {kld:.3f}, added loss: {added_loss:.3f}"
        )

    torch.save(
        vae.state_dict(),
        saved_models_folder / f"vanilla_vae_augmented_joints_{loss_type}{loss_scale}",
    )

    return vae


if __name__ == "__main__":
    # Some hyperparameters
    max_epochs = 1000
    latent_dim = 3
    hidden_dim = 22
    dataset = "feet5"
    loss_type = "Stress"
    loss_scale = 1.5
    learning_rate = 0.005

    vae = train_vanilla_vae_on_augmented(
        max_epochs=max_epochs,
        latent_dim=latent_dim,
        hidden_dim=hidden_dim,
        dataset=dataset,
        loss_type=loss_type,
        loss_scale=loss_scale,
        learning_rate=learning_rate,
    )

    vae.eval()

    # Loading up the data for visualization
    _, training_data, _, shape_pose_names = load_data(dataset)

    # Visualizing the 3d latent space
    latent_codes = vae.encode(training_data).loc
    latent_colors = [
        color_function_shape_poses(pose_name) for pose_name in shape_pose_names
    ]
    latent_legends = text_function_shape_poses(shape_pose_names)

    plot_euclidean_gplvm_3d(
        latent_codes.detach().numpy(),
        latent_colors,
        latent_legends,
        save_path=str(
            ROOT_DIR / f"vanilla_vae_augmented_joints_{loss_type}{loss_scale}.png"
        ),
    )

    # Visualizing the distance matrix.
    graph_file_path = data_folder_path / "support_poses_augmented_closure.csv"
    graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path, shape_pose_names, augmented_taxonomy=True
    )
    latent_distances = (
        RBFKernel().covar_dist(latent_codes, latent_codes).detach().numpy()
    )

    plot_distance_matrix(
        latent_distances,
        max_distance=graph_distances.max() + 1,
        save_path=ROOT_DIR
        / f"distance_matrix_vanilla_vae_augmented_joints_{loss_type}{loss_scale}.png",
    )
