from pathlib import Path
import os
import time

import mujoco_py
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import parse_xml_hand_grasps_data, get_grasp_dataset


def set_joint_values(sim, joint_name_list, q):
    """
    Set the robot joint values to the given desired values.
    """
    if len(q.shape) == 2:
        q = q[0, :]

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        states.qpos[addr] = q[joint_id]

    sim.set_state(states)
    sim.forward()


def get_joint_values(sim, joint_name_list):
    """
    Get the robot joint values.
    """
    q = []

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        q.append(states.qpos[addr])

    return q

def compute_ts_fct(sim, joint_name_list, q, tcp_name):
    """
    Computes the end-effector pose using forward kinematics.

    Parameters
    ----------
    :param q: current joint angles

    Return
    ------
    :return: end-effector pose
    """
    if len(q.shape) == 2:
        q = q[0, :]

    set_joint_values(sim, joint_name_list, q)
    tcp_pos = sim.data.get_site_xpos(tcp_name)
    tcp_xmat = sim.data.get_site_xmat(tcp_name)
    tcp_quat = np.zeros(4)
    mujoco_py.functions.mju_mat2Quat(tcp_quat, np.reshape(tcp_xmat, (-1,), order='C'))
    x_fct = np.concatenate([tcp_pos, tcp_quat], axis=-1)
    return x_fct


if __name__ == '__main__':
    mj_path = mujoco_py.utils.discover_mujoco()

    base_path = Path(__file__).parent.parent.parent.resolve()
    data_folder_path = base_path / "data"
    xml_path = str(data_folder_path / 'mmm_mujoco/environment/mmm_bimanual_kitchen.xml')
    model = mujoco_py.load_model_from_path(xml_path)
    sim = mujoco_py.MjSim(model)
    viewer_show = True
    # Remark: the model is not scaled (i.e., normalized to 1m height)

    body_joint_names = ["BLNx_joint", "BLNy_joint", "BLNz_joint",
                        "BPx_joint", "BPy_joint", "BPz_joint",
                        "BTx_joint", "BTy_joint", "BTz_joint",
                        "BUNx_joint", "BUNy_joint", "BUNz_joint",
                        "LAx_joint", "LAy_joint", "LAz_joint",
                        "LEx_joint", "LEz_joint",
                        "LHx_joint", "LHy_joint", "LHz_joint",
                        "LKx_joint",
                        "LSx_joint", "LSy_joint", "LSz_joint",
                        "LWx_joint", "LWy_joint",
                        "LFx_joint",
                        "LMrot_joint",
                        "RAx_joint", "RAy_joint", "RAz_joint",
                        "REx_joint", "REz_joint",
                        "RHx_joint", "RHy_joint", "RHz_joint",
                        "RKx_joint",
                        "RSx_joint", "RSy_joint", "RSz_joint",
                        # "RWx_joint", "RWy_joint", # in hand joints
                        "RFx_joint",
                        "RMrot_joint"
                        ]

    # Load data
    data_path = str(data_folder_path / 'hand_grasps/2123/PrecisionSphere.xml')
    single_hand_joint_data, hand_joint_data, object_motion_data, hand_joint_names = parse_xml_hand_grasps_data(data_path)

    # Datasets
    data_path = str(data_folder_path / 'hand_grasps')
    hand_joint_data, grasp_names, joint_names = get_grasp_dataset(data_path)
    # Sort data
    # from operator import itemgetter
    # idx = range(len(grasp_names))
    # sorted_idx = sorted(idx, key=lambda x: grasp_names[x])
    # grasp_names = itemgetter(*sorted_idx)(grasp_names)
    # hand_joint_data = hand_joint_data[sorted_idx]

    # Visualize each shape pose
    if viewer_show is True:
        viewer = mujoco_py.MjViewer(sim)
        viewer._paused = True

    sim.step()
    body_joint_data = get_joint_values(sim, body_joint_names)

    body_joint_data[body_joint_names.index("RSx_joint")] = 0.48744
    body_joint_data[body_joint_names.index("RSy_joint")] = -0.516448
    body_joint_data[body_joint_names.index("RSz_joint")] = 0.150373
    body_joint_data[body_joint_names.index("REx_joint")] = 1.1333
    body_joint_data[body_joint_names.index("REz_joint")] = 0.878193

    # print(np.hstack((body_joint_data, hand_joint_data[0])))

    for n in range(hand_joint_data.shape[0]):
        print(grasp_names[n])
        set_joint_values(sim, body_joint_names + hand_joint_names, np.hstack((body_joint_data, hand_joint_data[n])))

        sim.step()

        if viewer_show is True:
            viewer.render()

        time.sleep(0.1)
        viewer._paused = True

    print('End')
