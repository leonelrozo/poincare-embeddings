import random
import os
from pathlib import Path
import numpy as np
import torch
import matplotlib
import matplotlib.pyplot as plt

from HyperbolicEmbeddings.kernels.kernels_hyperbolic import HyperbolicRiemannianGaussianKernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch, lorentz_geodesic

plt.rcParams['text.usetex'] = True  # use Latex font for plots
plt.rcParams['text.latex.preamble'] = r'\usepackage{bm}'

device = 'cpu'

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.resolve()

if __name__ == '__main__':

    seed = 1234
    # Set numpy and pytorch seeds
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # Define the dimension
    dim_data = 3
    dim_hyperbolic = dim_data - 1  # We are on H^d embedded in R^d+1

    # Generate data
    data = np.array([[7.14142843, -5., -5.],
                     [5.2308406, -3.63054358, -3.63054358],
                     [3.84582629, -2.62586936, -2.62586936],
                     [2.8472249, -1.88503178, -1.88503178],
                     [2.13470104, -1.33359449, -1.33359449],
                     [1.63666324, -0.91615134, -0.91615134],
                     [1.30307067, -0.59075933, -0.59075933],
                     [1.10040533, -0.32472441, -0.32472441],
                     [1.00830422, -0.09131647, -0.09131647],
                     [1.0175134, 0.13291636, 0.13291636],
                     [1.12895818, 0.37050409, 0.37050409],
                     [1.35383606, 0.64531856, 0.64531856],
                     [1.71474186, 0.98497199, 0.98497199],
                     [2.24793793, 1.4235914, 1.4235914],
                     [3.00699763, 2.00524746, 2.00524746],
                     [4.06818817, 2.78838259, 2.78838259],
                     [5.53813365, 3.85168303, 3.85168303],
                     [7.56452819, 5.30198485, 5.30198485],
                     [10.35097565, 7.28500847, 7.28500847],
                     [14.17744688, 10., 10.]])
    # Generate data
    nb_data = 100
    base = -5 * np.ones(dim_data)
    base[0] = np.sqrt(1. + np.sum(base[1:] ** 2, 0))
    point = 10*np.ones(dim_data)
    point[0] = np.sqrt(1. + np.sum(point[1:]**2, 0))
    base = torch.from_numpy(base)
    point = torch.from_numpy(point)
    x1 = lorentz_geodesic(base, point, nb_data)
    x2 = x1[-1][None]

    lengthscale = 1.0

    # RBF kernel
    nb_points_integral = [100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000]
    kernels = [HyperbolicRiemannianGaussianKernel(dim_hyperbolic, nb_points_integral=nb) for nb in nb_points_integral]

    kernel_values = []

    for kernel in kernels:
        kernel.lengthscale = lengthscale
        kernel.to(device)
        K_rbfm = kernel.forward(x1, x2)
        K_rbfm = K_rbfm / K_rbfm[-1]
        kernel_values.append(K_rbfm.cpu().detach().numpy())

    # Distance between x1 and x2
    distance = lorentz_distance_torch(x1, x2)
    distance_np = distance.cpu().detach().numpy()

    # Colors
    cmap = matplotlib.cm.get_cmap('magma_r')
    colors = cmap(np.linspace(0.1, 1.0, len(nb_points_integral))).tolist()

    # Plot kernel value in function of the distance
    plt.figure(figsize=(12, 6))
    ax = plt.gca()
    for values, color in zip(kernel_values, colors):
        plt.plot(distance_np, values, color=color, linewidth=1.5)
    xmin, xmax = plt.xlim()
    plt.hlines(0.0, xmin, xmax, colors='grey', linestyles=':')
    ax.tick_params(labelsize=22)
    ax.set_xlabel(r"$d_{\mathcal{L}^2}(\bm{x},\bm{x}')$", fontsize=30)
    ax.set_ylabel(r"$k(\bm{x},\bm{x}')$", fontsize=30)
    ax.legend(nb_points_integral, fontsize=24)
    ax.set_xlim(left=0.0, right=8.0)
    plt.savefig(ROOT_DIR / ('Figures/hyperbolic_kernel_analysis.png'), bbox_inches='tight')
    plt.show()