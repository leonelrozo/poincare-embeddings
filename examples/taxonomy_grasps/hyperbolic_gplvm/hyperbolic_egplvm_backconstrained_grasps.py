import os
from pathlib import Path
import numpy as np
import torch
from matplotlib import pyplot as plt
import matplotlib.colors as pltc
from mayavi import mlab
from argparse import ArgumentParser

import gpytorch
from gpytorch.kernels import ScaleKernel, RBFKernel
import gpytorch.priors.torch_priors as torch_priors

from geoopt.optim.radam import RiemannianAdam

from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import get_grasp_dataset, load_grasps_data, \
    hand_grasp_name_to_grasp_labels, color_function_grasp, simple_color_function_grasp
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_initializations import \
    hyperbolic_tangent_pca_initialization, hyperbolic_stress_loss_initialization
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import BackConstrainedHyperbolicExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_distributions.hyperbolic_wrapped_normal import LorentzWrappedNormalPrior
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_distance_torch
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphGaussianKernel, GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.normalization import centering


torch.set_default_dtype(torch.float64)
# gpytorch.settings.cholesky_jitter._global_double_value = 1e-4
# gpytorch.settings.cholesky_jitter._global_float_value = 1e-4

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, '../../../grasps_saved_models/')


def main(latent_dim, model_name, dataset='grasps', loss_type='Zero', loss_scale=0.05, init_type="PCA", plot_on=False,
         max_manifold_distance=None):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/"
    # hand_joint_data, grasp_names, joint_names = get_grasp_dataset(str(data_folder_path))  #, subject_list=['2125', '2177'])
    #
    # # Adjacency matrix
    # adjacency_file_path = data_folder_path / 'hand_grasps_closure.csv'
    # adjacency_matrix, grasps_indices_in_tree = get_indices_grasps_graph(adjacency_file_path, grasp_names)
    #
    # # Distances
    # graph_file_path = data_folder_path / 'hand_grasps_taxonomy.csv'
    # grasps_graph_distances = hand_grasps_distance_mapping(graph_file_path, grasp_names)
    #
    # # Check wrt distance computed from the adjacency, the difference below should be 0 everywhere
    # # grasps_graph_distances_from_adjacency = hand_grasps_distance_mapping_from_adjacency(adjacency_file_path,
    # #                                                                                     grasp_names)
    # # distance_check = grasps_graph_distances - grasps_graph_distances_from_adjacency
    #
    # # Rescale distance
    # if max_manifold_distance:
    #     max_graph_distance = np.max(grasps_graph_distances.detach().numpy())
    #     grasps_graph_distances = grasps_graph_distances / max_graph_distance * max_manifold_distance
    #
    # grasps_legend_for_plot = hand_grasp_name_to_grasp_labels(grasp_names)
    #
    # # TODO check kernel parameters, check if rescaled graph distances have an influence on the kernel
    #
    # # Set training data
    # training_data = torch.from_numpy(hand_joint_data)
    # grasps_indices_in_tree = torch.from_numpy(grasps_indices_in_tree)
    # # Center
    # training_data, data_mean = centering(training_data)

    training_data, adjacency_matrix, grasps_graph_distances, grasp_names, grasps_indices_in_tree, \
    grasps_legend_for_plot, color_function = \
        load_grasps_data(data_folder_path)

    if max_manifold_distance:
        max_graph_distance = np.max(grasps_graph_distances.detach().numpy())
        grasps_graph_distances = grasps_graph_distances / max_graph_distance * max_manifold_distance

    # Model parameters
    # While we need to specify the dimensionality of the latent variables at the outset, one of the advantages of the
    # Bayesian framework is that by using a ARD kernel we can prune dimensions corresponding to small inverse
    # lengthscales.
    N = len(training_data)

    # Prior on latent variable
    hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)
    hyperbolic_kernel_outputscale_prior = None

    # Initialization
    if init_type == 'Stress':
        X_init = hyperbolic_stress_loss_initialization(training_data, latent_dim, grasps_graph_distances)
    elif init_type == 'PCA':
        X_init = hyperbolic_tangent_pca_initialization(training_data, latent_dim)
    else:
        X_init = None

    # Model
    data_kernel = ScaleKernel(RBFKernel())
    classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

    if loss_type == 'Stress':
        data_kernel.base_kernel.lengthscale = 1.8
        classes_kernel.base_kernel.lengthscale = 1.5
        data_kernel.outputscale = 2.0
        classes_kernel.outputscale = 1.0
        taxonomy_bc = True
    elif loss_type == 'Zero':
        data_kernel.base_kernel.lengthscale = 1.5  # TODO
        classes_kernel.base_kernel.lengthscale = 1.5
        data_kernel.outputscale = 1.0
        classes_kernel.outputscale = 1.0
        taxonomy_bc = False

    model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, grasps_indices_in_tree,
                                                data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                                kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                                X_init=X_init, batch_params=False,
                                                taxonomy_based_back_constraints=taxonomy_bc)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(grasps_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(grasps_graph_distances, loss_scale)
    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)
    #
    # # Plot initial latent variables
    # if plot_on:
    #     # From Lorentz to Poincaré
    #     x_latent_init = model.X()
    #     x_poincare_init = lorentz_to_poincare(x_latent_init).detach().numpy()
    #     # Get colors
    #     x_colors = []
    #     for n in range(N):
    #         x_colors.append(color_function_shape_poses(shape_pose_names[n]))
    #
    #     # If the latent space is H2, we plot the embedding in the Poincaré disk
    #     if latent_dim == 2:
    #         # Plot hyperbolic latent space
    #         plot_hyperbolic_gplvm_2d(x_poincare_init, x_colors, shape_poses_legend_for_plot)
    #
    #     # If the latent space is H3, we plot the embedding in the Poincaré ball
    #     elif latent_dim == 3:
    #         # Plot hyperbolic latent space
    #         plot_hyperbolic_gplvm_3d(x_poincare_init, x_colors, shape_poses_legend_for_plot)
    #
    #     # Plot distances in the latent space
    #     distances_latent = lorentz_distance_torch(x_latent_init, x_latent_init).detach().numpy()
    #     plot_distance_matrix(distances_latent)
    #     # Plot distances between classes
    #     plot_distance_matrix(shape_pose_graph_distances)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH + model_name, options={"maxiter": 500})
    # mll.load_state_dict(torch.load(MODEL_PATH + model_name))  # load the model from file
    mll.eval()

    # Test evaluation
    model.eval()
    posterior = model(model.X())
    error = (posterior.mean.T - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    print(added_loss.loss(model.X()).detach().numpy() / loss_scale)

    # Plot results
    if plot_on:
        # From Lorentz to Poincaré
        x_latent = model.X()
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()
        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(simple_color_function_grasp(grasp_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, grasps_legend_for_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors, grasps_legend_for_plot)

        # Plot distances in the latent space
        distances_latent = lorentz_distance_torch(x_latent, x_latent).detach().numpy()
        max_distance = np.max(distances_latent)
        plot_distance_matrix(distances_latent, max_distance=max_distance)
        # Plot distances between classes
        plot_distance_matrix(grasps_graph_distances, max_distance=max_distance)

        print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_name", dest="model_name", default="hyperbolic_egplvm_backconstrained",
                        help="Set the path to save the model.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=1200.,
    # parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=30.0,  #37.2,
                        help="Set the loss scale.")
    # dim 3: 0.8
    parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
                        help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_name = args.model_name
    dataset = args.dataset
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on

    model_name = model_name + '_dim' + str(latent_dim) + '_' + dataset + '_maxdist' + str(max_manifold_distance) + '_' + loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)

    main(latent_dim, model_name, dataset, loss_type, loss_scale, init_type, plot_on, max_manifold_distance)
