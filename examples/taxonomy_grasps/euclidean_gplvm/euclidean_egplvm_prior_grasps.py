from pathlib import Path

import os
import numpy as np
import torch
from argparse import ArgumentParser
from gpytorch.kernels.kernel import Kernel
from geoopt.optim.radam import RiemannianAdam
import gpytorch.priors.torch_priors as torch_priors

from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import get_grasp_dataset, hand_grasps_distance_mapping,\
    hand_grasp_name_to_grasp_labels, color_function_grasp, simple_color_function_grasp
from HyperbolicEmbeddings.gplvm.gplvm_initializations import euclidean_stress_loss_initialization, pca_initialization
from HyperbolicEmbeddings.gplvm.gplvm_models import MapExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, EuclideanStressLossTermExactMLL, \
    EuclideanDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.normalization import centering


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, '../../../grasps_saved_models/')


def main(latent_dim, model_name, dataset='grasps', loss_type='Zero', loss_scale=0.05, init_type="PCA", plot_on=False,
         max_manifold_distance=None, outputscale_prior=None, lengthscale_prior=None):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/hand_grasps"
    hand_joint_data, grasp_names, joint_names = get_grasp_dataset(str(data_folder_path))

    graph_file_path = data_folder_path / 'hand_grasps_taxonomy.csv'
    grasps_graph_distances = hand_grasps_distance_mapping(graph_file_path, grasp_names)

    # Rescale distance
    if max_manifold_distance:
        max_graph_distance = np.max(grasps_graph_distances.detach().numpy())
        grasps_graph_distances = grasps_graph_distances / max_graph_distance * max_manifold_distance

    grasps_legend_for_plot = hand_grasp_name_to_grasp_labels(grasp_names)

    # Set training data
    training_data = torch.from_numpy(hand_joint_data)
    # Center
    training_data, data_mean = centering(training_data)

    # Model parameters
    # While we need to specify the dimensionality of the latent variables at the outset, one of the advantages of the
    # Bayesian framework is that by using a ARD kernel we can prune dimensions corresponding to small inverse
    # lengthscales.
    N = len(training_data)

    # Parameter priors
    if outputscale_prior is not None:
        kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
    else:
        kernel_outputscale_prior = None

    if lengthscale_prior is not None:
        kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
    else:
        kernel_lengthscale_prior = None
        kernel_outputscale_prior = None

    print("Selecting these priors:")
    print(f"output scale prior:", kernel_outputscale_prior)
    print(f"length scale prior:", kernel_lengthscale_prior)

    # Initialization
    if init_type == 'Stress':
        X_init = euclidean_stress_loss_initialization(training_data, latent_dim, grasps_graph_distances)
    elif init_type == 'PCA':
        X_init = pca_initialization(training_data, latent_dim)
    else:
        X_init = None

    # Model
    model = MapExactGPLVM(training_data, latent_dim, X_init=X_init, batch_params=False,
                          kernel_lengthscale_prior=kernel_lengthscale_prior,
                          kernel_outputscale_prior=kernel_outputscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = EuclideanStressLossTermExactMLL(grasps_graph_distances, loss_scale)  #loss_scale=0.1)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = EuclideanDistortionLossTermExactMLL(grasps_graph_distances, loss_scale)  #, distance_regularizer=10.0)  # loss_scale=0.05)
    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Plot initial latent variables
    if plot_on:
        x_latent_init = model.X.X.detach().numpy()
        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(simple_color_function_grasp(grasp_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_2d(x_latent_init, x_colors, grasps_legend_for_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_3d(x_latent_init, x_colors, grasps_legend_for_plot)

        # Plot distances in the latent space
        distances_latent_init = model.covar_module.covar_dist(model.X.X, model.X.X).detach().numpy()
        plot_distance_matrix(distances_latent_init)
        # Plot distances between classes
        plot_distance_matrix(grasps_graph_distances)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH + model_name)  #, options={"maxiter": 1000, "disp": True, "lr": 0.01})
    mll.eval()

    # Test evaluation
    model.eval()
    posterior = model(model.X.X)
    error = (posterior.mean.T - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    print(added_loss.loss(model.X.X).detach().numpy() / loss_scale)

    # Plot results
    if plot_on:
        x_latent = model.X.X.detach().numpy()

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_2d(x_latent, x_colors, grasps_legend_for_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_3d(x_latent, x_colors, grasps_legend_for_plot)

        # Plot distances in the latent space
        distances_latent = model.covar_module.covar_dist(model.X.X, model.X.X).detach().numpy()
        plot_distance_matrix(distances_latent)
        # Plot distances between classes
        plot_distance_matrix(grasps_graph_distances)

    print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim.")
    parser.add_argument("--model_name", dest="model_name", default="euclidean_egplvm_prior",
                        help="Set the path to save the model.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=6000.0,
                        help="Set the loss scale.")
    parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
                        help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")
    parser.add_argument("--outputscale_prior", dest="outputscale_prior", type=str,
                        default=None, help="The position of a gamma prior for the output scale")
    parser.add_argument("--lengthscale_prior", dest="lengthscale_prior", type=str,
                        default=None, help="The position of a gamma prior for the length scale")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_name = args.model_name
    dataset = args.dataset
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on

    model_name = model_name + '_dim' + str(latent_dim) + '_' + dataset + '_maxdist' + str(max_manifold_distance) + '_' + loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)

    main(latent_dim, model_name, dataset, loss_type, loss_scale, init_type, plot_on, max_manifold_distance)
