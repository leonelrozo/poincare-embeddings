"""
This script loads and trains on a Hyperbolic VAE, implemented under
HyperbolicEmbeddings/vae_baselines/hyperbolic_vae.
"""

from pathlib import Path
from typing import List, Tuple

import matplotlib.pyplot as plt
import torch
import numpy as np

from gpytorch.kernels import RBFKernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import (
    lorentz_distance_torch,
    lorentz_to_poincare,
)

from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import (
    plot_hyperbolic_gplvm_2d,
)
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import (
    color_function_shape_poses,
    parse_xml_pose_shape_data,
    get_reduced_dataset_2poses,
    get_reduced_dataset_knees_2poses,
    get_reduced_dataset_feet_5poses,
    get_semifull_dataset,
    shape_poses_graph_distance_mapping,
    text_function_shape_poses,
)
from HyperbolicEmbeddings.vae_baselines.hyperbolic_vae import HyperbolicVAEBaseline


CURRENT_DIR = Path(__file__).parent.resolve()
ROOT_DIR = CURRENT_DIR.parent.parent.parent.resolve()
data_folder_path = ROOT_DIR / "data/support_poses"
saved_models_folder = ROOT_DIR / "final_models" / "vae_baselines"

torch.set_default_dtype(torch.float64)


def load_training_data_suppport_poses(dataset="semifull") -> Tuple[torch.Tensor, List[str]]:
    # Load data
    data_path = (
        data_folder_path / "keyPoseShapesXPose.xml"
    )  # Path of XML file including Shape poses dataset
    if dataset == "full":
        pose_data, _, shape_pose_names, _ = parse_xml_pose_shape_data(data_path)
    elif dataset == "semifull":
        pose_data, _, shape_pose_names, _ = get_semifull_dataset(data_path)
    elif dataset == "reduced":
        pose_data, _, shape_pose_names, _ = get_reduced_dataset_2poses(data_path)
    elif dataset == "feet5":
        pose_data, _, shape_pose_names, _ = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == "knees2":
        pose_data, _, shape_pose_names, _ = get_reduced_dataset_knees_2poses(data_path)

    pose_data = torch.from_numpy(pose_data)

    return pose_data, shape_pose_names


def train_hyperbolic_vae(
    max_epochs: int = 1000,
    learning_rate: float = 0.05,
    loss_type: str = "Zero",
    loss_scale: float = 0.1,
) -> HyperbolicVAEBaseline:
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Loading up the data and distances
    pose_data, shape_pose_names = load_training_data_suppport_poses()
    graph_file_path = data_folder_path / "support_poses_closure.csv"
    graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path, shape_pose_names
    )

    # Setting up the model and optimizer
    vae = HyperbolicVAEBaseline(
        input_dim=pose_data.shape[1],
        latent_dim=2,
        hidden_dim=6,
        loss_type=loss_type,
        graph_distance_matrix=graph_distances,
        loss_scale=loss_scale,
    )
    optimizer = torch.optim.Adam(vae.parameters(), lr=learning_rate)

    # Training full batch and without splitting train/test.
    for iteration in range(max_epochs):
        optimizer.zero_grad()
        loss, rec_loss, kld, added_loss = vae.elbo_loss(pose_data)
        loss.backward()
        optimizer.step()

        print(
            f"Step: {iteration:05d}, loss: {loss:.3f}, rec loss: {rec_loss:.3f}, kld: {kld:.3f}, added loss: {added_loss:.3f}"
        )

    torch.save(
        vae.state_dict(),
        saved_models_folder / f"hyperbolic_vae_{loss_type}{loss_scale}",
    )

    return vae


if __name__ == "__main__":
    # Some hyperparameters
    max_epochs = 1000
    loss_type = "Stress"
    loss_scale = 6.0

    # Loading up the data.
    pose_data, shape_pose_names = load_training_data_suppport_poses()

    # Training and saving the VAE.
    vae = train_hyperbolic_vae(
        max_epochs=max_epochs, loss_type=loss_type, loss_scale=loss_scale
    )

    # Setting up the visualization of the mean locations
    # of latent variables.
    latent_codes = vae.encode(pose_data).loc
    latent_colors = [
        color_function_shape_poses(pose_name) for pose_name in shape_pose_names
    ]
    latent_legends = text_function_shape_poses(shape_pose_names)

    # Visualizing the latent space.
    plot_hyperbolic_gplvm_2d(
        lorentz_to_poincare(latent_codes).detach().numpy(),
        latent_colors,
        # latent_legends,
        save_path=ROOT_DIR / f"hyperbolic_vae_{loss_type}{loss_scale}.png",
        model=vae,
    )

    # Visualizing the distance matrix.
    graph_file_path = data_folder_path / "support_poses_closure.csv"
    graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path, shape_pose_names
    )
    distances_latent = (
        lorentz_distance_torch(latent_codes, latent_codes).detach().numpy()
    )
    plot_distance_matrix(
        distances_latent,
        max_distance=graph_distances.max() + 1,
        save_path=ROOT_DIR
        / f"distance_matrix_hyperbolic_vae_{loss_type}{loss_scale}.png",
    )

    # plt.close()
