#!/bin/bash

python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="feet5" --latent_dim=2 --loss_type="Stress" --loss_scale=0.7
python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=1.3
python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=1.5

#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="feet5" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="knees2" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="reduced" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="semifull" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0

#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="feet5" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="knees2" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="reduced" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 hyperbolic_egplvm_taxonomy_backconstrained.py --dataset="semifull" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0