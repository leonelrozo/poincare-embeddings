import json
from pathlib import Path

import os
import numpy as np
import torch
from matplotlib import pyplot as plt
import matplotlib.colors as pltc
from mayavi import mlab
from argparse import ArgumentParser

import gpytorch
from gpytorch.likelihoods import GaussianLikelihood
import gpytorch.priors.torch_priors as torch_priors

from geoopt.optim.radam import RiemannianAdam

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, color_function_shape_poses, text_function_shape_poses, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_one_of_each_class_dataset, get_semifull_dataset
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_initializations import \
    hyperbolic_tangent_pca_initialization, hyperbolic_stress_loss_initialization
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import MapExactHyperbolicGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_distributions.hyperbolic_wrapped_normal import LorentzWrappedNormalPrior
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_distance_torch
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL, VanillaHyperbolicDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.normalization import centering


torch.set_default_dtype(torch.float64)
# gpytorch.settings.cholesky_jitter._global_double_value = 1e-4
# gpytorch.settings.cholesky_jitter._global_float_value = 1e-4

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()
MODEL_PATH = ROOT_DIR / 'saved_models'
MODEL_PATH.mkdir(exist_ok=True)
VIZ_DIR = ROOT_DIR / "model_viz"
VIZ_DIR.mkdir(exist_ok=True)
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../../saved_models/')


def main(latent_dim, model_name, dataset='full', loss_type='Zero', loss_scale=0.05, init_type="PCA", plot_on=False):
    # Setting manual seed for reproducibility
    seed = 73
    torch.manual_seed(seed)
    np.random.seed(seed)

    model_name = f"{model_name}_seed_{seed}"

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/support_poses"
    data_path = data_folder_path / "keyPoseShapesXPose.xml"
    # data_path = '../../data/keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        print("Dataset = Full")
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset == 'reduced':
        print("Dataset = reduced")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        print("Dataset = feet5")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == 'knees2':
        print("Dataset = knees2")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)
    elif dataset == 'oneofeach':
        print("Dataset = oneofeach")
        pose_data, joint_data, shape_pose_names, joint_names = get_one_of_each_class_dataset(data_path)

    graph_file_path = data_folder_path / 'support_poses_closure.csv'
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names)
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    # Set training data
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    training_data = pose_data
    # Center
    training_data, data_mean = centering(training_data)

    # Model parameters
    # While we need to specify the dimensionality of the latent variables at the outset, one of the advantages of the
    # Bayesian framework is that by using a ARD kernel we can prune dimensions corresponding to small inverse
    # lengthscales.
    N = len(training_data)

    # Priors
    hyperbolic_kernel_lengthscale_prior = None
    hyperbolic_kernel_outputscale_prior = None

    print("Selecting these priors:")
    print(f"output scale prior:", hyperbolic_kernel_outputscale_prior)
    print(f"length scale prior:", hyperbolic_kernel_lengthscale_prior)

    # Initialization
    if init_type == 'Stress':
        X_init = hyperbolic_stress_loss_initialization(training_data, latent_dim, shape_pose_graph_distances)
    elif init_type == 'PCA':
        X_init = hyperbolic_tangent_pca_initialization(training_data, latent_dim)
    else:
        X_init = None

    # Model
    model = MapExactHyperbolicGPLVM(training_data, latent_dim, X_init=X_init, batch_params=False,
                                    kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                    kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    elif loss_type == "VanillaDistortion":
        print("Loss_type = VanillaDistortion")
        added_loss = VanillaHyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)

    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Plot initial latent variables
    print(f"Plotting?: {plot_on}")
    if plot_on:
        x_latent_init = model.X.X
        # From Lorentz to Poincaré
        x_poincare_init = lorentz_to_poincare(x_latent_init).detach().numpy()
        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(color_function_shape_poses(shape_pose_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare_init, x_colors, shape_poses_legend_for_plot,
                                     save_path=ROOT_DIR / f"model_viz/{model_name}.png", show=False)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare_init, x_colors, shape_poses_legend_for_plot,
                                     save_path=VIZ_DIR / f"{model_name}.png")

        # Plot distances in the latent space
        distances_latent = lorentz_distance_torch(x_latent_init, x_latent_init).detach().numpy()
        plot_distance_matrix(distances_latent, show=False)
        # Plot distances between classes
        plot_distance_matrix(shape_pose_graph_distances, show=False)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    try:
        fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH / model_name,
                        options={"maxiter": 1000})  #, options={"maxiter": 1000, "disp": True, "lr": 0.01})
    except KeyboardInterrupt:
        pass
    mll.eval()

    # Test evaluation
    model.eval()
    posterior = model(model.X())
    error = (posterior.mean.T - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    print(added_loss.loss(model.X()).detach().numpy() / loss_scale)

    # Plot results
    if plot_on:
        # From Lorentz to Poincaré
        x_latent = model.X.X
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, shape_poses_legend_for_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors, shape_poses_legend_for_plot)

        # Plot distances in the latent space
        distances_latent = lorentz_distance_torch(x_latent, x_latent).detach().numpy()
        plot_distance_matrix(distances_latent, save_path=VIZ_DIR / f"distance_latent_{model_name}.png", show=False)
        # Plot distances between classes
        plot_distance_matrix(shape_pose_graph_distances, save_path=VIZ_DIR / f"distance_graph_{model_name}.png", show=False)

    print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_name", dest="model_name", default="hyperbolic_egplvm_prior",
                        help="Set the path to save the model.")
    parser.add_argument("--dataset", dest="dataset", default="semifull",
                        help="Set the dataset. Options: full, semifull, reduced, feet5, knees2, oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=5000.0,
                        help="Set the loss scale.")
                        # dim 2 - semifull, stress 6.0; feet5, stress = 5.0;
                        # dim 3 - semifull, stress 10.0;
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=False,
                        help="If True, generate plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_name = args.model_name
    dataset = args.dataset
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    init_type = args.init_type
    plot_on = args.plot_on

    model_name = model_name + '_dim' + str(latent_dim) + '_' + dataset + '_' + loss_type + str(loss_scale)

    main(latent_dim, model_name, dataset, loss_type, loss_scale, init_type, plot_on)
