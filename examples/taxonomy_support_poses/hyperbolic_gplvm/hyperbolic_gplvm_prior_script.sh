#!/bin/bash

# Hyperbolic 2 - stress
python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=2 --loss_type="Stress" --loss_scale=5.0
python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=6.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=2 --loss_type="Stress" --loss_scale=3.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=2 --loss_type="Stress" --loss_scale=10.0

## Hyperbolic 3 - stress
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=3 --loss_type="Stress" --loss_scale=3.0
python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=10.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=3 --loss_type="Stress" --loss_scale=6.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=3 --loss_type="Stress" --loss_scale=5.0

# Hyperbolic 2 - distorsion
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=2 --loss_type="Distortion" --loss_scale=100.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=2 --loss_type="Distortion" --loss_scale=42.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=2 --loss_type="Distortion" --loss_scale=50.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=2 --loss_type="Distortion" --loss_scale=25.0
#
## Hyperbolic 3 - distorsion
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=3 --loss_type="Distortion" --loss_scale=50.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=3 --loss_type="Distortion" --loss_scale=42.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=3 --loss_type="Distortion" --loss_scale=35.0
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=3 --loss_type="Distortion" --loss_scale=25.0

# Hyperbolic 2 - zero
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=2 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=2 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=2 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=2 --loss_type="Zero" --loss_scale=0.
#
## Hyperbolic 3 - zero
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="feet5" --latent_dim=3 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="semifull" --latent_dim=3 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="knees2" --latent_dim=3 --loss_type="Zero" --loss_scale=0.
#python3 hyperbolic_egplvm_taxonomy_prior.py --dataset="reduced" --latent_dim=3 --loss_type="Zero" --loss_scale=0.