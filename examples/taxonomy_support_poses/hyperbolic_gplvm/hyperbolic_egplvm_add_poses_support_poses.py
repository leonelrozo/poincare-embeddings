import os
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser

import gpytorch
from gpytorch.kernels import ScaleKernel, RBFKernel
from gpytorch.priors import torch_priors


from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, color_function_shape_poses, text_function_shape_poses, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_semifull_dataset, get_missing_data_semifull_dataset, get_missing_data_reduced_dataset_2poses, \
    get_indices_shape_poses_graph, get_missing_data_feet5
from HyperbolicEmbeddings.gplvm.gplvm_models import BackConstrainedExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import BackConstrainedHyperbolicExactGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_distance_torch
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphGaussianKernel, GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, '../../../final_models/')


def load_trained_hyperbolic_gplvm_model(model_name, training_data, adjacency_matrix, shape_poses_indices,
                                        shape_pose_graph_distances, loss_type, loss_scale):
    # Prior on latent variable
    kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)
    kernel_outputscale_prior = None

    # Model
    data_kernel = ScaleKernel(RBFKernel())
    classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))
    if "hyperbolic" in model_name:
        model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                                    data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                    kernel_lengthscale_prior=kernel_lengthscale_prior,
                                                    kernel_outputscale_prior=kernel_outputscale_prior,
                                                    taxonomy_based_back_constraints=True)
    else:
        model = BackConstrainedExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                          data_kernel=data_kernel, classes_kernel=classes_kernel,
                                          kernel_lengthscale_prior=kernel_lengthscale_prior,
                                          kernel_outputscale_prior=kernel_outputscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    mll.load_state_dict(torch.load(MODEL_PATH + model_name))  # load the model from file
    mll.eval()

    return mll


def main(latent_dim, model_name, dataset='semifull', loss_type='Zero', loss_scale=0.05, plot_on=False):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset == 'reduced':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == 'knees2':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)

    graph_file_path = data_folder_path / 'support_poses_closure.csv'
    adjacency_matrix, shape_poses_indices = get_indices_shape_poses_graph(graph_file_path, shape_pose_names)
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names)
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    # Set training data
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    shape_poses_indices = torch.from_numpy(shape_poses_indices)

    training_data = pose_data
    # training_data = joint_data

    # Model parameters
    N = len(training_data)

    mll = load_trained_hyperbolic_gplvm_model(model_name, training_data, adjacency_matrix, shape_poses_indices,
                                              shape_pose_graph_distances, loss_type, loss_scale)

    # Load additional data
    if dataset == 'semifull':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_semifull_dataset(data_path)
    elif dataset == 'reduced':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_feet5(data_path)

    added_pose_data = torch.from_numpy(added_pose_data)
    added_joint_data = torch.from_numpy(added_joint_data)
    N_added = len(added_pose_data)

    # Compute latent variable corresponding to all added data
    _, added_shape_poses_indices = get_indices_shape_poses_graph(graph_file_path, added_shape_pose_names)
    added_shape_poses_indices = torch.from_numpy(added_shape_poses_indices)
    all_shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path,
                                                                        shape_pose_names + added_shape_pose_names)
    x_latent_added = mll.model.X.back_constraint_function(added_pose_data, added_shape_poses_indices)

    # Latent variable
    x_latent = mll.model.X()
    x_latent_all = torch.vstack((x_latent, x_latent_added))

    # Test evaluation
    # model.eval()
    # posterior = model(model.X())
    # error = (posterior.mean.T - training_data).detach().numpy()

    # Plot results
    if plot_on:
        if "hyperbolic" in model_name:
            # From Lorentz to Poincaré
            x_poincare = lorentz_to_poincare(x_latent).detach().numpy()
            x_poincare_added = lorentz_to_poincare(x_latent_added).detach().numpy()
            x_poincare_all = np.vstack((x_poincare, x_poincare_added))
            distances_latent = lorentz_distance_torch(x_latent_all, x_latent_all).detach().numpy()
        else:
            distances_latent = mll.model.covar_module.covar_dist(x_latent_all, x_latent_all).detach().numpy()

        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(color_function_shape_poses(shape_pose_names[n]))
        for n in range(N_added):
            x_colors.append('red')

        # Get legend
        shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names + added_shape_pose_names)

        if latent_dim == 2:
            if "hyperbolic" in model_name:
                plot_hyperbolic_gplvm_2d(x_poincare_all, x_colors, shape_poses_legend_for_plot)
            else:
                plot_euclidean_gplvm_2d(x_latent_all.detach().numpy(), x_colors, shape_poses_legend_for_plot)

        elif latent_dim == 3:
            if "hyperbolic" in model_name:
                plot_hyperbolic_gplvm_3d(x_poincare_all, x_colors, shape_poses_legend_for_plot)
            else:
                plot_euclidean_gplvm_3d(x_latent_all.detach().numpy(), x_colors, shape_poses_legend_for_plot)

        # Plot distances in the latent space
        plot_distance_matrix(distances_latent)
        # Plot distances between classes
        plot_distance_matrix(all_shape_pose_graph_distances)

        print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--manifold", dest="manifold", default="hyperbolic",
                        help="Latent manifold. Options: hyperbolic, euclidean")
    parser.add_argument("--dataset", dest="dataset", default="semifull",
                        help="Set the dataset. Options: semifull, reduced.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    manifold = args.manifold
    dataset = args.dataset
    loss_type = args.loss_type
    plot_on = args.plot_on

    if loss_type == "Stress":
        if latent_dim == 2:
            if dataset == "semifull":
                loss_scale = 1.3
            elif dataset == "feet5":
                loss_scale = 0.7
        elif latent_dim == 3:
            if dataset == "semifull":
                loss_scale = 1.5

    model_name = manifold + '_egplvm_backconstrained_dim' + str(latent_dim) + '_' + dataset + '_' + loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)

    main(latent_dim, model_name, dataset, loss_type, loss_scale, plot_on)
