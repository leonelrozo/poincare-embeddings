import os
import gym
from pathlib import Path
import numpy as np
from robot_env import mjc_env
import mujoco

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import get_semifull_dataset
from HyperbolicEmbeddings.mmm_mujoco_utils.mmm_controllers import create_controllers, inverse_kinematics

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, '../../../final_models/geodesics/')
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../saved_models/geodesics/')


if __name__ == '__main__':
    # Load mujoco environment and controller
    env = gym.make('MMMFullEmptyWorldEnv-v0')
    env.reset()

    # Create controllers in the order left leg, right leg, left arm, right arm
    controllers = create_controllers(env)

    # Load data (for initial joint configuration and joint names)
    base_path = Path(__file__).parent.parent.parent.parent.resolve()
    data_folder_path = base_path / "data/support_poses"
    data_path = str(data_folder_path / 'keyPoseShapesXPose.xml')  # Path of XML file including Shape poses dataset
    pose_data, joint_data, _, joint_names = get_semifull_dataset(data_path)

    # Get joints ids
    joint_ids, _, _, _, _, _ = controllers[0].get_joint_ids(joint_names)

    # Load trajectories
    # model_name = 'hyperbolic_egplvm_contacts_backconstrained_dim3_feet5_Stress0.5'
    model_name = 'hyperbolic_egplvm_backconstrained_dim2_semifull_Stress1.3'
    # model_name = 'euclidean_egplvm_backconstrained_dim2_semifull_Stress1.3'
    geodesic_data = np.load(MODEL_PATH + model_name + '_geodesics.npz')
    geodesics = geodesic_data["geodesics"]
    geodesic_idx = geodesic_data["geodesic_idx"]
    geodesics_closest_class_names = geodesic_data["geodesics_closest_class_names"]
    geodesics_task_space_mean = geodesic_data["geodesics_task_space_mean"]
    geodesics_task_space_variance = geodesic_data["geodesics_task_space_variance"]

    for g in range(len(geodesics_task_space_mean)):
        print('Geodesic: ', g)
        geodesic = geodesics_task_space_mean[g]
        joint_position_log = []
        xpos_log = []

        # Set initial joint angle set
        env.data.qpos[joint_ids] = joint_data[geodesic_idx[g][0]]
        mujoco.mj_step(controllers[0].model, controllers[0].data, nstep=1)  # 1 step to actualize the data (any controller)
        for _ in range(10):
            env.render()

        for n in range(geodesic.shape[0]):
            print(n)
            desired_position = [geodesic[n, :3], geodesic[n, 3:6], geodesic[n, 6:9], geodesic[n, 9:12]]
            positions = []
            joint_position = []

            # Inverse kinematics
            cmd_joints, tcp_from_ik_result = inverse_kinematics(controllers, desired_position)

            for i in range(101):
                env.step(cmd_joints)
                env.render()

                for j in range(len(controllers)):
                    controller = controllers[j]
                    controller.get_root_status()
                    des_xpos_t = controller.root_xmat.dot(desired_position[j]) + controller.root_xpos  # to global
                    controller.vis_point(des_xpos_t)
                    controller.vis_point(tcp_from_ik_result[j], rgba=np.array([0, 1, 1, 1]))
                    xpos, _, _, _ = controller.get_site_status(controller.tcp_name)
                    controller.vis_point(xpos, rgba=np.array([0, 0, 1, 1]))
                    if i == 100:
                        joints = controller.get_joint_position()
                        xpos_t, xmat_t, _ = controller.forward_kinematics(joints,
                                                                          controller.tcp_name, mode="site")
                        controller.get_root_status()
                        xmat_t = np.reshape(xmat_t, (3, 3))
                        xpos_t_r, xmat_t_r = controller.pose_to_root_frame(xpos_t, xmat_t)
                        # print(xpos_t)
                        # print(des_xpos_t)
                        joint_position = np.hstack((positions, joints))
                        positions = np.hstack((positions, xpos_t_r))

            # joint_position_log.append(joint_position)
            joint_position_log.append(env.data.qpos[joint_ids])
            xpos_log.append(positions)

        path = MODEL_PATH + model_name + '_joints_geodesic' + str(g) + '.npz'
        np.savez(path, joint_positions=np.array(joint_position_log), xpositions=np.array(xpos_log),
                 geodesic_idx=geodesic_idx[g], joint_names=joint_names)
