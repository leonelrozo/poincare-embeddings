import os
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser

import gpytorch
from gpytorch.kernels import ScaleKernel, RBFKernel
import gpytorch.priors.torch_priors as torch_priors

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, color_function_shape_poses, text_function_shape_poses, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_semifull_dataset, get_missing_data_semifull_dataset, get_missing_data_reduced_dataset_2poses, \
    get_indices_shape_poses_graph, reorder_taxonomy_data
from HyperbolicEmbeddings.gplvm.gplvm_models import BackConstrainedExactGPLVM, MapExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import BackConstrainedHyperbolicExactGPLVM, \
    MapExactHyperbolicGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_geodesic, \
    lorentz_distance_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphGaussianKernel, GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../../saved_models/')
# MODEL_PATH = os.path.join(CURRENT_DIR, '../../../final_models_old/')
MODEL_PATH = os.path.join(CURRENT_DIR, '../../../final_models/')
FIGURE_PATH = os.path.join(CURRENT_DIR, '../../../images/')


def load_trained_hyperbolic_gplvm_model(model_name, training_data, adjacency_matrix, shape_poses_indices,
                                        shape_pose_graph_distances, loss_type, loss_scale):
    # Prior on latent variable
    kernel_lengthscale_prior = None
    kernel_outputscale_prior = None

    # Model
    data_kernel = ScaleKernel(RBFKernel())
    classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))
    if "hyperbolic" in model_name:
        if "backconstrained" in model_name:
            kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)

            model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                                        data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                        kernel_lengthscale_prior=kernel_lengthscale_prior,
                                                        kernel_outputscale_prior=kernel_outputscale_prior,
                                                        pca=True, taxonomy_based_back_constraints=True)
        else:
            model = MapExactHyperbolicGPLVM(training_data, latent_dim, pca=True,
                                            kernel_lengthscale_prior=kernel_lengthscale_prior,
                                            kernel_outputscale_prior=kernel_outputscale_prior)
    else:
        if "backconstrained" in model_name:
            kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)
            model = BackConstrainedExactGPLVM(training_data, latent_dim, shape_poses_indices,
                                              data_kernel=data_kernel, classes_kernel=classes_kernel,
                                              kernel_lengthscale_prior=kernel_lengthscale_prior,
                                              kernel_outputscale_prior=kernel_outputscale_prior,
                                              pca=True)
        else:
            model = MapExactGPLVM(training_data, latent_dim, pca=True,
                                  kernel_lengthscale_prior=kernel_lengthscale_prior,
                                  kernel_outputscale_prior=kernel_outputscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    print(f"Loading model: {MODEL_PATH + model_name}")
    mll.load_state_dict(torch.load(MODEL_PATH + model_name))  # load the model from file
    mll.eval()

    return mll


def main(latent_dim, model_name, dataset='semifull', loss_type='Zero', loss_scale=0.05, plot_on=False):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Load data
    data_folder_path = Path(CURRENT_DIR).parent.parent.parent.resolve() / "data/support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset == 'reduced':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == 'knees2':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)

    graph_file_path = data_folder_path / 'support_poses_closure.csv'
    adjacency_matrix, shape_poses_indices = get_indices_shape_poses_graph(graph_file_path, shape_pose_names)
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names)
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    # Set training data
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    shape_poses_indices = torch.from_numpy(shape_poses_indices)

    training_data = pose_data
    # training_data = joint_data

    # Load model parameters
    N = len(training_data)
    mll = load_trained_hyperbolic_gplvm_model(model_name, training_data, adjacency_matrix, shape_poses_indices,
                                              shape_pose_graph_distances, loss_type, loss_scale)

    # Test evaluation
    # posterior = mll.model(mll.model.X())
    # error = (posterior.mean.T - training_data).detach().numpy()

    # Latent variables
    x_latent = mll.model.X()

    # Get colors
    x_colors = []
    for n in range(N):
        x_colors.append(color_function_shape_poses(shape_pose_names[n]))

    # Select points to form the trajectory
    if dataset == "feet5":
        geodesic_idx = [[0, 21],   # lf -> lfrf rh
                        [0, 45],   # lf -> lfrf lhrh
                        [35, 53],  # rf -> rf lhrh
                        [10, 55],  # lfrf lh -> lf lhrh
                        ]
    elif dataset == "semifull":
        geodesic_idx = [[0, 23],   # lf -> lfrf rh
                        [0, 54],   # lf -> lfrf lhrh
                        [45, 63],  # rf -> rf lhrh
                        [12, 66],  # lfrf lh -> lf lhrh
                        [0, 74],   # lf -> lf rk
                        [17, 97],  # lfrf -> lkrk
                        [31, 96],  # lfrh -> lkrk rh
                        ]
        # geodesic_idx = [[0, 29],   # lf -> lfrf rh
        #                 [0, 54],   # lf -> lfrf lhrh
        #                 [45, 63],  # rf -> rf lhrh
        #                 [45, 18],  # rf -> lfrf
        #                 [12, 66],  # lfrf lh -> lf lhrh
        #                 [0, 74],   # lf -> lf rk
        #                 [17, 97],  # lfrf -> lkrk
        #                 [31, 96],  # lfrh -> lkrk rh
        #                 ]
    else:
        geodesic_idx = []

    # Compute geodesics
    geodesics = []
    geodesics_task_space_mean = []
    geodesics_task_space_variance = []
    geodesics_closest_class_names = []
    geodesics_closest_class_idx = []
    geodesics_color = []
    nb_points_geodesic = 50
    for idx in geodesic_idx:
        if "hyperbolic" in model_name:
            # Compute geodesic in Lorentz and project to Poincare
            geodesic = lorentz_geodesic(x_latent[idx[0]], x_latent[idx[1]], nb_points=nb_points_geodesic)
            geodesic_poincare = lorentz_to_poincare(geodesic)
            geodesics.append(geodesic_poincare.detach().numpy())
        else:
            t = torch.linspace(0., 1., nb_points_geodesic)[:, None]
            geodesic = x_latent[idx[0]] + t * (x_latent[idx[1]] - x_latent[idx[0]])
            geodesics.append(geodesic.detach().numpy())

        # For each geodesic check closest class (=class of closest data point in the latent space) along the way
        if "hyperbolic" in model_name:
            distance_matrix = lorentz_distance_torch(geodesic, x_latent)
        else:
            distance_matrix = mll.model.covar_module.covar_dist(geodesic, x_latent)

        closest_point_idx = torch.argmin(distance_matrix, dim=1).detach().numpy()
        closest_class_idx = shape_poses_indices[closest_point_idx]
        closest_class_name = [shape_pose_names[i] for i in closest_point_idx.tolist()]
        geodesics_closest_class_idx.append(closest_class_idx)
        geodesics_closest_class_names.append(closest_class_name)

        # Trajectories in observation / task space
        posterior = mll.model(geodesic)
        geodesics_task_space_mean.append(posterior.mean.T.detach().numpy())
        geodesics_task_space_variance.append(posterior.variance.T.detach().numpy())
        del posterior  # clear up memory

        # Get colors
        geodesic_color = []
        for j in range(nb_points_geodesic):
            geodesic_color.append(color_function_shape_poses(closest_class_name[j]))
        geodesics_color.append(geodesic_color)

    # np.savez(MODEL_PATH + 'geodesics/' + model_name + '_geodesics.npz', geodesics=geodesics, geodesic_idx=geodesic_idx,
    #          geodesics_closest_class_names=geodesics_closest_class_names,
    #          geodesics_task_space_mean=geodesics_task_space_mean,
    #          geodesics_task_space_variance=geodesics_task_space_variance)

    # Plot geodesics in latent space
    if "hyperbolic" in model_name:
        # From Lorentz to Poincaré
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()

    # If the latent space is H2, we plot the embedding in the Poincaré disk
    if latent_dim == 2:
        if "hyperbolic" in model_name:
            if "backconstrained" in model_name:
                max_magnification = 1.5
            elif "Stress" in model_name and "semifull" in model_name:
                max_magnification = 0.5
            elif "Stress" in model_name:
                max_magnification = 5.0
            elif "Zero" in model_name:
                max_magnification = 1.5
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, geodesics=geodesics, geodesics_colors=geodesics_color,
                                     model=mll.model, model_magnification_path=MODEL_PATH + model_name + '_magfac.npz',
                                     max_magnification=max_magnification, save_path=FIGURE_PATH + model_name + '_latent.png')
        else:
            plot_euclidean_gplvm_2d(x_latent.detach().numpy(), x_colors, geodesics=geodesics,
                                    geodesics_colors=geodesics_color, model=mll.model,
                                    save_path=FIGURE_PATH + model_name + '_latent.png')

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    elif latent_dim == 3:
        if "hyperbolic" in model_name:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors, geodesics=geodesics, geodesics_colors=geodesics_color,)
                                     # save_path=FIGURE_PATH + model_name + '_latent.pdf', show=False)
        else:
            plot_euclidean_gplvm_3d(x_latent.detach().numpy(), x_colors, geodesics=geodesics,
                                    geodesics_colors=geodesics_color)

    # Plot distances in the latent space
    x_latent_ordered = torch.vstack(reorder_taxonomy_data(x_latent, shape_pose_names))
    x_colors_ordered = reorder_taxonomy_data(x_colors, shape_pose_names)
    if "hyperbolic" in model_name:
        distances_latent = lorentz_distance_torch(x_latent_ordered, x_latent_ordered).detach().numpy()
    else:
        distances_latent = mll.model.covar_module.covar_dist(x_latent_ordered, x_latent_ordered).detach().numpy()

    max_distance = np.max(shape_pose_graph_distances.detach().numpy()) + 1.0
    plot_distance_matrix(distances_latent, max_distance=max_distance, x_colors=x_colors_ordered,
                         save_path=FIGURE_PATH + model_name + '_distances.png')

    # Plot distances between classes
    shape_pose_names_ordered = reorder_taxonomy_data(shape_pose_names, shape_pose_names)
    shape_pose_graph_distances_ordered = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names_ordered)
    plot_distance_matrix(shape_pose_graph_distances_ordered, max_distance=max_distance, x_colors=x_colors_ordered)
                         # save_path=FIGURE_PATH + 'semifull_distances.png')

    # pose_data_ordered = reorder_taxonomy_data(pose_data.detach().numpy(), shape_pose_names)
    #
    # np.savez('../../data/feet_data', pose_data=pose_data, shape_pose_names=shape_pose_names,
    #          x_colors=x_colors, adjacency_matrix=adjacency_matrix, shape_poses_indices=shape_poses_indices,
    #          shape_pose_graph_distances=shape_pose_graph_distances)
    # np.savez('../../data/feet_data', pose_data=pose_data_ordered, shape_pose_names=shape_pose_names_ordered,
    #          x_colors=x_colors_ordered,
    #          shape_pose_graph_distances=shape_pose_graph_distances_ordered)

    print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--manifold", dest="manifold", default="euclidean",
                        help="Latent manifold. Options: hyperbolic, euclidean")
    parser.add_argument("--dataset", dest="dataset", default="semifull",
                        help="Set the dataset. Options: semifull, reduced, feet5, knees2.")
    parser.add_argument("--model_type", dest="model_type", default="backconstrained",
                        help="Set the loss type. Options: backconstrained, prior.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    manifold = args.manifold
    dataset = args.dataset
    model_type = args.model_type
    loss_type = args.loss_type
    plot_on = args.plot_on

    loss_scale = 0.
    if loss_type == "Stress" and model_type == "backconstrained":
        if latent_dim == 2:
            if dataset == "semifull":
                loss_scale = 1.3
            elif dataset == "reduced" or dataset == "knees2":
                loss_scale = 1.2
            elif dataset == "feet5":
                loss_scale = 0.7
        elif latent_dim == 3:
            if dataset == "semifull":
                loss_scale = 1.5
            elif dataset == "reduced":
                loss_scale = 1.8
            elif dataset == "feet5":
                loss_scale = 0.9
            elif dataset == "knees2":
                loss_scale = 3.3
    elif loss_type == "Stress":
        if latent_dim == 2:
            if dataset == "semifull":
                loss_scale = 6.0
            elif dataset == "reduced":
                loss_scale = 10.
            elif dataset == "knees2":
                loss_scale = 3.0
            elif dataset == "feet5":
                loss_scale = 5.0
        elif latent_dim == 3:
            if dataset == "semifull":
                loss_scale = 10.0
            elif dataset == "reduced":
                loss_scale = 5.0
            elif dataset == "feet5":
                loss_scale = 3.0
            elif dataset == "knees2":
                loss_scale = 6.0
    elif loss_type == "Distortion":
        if latent_dim == 2:
            if dataset == "semifull":
                loss_scale = 42.
            elif dataset == "reduced":
                loss_scale = 25.
            elif dataset == "knees2":
                loss_scale = 50.
            elif dataset == "feet5":
                loss_scale = 100.
        elif latent_dim == 3:
            if dataset == "semifull":
                loss_scale = 42.
            elif dataset == "reduced":
                loss_scale = 25.
            elif dataset == "feet5":
                loss_scale = 50.
            elif dataset == "knees2":
                loss_scale = 35.

    model_name = manifold + '_egplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_' + loss_type + str(loss_scale)
    # if loss_type != 'Zero':
    #     model_name += str(loss_scale)

    main(latent_dim, model_name, dataset, loss_type, loss_scale, plot_on)
