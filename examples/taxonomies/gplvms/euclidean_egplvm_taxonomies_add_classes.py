from pathlib import Path

import os
import numpy as np
import torch
from argparse import ArgumentParser

from geoopt.optim.radam import RiemannianAdam
import gpytorch.priors.torch_priors as torch_priors
from gpytorch.kernels import ScaleKernel, RBFKernel

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_added_class, load_taxonomy_data
from HyperbolicEmbeddings.model_loading_utils.load_gplvm import load_trained_gplvm_model
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import ordered_hand_grasps_names
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import ordered_shape_pose_names
from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import ordered_bimanual_names
from HyperbolicEmbeddings.taxonomy_utils.taxonomy_functions import reorder_taxonomy_data, reorder_distance_matrix
from HyperbolicEmbeddings.gplvm.gplvm_initializations import euclidean_stress_loss_initialization, pca_initialization
from HyperbolicEmbeddings.gplvm.gplvm_models import BackConstrainedExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, EuclideanStressLossTermExactMLL, \
    EuclideanDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d, \
    plot_euclidean_gplvm_2d_added_data, plot_euclidean_gplvm_3d_added_data
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix_added_data, plot_distance_matrix


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()


def main(latent_dim, model_type, dataset, dataset_type=None,
         loss_type='Zero', loss_scale=1.0, init_type="PCA",
         plot_on=False, show_plot=False, outputscale_prior=None, lengthscale_prior=None):

    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    reduced_data, skipped_data, data_mean, adjacency_matrix, reduced_graph_distances, all_graph_distances, \
    reduced_nodes_names, skipped_nodes_names, reduced_indices_in_graph, skipped_indices_in_graph, \
    nodes_legend_for_plot, all_nodes_legend_for_plot, color_function, max_manifold_distance = \
        load_taxonomy_added_class(dataset, dataset_type)
    
    if dataset == 'grasps':
        ordered_nodes_name = ordered_hand_grasps_names

    elif dataset == 'support-poses':
        pass

    elif dataset == 'augmented-support-poses':
        ordered_nodes_name = ordered_shape_pose_names

    elif dataset == 'bimanual':
        ordered_nodes_name = ordered_bimanual_names

    # Model name
    model_name = 'euclidean_gplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)

    model_name += '_missing_classes'

    # Model parameters
    N = len(reduced_data)
    N_added = len(skipped_data)

    # If model exists, load it. Otherwise train it.
    if os.path.isfile(MODEL_PATH / model_name):
        # Load model parameters
        mll = load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, 'BC', dataset, reduced_data,
                                       adjacency_matrix, reduced_indices_in_graph, reduced_graph_distances, loss_type, loss_scale)
        model = mll.model

    else:
        # Initialization
        if init_type == 'Stress':
            X_init = euclidean_stress_loss_initialization(reduced_data, latent_dim, reduced_graph_distances)
        elif init_type == 'PCA':
            X_init = pca_initialization(reduced_data, latent_dim)
        else:
            X_init = None
    
        # Model definition
        # Prior on latent variable
        if outputscale_prior is not None:
            kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
        else:
            kernel_outputscale_prior = None
    
        if lengthscale_prior is not None:
            kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
        else:
            kernel_lengthscale_prior = None
    
        # Back constraints
        data_kernel = ScaleKernel(RBFKernel())
        classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))
    
        if loss_type == 'Stress':
            if dataset == 'grasps':
                data_kernel.base_kernel.lengthscale = 1.8
                # INCREASED COMPARED TO OTHER MODELS TO HAVE MORE INFLUENCE ON CLASSES!
                classes_kernel.base_kernel.lengthscale = 3.0  # INCREASED
    
            elif dataset == 'support-poses':
                data_kernel.base_kernel.lengthscale = 0.9
                classes_kernel.base_kernel.lengthscale = 0.6
    
            elif dataset == 'augmented-support-poses':
                data_kernel.base_kernel.lengthscale = 2.0
                # INCREASED COMPARED TO OTHER MODELS TO HAVE MORE INFLUENCE ON CLASSES!
                classes_kernel.base_kernel.lengthscale = 1.3  # INCREASED

            elif dataset == 'bimanual':
                # BOTH INCREASED COMPARED TO OTHER MODELS TO HAVE MORE INFLUENCE ON CLASSES!
                data_kernel.base_kernel.lengthscale = 5.0  # INCREASED
                classes_kernel.base_kernel.lengthscale = 4.0  # INCREASED
    
            data_kernel.outputscale = 2.0
            classes_kernel.outputscale = 1.0
    
            taxonomy_bc = True
    
        elif loss_type == 'Zero':
            data_kernel.base_kernel.lengthscale = 1.5  # TODO
            classes_kernel.base_kernel.lengthscale = 1.5
            data_kernel.outputscale = 1.0
            classes_kernel.outputscale = 1.0
    
            taxonomy_bc = False
    
        # Kernel
        model = BackConstrainedExactGPLVM(reduced_data, latent_dim, reduced_indices_in_graph,
                                          data_kernel=data_kernel, classes_kernel=classes_kernel,
                                          kernel_lengthscale_prior=kernel_lengthscale_prior,
                                          kernel_outputscale_prior=kernel_outputscale_prior,
                                          X_init=X_init, batch_params=False,
                                          taxonomy_based_back_constraints=taxonomy_bc)
    
        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
        if loss_type == 'Zero' or None:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
        elif loss_type == 'Stress':
            print("Loss_type = Stress")
            added_loss = EuclideanStressLossTermExactMLL(reduced_graph_distances, loss_scale)
        elif loss_type == 'Distortion':
            print("Loss_type = Distortion")
            added_loss = EuclideanDistortionLossTermExactMLL(reduced_graph_distances, loss_scale)
        model.add_loss_term(added_loss)

        # Declaring the objective to be optimised along with optimiser
        mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)
    
        # Plot initial latent variables
        if plot_on:
            x_latent_init = model.X()
            x_latent_init_np = model.X().detach().numpy()
            # Get colors
            x_colors = []
            for n in range(N):
                x_colors.append(color_function(reduced_nodes_names[n]))
    
            # If the latent space is H2, we plot the embedding in the Poincaré disk
            if latent_dim == 2:
                # Plot hyperbolic latent space
                plot_euclidean_gplvm_2d(x_latent_init_np, x_colors, nodes_legend_for_plot,
                                        save_path=VIZ_DIR / f"{model_name}_init_added_classes.png", show=show_plot)
    
            # If the latent space is H3, we plot the embedding in the Poincaré ball
            elif latent_dim == 3:
                # Plot hyperbolic latent space
                plot_euclidean_gplvm_3d(x_latent_init_np, x_colors, nodes_legend_for_plot,
                                        save_path=VIZ_DIR / f"{model_name}_init_added_classes.png", show=show_plot)
    
            # Plot distances in the latent space
            distances_latent_init = model.covar_module.covar_dist(x_latent_init, x_latent_init).detach().numpy()
            plot_distance_matrix(distances_latent_init,
                                 save_path=VIZ_DIR / f"distance_latent_{model_name}_init_added_classes.png", show=show_plot)
            # Plot distances between classes
            plot_distance_matrix(reduced_graph_distances,
                                 save_path=VIZ_DIR / f"distance_latent_{model_name}_init_added_classes.png", show=show_plot)
    
        # Train the model in a single batch with automatic convergence checks
        mll.train()
        try:
            fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, options={"maxiter": 257},
                            model_path=MODEL_PATH / model_name)  #, options={"maxiter": 1000, "disp": True, "lr": 0.01})
        except KeyboardInterrupt:
            pass
        mll.eval()

        # Test evaluation
        model.eval()
        posterior = model(model.X())
        error = (posterior.mean.T - reduced_data).detach().numpy()
        print(np.mean(np.abs(error)))
        print(added_loss.loss(model.X()).detach().numpy() / loss_scale)

    # Compute latent variable corresponding to all added data
    added_x_latent = mll.model.X.back_constraint_function(skipped_data, skipped_indices_in_graph)

    # Latent variable
    x_latent = mll.model.X()
    all_x_latent = torch.vstack((x_latent, added_x_latent))

    # Get colors
    x_colors = []
    for n in range(N):
        x_colors.append(color_function(reduced_nodes_names[n]))

    added_x_colors = []
    for n in range(N_added):
        added_x_colors.append(color_function(skipped_nodes_names[n]))

    # If the latent space is H2, we plot the embedding in the Poincaré disk
    if latent_dim == 2:
        plot_euclidean_gplvm_2d_added_data(x_latent.detach().numpy(), x_colors, added_x_latent.detach().numpy(),
                                           added_x_colors, model=mll.model,
                                           save_path=VIZ_DIR / (model_name + '_latent_added_classes.png'))

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    # elif latent_dim == 3:
    #     plot_euclidean_gplvm_3d_added_data(x_latent.detach().numpy(), x_colors, added_x_latent.detach().numpy(),
    #                                        added_x_colors)

    # Diffentiate added pose names for distance plot
    skipped_nodes_names = [pose_name + "Added" for pose_name in skipped_nodes_names]
    all_nodes_names = reduced_nodes_names + skipped_nodes_names

    # Plot distances in the latent space
    x_latent_ordered = torch.vstack(reorder_taxonomy_data(all_x_latent, all_nodes_names, ordered_nodes_name))
    x_colors_ordered = reorder_taxonomy_data(x_colors + added_x_colors, all_nodes_names, ordered_nodes_name)
    all_nodes_names_ordered = reorder_taxonomy_data(all_nodes_names, all_nodes_names, ordered_nodes_name)

    distances_latent = mll.model.covar_module.covar_dist(x_latent_ordered, x_latent_ordered).detach().numpy()

    max_distance = np.max(all_graph_distances.detach().numpy()) + 1.0
    plot_distance_matrix_added_data(distances_latent, all_nodes_names_ordered,
                                    max_distance=max_distance, x_colors=x_colors_ordered,
                                    save_path=VIZ_DIR / (model_name + '_distances_added_classes.png'))

    # Plot distances between classes
    _, _, _, all_graph_distances, all_nodes_names, _, _, _, _, _ = \
        load_taxonomy_data(dataset, dataset_type=dataset_type)  # To reorder distances. If not reloaded, plots are wrong
    graph_distances_ordered = reorder_distance_matrix(all_graph_distances, all_nodes_names, ordered_nodes_name)
    plot_distance_matrix_added_data(graph_distances_ordered, all_nodes_names_ordered, max_distance=max_distance,
                                    x_colors=x_colors_ordered)
                                    #, save_path=VIZ_DIR / (model_name + '_ground_truthdistances_added_classes.png'))

    if latent_dim == 2:
        if "grasps" in model_name:
            max_error = 2.3
        if "augmented-support-poses" in model_name:
            max_error = 4.0
        if "bimanual" in model_name:
            max_error = 0.9
    elif latent_dim == 3:
        if "grasps" in model_name:
            max_error = 2.5
        if "augmented-support-poses" in model_name:
            max_error = 3.6
        if "bimanual" in model_name:
            max_error = 0.7

    plot_distance_matrix_added_data(np.abs(distances_latent - graph_distances_ordered), all_nodes_names_ordered,
                                    max_distance=max_error,
                                    x_colors=x_colors_ordered,
                                    save_path=VIZ_DIR / (model_name + '_distances_added_classes_error.png'))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim.")
    parser.add_argument("--model_type", dest="model_type", default="BC",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual.")
    parser.add_argument("--dataset_type", dest="dataset_type", default=None,
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=2000.0,
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=False,
                        help="If True, generate plots.")
    parser.add_argument("--show_plot", dest="show_plot", type=bool, default=False,
                        help="If True, show plots.")
    # parser.add_argument("--outputscale_prior", dest="outputscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the output scale")
    # parser.add_argument("--lengthscale_prior", dest="lengthscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the length scale")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on
    show_plot = args.show_plot

    main(latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, init_type, plot_on, show_plot)
