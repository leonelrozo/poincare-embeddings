import os
import time
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser
# import mujoco_py

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_data


CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()

def set_joint_values(sim, joint_name_list, q):
    """
    Set the robot joint values to the given desired values.
    """
    if len(q.shape) == 2:
        q = q[0, :]

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        states.qpos[addr] = q[joint_id]

    sim.set_state(states)
    sim.forward()


def get_joint_values(sim, joint_name_list):
    """
    Get the robot joint values.
    """
    q = []

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        q.append(states.qpos[addr])

    return q

def compute_ts_fct(sim, joint_name_list, q, tcp_name):
    """
    Computes the end-effector pose using forward kinematics.

    Parameters
    ----------
    :param q: current joint angles

    Return
    ------
    :return: end-effector pose
    """
    if len(q.shape) == 2:
        q = q[0, :]

    set_joint_values(sim, joint_name_list, q)
    tcp_pos = sim.data.get_site_xpos(tcp_name)
    tcp_xmat = sim.data.get_site_xmat(tcp_name)
    tcp_quat = np.zeros(4)
    mujoco_py.functions.mju_mat2Quat(tcp_quat, np.reshape(tcp_xmat, (-1,), order='C'))
    x_fct = np.concatenate([tcp_pos, tcp_quat], axis=-1)
    return x_fct


body_joint_names = ["BLNx_joint", "BLNy_joint", "BLNz_joint",
                    "BPx_joint", "BPy_joint", "BPz_joint",
                    "BTx_joint", "BTy_joint", "BTz_joint",
                    "BUNx_joint", "BUNy_joint", "BUNz_joint",
                    "LAx_joint", "LAy_joint", "LAz_joint",
                    "LEx_joint", "LEz_joint",
                    "LHx_joint", "LHy_joint", "LHz_joint",
                    "LKx_joint",
                    "LSx_joint", "LSy_joint", "LSz_joint",
                    "LWx_joint", "LWy_joint",
                    "LFx_joint",
                    "LMrot_joint",
                    "RAx_joint", "RAy_joint", "RAz_joint",
                    "REx_joint", "REz_joint",
                    "RHx_joint", "RHy_joint", "RHz_joint",
                    "RKx_joint",
                    "RSx_joint", "RSy_joint", "RSz_joint",
                    # "RWx_joint", "RWy_joint", # in hand joints
                    "RFx_joint",
                    "RMrot_joint"
                    ]


def compute_smoothness(trajectories, dt):
    # Assumes trajectories to be batch x timesteps x dimension
    jerk_euclidean = np.sum(np.sqrt(np.sum((np.diff(np.diff(np.diff(trajectories, axis=1), axis=1), axis=1) / (dt ** 3)) ** 2, -1)), 1)
    return jerk_euclidean


def main(manifold, latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, plot_on, pullback_metric=False):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    data_folder_path = ROOT_DIR / 'data'
    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Model name
    model_name = manifold + '_gplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)
    if pullback_metric:
        model_name += '_pullback'

    geodesic_data = np.load(MODEL_PATH / ('geodesics/' + model_name + '_geodesics.npz'))
    geodesics = geodesic_data["geodesics"]
    geodesic_idx = geodesic_data["geodesic_idx"]
    geodesics_closest_class_names = geodesic_data["geodesics_closest_class_names"]
    geodesics_task_space_mean = geodesic_data["geodesics_task_space_mean"]

    # # Mujoco and model initialization
    # mj_path = mujoco_py.utils.discover_mujoco()
    #
    # base_path = Path(__file__).parent.parent.parent.parent.resolve()
    # data_folder_path = base_path / "data"
    # xml_path = str(data_folder_path / 'mmm_mujoco/environment/mmm_bimanual_kitchen.xml')
    # model = mujoco_py.load_model_from_path(xml_path)
    # sim = mujoco_py.MjSim(model)
    # viewer_show = True
    # # Remark: the model is not scaled (i.e., normalized to 1m height)

    for g in range(len(geodesics_task_space_mean)):
        print('Geodesic: ', g)
        geodesic = geodesics_task_space_mean[g] + data_mean.detach().numpy()[None]

        # # Visualize each shape pose
        # if viewer_show is True:
        #     viewer = mujoco_py.MjViewer(sim)
        #     viewer._paused = True
        #
        # if dataset == 'grasps':
        #     sim.step()
        #     body_joint_data = get_joint_values(sim, body_joint_names)
        #
        #     body_joint_data[body_joint_names.index("RSx_joint")] = 0.48744
        #     body_joint_data[body_joint_names.index("RSy_joint")] = -0.516448
        #     body_joint_data[body_joint_names.index("RSz_joint")] = 0.150373
        #     body_joint_data[body_joint_names.index("REx_joint")] = 1.1333
        #     body_joint_data[body_joint_names.index("REz_joint")] = 0.878193
        #
        # for n in range(geodesic.shape[0]):
        #     if dataset == 'augmented_support_poses' or dataset == 'bimanual':
        #         set_joint_values(sim, joint_names, geodesic[n])
        #     elif dataset == 'grasps':
        #         set_joint_values(sim, body_joint_names + joint_names, np.hstack((body_joint_data, geodesic[n])))
        #
        #     # set_joint_values(sim, ['LSCy_joint', 'LSCz_joint', 'RSCy_joint', 'RSCz_joint'], np.array([0.0, 0.0, 0.0, 0.0]))  # Added now in the data
        #     # print(get_joint_values(sim, ['LSCy_joint', 'LSCz_joint', 'RSCy_joint', 'RSCz_joint']))
        #
        #     sim.step()
        #
        #     if viewer_show is True:
        #         viewer.render()
        #
        #     # if n > 0:
        #     #     time.sleep(0.1)
        #     #     viewer._paused = True

        if dataset == 'grasps':
            saved_joint_names = joint_names + ["RSx_joint", "RSy_joint", "RSz_joint", "REx_joint", "REz_joint"]
            geodesic = np.hstack((geodesic, np.repeat(np.array([0.48744, -0.516448, 0.150373, 1.1333, 0.878193])[None], geodesic.shape[0], axis=0)))
        else:
            saved_joint_names = joint_names
        # path = MODEL_PATH / ('geodesics/' + dataset + '_joints_geodesic_euclidean' + str(g) + '.npz')
        path = MODEL_PATH / ('geodesics/' + dataset + '_joints_geodesic' + str(g) + '.npz')
        np.savez(path, joint_positions=geodesic, #xpositions=np.array(xpos_log),
                 geodesic_idx=geodesic_idx[g], joint_names=saved_joint_names)

    # Compute smoothness
    smoothness_vals = compute_smoothness(geodesics_task_space_mean + data_mean.detach().numpy()[None], dt=0.1)
    print(np.mean(smoothness_vals), np.std(smoothness_vals))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--manifold", dest="manifold", default="euclidean",
                        help="Latent manifold. Options: hyperbolic, euclidean")
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_type", dest="model_type", default="BC",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual.")
    parser.add_argument("--dataset_type", dest="dataset_type", default=None,
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=2000.0,
                        # 5000 augm. sup. poses; grasps 2000
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")
    parser.add_argument("--pullback_metric", dest="pullback_metric", type=bool, default=False,
                        help="If True, compute and plot pullback metric and geodesics.")


    args = parser.parse_args()

    manifold = args.manifold
    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on
    pullback_metric = args.pullback_metric

    main(manifold, latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, plot_on, pullback_metric)
