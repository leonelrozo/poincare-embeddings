#!/bin/bash

# Euclidean 2 - grasps - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=6500.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=5500.0

# Euclidean 2 - augm. support poses - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=6500.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=5500.0

# Euclidean 2 - grasps - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=2000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=4000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=3000.0
#
### Euclidean 2 - augm. support poses - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=4000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=3000.0

## Euclidean 3 - grasps - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=8000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=10000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=12000.0
#
## Euclidean 3 - augm. support poses - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=8000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=10000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="MAP" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=12000.0
#
## Euclidean 3 - grasps - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=2000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=3000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=4000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#
#
### Euclidean 3 - augm. support poses - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=4000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=8000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=10000.0


### Euclidean 2 - bimanual - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=800.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=1000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=1200.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=1500.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=2 --loss_type="Stress" --loss_scale=2000.0
#
## Euclidean 2 - bimanual - stress
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=800.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1000.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1200.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1500.0

## Euclidean 3 - bimanual - stress
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=3 --loss_type="Zero" --loss_scale=0.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=1000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=2500.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=3000.0
#python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="MAP" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0

# Euclidean 3 - bimanual - stress
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=1000.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=2000.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=2500.0
python3 euclidean_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=3000.0
