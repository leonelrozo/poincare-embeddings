from pathlib import Path

import os
import time
import numpy as np
import torch
from argparse import ArgumentParser

from geoopt.optim.radam import RiemannianAdam
import gpytorch.priors.torch_priors as torch_priors
from gpytorch.kernels import ScaleKernel, RBFKernel, Kernel

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_data
from HyperbolicEmbeddings.gplvm.gplvm_initializations import euclidean_stress_loss_initialization, pca_initialization
from HyperbolicEmbeddings.gplvm.gplvm_models import MapExactGPLVM, BackConstrainedExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, EuclideanStressLossTermExactMLL, \
    EuclideanDistortionLossTermExactMLL, stress
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.normalization import centering
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_gplvm_pullback_metric
from HyperbolicEmbeddings.gplvm.gplvm_stochman_wrapper import DiscreteGPLVMManifoldWrapper, GPLVMManifoldWrapper
from HyperbolicEmbeddings.gplvm.gplvm_predict_pullback_metric import get_pullback_metric_function
from HyperbolicEmbeddings.utils.geodesics import get_geodesic_on_learned_manifold, \
    get_geodesic_on_discrete_learned_manifold


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()


def main(latent_dim, model_type, dataset, dataset_type=None,
         loss_type='Zero', loss_scale=1.0, init_type="PCA",
         plot_on=False, show_plot=False, outputscale_prior=None, lengthscale_prior=None, pullback_metric=False):

    # Check that the pullback metric does not come with additional loss
    if pullback_metric and loss_type != 'Zero':
        raise NotImplementedError

    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Model name
    model_name = 'euclidean_gplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)
    if pullback_metric:
        model_name += '_pullback'

    # Model parameters
    N = len(training_data)

    # Initialization
    if init_type == 'Stress':
        X_init = euclidean_stress_loss_initialization(training_data, latent_dim, graph_distances)
    elif init_type == 'PCA':
        X_init = pca_initialization(training_data, latent_dim)
    else:
        X_init = None

    # Model definition
    if model_type == 'MAP':
        # Priors
        if outputscale_prior is not None:
            kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
        else:
            kernel_outputscale_prior = None

        if lengthscale_prior is not None:
            kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
        else:
            kernel_lengthscale_prior = None

        # Model
        model = MapExactGPLVM(training_data, latent_dim, X_init=X_init, batch_params=False,
                              kernel_lengthscale_prior=kernel_lengthscale_prior,
                              kernel_outputscale_prior=kernel_outputscale_prior)
    elif model_type == 'BC':
        # Prior on latent variable
        if outputscale_prior is not None:
            kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
        else:
            kernel_outputscale_prior = None

        if lengthscale_prior is not None:
            kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
        else:
            kernel_lengthscale_prior = None

        # Back constraints
        data_kernel = ScaleKernel(RBFKernel())
        classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

        if loss_type == 'Stress':
            if dataset == 'grasps':
                data_kernel.base_kernel.lengthscale = 1.8
                classes_kernel.base_kernel.lengthscale = 1.5

            elif dataset == 'support-poses':
                data_kernel.base_kernel.lengthscale = 0.9
                classes_kernel.base_kernel.lengthscale = 0.6

            elif dataset == 'augmented-support-poses':
                data_kernel.base_kernel.lengthscale = 2.0
                classes_kernel.base_kernel.lengthscale = 0.8

            elif dataset == 'bimanual':
                data_kernel.base_kernel.lengthscale = 3.0
                classes_kernel.base_kernel.lengthscale = 1.5

            data_kernel.outputscale = 2.0
            classes_kernel.outputscale = 1.0

            taxonomy_bc = True

        elif loss_type == 'Zero':
            data_kernel.base_kernel.lengthscale = 1.5  # TODO
            classes_kernel.base_kernel.lengthscale = 1.5
            data_kernel.outputscale = 1.0
            classes_kernel.outputscale = 1.0

            taxonomy_bc = False

        # Kernel
        model = BackConstrainedExactGPLVM(training_data, latent_dim, indices_in_graph,
                                          data_kernel=data_kernel, classes_kernel=classes_kernel,
                                          kernel_lengthscale_prior=kernel_lengthscale_prior,
                                          kernel_outputscale_prior=kernel_outputscale_prior,
                                          X_init=X_init, batch_params=False,
                                          taxonomy_based_back_constraints=taxonomy_bc)

    # Print initial stress
    init_stress = stress(Kernel().covar_dist(model.X(), model.X()), graph_distances)
    print(init_stress.mean(), init_stress.std())

    # print("Selecting these priors:")
    # print(f"output scale prior:", kernel_outputscale_prior)
    # print(f"length scale prior:", kernel_lengthscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = EuclideanStressLossTermExactMLL(graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = EuclideanDistortionLossTermExactMLL(graph_distances, loss_scale)
    model.add_loss_term(added_loss)

    if pullback_metric:
        model.likelihood.noise = 0.001  # It does not seem to affect the metric in the default case of this code.
        pass

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Plot initial latent variables
    if plot_on:
        x_latent_init = model.X()
        x_latent_init_np = model.X().detach().numpy()
        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(color_function(nodes_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_2d(x_latent_init_np, x_colors, nodes_legend_for_plot,
                                    save_path=VIZ_DIR / f"{model_name}_init.png", show=show_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_3d(x_latent_init_np, x_colors, nodes_legend_for_plot, show=show_plot)
                                    # save_path=VIZ_DIR / f"{model_name}_init.png", )

        # Plot distances in the latent space
        distances_latent_init = model.covar_module.covar_dist(x_latent_init, x_latent_init).detach().numpy()
        plot_distance_matrix(distances_latent_init,
                             save_path=VIZ_DIR / f"distance_latent_{model_name}_init.png", show=show_plot)
        # Plot distances between classes
        plot_distance_matrix(graph_distances,
                             save_path=VIZ_DIR / f"distance_latent_{model_name}_init.png", show=show_plot)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    try:
        # start = time.time()
        # fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, options={"maxiter": 500, "lr": 0.01, 'disp': False})
        # print(time.time() - start)
        fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH / model_name
                        , options={"maxiter": 1000, "disp": True, "lr": 0.01})
    except KeyboardInterrupt:
        pass
    mll.eval()

    # Test evaluation
    model.eval()
    posterior = model(model.X())
    error = (posterior.mean.T - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    final_stress = stress(Kernel().covar_dist(model.X(), model.X()), graph_distances)
    print(final_stress.mean(), final_stress.std())
    # print(added_loss.loss(model.X()).detach().numpy() / loss_scale)

    # Compute pullback metric
    if pullback_metric:
        latent_vars = model.X.X.detach()
        # Compute geodesic latent distances
        continuous_geodesic = True
        pullback_metric_function = get_pullback_metric_function(latent_vars, training_data, model.likelihood,
                                                                model.covar_module)
        geodesic_distances, geodesic_paths = _pullback_metric_geodesics(latent_vars, pullback_metric_function,
                                                                        continuous_geodesic)
        np.savez(MODEL_PATH / (model_name + '_geodesic_distances'),
                 geodesic_distances=geodesic_distances.detach().numpy())

    # Plot results
    if plot_on:
        x_latent = model.X()
        x_latent_np = x_latent.detach().numpy()

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            if pullback_metric:
                plot_metric_tensor(x_latent, training_data, model.likelihood, model.covar_module,
                                   x_colors=x_colors, grid_size=128, save_path=VIZ_DIR / f"{model_name}.png",
                                   show=show_plot)
            else:
                # Plot euclidean latent space
                plot_euclidean_gplvm_2d(x_latent_np, x_colors, nodes_legend_for_plot,
                                        save_path=VIZ_DIR / f"{model_name}.png", show=show_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_euclidean_gplvm_3d(x_latent_np, x_colors, nodes_legend_for_plot)
                                    # save_path=VIZ_DIR / f"{model_name}.png", show=show_plot)

        # Plot distances in the latent space
        distances_latent = model.covar_module.covar_dist(x_latent, x_latent).detach().numpy()
        if pullback_metric:
            distances_latent = geodesic_distances.detach().numpy()

        max_distance = np.max(distances_latent)
        plot_distance_matrix(distances_latent, max_distance=max_distance,
                             save_path=VIZ_DIR / f"distance_latent_{model_name}.png", show=show_plot)
        # Plot distances between classes
        plot_distance_matrix(graph_distances, max_distance=max_distance,
                             save_path=VIZ_DIR / f"distance_graph_{model_name}.png", show=show_plot)

    print('End')


def _pullback_metric_geodesics(latent_vars, pullback_metric_function, continuous_geodesic) -> tuple[list, list]:
    """

    Parameters
    ----------
    latent_vars
    pullback_metric_function
    continuous_geodesic

    Returns
    -------

    """
    manifold = GPLVMManifoldWrapper(pullback_metric_function) if continuous_geodesic \
        else DiscreteGPLVMManifoldWrapper(pullback_metric_function)

    N = len(latent_vars)
    n_eval_points = 50
    geodesic_time = torch.linspace(0, 1, n_eval_points)[:, None]
    # geodesic_distances = []  # List of pairwise geodesic distances
    geodesic_distances = torch.zeros((N, N))
    geodesic_paths = []  # List of geodesics (used, e.g., for plotting purposes)
    for m in range(N):  # We need to loop as every geodesic distance is an optimization process
        # Initialize geodesic with initial and final latent codes
        print(m)
        geodesic_init = latent_vars[m]
        for n in range(m + 1, N):
            geodesic_final = latent_vars[n]
            # Geodesic initialization with a straight line
            initial_geodesic = geodesic_init * (1 - geodesic_time) + geodesic_final * geodesic_time
            # Computes geodesic path
            if continuous_geodesic:  # Gradient-based energy minimization
                geodesic_path = get_geodesic_on_learned_manifold(initial_geodesic, pullback_metric_function,
                                                                 n_eval=n_eval_points,
                                                                 num_nodes_to_optimize=n_eval_points)
            else:
                geodesic_path = get_geodesic_on_discrete_learned_manifold(initial_geodesic, latent_vars,
                                                                          pullback_metric_function,
                                                                          n_eval=n_eval_points, grid_size=256,
                                                                          num_nodes_to_optimize=50)
            # Compute the geodesic length
            # geodesic_distances.append(manifold.curve_length(geodesic_path))
            geodesic_distances[m, n] = manifold.curve_length(geodesic_path)
            geodesic_distances[n, m] = manifold.curve_length(geodesic_path)
            geodesic_paths.append(geodesic_path)

    return geodesic_distances, geodesic_paths


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=3,
                        help="Set the latent dim.")
    parser.add_argument("--model_type", dest="model_type", default="MAP",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual.")
    parser.add_argument("--dataset_type", dest="dataset_type", default=None,
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=20000.0,
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=False,
                        help="If True, generate plots.")
    parser.add_argument("--show_plot", dest="show_plot", type=bool, default=False,
                        help="If True, show plots.")
    # parser.add_argument("--outputscale_prior", dest="outputscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the output scale")
    # parser.add_argument("--lengthscale_prior", dest="lengthscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the length scale")
    parser.add_argument("--pullback_metric", dest="pullback_metric", type=bool, default=False,
                        help="If True, compute and plot pullback metric and geodesics.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on
    show_plot = args.show_plot
    pullback_metric = args.pullback_metric

    main(latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, init_type, plot_on, show_plot,
         pullback_metric=pullback_metric)
