import os
from pathlib import Path
import numpy as np
import torch
from argparse import ArgumentParser

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_added_data
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import ordered_hand_grasps_names
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import ordered_shape_pose_names
from HyperbolicEmbeddings.taxonomy_utils.taxonomy_functions import reorder_taxonomy_data, reorder_distance_matrix
from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import ordered_bimanual_names
from HyperbolicEmbeddings.model_loading_utils.load_gplvm import load_trained_gplvm_model
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_geodesic, \
    lorentz_distance_torch
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d_added_data, \
    plot_hyperbolic_gplvm_3d_added_data
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d_added_data, \
    plot_euclidean_gplvm_3d_added_data
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix_added_data


torch.set_default_dtype(torch.float64)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()


def main(manifold, latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, plot_on):
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    training_data, added_data, data_mean, adjacency_matrix, graph_distances, all_graph_distances, \
    nodes_names, added_nodes_names, indices_in_graph, added_indices_in_graph, \
    nodes_legend_for_plot, all_nodes_legend_for_plot, color_function, max_manifold_distance = \
        load_taxonomy_added_data(dataset, dataset_type)

    if dataset == 'grasps':
        ordered_nodes_name = ordered_hand_grasps_names

    elif dataset == 'support-poses':
        pass

    elif dataset == 'augmented-support-poses':
        ordered_nodes_name = ordered_shape_pose_names

    elif dataset == 'bimanual':
        ordered_nodes_name = ordered_bimanual_names

    # Model parameters
    N = len(training_data)

    # Model name
    model_name = manifold + '_gplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)

    # Load model parameters
    mll = load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, model_type, dataset, training_data,
                                   adjacency_matrix, indices_in_graph, graph_distances, loss_type, loss_scale)

    N_added = len(added_data)

    # Compute latent variable corresponding to all added data
    added_x_latent = mll.model.X.back_constraint_function(added_data, added_indices_in_graph)

    # Latent variable
    x_latent = mll.model.X()
    all_x_latent = torch.vstack((x_latent, added_x_latent))

    # Test evaluation
    # model.eval()
    # posterior = model(model.X())
    # error = (posterior.mean.T - training_data).detach().numpy()

    # Plot geodesics in latent space
    if "hyperbolic" in model_name:
        # From Lorentz to Poincaré
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()
        added_x_poincare = lorentz_to_poincare(added_x_latent).detach().numpy()

    # Get colors
    x_colors = []
    for n in range(N):
        x_colors.append(color_function(nodes_names[n]))

    added_x_colors = []
    for n in range(N_added):
        added_x_colors.append(color_function(added_nodes_names[n]))

    # If the latent space is H2, we plot the embedding in the Poincaré disk
    if latent_dim == 2:
        if "hyperbolic" in model_name:
            if "grasps" in model_name:
                if "BC" in model_name:
                    max_magnification = 3.0
                elif "Zero" in model_name:
                    max_magnification = 2.2
                elif "MAP" in model_name:
                    max_magnification = 2.5
            if "augmented-support-poses" in model_name:
                if "BC" in model_name:
                    max_magnification = 4.0
                elif "Zero" in model_name:
                    max_magnification = 5.5
                elif "MAP" in model_name:
                    max_magnification = 4.0
            if "bimanual" in model_name:
                if "BC" in model_name:
                    max_magnification = 8.0
                elif "Zero" in model_name:
                    max_magnification = 7.0
                elif "MAP" in model_name:
                    max_magnification = 8.0

            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d_added_data(x_poincare, x_colors, added_x_poincare, added_x_colors,
                                                model=mll.model,
                                                model_magnification_path=MODEL_PATH / (model_name + '_magfac.npz'),
                                                max_magnification=max_magnification,
                                                save_path=VIZ_DIR / (model_name + '_latent_added_poses.png'))
        else:
            plot_euclidean_gplvm_2d_added_data(x_latent.detach().numpy(), x_colors, added_x_latent.detach().numpy(),
                                               added_x_colors, model=mll.model,
                                               save_path=VIZ_DIR / (model_name + '_latent_added_poses.png'))

    # If the latent space is H3, we plot the embedding in the Poincaré ball
    elif latent_dim == 3:
        if "hyperbolic" in model_name:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d_added_data(x_poincare, x_colors, added_x_poincare, added_x_colors,)
            # save_path=FIGURE_PATH + model_name + '_latent.pdf', show=False)
        else:
            plot_euclidean_gplvm_3d_added_data(x_latent.detach().numpy(), x_colors, added_x_latent.detach().numpy(),
                                               added_x_colors)

    # Diffentiate added pose names for distance plot
    added_nodes_names = [pose_name + "Added" for pose_name in added_nodes_names]
    all_nodes_names = nodes_names + added_nodes_names

    # Plot distances in the latent space
    x_latent_ordered = torch.vstack(reorder_taxonomy_data(all_x_latent, all_nodes_names, ordered_nodes_name))
    x_colors_ordered = reorder_taxonomy_data(x_colors + added_x_colors, all_nodes_names, ordered_nodes_name)
    all_nodes_names_ordered = reorder_taxonomy_data(all_nodes_names, all_nodes_names, ordered_nodes_name)

    if "hyperbolic" in model_name:
        distances_latent = lorentz_distance_torch(x_latent_ordered, x_latent_ordered).detach().numpy()
    else:
        distances_latent = mll.model.covar_module.covar_dist(x_latent_ordered, x_latent_ordered).detach().numpy()

    max_distance = np.max(graph_distances.detach().numpy()) + 1.0
    plot_distance_matrix_added_data(distances_latent, all_nodes_names_ordered,
                                    max_distance=max_distance, x_colors=x_colors_ordered,
                                    save_path=VIZ_DIR / (model_name + '_distances_added_poses.png'))

    # Plot distances between classes
    graph_distances_ordered = reorder_distance_matrix(all_graph_distances, all_nodes_names, ordered_nodes_name)
    plot_distance_matrix_added_data(graph_distances_ordered, all_nodes_names_ordered, max_distance=max_distance,
                                    x_colors=x_colors_ordered)
    # , save_path=VIZ_DIR / (model_name + '_ground_truthdistances_added_poses.png'))

    if latent_dim == 2:
        if "grasps" in model_name:
            max_error = 3.2
        if "augmented-support-poses" in model_name:
            max_error = 3.2
        if "bimanual" in model_name:
            max_error = 1.7
    elif latent_dim == 3:
        if "grasps" in model_name:
            max_error = 3.0
        if "augmented-support-poses" in model_name:
            max_error = 2.7
        if "bimanual" in model_name:
            max_error = 1.6

    plot_distance_matrix_added_data(np.abs(distances_latent - graph_distances_ordered), all_nodes_names_ordered,
                                    max_distance=max_error,
                                    x_colors=x_colors_ordered,
                                    save_path=VIZ_DIR / (model_name + '_distances_added_poses_error.png'))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--manifold", dest="manifold", default="hyperbolic",
                        help="Latent manifold. Options: hyperbolic, euclidean")
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_type", dest="model_type", default="BC",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="grasps",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual.")
    parser.add_argument("--dataset_type", dest="dataset_type", default=None,
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=2000.0,
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")


    args = parser.parse_args()

    manifold = args.manifold
    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on

    if loss_type == "Stress" and model_type == "backconstrained":
        if latent_dim == 3:
            if dataset == "feet5":
                loss_scale = 1.5

    # if loss_type != 'Zero':
    #     model_name += str(loss_scale)

    main(manifold, latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, plot_on)

