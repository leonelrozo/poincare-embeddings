import json
from pathlib import Path

import os
import time
import numpy as np
import torch
from matplotlib import pyplot as plt
import matplotlib.colors as pltc
from mayavi import mlab
from argparse import ArgumentParser

import gpytorch.priors.torch_priors as torch_priors
from gpytorch.kernels import ScaleKernel, RBFKernel

from geoopt.optim.radam import RiemannianAdam

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_data
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_initializations import \
    hyperbolic_tangent_pca_initialization, hyperbolic_stress_loss_initialization
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import MapExactHyperbolicGPLVM, \
    BackConstrainedHyperbolicExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_torch, fit_gplvm_torch_with_logger
from HyperbolicEmbeddings.kernels.kernels_graph import GraphMaternKernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare, lorentz_distance_torch
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL, VanillaHyperbolicDistortionLossTermExactMLL, stress
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.utils.logger import compose_loggers, ConsoleLogger, MemoryLogger

torch.set_default_dtype(torch.float64)
# gpytorch.settings.cholesky_jitter._global_double_value = 1e-4
# gpytorch.settings.cholesky_jitter._global_float_value = 1e-4

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.parent.resolve()


def main(latent_dim, model_type, dataset, dataset_type=None,
         loss_type='Zero', loss_scale=1.0, init_type="PCA",
         plot_on=False, show_plot=False, outputscale_prior=None, lengthscale_prior=None):

    # Setting manual seed for reproducibility
    seed = 73
    torch.manual_seed(seed)
    np.random.seed(seed)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_gplvm')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Model parameters
    N = len(training_data)

    # Model name
    model_name = 'hyperbolic_gplvm_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)

    # Initialization
    if init_type == 'Stress':
        X_init = hyperbolic_stress_loss_initialization(training_data, latent_dim, graph_distances)
    elif init_type == 'PCA':
        X_init = hyperbolic_tangent_pca_initialization(training_data, latent_dim)
    else:
        X_init = None

    # Use rejection sampling kernel or not
    rejection_sampling_kernel = False
    if dataset == "multicellular-robots":
        rejection_sampling_kernel = True

    # Model definition
    if model_type == 'MAP':
        # Priors
        if outputscale_prior is not None:
            hyperbolic_kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
        else:
            hyperbolic_kernel_outputscale_prior = None
        if lengthscale_prior is not None:
            hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
        else:
            hyperbolic_kernel_lengthscale_prior = None

        # Model
        model = MapExactHyperbolicGPLVM(training_data, latent_dim, X_init=X_init, batch_params=False,
                                        kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                        kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                        rejection_sampling_kernel=rejection_sampling_kernel)

    elif model_type == 'BC':
        # Prior on latent variable
        if outputscale_prior is not None:
            hyperbolic_kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
        else:
            hyperbolic_kernel_outputscale_prior = None

        if lengthscale_prior is not None:
            hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
        else:
            hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)

        # Back constraints
        data_kernel = ScaleKernel(RBFKernel())
        classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

        if loss_type == 'Stress':
            if dataset == 'grasps':
                data_kernel.base_kernel.lengthscale = 1.8
                classes_kernel.base_kernel.lengthscale = 1.5

            elif dataset == 'support-poses':
                data_kernel.base_kernel.lengthscale = 0.9
                classes_kernel.base_kernel.lengthscale = 0.6

            elif dataset == 'augmented-support-poses':
                data_kernel.base_kernel.lengthscale = 2.0
                classes_kernel.base_kernel.lengthscale = 0.8

            elif dataset == 'bimanual':
                data_kernel.base_kernel.lengthscale = 3.0
                classes_kernel.base_kernel.lengthscale = 1.5

            data_kernel.outputscale = 2.0
            classes_kernel.outputscale = 1.0

            taxonomy_bc = True

        elif loss_type == 'Zero':
            data_kernel.base_kernel.lengthscale = 1.5  # TODO
            classes_kernel.base_kernel.lengthscale = 1.5
            data_kernel.outputscale = 1.0
            classes_kernel.outputscale = 1.0

            taxonomy_bc = False

        # Model
        model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, indices_in_graph,
                                                    data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                    kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                                    kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                                    X_init=X_init, batch_params=False,
                                                    taxonomy_based_back_constraints=taxonomy_bc,
                                                    rejection_sampling_kernel=rejection_sampling_kernel)

    # Print initial stress
    init_stress = stress(lorentz_distance_torch(model.X(), model.X()), graph_distances)
    print(init_stress.mean(), init_stress.std())

    # print("Selecting these priors:")
    # print(f"output scale prior:", hyperbolic_kernel_outputscale_prior)
    # print(f"length scale prior:", hyperbolic_kernel_lengthscale_prior)

    # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
    # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(graph_distances, loss_scale)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(graph_distances, loss_scale)
    elif loss_type == "VanillaDistortion":
        print("Loss_type = VanillaDistortion")
        added_loss = VanillaHyperbolicDistortionLossTermExactMLL(graph_distances, loss_scale)

    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Plot initial latent variables
    # print(f"Plotting?: {plot_on}")
    if plot_on:
        x_latent_init = model.X()
        # From Lorentz to Poincaré
        x_poincare_init = lorentz_to_poincare(x_latent_init).detach().numpy()
        # Get colors
        x_colors = []
        for n in range(N):
            x_colors.append(color_function(nodes_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare_init, x_colors, nodes_legend_for_plot,
                                     save_path=VIZ_DIR / f"{model_name}_init.png", show=show_plot)
    
        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare_init, x_colors, nodes_legend_for_plot,
                                     save_path=VIZ_DIR / f"{model_name}_init.png", show=show_plot)
    
        # Plot distances in the latent space
        distances_latent_init = lorentz_distance_torch(x_latent_init, x_latent_init).detach().numpy()
        plot_distance_matrix(distances_latent_init,
                             save_path=VIZ_DIR / f"distance_latent_{model_name}_init.png", show=show_plot)
        # Plot distances between classes
        plot_distance_matrix(graph_distances, show=show_plot)

    # Train the model in a single batch with automatic convergence checks
    maxiter = 1000
    if dataset == 'bimanual' and latent_dim == 2:
        lr = 0.025
    else:
        lr = 0.05
    model_path = MODEL_PATH / model_name
    model_path.mkdir(exist_ok=True)
    logger = compose_loggers(ConsoleLogger(n_logs=maxiter), MemoryLogger(model_path))

    mll.train()
    try:
        fit_gplvm_torch_with_logger(mll, optimizer_cls=RiemannianAdam, logger=logger, 
                                    options={"maxiter": maxiter, "lr": lr}, stop_crit_options={"maxiter": maxiter})

        logger.print_full_iteration('Best Iteration', logger.best_step, logger.best_loss, logger.best_parameters)
        mll.load_state_dict(logger.best_state_dict)
        print("Best iteration was achieved at step " + str(logger.best_step) + " with the best loss at " + str(logger.best_loss))

        # start = time.time()
        # fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, options={"maxiter": 500, "lr": lr, 'disp': False})
        # print(time.time() - start)
        # fit_gplvm_torch(mll, optimizer_cls=RiemannianAdam, model_path=MODEL_PATH / model_name,
        #                 options={"maxiter": 1000, "lr": lr})  #, options={"maxiter": 1000, "disp": True, "lr": 0.01})
        #                 # options={"maxiter": 1000, "lr": 0.5})  #, options={"maxiter": 1000, "disp": True, "lr": 0.01})
    except KeyboardInterrupt:
        pass
    mll.eval()

    # Test evaluation
    model.eval()
    posterior = model(model.X())
    error = (posterior.mean.T - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    final_stress = stress(lorentz_distance_torch(model.X(), model.X()), graph_distances)
    print(final_stress.mean(), final_stress.std())
    # print(added_loss.loss(model.X()).detach().numpy() / loss_scale)

    # Plot results
    if plot_on:
        # From Lorentz to Poincaré
        x_latent = model.X()
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, nodes_legend_for_plot,
                                     save_path=VIZ_DIR / f"{model_name}.png", show=show_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors, nodes_legend_for_plot,
                                     save_path=VIZ_DIR / f"{model_name}.png", show=show_plot)

        # Plot distances in the latent space
        distances_latent = lorentz_distance_torch(x_latent, x_latent).detach().numpy()
        max_distance = np.max(distances_latent)
        plot_distance_matrix(distances_latent, max_distance=max_distance,
                             save_path=VIZ_DIR / f"distance_latent_{model_name}.png", show=show_plot)
        # Plot distances between classes
        plot_distance_matrix(graph_distances, max_distance=max_distance,
                             save_path=VIZ_DIR / f"distance_graph_{model_name}.png", show=show_plot)

    print('End')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--model_type", dest="model_type", default="MAP",
                        help="Set the model type. Options: MAP, BC.")
    parser.add_argument("--dataset", dest="dataset", default="multicellular-robots",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual, multicellular-robots.")
    parser.add_argument("--dataset_type", dest="dataset_type", default=None,
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Stress",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=8000.0,
                        help="Set the loss scale.")
    # parser.add_argument("--max_manifold_distance", dest="max_manifold_distance", type=float, default=5.0,
    #                     help="Set the maximum desired manifold distance between nodes.")
    parser.add_argument("--init_type", dest="init_type", type=str, default="Stress",
                        help="Set the GPLVM initialization. Options: Random, PCA, Stress.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")
    parser.add_argument("--show_plot", dest="show_plot", type=bool, default=True,
                        help="If True, show plots.")
    # parser.add_argument("--outputscale_prior", dest="outputscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the output scale")
    # parser.add_argument("--lengthscale_prior", dest="lengthscale_prior", type=str,
    #                     default=None, help="The position of a gamma prior for the length scale")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    model_type = args.model_type
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    # max_manifold_distance = args.max_manifold_distance
    init_type = args.init_type
    plot_on = args.plot_on
    show_plot = args.show_plot

    main(latent_dim, model_type, dataset, dataset_type, loss_type, loss_scale, init_type, plot_on, show_plot)
