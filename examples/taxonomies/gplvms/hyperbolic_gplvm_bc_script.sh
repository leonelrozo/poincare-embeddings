#!/bin/bash

# Hyperbolic 2 - stress
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=2000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=3000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=5000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0


## Hyperbolic 2 - stress
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=6000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=5000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=4000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=7000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=2 --loss_type="Stress" --loss_scale=3000.0


## Hyperbolic 3 - stress
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=2000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=3000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=4000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="grasps" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#
#
### Hyperbolic 3 - stress
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=6000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=5000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=4000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=7000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=8000.0
#python3 hyperbolic_egplvm_taxonomies.py --dataset="augmented-support-poses" --model_type="BC" --dataset_type="semifull" --latent_dim=3 --loss_type="Stress" --loss_scale=10000.0

# Hyperbolic 2 - bimanual - stress
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=800.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1000.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1200.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=2 --loss_type="Stress" --loss_scale=1500.0

# Hyperbolic 2 - bimanual - stress
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=500.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=700.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=1000.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=1200.0
python3 hyperbolic_egplvm_taxonomies.py --dataset="bimanual" --model_type="BC" --latent_dim=3 --loss_type="Stress" --loss_scale=1500.0
