"""
This script trains a hyperbolic VAE with latent dimension 3 on
the augmented taxonomy.
"""
from typing import Tuple, List
from pathlib import Path

import torch
import numpy as np
from argparse import ArgumentParser

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_data
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import ordered_hand_grasps_names
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import ordered_shape_pose_names
from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import ordered_bimanual_names
from HyperbolicEmbeddings.taxonomy_utils.taxonomy_functions import reorder_taxonomy_data, reorder_distance_matrix
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch, lorentz_to_poincare
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.vae_baselines.hyperbolic_vae import HyperbolicVAEBaseline

torch.set_default_dtype(torch.float64)

CURRENT_DIR = Path(__file__).parent.resolve()
ROOT_DIR = CURRENT_DIR.parent.parent.parent.resolve()


def main(latent_dim, dataset, dataset_type=None, hidden_dim: int = 22, max_epochs: int = 1000,
         learning_rate: float = 0.05, loss_type='Zero', loss_scale=1.0, plot_on=False, show_plot=False):

    # Setting manual seed for reproducibility
    seed = 73
    torch.manual_seed(seed)
    np.random.seed(seed)

    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_vae')
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_vaes_viz")
    VIZ_DIR.mkdir(exist_ok=True)

    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = load_taxonomy_data(dataset, dataset_type=dataset_type)

    if dataset == 'grasps':
        ordered_nodes_name = ordered_hand_grasps_names

    elif dataset == 'support-poses':
        pass

    elif dataset == 'augmented-support-poses':
        ordered_nodes_name = ordered_shape_pose_names

    elif dataset == 'bimanual':
        ordered_nodes_name = ordered_bimanual_names

    # Model parameters
    N = len(training_data)

    # Model name
    model_name = 'hyperbolic_vae_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)

    # Setting up the model and optimizer
    vae = HyperbolicVAEBaseline(
        input_dim=training_data.shape[1],
        latent_dim=latent_dim,
        hidden_dim=hidden_dim,
        loss_type=loss_type,
        graph_distance_matrix=graph_distances,
        loss_scale=loss_scale,
    )
    optimizer = torch.optim.Adam(vae.parameters(), lr=learning_rate)

    # Training full batch and without splitting train/test.
    for iteration in range(max_epochs):
        optimizer.zero_grad()
        loss, rec_loss, kld, added_loss = vae.elbo_loss(training_data)
        loss.backward()
        optimizer.step()

        print(
            f"Step: {iteration:05d}, loss: {loss:.3f}, rec loss: {rec_loss:.3f}, kld: {kld:.3f}, added loss: {added_loss:.3f}"
        )

    torch.save(vae.state_dict(), MODEL_PATH / model_name)
    vae.eval()

    x_latent = vae.encode(training_data).loc
    x_reconstructed = vae.decode(x_latent)
    error = (x_reconstructed.loc - training_data).detach().numpy()
    print(np.mean(np.abs(error)))
    print(vae.added_loss.loss(x_latent).detach().numpy() / loss_scale)

    # Plot results
    if plot_on:
        # From Lorentz to Poincaré
        x_poincare = lorentz_to_poincare(x_latent).detach().numpy()

        # Colors
        x_colors = []
        for n in range(N):
            x_colors.append(color_function(nodes_names[n]))

        # If the latent space is H2, we plot the embedding in the Poincaré disk
        if latent_dim == 2:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_2d(x_poincare, x_colors, model=vae,
                                     save_path=VIZ_DIR / (model_name + '_latent.png'), show=show_plot)

        # If the latent space is H3, we plot the embedding in the Poincaré ball
        elif latent_dim == 3:
            # Plot hyperbolic latent space
            plot_hyperbolic_gplvm_3d(x_poincare, x_colors,
                                     save_path=VIZ_DIR / (model_name + '_latent.png'), show=show_plot)

        # Plot distances in the latent space
        x_latent_ordered = torch.vstack(reorder_taxonomy_data(x_latent, nodes_names, ordered_nodes_name))
        x_colors_ordered = reorder_taxonomy_data(x_colors, nodes_names, ordered_nodes_name)

        # Plot distances in the latent space
        distances_latent = lorentz_distance_torch(x_latent_ordered, x_latent_ordered).detach().numpy()
        max_distance = np.max(graph_distances.detach().numpy()) + 1.0
        plot_distance_matrix(distances_latent, max_distance=max_distance, x_colors=x_colors_ordered,
                             save_path=VIZ_DIR / (model_name + '_distances.png'), show=show_plot)

        if latent_dim == 2:
            if "grasps" in model_name:
                if "Zero" in model_name:
                    max_error = 5.0
                else:
                    max_error = 3.5
            if "augmented-support-poses" in model_name:
                if "Zero" in model_name:
                    max_error = 4.5
                else:
                    max_error = 3.5
            if "bimanual" in model_name:
                if "Zero" in model_name:
                    max_error = 3.5
                else:
                    max_error = 1.7
        elif latent_dim == 3:
            if "grasps" in model_name:
                if "Zero" in model_name:
                    max_error = 5.0
                else:
                    max_error = 2.5
            if "augmented-support-poses" in model_name:
                if "Zero" in model_name:
                    max_error = 5.0
                else:
                    max_error = 2.5
            if "bimanual" in model_name:
                if "Zero" in model_name:
                    max_error = 4.0
                else:
                    max_error = 1.6

        graph_distances_ordered = reorder_distance_matrix(graph_distances, nodes_names, ordered_nodes_name)
        plot_distance_matrix(np.abs(distances_latent - graph_distances_ordered),
                             max_distance=max_error,
                             x_colors=x_colors_ordered, show=True,
                             save_path=VIZ_DIR / (model_name + '_distances_error.png'))


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--latent_dim", dest="latent_dim", type=int, default=2,
                        help="Set the latent dim (H2 -> 2, H3 -> 3).")
    parser.add_argument("--dataset", dest="dataset", default="augmented-support-poses",
                        help="Set the dataset. Options: grasps, support-poses, augmented-support-poses, bimanual.")
    parser.add_argument("--dataset_type", dest="dataset_type", default="semifull",
                        help="Set the dataset for support poses. Options: full, semifull, reduced, feet5, knees2, "
                             "oneofeach.")
    parser.add_argument("--loss_type", dest="loss_type", default="Zero",
                        help="Set the loss type. Options: Zero, Stress, Distortion.")
    parser.add_argument("--loss_scale", dest="loss_scale", type=float, default=200.0,
                        help="Set the loss scale.")
    parser.add_argument("--plot_on", dest="plot_on", type=bool, default=True,
                        help="If True, generate plots.")
    parser.add_argument("--show_plot", dest="show_plot", type=bool, default=True,
                        help="If True, show plots.")

    args = parser.parse_args()

    latent_dim = args.latent_dim
    dataset = args.dataset
    dataset_type = args.dataset_type
    loss_type = args.loss_type
    loss_scale = args.loss_scale
    plot_on = args.plot_on
    show_plot = args.show_plot

    # Some hyperparameters
    # max_epochs = 1000
    # latent_dim = 3
    # hidden_dim = 22
    # dataset = "feet5"
    # loss_type = "Stress"
    # loss_scale = 1.5
    # learning_rate = 0.005

    main(latent_dim, dataset, dataset_type, hidden_dim=22, max_epochs=1000, learning_rate=0.005,
         loss_type=loss_type, loss_scale=loss_scale, plot_on=plot_on, show_plot=show_plot)

