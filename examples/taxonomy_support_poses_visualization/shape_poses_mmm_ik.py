import gym
from pathlib import Path
import numpy as np
from robot_env import mjc_env
import mujoco

from robot_utils.math.mju_quaternion import mat2Quat
from robot_utils.py.utils import load_dataclass_from_dict
from robot_policy.classical.common.controller_base import CtrlMode
from robot_policy.classical.common.joint_space_controllers import JSPDControllerCfg, JSPositionController

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    get_reduced_dataset_2poses, get_reduced_dataset_feet_5poses, get_reduced_dataset_knees_2poses


if __name__ == '__main__':
    # Load mujoco environment and controller
    # env = gym.make('MMMNoFingerJointsNoemieWorldEnv-v0')
    env = gym.make('MMMFullEmptyWorldEnv-v0')
    env.reset()

    # Left arm controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_la = JSPositionController(env, cfg)
    ic(controller_la.get_jac_site_kc(controller_la.tcp_name))

    # Right arm controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_right_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_ra = JSPositionController(env, cfg)
    ic(controller_ra.get_jac_site_kc(controller_ra.tcp_name))

    # Left leg controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_leg",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_ll = JSPositionController(env, cfg)
    ic(controller_ll.get_jac_site_kc(controller_ll.tcp_name))

    # Right leg controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_right_leg",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_rl = JSPositionController(env, cfg)
    ic(controller_rl.get_jac_site_kc(controller_rl.tcp_name))

    # Controller list
    controllers = [controller_la, controller_ra, controller_ll, controller_rl]
    # controllers = [controller_la, controller_ra]
    # controllers = [controller_la]

    # Load data
    base_path = Path(__file__).parent.parent.parent.resolve()
    data_folder_path = base_path / "data/support_poses"
    data_path = str(data_folder_path / 'keyPoseShapesXPose.xml')  # Path of XML file including Shape poses dataset
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path, flag_take_new_ee_poses=False)
    # Reduced datasets
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)
    joint_ids, _, _, _, _, _ = controller_la.get_joint_ids(joint_names)

    # Display all poses
    poses = []
    for n in range(joint_data.shape[0]):
        env.data.qpos[joint_ids] = joint_data[n]
        pose = []
        for controller in controllers:
            mujoco.mj_step(controller.model, controller.data, nstep=1)
            targets = controller.get_joint_position()
            controller.get_root_status()
            # print(controller.root_xpos)
            # print(controller.root_xmat)
            xpos_t, xmat_t, _ = controller.forward_kinematics(targets, controller.tcp_name, mode="site")
            xmat_t = np.reshape(xmat_t, (3, 3))
            # print(xpos_t)
            # print(xmat_t)
            xpos_t_r, xmat_t_r = controller.pose_to_root_frame(xpos_t, xmat_t)
            controller_pose = np.hstack((xpos_t_r, mat2Quat(xmat_t_r)))
            pose = np.hstack((pose, controller_pose))
        poses.append(np.array(pose))
        env.render()

    poses = np.array(poses)
    print(poses)
    x_leftarm = poses[:, :7]
    x_rightarm = poses[:, 7:14]
    x_leftleg = poses[:, 14:21]
    x_rightleg = poses[:, 21:]
    print(x_leftarm)

    # np.savez('../../data/recomputed_end_effector_poses_new_mmm', x_leftarm=x_leftarm, x_rightarm=x_rightarm,
    #          x_leftleg=x_leftleg, x_rightleg=x_rightleg)

    # Define desired pose
    env.data.qpos[joint_ids] = joint_data[1]
    mujoco.mj_step(controller_la.model, controller_la.data, nstep=1)  # 1 step to actualize the data (any controller)
    des_pose = []
    des_rot = []
    targets = []
    for controller in controllers:
        mujoco.mj_step(controller.model, controller.data, nstep=1)
        targets = controller.get_joint_position()
        controller.get_root_status()
        xpos_t, xmat_t, _ = controller.forward_kinematics(targets, controller.tcp_name, mode="site")
        xmat_t = np.reshape(xmat_t, (3, 3))
        xpos_t_r, xmat_t_r = controller.pose_to_root_frame(xpos_t, xmat_t)
        des_pose.append(xpos_t_r)
    print(des_pose)

    for _ in range(100):
        env.render()

    # Set to another joint angle set
    env.data.qpos[joint_ids] = joint_data[0]
    mujoco.mj_step(controller_la.model, controller_la.data, nstep=1)  # 1 step to actualize the data (any controller)
    for _ in range(100):
        env.render()

    # Inverse kinematics
    cmd_joints = None
    tcp_from_ik_result = []
    for i in range(len(controllers)):
        controller = controllers[i]
        # des_xpos_t = des_pose[i]
        controller.get_root_status()
        des_xpos_t = controller.root_xmat.dot(des_pose[i]) + controller.root_xpos

        # IK
        controller.vis_point(des_xpos_t)
        joint_values = controller.inverse_kinematics(controller.tcp_name, des_xpos_t, max_steps=500, tol=1e-4)
        ic(joint_values, targets)
        xpos, _, _ = controller.forward_kinematics(joint_values, controller.tcp_name, mode="site")
        tcp_from_ik_result.append(xpos)

        cmd_, _ = controller.calculate(joint_values)
        if cmd_joints is None:
            cmd_joints = cmd_
        else:
            cmd_joints += cmd_

    for i in range(100000):
        env.step(cmd_joints)
        env.render()

        for j in range(len(controllers)):
            controller = controllers[j]
            controller.vis_point(des_pose[j])
            controller.vis_point(tcp_from_ik_result[j], rgba=np.array([0, 1, 1, 1]))
            xpos, _, _, _ = controller.get_site_status(controller.tcp_name)
            controller.vis_point(xpos, rgba=np.array([0, 0, 1, 1]))
            if i == 100:
                xpos_t, xmat_t, _ = controller.forward_kinematics(controller.get_joint_position(),
                                                                  controller.tcp_name, mode="site")
                controller.get_root_status()
                xmat_t = np.reshape(xmat_t, (3, 3))
                xpos_t_r, xmat_t_r = controller.pose_to_root_frame(xpos_t, xmat_t)
                print(xpos_t)
                print(des_pose[j])
