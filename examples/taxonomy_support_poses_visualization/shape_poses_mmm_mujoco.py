from pathlib import Path
import os
import time

import mujoco_py
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    get_reduced_dataset_2poses, get_reduced_dataset_feet_5poses, get_reduced_dataset_knees_2poses, get_semifull_dataset


def set_joint_values(sim, joint_name_list, q):
    """
    Set the robot joint values to the given desired values.
    """
    if len(q.shape) == 2:
        q = q[0, :]

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        states.qpos[addr] = q[joint_id]

    sim.set_state(states)
    sim.forward()


def compute_ts_fct(sim, joint_name_list, q, tcp_name):
    """
    Computes the end-effector pose using forward kinematics.

    Parameters
    ----------
    :param q: current joint angles

    Return
    ------
    :return: end-effector pose
    """
    if len(q.shape) == 2:
        q = q[0, :]

    set_joint_values(sim, joint_name_list, q)
    tcp_pos = sim.data.get_site_xpos(tcp_name)
    tcp_xmat = sim.data.get_site_xmat(tcp_name)
    tcp_quat = np.zeros(4)
    mujoco_py.functions.mju_mat2Quat(tcp_quat, np.reshape(tcp_xmat, (-1,), order='C'))
    x_fct = np.concatenate([tcp_pos, tcp_quat], axis=-1)
    return x_fct


if __name__ == '__main__':
    mj_path = mujoco_py.utils.discover_mujoco()

    base_path = Path(__file__).parent.parent.parent.resolve()
    data_folder_path = base_path / "data"
    xml_path = str(data_folder_path / 'mmm_mujoco/environment/mmm_bimanual_kitchen.xml')
    model = mujoco_py.load_model_from_path(xml_path)
    sim = mujoco_py.MjSim(model)
    viewer_show = True
    # Remark: the model is not scaled (i.e., normalized to 1m height)

    # Load data
    data_path = str(data_folder_path / 'support_poses/keyPoseShapesXPose.xml')  # Path of XML file including Shape poses dataset
    # training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    # Reduced datasets
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    training_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    # training_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)

    # Visualize each shape pose
    if viewer_show is True:
        viewer = mujoco_py.MjViewer(sim)
        viewer._paused = True

    # leftArm7Dof_name_list = ["LSx_joint", "LSz_joint", "LSy_joint", "LEx_joint", "LEz_joint", "LWx_joint", "LWy_joint"]
    # rightArm7Dof_name_list = ["RSx_joint", "RSz_joint", "RSy_joint", "REx_joint", "REz_joint", "RWx_joint", "RWy_joint"]

    tcp_leftarm_name = 'Hand L TCP'  # TODO check if it is root or palm, 'Hand L Root/Palm'
    tcp_rightarm_name = 'Hand R TCP'  # TODO check if it is root or palm, 'Hand R Root/Palm'
    tcp_leftleg_name = 'Foot L TCP'
    tcp_rightleg_name = 'Foot R TCP'

    x_leftarm = []
    x_rightarm = []
    x_leftleg = []
    x_rightleg = []

    for n in range(joint_data.shape[0]):
        print(n, shape_pose_names[n])
        set_joint_values(sim, joint_names, joint_data[n])

        x_leftarm.append(compute_ts_fct(sim, joint_names, joint_data[n], tcp_leftarm_name))
        x_rightarm.append(compute_ts_fct(sim, joint_names, joint_data[n], tcp_rightarm_name))
        x_leftleg.append(compute_ts_fct(sim, joint_names, joint_data[n], tcp_leftleg_name))
        x_rightleg.append(compute_ts_fct(sim, joint_names, joint_data[n], tcp_rightleg_name))
        print("Left arm ", x_leftarm[-1][:3])
        print("Right arm ", x_rightarm[-1][:3])
        print("Left leg ", x_leftleg[-1][:3])
        print("Right leg ", x_rightleg[-1][:3])

        sim.step()

        if viewer_show is True:
            viewer.render()

        # time.sleep(0.1)
        viewer._paused = True

    x_leftarm = np.array(x_leftarm)
    x_rightarm = np.array(x_rightarm)
    x_leftleg = np.array(x_leftleg)
    x_rightleg = np.array(x_rightleg)

    # np.savez('../../data/recomputed_end_effector_poses', x_leftarm=x_leftarm, x_rightarm=x_rightarm,
    #          x_leftleg=x_leftleg, x_rightleg=x_rightleg)
    print('End')
