import gym
import numpy as np
from robot_env import mjc_env

from robot_utils.math.mju_quaternion import mat2Quat
from robot_utils.py.utils import load_dataclass_from_dict
from robot_policy.classical.common.controller_base import CtrlMode
from robot_policy.classical.common.joint_space_controllers import JSPDControllerCfg, JSPositionController


if __name__ == '__main__':

    env = gym.make('MMMFullEmptyWorldEnv-v0')
    env.reset()

    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION

    controller = JSPositionController(env, cfg)
    ic(controller.get_jac_site_kc(controller.tcp_name))

    targets = controller.get_joint_position()
    targets[5] = np.pi / 2
    xpos_t, xmat_t, _ = controller.forward_kinematics(targets, controller.tcp_name, mode="site")
    controller.vis_point(xpos_t)
    joint_values = controller.inverse_kinematics(controller.tcp_name, xpos_t, mat2Quat(xmat_t))
    ic(joint_values, targets)

    for _ in range(100000):
        cmd_1, _ = controller.calculate(joint_values)
        # cmd_2, _ = controller2.calculate(joint_values)
        env.step(cmd_1)
        env.render()
        env
