import gym
from pathlib import Path
import numpy as np
from robot_env import mjc_env
import mujoco

from robot_utils.math.mju_quaternion import mat2Quat
from robot_utils.py.utils import load_dataclass_from_dict
from robot_policy.classical.common.controller_base import CtrlMode
from robot_policy.classical.common.joint_space_controllers import JSPDControllerCfg, JSPositionController


if __name__ == '__main__':
    # Load mujoco environment and controller
    # env = gym.make('MMMNoFingerJointsNoemieWorldEnv-v0')
    env = gym.make('MMMFullEmptyWorldEnv-v0')
    env.reset()

    # Left arm controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_la = JSPositionController(env, cfg)

    # Right arm controller
    cfg.kinematic_chain = "kc_mmm_right_arm"
    controller_ra = JSPositionController(env, cfg)

    # Left leg controller
    cfg.kinematic_chain = "kc_mmm_left_leg"
    controller_ll = JSPositionController(env, cfg)

    # Right leg controller
    cfg.kinematic_chain = "kc_mmm_right_leg"
    controller_rl = JSPositionController(env, cfg)

    # Controller list
    controllers = [ controller_la, controller_ra, controller_ll, controller_rl]
    # controllers = [controller_la, controller_ra]
    # controllers = [controller_la]

    # Load data
    joint_names =     ["BLNx_joint", "BLNy_joint", "BLNz_joint",
                       "BPx_joint", "BPy_joint", "BPz_joint",
                       "BTx_joint", "BTy_joint", "BTz_joint",
                       "BUNx_joint", "BUNy_joint", "BUNz_joint",
                       "LAx_joint", "LAy_joint", "LAz_joint",
                       "LEx_joint", "LEz_joint",
                       "LHx_joint", "LHy_joint", "LHz_joint",
                       "LKx_joint",
                       "LSx_joint", "LSy_joint", "LSz_joint",
                       "LWx_joint", "LWy_joint",
                       "LFx_joint",
                       "LMrot_joint",
                       "RAx_joint", "RAy_joint", "RAz_joint",
                       "REx_joint", "REz_joint",
                       "RHx_joint", "RHy_joint", "RHz_joint",
                       "RKx_joint",
                       "RSx_joint", "RSy_joint", "RSz_joint",
                       "RWx_joint", "RWy_joint",
                       "RFx_joint",
                       "RMrot_joint"
                       ]
    joint_ids, _, _, _, _, _ = controller_la.get_joint_ids(joint_names)

    # Inverse kinematics
    # Define desired pose
    joint_data = np.array([[-0.559862, 0.18204, 0.152003, 0.533841, 0.166357, 0.0220207, -0.610865, -0.191307,
                            -0.137069, 0.523599, -0.221019, -0.106918, 0.320132, -0.157544, -0.523599, 0.710769,
                            -0.369677, -0.0625798, 0.00351101, 0.401468, -0.264219, 0.0690993, 0.4028, -0.206401,
                            -0.52357, -0.573364, 0., 0., 0.197858, 0.216368, 0.0609553, 1.02813, 0.878952, 0.652896,
                            -0.0928789, -0.20969, -0.846828, 0.686476, -0.434464, 0.25898, -0.516762, 0.663575, 0.,
                            0.]])
    env.data.qpos[joint_ids] = joint_data
    mujoco.mj_step(controller_la.model, controller_la.data, nstep=1)  # 1 step to actualize the data (any controller)
    des_pose = []
    for controller in controllers:
        mujoco.mj_step(controller.model, controller.data, nstep=1)
        targets = controller.get_joint_position()
        controller.get_root_status()
        xpos_t, xmat_t, _ = controller.forward_kinematics(targets, controller.tcp_name, mode="site")
        xmat_t = np.reshape(xmat_t, (3, 3))
        des_pose.append(xpos_t)
    print(des_pose)

    for _ in range(100):
        env.render()

    # Inverse kinematics
    cmd_joints = None
    tcp_from_ik_result = []
    for i in range(len(controllers)):
        controller = controllers[i]
        des_xpos_t = des_pose[i]

        # IK
        controller.vis_point(des_xpos_t)
        joint_values = controller.inverse_kinematics(controller.tcp_name, des_xpos_t, max_steps=500, tol=1e-4)
        ic(joint_values, targets)
        xpos, _, _ = controller.forward_kinematics(joint_values, controller.tcp_name, mode="site")
        tcp_from_ik_result.append(xpos)

        cmd_, _ = controller.calculate(joint_values)
        if cmd_joints is None:
            cmd_joints = cmd_
        else:
            cmd_joints += cmd_

    for i in range(100000):
        env.step(cmd_joints)
        env.render()

        for j in range(len(controllers)):
            controller = controllers[j]
            controller.vis_point(des_pose[j])
            controller.vis_point(tcp_from_ik_result[j], rgba=np.array([0, 1, 1, 1]))
            xpos, _, _, _ = controller.get_site_status(controller.tcp_name)
            controller.vis_point(xpos, rgba=np.array([0, 0, 1, 1]))
            if i == 100:
                xpos_t, xmat_t, _ = controller.forward_kinematics(controller.get_joint_position(), controller.tcp_name, mode="site")
                controller.get_root_status()
                xmat_t = np.reshape(xmat_t, (3, 3))
                xpos_t_r, xmat_t_r = controller.pose_to_root_frame(xpos_t, xmat_t)
                print(xpos_t)
                print(des_pose[j])
