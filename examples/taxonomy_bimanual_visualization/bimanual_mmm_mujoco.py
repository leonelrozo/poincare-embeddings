from pathlib import Path
import os
import time

import mujoco_py
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import parse_xml_bimanual_manipulation_data, \
    get_bimanual_manipulation_dataset


def set_joint_values(sim, joint_name_list, q):
    """
    Set the robot joint values to the given desired values.
    """
    if len(q.shape) == 2:
        q = q[0, :]

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        states.qpos[addr] = q[joint_id]

    sim.set_state(states)
    sim.forward()


def get_joint_values(sim, joint_name_list):
    """
    Get the robot joint values.
    """
    q = []

    states = sim.get_state()
    for joint_id, joint in enumerate(joint_name_list):
        addr = sim.model.get_joint_qpos_addr(joint)
        q.append(states.qpos[addr])

    return q

def compute_ts_fct(sim, joint_name_list, q, tcp_name):
    """
    Computes the end-effector pose using forward kinematics.

    Parameters
    ----------
    :param q: current joint angles

    Return
    ------
    :return: end-effector pose
    """
    if len(q.shape) == 2:
        q = q[0, :]

    set_joint_values(sim, joint_name_list, q)
    tcp_pos = sim.data.get_site_xpos(tcp_name)
    tcp_xmat = sim.data.get_site_xmat(tcp_name)
    tcp_quat = np.zeros(4)
    mujoco_py.functions.mju_mat2Quat(tcp_quat, np.reshape(tcp_xmat, (-1,), order='C'))
    x_fct = np.concatenate([tcp_pos, tcp_quat], axis=-1)
    return x_fct


if __name__ == '__main__':
    mj_path = mujoco_py.utils.discover_mujoco()

    base_path = Path(__file__).parent.parent.parent.resolve()
    data_folder_path = base_path / "data"
    xml_path = str(data_folder_path / 'mmm_mujoco/environment/mmm_bimanual_kitchen.xml')
    model = mujoco_py.load_model_from_path(xml_path)
    sim = mujoco_py.MjSim(model)
    viewer_show = True
    # Remark: the model is not scaled (i.e., normalized to 1m height)

    # Load data
    # data_path = '../../data/bimanual_manipulation/1480/1480_Cut1_c_0_05cm_03.xml'
    # data_path = '../../data/bimanual_manipulation/1480/1480_Peel1_cucumber_cb_05.xml'
    # data_path = '../../data/bimanual_manipulation/1480/1480_Roll1_dough_start_04.xml'
    # data_path = '../../data/bimanual_manipulation/1480/1480_Stir1_cl_t_right_01.xml'
    # data_path = '../../data/bimanual_manipulation/1480/1480_Wipe1_pl_sp_front_angle_01.xml'

    # The motions above have more issues with joint angles of the fingers => Use the ones below instead.
    data_path = '../../data/bimanual_manipulation/1723/1723_Cut2_c_0_2cm_01.xml'  # Has issues with angles at the end
    # data_path = '../../data/bimanual_manipulation/1723/1723_Peel1_cucumber_cb_04.xml'
    # data_path = '../../data/bimanual_manipulation/1723/1723_Roll1_dough_start_05.xml'
    # data_path = '../../data/bimanual_manipulation/1723/1723_Stir1_bc_s_right_01.xml'
    # data_path = '../../data/bimanual_manipulation/1723/1723_Wipe1_pl_sp_front_angle_01.xml'

    joint_data, joint_names, annotations = parse_xml_bimanual_manipulation_data(data_path, mirror=True)

    data_path = '../../data/bimanual_manipulation'
    joint_data, annotations, joint_names = get_bimanual_manipulation_dataset(data_path)
    print(annotations)

    # Visualize each shape pose
    if viewer_show is True:
        viewer = mujoco_py.MjViewer(sim)
        viewer._paused = True

    # print(np.hstack((body_joint_data, hand_joint_data[0])))

    for n in range(joint_data.shape[0]):
        print(annotations[n])
        set_joint_values(sim, joint_names, joint_data[n])
        # set_joint_values(sim, ['LSCy_joint', 'LSCz_joint', 'RSCy_joint', 'RSCz_joint'], np.array([0.0, 0.0, 0.0, 0.0]))  # Added now in the data
        # print(get_joint_values(sim, ['LSCy_joint', 'LSCz_joint', 'RSCy_joint', 'RSCz_joint']))

        sim.step()

        if viewer_show is True:
            viewer.render()

        if n > 0:
            time.sleep(0.1)
            viewer._paused = True

    print('End')
