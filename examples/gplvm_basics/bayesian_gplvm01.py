import os
import numpy as np
import torch
import matplotlib.pylab as plt
import urllib.request
import tarfile

import gpytorch
from gpytorch.mlls import VariationalELBO
from gpytorch.priors import NormalPrior
from gpytorch.models.gplvm.latent_variable import VariationalLatentVariable, MAPLatentVariable, PointLatentVariable
from gpytorch.models.gplvm.bayesian_gplvm import BayesianGPLVM
from matplotlib import pyplot as plt
from tqdm import trange
from gpytorch.means import ZeroMean
from gpytorch.mlls import VariationalELBO
from gpytorch.priors import NormalPrior
from gpytorch.likelihoods import GaussianLikelihood
from gpytorch.variational import VariationalStrategy
from gpytorch.variational import CholeskyVariationalDistribution
from gpytorch.kernels import ScaleKernel, RBFKernel
from gpytorch.distributions import MultivariateNormal

# This file has been copied from the GPLVM notebook of GPyTorch:
# https://github.com/cornellius-gp/gpytorch/blob/master/examples/045_GPLVM/Gaussian_Process_Latent_Variable_Models_with_Stochastic_Variational_Inference.ipynb
# == Gaussian_Process_Latent_Variable_Models_with_Stochastic_Variational_Inference.ipynb in the gplvm_basics folder.


def _init_pca(Y, latent_dim):
    U, S, V = torch.pca_lowrank(Y, q=latent_dim)
    return torch.nn.Parameter(torch.matmul(Y, V[:, :latent_dim]))


class bGPLVM(BayesianGPLVM):
    def __init__(self, n, data_dim, latent_dim, n_inducing, pca=False):
        self.n = n
        self.batch_shape = torch.Size([data_dim])

        # Locations Z_{d} corresponding to u_{d}, they can be randomly initialized or
        # regularly placed with shape (D x n_inducing x latent_dim).
        self.inducing_inputs = torch.randn(data_dim, n_inducing, latent_dim)

        # Sparse Variational Formulation (inducing variables initialised as randn)
        q_u = CholeskyVariationalDistribution(n_inducing, batch_shape=self.batch_shape)
        q_f = VariationalStrategy(self, self.inducing_inputs, q_u, learn_inducing_locations=True)

        # Define prior for X
        X_prior_mean = torch.zeros(n, latent_dim)  # shape: N x Q
        prior_x = NormalPrior(X_prior_mean, torch.ones_like(X_prior_mean))

        # Initialise X with PCA or randn
        if pca == True:
            X_init = _init_pca(Y, latent_dim)  # Initialise X to PCA
        else:
            X_init = torch.nn.Parameter(torch.randn(n, latent_dim))

        # LatentVariable (c)
        X = VariationalLatentVariable(n, data_dim, latent_dim, X_init, prior_x)
        # For (a) or (b) change to below:
        # X = PointLatentVariable(n, latent_dim, X_init)
        # X = MAPLatentVariable(n, latent_dim, X_init, prior_x)

        super().__init__(X, q_f)

        # Kernel (acting on latent dimensions)
        self.mean_module = ZeroMean(ard_num_dims=latent_dim)
        self.covar_module = ScaleKernel(RBFKernel(ard_num_dims=latent_dim))

    def forward(self, X):
        mean_x = self.mean_module(X)
        covar_x = self.covar_module(X)
        dist = MultivariateNormal(mean_x, covar_x)
        return dist

    def _get_batch_idx(self, batch_size):
        valid_indices = np.arange(self.n)
        batch_indices = np.random.choice(valid_indices, size=batch_size, replace=False)
        return np.sort(batch_indices)


if __name__ == '__main__':
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Set up training data
    # We use the canonical multi-phase oilflow dataset used in Titsias & Lawrence, 2010 that consists of 1000,
    # 12 dimensional observations belonging to three known classes corresponding to different phases of oilflow.
    url = "http://staffwww.dcs.shef.ac.uk/people/N.Lawrence/resources/3PhData.tar.gz"
    urllib.request.urlretrieve(url, '3PhData.tar.gz')
    with tarfile.open('3PhData.tar.gz', 'r') as f:
        f.extract('DataTrn.txt')
        f.extract('DataTrnLbls.txt')

    Y = torch.Tensor(np.loadtxt(fname='DataTrn.txt'))
    labels = torch.Tensor(np.loadtxt(fname='DataTrnLbls.txt'))
    labels = (labels @ np.diag([1, 2, 3])).sum(axis=1)

    # Model parameters
    # While we need to specify the dimensionality of the latent variables at the outset, one of the advantages of the
    # Bayesian framework is that by using a ARD kernel we can prune dimensions corresponding to small inverse
    # lengthscales.
    N = len(Y)
    data_dim = Y.shape[1]
    latent_dim = data_dim
    n_inducing = 25
    pca = False

    # Model
    model = bGPLVM(N, data_dim, latent_dim, n_inducing, pca=pca)

    # Likelihood
    likelihood = GaussianLikelihood(batch_shape=model.batch_shape)

    # Declaring the objective to be optimised along with optimiser
    # (see models/latent_variable.py for how the additional loss terms are accounted for)
    mll = VariationalELBO(likelihood, model, num_data=len(Y))

    optimizer = torch.optim.Adam([
        {'params': model.parameters()},
        {'params': likelihood.parameters()}
    ], lr=0.01)

    # Training loop - optimises the objective wrt kernel hypers, variational params and inducing inputs using the
    # optimizer provided. We use mini-batch training for scalability where only a subset of the local variational
    # parameters are optimised in each iteration.
    loss_list = []
    iterator = trange(10000, leave=True)
    batch_size = 100
    for i in iterator:
        batch_index = model._get_batch_idx(batch_size)
        optimizer.zero_grad()
        sample = model.sample_latent_variable()  # a full sample returns latent x across all N
        sample_batch = sample[batch_index]
        # sample_batch = sample
        output_batch = model(sample_batch)
        loss = -mll(output_batch, Y[batch_index].T).sum()
        # loss = -mll(output_batch, Y.T).sum()
        loss_list.append(loss.item())
        iterator.set_description('Loss: ' + str(float(np.round(loss.item(), 2))) + ", iter no: " + str(i))
        loss.backward()
        optimizer.step()

    # Visualising a two dimensional slice of the latent space corresponding to the most dominant latent dimensions.
    inv_lengthscale = 1 / model.covar_module.base_kernel.lengthscale
    values, indices = torch.topk(model.covar_module.base_kernel.lengthscale, k=2, largest=False)

    l1 = indices.numpy().flatten()[0]
    l2 = indices.numpy().flatten()[1]

    plt.figure(figsize=(20, 8))
    colors = ['r', 'b', 'g']

    plt.subplot(131)
    X = model.X.q_mu.detach().numpy()
    std = torch.nn.functional.softplus(model.X.q_log_sigma).detach().numpy()
    plt.title('2d latent subspace corresponding to 3 phase oilflow', fontsize='small')
    plt.xlabel('Latent dim 1')
    plt.ylabel('Latent dim 2')

    # Select index of the smallest lengthscales by examining model.covar_module.base_kernel.lengthscales
    for i, label in enumerate(np.unique(labels)):
        X_i = X[labels == label]
        scale_i = std[labels == label]
        plt.scatter(X_i[:, l1], X_i[:, l2], c=[colors[i]], label=label)
        plt.errorbar(X_i[:, l1], X_i[:, l2], xerr=scale_i[:, l1], yerr=scale_i[:, l2], label=label, c=colors[i],
                     fmt='none')

    plt.subplot(132)
    plt.bar(np.arange(latent_dim), height=inv_lengthscale.detach().numpy().flatten())
    plt.title('Inverse Lengthscale with SE-ARD kernel', fontsize='small')

    plt.subplot(133)
    plt.plot(loss_list, label='batch_size=100')
    plt.title('Neg. ELBO Loss', fontsize='small')

    plt.show()

    print('End')
