import os
import numpy as np
import torch
from tqdm import trange
import matplotlib.pylab as plt
import urllib.request
import tarfile

from matplotlib import pyplot as plt
# from gpytorch.likelihoods import GaussianLikelihood
from gpytorch.mlls import ExactMarginalLogLikelihood

from HyperbolicEmbeddings.gplvm.gplvm_models import MapExactGPLVM, BackConstrainedExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_scipy, fit_gplvm_torch
from HyperbolicEmbeddings.gplvm.gplvm_initializations import pca_initialization


if __name__ == '__main__':
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Set up training data
    # We use the canonical multi-phase oilflow dataset used in Titsias & Lawrence, 2010 that consists of 1000,
    # 12 dimensional observations belonging to three known classes corresponding to different phases of oilflow.
    url = "http://staffwww.dcs.shef.ac.uk/people/N.Lawrence/resources/3PhData.tar.gz"
    urllib.request.urlretrieve(url, '3PhData.tar.gz')
    with tarfile.open('3PhData.tar.gz', 'r') as f:
        f.extract('DataTrn.txt')
        f.extract('DataTrnLbls.txt')

    Y = torch.Tensor(np.loadtxt(fname='DataTrn.txt'))
    labels = torch.Tensor(np.loadtxt(fname='DataTrnLbls.txt'))
    labels = (labels @ np.diag([1, 2, 3])).sum(axis=1)

    # Remove some data
    Y = Y[:200]
    labels = labels[:200]

    # Model parameters
    N = len(Y)
    data_dim = Y.shape[1]
    latent_dim = 2  #data_dim

    # Model
    X_init = pca_initialization(Y, latent_dim)
    model = MapExactGPLVM(Y, latent_dim, X_init=X_init, batch_params=False)
    # model = BackConstrainedExactGPLVM(Y, latent_dim, X_init=X_init, batch_params=False, class_idx=None, taxonomy_based_back_constraints=False)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    model.eval()
    init_error = torch.abs(model(model.train_inputs[0]).mean - Y.T)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    _, info_dict = fit_gplvm_scipy(mll, options={"maxiter": 500})  # Here we prefer the scipy optimizer as the MLL is exact
    # fit_gplvm_torch(mll, options={"maxiter": 500})
    mll.eval()

    final_error = torch.abs(model(model.train_inputs[0]).mean - Y.T)

    # Visualising a two dimensional slice of the latent space corresponding to the most dominant latent dimensions.
    # inv_lengthscale = 1 / model.covar_module.base_kernel.lengthscale
    # values, indices = torch.topk(model.covar_module.base_kernel.lengthscale, k=1, largest=False)
    #
    # l1 = indices.numpy().flatten()[0]
    # l2 = indices.numpy().flatten()[1]

    plt.figure(figsize=(8, 8))
    colors = ['r', 'b', 'g']

    X = model.X().detach().numpy()
    plt.title('2d latent subspace corresponding to 3 phase oilflow', fontsize='small')
    plt.xlabel('Latent dim 1')
    plt.ylabel('Latent dim 2')

    # Select index of the smallest lengthscales by examining model.covar_module.base_kernel.lengthscales
    for i, label in enumerate(np.unique(labels)):
        X_i = X[labels == label]
        plt.scatter(X_i[:, 0], X_i[:, 1], c=[colors[i]], label=label)

    plt.show()

    print('End')
