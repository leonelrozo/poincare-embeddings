import os
import numpy as np
import torch
import matplotlib.pylab as plt
import urllib.request
import tarfile

from matplotlib import pyplot as plt
from gpytorch.mlls import VariationalELBO
from gpytorch.likelihoods import GaussianLikelihood

# import botorch

from HyperbolicEmbeddings.gplvm.bayesian_gplvm_models import VariationalBayesianGPLVM, MapBayesianGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_scipy, fit_gplvm_torch
from HyperbolicEmbeddings.gplvm.gplvm_initializations import pca_initialization

# This file has been adapted from the GPLVM notebook of GPyTorch:
# https://github.com/cornellius-gp/gpytorch/blob/master/examples/045_GPLVM/Gaussian_Process_Latent_Variable_Models_with_Stochastic_Variational_Inference.ipynb
# == Gaussian_Process_Latent_Variable_Models_with_Stochastic_Variational_Inference.ipynb in the gplvm_basics folder.


if __name__ == '__main__':
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    # Set up training data
    # We use the canonical multi-phase oilflow dataset used in Titsias & Lawrence, 2010 that consists of 1000,
    # 12 dimensional observations belonging to three known classes corresponding to different phases of oilflow.
    url = "http://staffwww.dcs.shef.ac.uk/people/N.Lawrence/resources/3PhData.tar.gz"
    urllib.request.urlretrieve(url, '3PhData.tar.gz')
    with tarfile.open('3PhData.tar.gz', 'r') as f:
        f.extract('DataTrn.txt')
        f.extract('DataTrnLbls.txt')

    Y = torch.Tensor(np.loadtxt(fname='DataTrn.txt'))
    labels = torch.Tensor(np.loadtxt(fname='DataTrnLbls.txt'))
    labels = (labels @ np.diag([1, 2, 3])).sum(axis=1)

    # Model parameters
    # While we need to specify the dimensionality of the latent variables at the outset, one of the advantages of the
    # Bayesian framework is that by using a ARD kernel we can prune dimensions corresponding to small inverse
    # lengthscales.
    N = len(Y)
    data_dim = Y.shape[1]
    latent_dim = data_dim
    n_inducing = 25
    pca = False

    # Model
    X_init = pca_initialization(Y, latent_dim)
    model = VariationalBayesianGPLVM(Y, latent_dim, n_inducing, X_init=X_init)
    # model = MapBayesianGPLVM(Y, latent_dim, n_inducing, X_init=X_init)

    # Likelihood
    likelihood = GaussianLikelihood(batch_shape=model.batch_shape)

    # Declaring the objective to be optimised along with optimiser
    # (see models/latent_variable.py for how the additional loss terms are accounted for)
    mll = VariationalELBO(likelihood, model, num_data=len(Y))

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    fit_gplvm_torch(mll)  #, approx_mll=False)
    # fit_gplvm_scipy(mll)
    mll.eval()

    # Equivalently, we cann fit the model in a single batch using the botorch fit function with our optimizer.
    # This is advantageous in the sense that the botorch fit function will restart the optimization if an error occurs.
    # mll = botorch.fit_gpytorch_model(mll, optimizer=fit_gplvm_torch)

    # Optimization of the original example modified to handle the data in a single batch
    # optimizer = torch.optim.Adam([
    #     {'params': model.parameters()},
    #     {'params': likelihood.parameters()}
    # ], lr=0.01)
    #
    # # Training loop - optimises the objective wrt kernel hypers, variational params and inducing inputs using the
    # # optimizer provided. We use mini-batch training for scalability where only a subset of the local variational
    # # parameters are optimised in each iteration.
    # loss_list = []
    # iterator = trange(1000, leave=True)
    # # iterator = trange(10000, leave=True)
    # batch_size = 100
    # for i in iterator:
    #     batch_index = model._get_batch_idx(batch_size)
    #     optimizer.zero_grad()
    #     output_batch = model(model.train_inputs())
    #     loss = -mll(output_batch, Y.T).sum()
    #     loss_list.append(loss.item())
    #     iterator.set_description('Loss: ' + str(float(np.round(loss.item(), 2))) + ", iter no: " + str(i))
    #     loss.backward()
    #     optimizer.step()

    # Visualising a two dimensional slice of the latent space corresponding to the most dominant latent dimensions.
    inv_lengthscale = 1 / model.covar_module.base_kernel.lengthscale
    values, indices = torch.topk(model.covar_module.base_kernel.lengthscale, k=2, largest=False)

    l1 = indices.numpy().flatten()[0]
    l2 = indices.numpy().flatten()[1]

    plt.figure(figsize=(20, 8))
    colors = ['r', 'b', 'g']

    plt.subplot(121)
    X = model.X.q_mu.detach().numpy()
    std = torch.nn.functional.softplus(model.X.q_log_sigma).detach().numpy()
    plt.title('2d latent subspace corresponding to 3 phase oilflow', fontsize='small')
    plt.xlabel('Latent dim 1')
    plt.ylabel('Latent dim 2')

    # Select index of the smallest lengthscales by examining model.covar_module.base_kernel.lengthscales
    for i, label in enumerate(np.unique(labels)):
        X_i = X[labels == label]
        scale_i = std[labels == label]
        plt.scatter(X_i[:, l1], X_i[:, l2], c=[colors[i]], label=label)
        plt.errorbar(X_i[:, l1], X_i[:, l2], xerr=scale_i[:, l1], yerr=scale_i[:, l2], label=label, c=colors[i],
                     fmt='none')

    plt.subplot(122)
    plt.bar(np.arange(latent_dim), height=inv_lengthscale.detach().numpy().flatten())
    plt.title('Inverse Lengthscale (shared between dimensions)', fontsize='small')

    # plt.subplot(133)
    # plt.plot(loss_list, label='batch_size=100')
    # plt.title('Neg. ELBO Loss', fontsize='small')

    plt.show()

    print('End')
