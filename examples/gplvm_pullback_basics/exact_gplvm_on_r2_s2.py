"""
This script contains the implementation of the Exact Euclidean GPLVM
baseline for the toy experiment on R2xS2. More details about this
experiment can be found in Sec. 5.1 and in the wrapped GPLVM script
in this same folder.

We use a GPLVM where each dimension is modelled independently using
a GP. We assume that the ambient space is Euclidean and we pull back
the learned GPLVM just like [Tosi14]. One key difference is that
our model has slighly more parameters. Namely, we use a multi-task
kernel with 0 rank which translates to learning diagonal task covariances
instead of the identity.
(TODO: in an ideal world, we would use identity task covariances. This
can be achieved by using GPyTorch's implementation or RBFKernelGrad)

[Tosi14]: Metrics for Probabilistic Geometries
"""
from pathlib import Path
from typing import Tuple

import matplotlib.pyplot as plt
import torch

import gpytorch
from gpytorch.likelihoods import MultitaskGaussianLikelihood

from HyperbolicEmbeddings.wrapped_gplvm.exact_euclidean_gplvm import ExactEuclideanGPLVM

from utils.data.load_letter_manifolds import load_toy_example_data
from HyperbolicEmbeddings.wrapped_gplvm.utils.training.gplvm import train_exact_gplvm
# from utils.visualization.gplvm.latent_space import visualize_latent_space
# from utils.visualization.gplvm.r2_s2 import visualize_wrapped_gplvm_on_r2_s2
from utils.wrappers.stochman_manifolds import GPLVMManifoldWrapper
from utils.projections.latent_space import project_to_latent_space

ROOT_DIR = Path(__file__).parent.parent.parent.parent.resolve()


def load_exact_gplvm_on_toy_experiment() -> Tuple[
    ExactEuclideanGPLVM, torch.Tensor, torch.Tensor
]:
    """
    Returns an Exact Euclidean GPLVM alongside the training and test
    data in a deterministic way. This way, we have a reliable way of
    instantiating the model (keeping the same hyperparameters and
    train-test split.)

    Returns
    -------

    - exact_gplvm (ExactEuclideanGPLVM): an Exact Euclidean GPLVM
      instantiated on the toy experiment dataset.

    - training_data (torch.Tensor): an 80% sample of the original
      dataset, used for training.

    - test_data (torch.Tensor): the remaining 20% of the original
      dataset, used for testing.
    """
    # Hardcoding some hyperparameters

    # Dim. of latent space (we have only tried 2)
    latent_dim = 2

    # Whether to split into train/test, and test percentage
    split_into_train_test = True
    test_percentage = 0.2

    # A subsample rate, to consider less data. (1 == all dataset)
    subsample_rate = 1

    # Noise that is synthetically added to the data.
    noise_level_in_data = 0.0

    # Random state to have the train/test split be deterministic.
    random_state_for_split = 420

    # Prior for likelihood noise
    noise_prior = None

    # Loading up the training/testing data
    toy_example_training_data, toy_example_test_data = load_toy_example_data(
        noise_level=noise_level_in_data,
        split_into_train_test=split_into_train_test,
        random_state_for_split=random_state_for_split,
        test_percentage=test_percentage,
    )

    # Subsampling the training data (to have a smaller dataset)
    toy_example_training_data = toy_example_training_data[::subsample_rate]
    toy_example_test_data = toy_example_test_data[::subsample_rate]

    # Defining the likelihood.
    ambient_dim = toy_example_training_data.shape[-1]
    likelihood = MultitaskGaussianLikelihood(
        num_tasks=ambient_dim,
        noise_constraint=gpytorch.constraints.GreaterThan(1e-8),
        noise_prior=noise_prior,
    )

    # Creating the exact Euclidean model
    exact_gplvm_on_r2_s2 = ExactEuclideanGPLVM(
        latent_dim, toy_example_training_data, likelihood, initialization="pca"
    )

    return exact_gplvm_on_r2_s2, toy_example_training_data, toy_example_test_data


def toy_experiment_using_exact_gplvm():
    """
    Fits an exact Euclidean GPLVM to the toy experiment dataset (which
    is composed of letter data from the LASA dataset) in R2 x S2.
    """
    # Training hyperparameters:
    # TODO: if we want to be even fairer, we would tweak the
    # priors of the kernel on this one too. We might want to
    # expose the kernel then.
    exp_name = "exact_gplvm_on_toy_experiment"
    max_iterations = 200
    learning_rate = 0.1

    # Visualization hyperparameters
    uncertainty_and_volume_grid_size = 50
    discretized_grid_size = 10
    projection_grid_size = 75

    # Load the model
    exact_gplvm_on_r2_s2, _, test_data = load_exact_gplvm_on_toy_experiment()

    # Train it
    exact_gplvm_on_r2_s2 = train_exact_gplvm(
        exact_gplvm_on_r2_s2,
        verbose=True,
        save_as=exp_name,
        max_iterations=max_iterations,
        learning_rate=learning_rate,
    )
    exact_gplvm_on_r2_s2.eval()

    # Visualize it
    fig = plt.figure(figsize=(7 * 4, 7))
    ax_for_latent = fig.add_subplot(141)
    ax_for_volume = fig.add_subplot(142)
    ax_for_r2 = fig.add_subplot(143)
    ax_for_s2 = fig.add_subplot(144, projection="3d")

    print("Visualizing latent space")
    visualize_latent_space(
        ax_for_latent,
        exact_gplvm_on_r2_s2,
        grid_size_in_latent_space=uncertainty_and_volume_grid_size,
    )
    limits = visualize_latent_space(
        ax_for_volume,
        exact_gplvm_on_r2_s2,
        grid_size_in_latent_space=uncertainty_and_volume_grid_size,
        plot_uncertainty=False,
        plot_volume=True,
        return_limits=True,
    )

    # Projecting test data onto the latent space:
    # and plotting the projections
    print("Projecting the tested data unto latent space")
    projected_test_data = project_to_latent_space(
        test_data,
        exact_gplvm_on_r2_s2,
        limits=limits,
        grid_size_in_latent_space=projection_grid_size,
    )
    for ax_ in [ax_for_latent, ax_for_latent]:
        ax_.scatter(
            projected_test_data[:, 0].detach().numpy(),
            projected_test_data[:, 1].detach().numpy(),
            c="r",
            marker="d",
            alpha=0.1,
        )

    # Compute geodesics
    print("Computing geodesics")
    egplvm_as_manifold = GPLVMManifoldWrapper(
        exact_gplvm_on_r2_s2,
        [
            torch.linspace(*limits, discretized_grid_size),
            torch.linspace(*limits, discretized_grid_size),
        ],
    )
    egplvm_as_manifold.fit()
    geodesic, _ = egplvm_as_manifold.connecting_geodesic(
        exact_gplvm_on_r2_s2.latent_variable.X[0],
        exact_gplvm_on_r2_s2.latent_variable.X[-1],
    )
    time = torch.linspace(0, 1, 50)

    # Visualize them in latent space.
    geodesic_points = geodesic(time).detach().numpy()
    ax_for_latent.plot(geodesic_points[:, 0], geodesic_points[:, 1], "--r")
    ax_for_volume.plot(geodesic_points[:, 0], geodesic_points[:, 1], "--r")

    # Plot the predictions and geodesics on the manifolds themselves.
    print("Plotting predictions on the manifold")
    visualize_wrapped_gplvm_on_r2_s2(
        [ax_for_r2, ax_for_s2],
        exact_gplvm_on_r2_s2,
        plot_basepoint_function=False,
        plot_tangent_vectors=False,
        geodesic=geodesic(time),
    )

    # Clean up the axis labels.
    for ax in [ax_for_latent, ax_for_volume, ax_for_r2, ax_for_s2]:
        ax.axis("off")

    plt.tight_layout()

    fig.savefig(ROOT_DIR / "plots" / f"{exp_name}.png")

    plt.show()
    plt.close()


if __name__ == "__main__":
    toy_experiment_using_exact_gplvm()
