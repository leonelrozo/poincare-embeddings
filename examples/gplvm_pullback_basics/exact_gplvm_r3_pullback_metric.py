import os
import numpy as np
import torch
import pickle
from matplotlib import pyplot as plt

from HyperbolicEmbeddings.gplvm.gplvm_models import MapExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.gplvm.gplvm_optimization import fit_gplvm_scipy, fit_gplvm_torch
from HyperbolicEmbeddings.gplvm.gplvm_initializations import pca_initialization
from HyperbolicEmbeddings.gplvm.gplvm_predict_pullback_metric import get_pullback_metric_function
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_gplvm_pullback_metric
from HyperbolicEmbeddings.utils.normalization import centering
from HyperbolicEmbeddings.utils.geodesics import get_geodesic_on_learned_manifold, \
    get_geodesic_on_discrete_learned_manifold
from HyperbolicEmbeddings.gplvm.gplvm_stochman_wrapper import DiscreteGPLVMManifoldWrapper, GPLVMManifoldWrapper

if __name__ == '__main__':
    torch.set_default_tensor_type('torch.DoubleTensor')
    # Setting manual seed for reproducibility
    torch.manual_seed(73)
    np.random.seed(73)

    plot_training = True

    # Set up training data
    # Load circular data in R^3
    CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
    DATA_PATH = os.path.join(CURRENT_DIR, '../../data/circular_3d_trajectories/')
    # List all .p files in the directory
    file_list = [f for f in os.listdir(DATA_PATH) if f.endswith('.p')]

    data_counter = False
    for file_name in file_list:  # Loop through each .p file and load its contents
        file_path = os.path.join(DATA_PATH, file_name)

        with open(file_path, 'rb') as file:
            data = pickle.load(file)
            if not data_counter:
                loaded_data = torch.tensor(data)
                data_counter = True
            else:
                loaded_data = torch.cat((loaded_data, torch.Tensor(data)))

    Y = loaded_data
    # Center
    Y, _ = centering(Y)
    # Remove some data
    Y = Y[1::1] * 100.  # The more datapoints were used, the better the pullback metric behaved

    # Plot training data
    if plot_training:
        ax = plt.figure().add_subplot(projection='3d')
        ax.scatter(Y[:, 0], Y[:, 1], Y[:, 2])
        plt.show()

    # Model parameters
    N = len(Y)
    data_dim = Y.shape[1]
    latent_dim = 2  # data_dim
    pca = True

    # Model
    if pca:
        X_init = pca_initialization(Y, latent_dim)  # Initialise X to PCA
        model = MapExactGPLVM(Y, latent_dim, X_init, batch_params=False)
    else:
        model = MapExactGPLVM(Y, latent_dim)

    # !!! IMPORTANT !!! Initialize the noise low: If the likelihood noise is too high, it influences badly the metric!
    model.likelihood.noise = 0.0040

    # Plot initialization
    X = model.X.X.detach().numpy()
    plt.figure(figsize=(8, 8))
    plt.title('2d latent subspace', fontsize='small')
    plt.xlabel('Latent dim 1')
    plt.ylabel('Latent dim 2')
    plt.scatter(X[:, 0], X[:, 1])
    plt.show()

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    model.eval()
    init_error = torch.abs(model(model.train_inputs[0]).mean - Y.T)

    # Train the model in a single batch with automatic convergence checks
    mll.train()
    # _, info_dict = fit_gplvm_scipy(mll, options={"maxiter": 1500})  # Here we prefer the scipy optimizer as the MLL is exact
    fit_gplvm_torch(mll, options={"maxiter": 1000})
    mll.eval()
    final_error = torch.abs(model(model.train_inputs[0]).mean - Y.T)
    X = model.X.X.detach()

    # Compute pullback metric
    continuous_geodesic = True
    pullback_metric_function = get_pullback_metric_function(X, Y, model.likelihood, model.covar_module)
    n_eval_points = 50
    geodesic_init = torch.tensor([-0.2, -6.5])
    geodesic_final = torch.tensor([1.0, 6.5])
    # Geodesic initialization with a straight line
    geodesic_time = torch.linspace(0, 1, n_eval_points)[:, None]
    initial_geodesic = geodesic_init * (1 - geodesic_time) + geodesic_final * geodesic_time
    # Computes geodesic path
    if continuous_geodesic:
        geodesic_path = get_geodesic_on_learned_manifold(initial_geodesic, pullback_metric_function,
                                                         n_eval=n_eval_points, num_nodes_to_optimize=n_eval_points)
        manifold = GPLVMManifoldWrapper(pullback_metric_function)
        manifold.curve_length(geodesic_path)
    else:
        geodesic_path = get_geodesic_on_discrete_learned_manifold(initial_geodesic, X, pullback_metric_function,
                                                                  n_eval=n_eval_points, grid_size=256,
                                                                  num_nodes_to_optimize=50)
    plot_gplvm_pullback_metric(X, Y, model.likelihood, model.covar_module, geodesics=geodesic_path, grid_size=512)

    plt.figure(figsize=(8, 8))
    plt.title('2d latent subspace', fontsize='small')
    plt.xlabel('Latent dim 1')
    plt.ylabel('Latent dim 2')
    plt.scatter(X[:, 0], X[:, 1])

    plt.show()

    print('End')
