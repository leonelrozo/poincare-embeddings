# Hyperbolic embeddings

This repository contains the source code to perform hyperbolic embeddings on taxonomies of whole-body human poses.

# Installation 
To install this repository, first clone the repository and install the related packages, as explained below.

```
pip install -r requirements.txt
```
If you want to use the facebook_poincare functions (from Nickel & Kiela, ICML'17, https://github.com/facebookresearch/poincare-embeddings), from the poincare-embeddings folder, run
```
pip install -e facebook_poincare
```
