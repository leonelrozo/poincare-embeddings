import numpy as np

from robot_utils.py.utils import load_dataclass_from_dict
from robot_policy.classical.common.controller_base import CtrlMode
from robot_policy.classical.common.joint_space_controllers import JSPDControllerCfg, JSPositionController


def create_controllers(env):
    # Left arm controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_la = JSPositionController(env, cfg)
    ic(controller_la.get_jac_site_kc(controller_la.tcp_name))

    # Right arm controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_right_arm",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_ra = JSPositionController(env, cfg)
    ic(controller_ra.get_jac_site_kc(controller_ra.tcp_name))

    # Left leg controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_left_leg",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_ll = JSPositionController(env, cfg)
    ic(controller_ll.get_jac_site_kc(controller_ll.tcp_name))

    # Right leg controller
    cfg = load_dataclass_from_dict(JSPDControllerCfg, dict(
        name="min_jerk",
        robot_name="mmm",
        kinematic_chain="kc_mmm_right_leg",
    ))
    cfg.ctrl_mode = CtrlMode.POSITION
    controller_rl = JSPositionController(env, cfg)
    ic(controller_rl.get_jac_site_kc(controller_rl.tcp_name))

    # Controller list
    return [controller_ll, controller_rl, controller_la, controller_ra]
    # controllers = [controller_la, controller_ra]
    # controllers = [controller_la]


def inverse_kinematics(controllers, desired_positions):
    # Inverse kinematics
    cmd_joints = None
    tcp_from_ik_result = []
    for i in range(len(controllers)):
        controller = controllers[i]
        # des_xpos_t = des_pose[i]
        controller.get_root_status()
        des_xpos_t = controller.root_xmat.dot(desired_positions[i]) + controller.root_xpos

        # IK
        controller.vis_point(des_xpos_t)
        joint_values = controller.inverse_kinematics(controller.tcp_name, des_xpos_t, max_steps=500, tol=1e-4)
        xpos, _, _ = controller.forward_kinematics(joint_values, controller.tcp_name, mode="site")
        tcp_from_ik_result.append(xpos)

        cmd_, _ = controller.calculate(joint_values)
        if cmd_joints is None:
            cmd_joints = cmd_
        else:
            cmd_joints += cmd_

    return cmd_joints, tcp_from_ik_result
