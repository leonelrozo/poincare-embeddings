import os
import numpy as np
from facebook_poincare.hype.graph import load_edge_list

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))


def adjacency_from_edges(edges_list, nb_edges, weights=None, symmetrize=False):
    adjacency = np.zeros((nb_edges, nb_edges))
    if weights is None:
        weights = np.ones(nb_edges)

    idx = 0
    for edge in edges_list:
        adjacency[edge[0], edge[1]] = weights[idx]
        if symmetrize:
            adjacency[edge[1], edge[0]] = weights[idx]
        idx += 1

    return adjacency


def graph_distance_matrix_from_adjacency(adjacency):
    distances = np.empty_like(adjacency)
    distances[:] = np.NaN
    # Diagonal has 0 distances
    distances[np.diag_indices(distances.shape[0])] = 0.

    adjacency_pow = np.eye(adjacency.shape[0])
    degree = 0
    while np.isnan(np.sum(distances)):
        adjacency_pow = np.dot(adjacency_pow, adjacency)
        degree += 1
        distances[np.isnan(distances) & (adjacency_pow > 0)] = degree

    return distances


if __name__ == '__main__':
    dset = os.path.join(CURRENT_DIR, '../../examples/embeddings_support_poses_taxonomy/support_poses_closure.csv')
    idx, objects, weights = load_edge_list(dset, symmetrize=True)

    adjacency = adjacency_from_edges(edges_list=idx, nb_edges=len(objects), weights=weights, symmetrize=True)
    distances = graph_distance_matrix_from_adjacency(adjacency)

    print(adjacency)
    print(distances)

