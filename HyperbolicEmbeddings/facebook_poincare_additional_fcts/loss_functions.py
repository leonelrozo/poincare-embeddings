import numpy as np
import torch
from facebook_poincare.hype.energy_function import EnergyFunction


class DistorsionEnergyFunction(EnergyFunction):
    def __init__(self, manifold, dim, size, distance_matrix, sparse=False, **kwargs):
        super().__init__(manifold, dim, size, sparse, **kwargs)
        self.distance_matrix = distance_matrix

    def forward(self, inputs):
        e = self.lt(inputs)
        with torch.no_grad():
            e = self.manifold.normalize(e)
        o = e.narrow(1, 1, e.size(1) - 1)
        s = e.narrow(1, 0, 1).expand_as(o)
        # TODO add a scalar parameter to weight one of the distance? -> seems ok...
        # The embedding is perfect if dM(x,y) = a dG(x,y) for all x, y
        return self.energy(s, o).squeeze(-1) / self.graph_distance(inputs)

    def energy(self, s, o):
        return self.manifold.distance(s, o)

    def graph_distance(self, inputs):
        distances = torch.zeros((inputs.shape[0], inputs.shape[1]-1))
        for i in range(inputs.shape[1]-1):
            distances[:, i] = torch.from_numpy(self.distance_matrix[list(inputs[:, [0, i+1]].T.cpu().numpy())])
        return distances.to(inputs.device)

    def loss(self, inp, target, **kwargs):
        """
        :param inp: batchsize x (1 + neg)
        """
        return torch.sum(torch.abs(inp**2 - 1.0)) / (inp.shape[0] * inp.shape[1])

# TODO test stress energy function \sum (dG(x, y) - dM(x, y))^2
