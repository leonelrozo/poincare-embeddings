import torch
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import sample_2d_grid
from HyperbolicEmbeddings.gplvm.gplvm_stochman_wrapper import DiscreteGPLVMManifoldWrapper, GPLVMManifoldWrapper
from stochman.curves import CubicSpline
from stochman.geodesic import geodesic_minimizing_energy


def get_geodesic_on_discrete_learned_manifold(x_new_init: torch.Tensor, x: torch.Tensor, metric_function, n_eval=20,
                                              grid_size=32, num_nodes_to_optimize=20) -> torch.Tensor:
    _, limits = sample_2d_grid(x, grid_size)
    discrete_manifold = DiscreteGPLVMManifoldWrapper([torch.linspace(*limits, grid_size),
                                                      torch.linspace(*limits, grid_size), ], metric_function)
    discrete_manifold.fit()
    geodesic, _ = discrete_manifold.connecting_geodesic(x_new_init[0], x_new_init[-1],
                                                        CubicSpline(x_new_init[0], x_new_init[-1],
                                                                    num_nodes=num_nodes_to_optimize))
    time = torch.linspace(0, 1, n_eval)
    return geodesic(time).detach()


def get_geodesic_on_learned_manifold(x_new_init: torch.Tensor, metric_function, n_eval=20, num_nodes_to_optimize=20) \
        -> torch.Tensor:
    manifold = GPLVMManifoldWrapper(metric_function)
    # start, end = x_new_init[0].unsqueeze(0), x_new_init[-1].unsqueeze(0)
    geodesic = CubicSpline(x_new_init[0], x_new_init[-1], num_nodes=num_nodes_to_optimize)
    # geodesic = CubicSpline(start, end)
    geodesic_minimizing_energy(geodesic, manifold, max_iter=500)
    time = torch.linspace(0, 1, n_eval)
    return geodesic(time).detach()
