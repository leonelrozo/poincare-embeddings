import torch
import gpytorch
import numpy as np
from gpytorch.distributions import MultivariateNormal
from linear_operator.operators import MatmulLinearOperator
from linear_operator import to_dense
from functools import lru_cache
from dataclasses import dataclass


@lru_cache()
def get_train_train_inverse_covariance(x: torch.Tensor, likelihood: gpytorch.likelihoods.Likelihood,
                                       kernel_module: gpytorch.kernels.Kernel):
    gp_prior = MultivariateNormal(torch.zeros(x.shape[0]), kernel_module(x))
    train_train_covar = likelihood(gp_prior).lazy_covariance_matrix
    with gpytorch.settings.max_root_decomposition_size(3000):
        train_train_covar_inv_root = to_dense(train_train_covar.root_inv_decomposition().root)
    return MatmulLinearOperator(train_train_covar_inv_root, train_train_covar_inv_root.transpose(-1, -2))


@dataclass
class Prediction:
    mean: np.ndarray    # np.shape[M, D_y]
    stddev: np.ndarray  # np.shape[M, D_y]

    def get_confidence_region(self, dim: int = None) -> tuple[np.ndarray, np.ndarray]:
        if dim != None:
            return self.mean[:, dim] - 2*self.stddev[:, dim], self.mean[:, dim] + 2*self.stddev[:, dim]
        return self.mean - 2*self.stddev, self.mean + 2*self.stddev

    def __getitem__(self, index: int):
        return Prediction(self.mean[index], self.stddev[index])
