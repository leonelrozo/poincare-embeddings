"""
[DEPRECATED]
This script contains examples on how to load
and inspect a model's embeddings.
"""
from pathlib import Path
import os

import torch

import gpytorch

from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import HyperbolicExactGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_one_of_each_class_dataset, get_semifull_dataset, shape_poses_graph_distance_mapping, \
    color_function_shape_poses, text_function_shape_poses

from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.resolve()


def load_dataset(dataset: str):
    # Load data
    data_path = ROOT_DIR / "data/support_poses" / "keyPoseShapesXPose.xml"
    
    if dataset == 'full':
        print("Dataset = Full")
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset == 'reduced':
        print("Dataset = reduced")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset == 'feet5':
        print("Dataset = feet5")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset == 'knees2':
        print("Dataset = knees2")
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)
    elif dataset == 'oneofeach':
        print("Dataset = oneofeach")
        pose_data, joint_data, shape_pose_names, joint_names = get_one_of_each_class_dataset(data_path)

    return pose_data, joint_data, shape_pose_names, joint_names

def load_hyperbolic_mll_model(model_name: str) -> gpytorch.ExactMarginalLogLikelihood:
    """
    Returns the mll object that was trained and saved
    under final_models/{model_name}.
    """
    model_hyperparams = model_name.split("_")
    
    dataset = model_hyperparams[4]
    latent_dim = int(model_hyperparams[3][-1])
    loss_type = model_hyperparams[5]
    loss_scale = float(model_hyperparams[6])

    pose_data, _, shape_pose_names, _ = load_dataset(dataset)
    training_data = torch.from_numpy(pose_data)

    graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names)

    model = HyperbolicExactGPLVM(training_data, latent_dim, pca=True, map_latent_variable=True,
                                 kernel_lengthscale_prior=None,
                                 kernel_outputscale_prior=None)

    if loss_type == 'Zero' or None:
        print("Loss_type = Zero")
        added_loss = ZeroAddedLossTermExactMLL()
    elif loss_type == 'Stress':
        print("Loss_type = Stress")
        added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)  #loss_scale=0.1)
    elif loss_type == 'Distortion':
        print("Loss_type = Distortion")
        added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)  #, distance_regularizer=10.0)  # loss_scale=0.05)

    model.add_loss_term(added_loss)

    mll = gpytorch.ExactMarginalLogLikelihood(model.likelihood, model)
    mll.load_state_dict(torch.load(ROOT_DIR / "final_models" / model_name))
    
    return mll


def load_hyperbolic_model(model_name: str) -> HyperbolicExactGPLVM:
    mll = load_hyperbolic_mll_model(model_name)
    return mll.model


if __name__ == "__main__":
    model_name = "hyperbolic_egplvm_prior_dim3_semifull_Zero_0.0_seed_73"
    model = load_hyperbolic_model(model_name)

    _, _, shape_pose_names, _ = load_dataset("semifull")
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    sample = model.X.X
    sample = lorentz_to_poincare(sample).detach().numpy()
    sample_colors = [color_function_shape_poses(pose_name) for pose_name in shape_pose_names]

    plot_hyperbolic_gplvm_3d(sample, sample_colors, shape_poses_legend_for_plot)
