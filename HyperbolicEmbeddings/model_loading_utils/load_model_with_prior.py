"""
A small util for automatically loading trained
back-constraints models, both Euclidean and Hyperbolic
"""
from typing import Tuple, Union
from pathlib import Path

import torch
import numpy as np

from gpytorch.mlls import ExactMarginalLogLikelihood
from gpytorch.kernels import ScaleKernel, RBFKernel

from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import HyperbolicExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_models import ExactGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch
from HyperbolicEmbeddings.kernels.kernels_graph import GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import HyperbolicStressLossTermExactMLL, ZeroAddedLossTermExactMLL,\
    HyperbolicDistortionLossTermExactMLL, VanillaHyperbolicDistortionLossTermExactMLL, \
    EuclideanStressLossTermExactMLL, EuclideanDistortionLossTermExactMLL
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_3d
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import color_function_shape_poses, parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, \
    get_reduced_dataset_feet_5poses, get_semifull_dataset, get_indices_shape_poses_graph, text_function_shape_poses, \
    NAME_CLASS_MAPPING
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import get_indices_grasps_graph, \
    hand_grasps_distance_mapping_from_adjacency

from HyperbolicEmbeddings.model_loading_utils.data_loading import load_training_data_suppport_poses, \
    load_training_data_grasps

ExactGPLVMModel = Union[HyperbolicExactGPLVM, ExactGPLVM]


def load_model_with_prior_suppport_poses(
    model_path: Path,
    training_data: torch.Tensor = None,
    shape_pose_names: torch.Tensor = None
) -> ExactGPLVMModel:
    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    DATA_PATH = ROOT_DIR / "data/support_poses"
    model_name = model_path.name

    # Loading the training data and so on if its none:
    if training_data is None or shape_pose_names is None:
        if 'joints' in model_name:
            training_data_, shape_pose_names_ = load_training_data_suppport_poses(model_name.replace('augmented_joints_', '').split("_")[4], pose_training=False)
        else:
            training_data_, shape_pose_names_ = load_training_data_suppport_poses(model_name.split("_")[4])

    # Override Nones with the loaded ones.
    training_data = training_data or training_data_
    shape_pose_names = shape_pose_names or shape_pose_names_

    # Parsing the name to get latent dim and so on.
    ## Getting the latent dim
    if "dim2" in model_name:
        latent_dim = 2
    elif "dim3" in model_name:
        latent_dim = 3
    else:
        raise ValueError(f"Expected 'dim2' or 'dim3' to be in {model_name}")

    ## Getting the loss scale
    if "Zero" in model_name:
        loss_scale = 0.0
    elif "Stress" in model_name:
        loss_scale = model_name.replace('augmented_joints_', '').split("_")[5].replace("Stress", "")
        loss_scale = float(loss_scale)
    elif "VanillaDistortion" in model_name:
        loss_scale = model_name.replace('augmented_joints_', '').split("_")[5].replace("VanillaDistortion", "")
        loss_scale = float(loss_scale)
    elif "Distortion" in model_name:
        loss_scale = model_name.replace('augmented_joints_', '').split("_")[5].replace("Distortion", "")
        loss_scale = float(loss_scale)
    else:
        raise ValueError(f"Weird input: couldn't infer prior from {model_name}")

    # Getting adjaceny matrix and the such
    graph_file_path = DATA_PATH / 'support_poses_closure.csv'
    adjacency_matrix, shape_poses_indices = get_indices_shape_poses_graph(graph_file_path, shape_pose_names)
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names)

    shape_poses_indices = torch.from_numpy(shape_poses_indices)

    # Model
    if "hyperbolic" in model_name:
        model = HyperbolicExactGPLVM(training_data, latent_dim, pca=True, map_latent_variable=True)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        if 'Zero' in model_name:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
            added_loss.graph_distances = shape_pose_graph_distances  # Needed to load the graph distance when computing stress
        elif 'Stress' in model_name:
            print("Loss_type = Stress")
            added_loss = HyperbolicStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        elif 'VanillaDistortion' in model_name:
            print("Loss_type = VanillaDistortion")
            added_loss = VanillaHyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        elif 'Distortion' in model_name:
            print("Loss_type = Distortion")
            added_loss = HyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        model.add_loss_term(added_loss)

    else:
        model = ExactGPLVM(training_data, latent_dim, pca=True, map_latent_variable=True)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        if 'Zero' in model_name:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
            added_loss.graph_distances = shape_pose_graph_distances  # Needed to load the graph distance when computing stress
        elif 'Stress' in model_name:
            print("Loss_type = Stress")
            added_loss = EuclideanStressLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        elif 'VanillaDistortion' in model_name:
            print("Loss_type = VanillaDistortion")
            NotImplementedError()
            # added_loss = VanillaHyperbolicDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        elif 'Distortion' in model_name:
            print("Loss_type = Distortion")
            added_loss = EuclideanDistortionLossTermExactMLL(shape_pose_graph_distances, loss_scale)
        model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = ExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    mll.load_state_dict(torch.load(model_path))  # load the model from file
    mll.eval()
    mll.model.eval()

    return mll.model


def load_model_with_prior_grasps(
    model_path: Path,
    training_data: torch.Tensor = None,
    shape_pose_names: torch.Tensor = None
) -> ExactGPLVMModel:
    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    DATA_PATH = ROOT_DIR / "data/hand_grasps"
    model_name = model_path.name

    # Loading the training data and so on if its none:
    if training_data is None or shape_pose_names is None:
        training_data_, grasp_names_ = load_training_data_grasps()

        # Override Nones with the loaded ones.
        training_data = training_data_
        grasp_names = grasp_names_ 

    # Parsing the name to get latent dim and so on.
    ## Getting the latent dim
    if "dim2" in model_name:
        latent_dim = 2
    elif "dim3" in model_name:
        latent_dim = 3
    else:
        raise ValueError(f"Expected 'dim2' or 'dim3' to be in {model_name}")

    ## Getting the loss scale
    if "Zero" in model_name:
        loss_scale = 0.0
    elif "Stress" in model_name:
        loss_scale = model_name.split("_")[6].replace("Stress", "")
        loss_scale = float(loss_scale)
    elif "VanillaDistortion" in model_name:
        loss_scale = model_name.split("_")[6].replace("VanillaDistortion", "")
        loss_scale = float(loss_scale)
    elif "Distortion" in model_name:
        loss_scale = model_name.split("_")[6].replace("Distortion", "")
        loss_scale = float(loss_scale)
    else:
        raise ValueError(f"Weird input: couldn't infer prior from {model_name}")

    # Getting adjaceny matrix and the such
    adjacency_file_path = DATA_PATH / 'hand_grasps_closure.csv'
    adjacency_matrix, grasps_indices_in_tree = get_indices_grasps_graph(adjacency_file_path, grasp_names)
    grasps_graph_distances_from_adjacency = hand_grasps_distance_mapping_from_adjacency(adjacency_file_path,
                                                                                        grasp_names)

    # If maximum graph distance, rescale the distance
    if "maxdist" in model_name:
        max_manifold_distance = model_name.split("_")[5].replace("maxdist", "")
        max_manifold_distance = float(max_manifold_distance)
        max_graph_distance = np.max(grasps_graph_distances_from_adjacency.detach().numpy())
        grasps_graph_distances_from_adjacency = grasps_graph_distances_from_adjacency / max_graph_distance * max_manifold_distance

    # Model
    if "hyperbolic" in model_name:
        model = HyperbolicExactGPLVM(training_data, latent_dim, pca=True, map_latent_variable=True)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        if 'Zero' in model_name:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
            added_loss.graph_distances = grasps_graph_distances_from_adjacency  # Needed to load the graph distance when computing stress
        elif 'Stress' in model_name:
            print("Loss_type = Stress")
            added_loss = HyperbolicStressLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        elif 'VanillaDistortion' in model_name:
            print("Loss_type = VanillaDistortion")
            added_loss = VanillaHyperbolicDistortionLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        elif 'Distortion' in model_name:
            print("Loss_type = Distortion")
            added_loss = HyperbolicDistortionLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        model.add_loss_term(added_loss)
    else:
        model = ExactGPLVM(training_data, latent_dim, pca=True, map_latent_variable=True)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        if 'Zero' in model_name:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
            added_loss.graph_distances = grasps_graph_distances_from_adjacency  # Needed to load the graph distance when computing stress
        elif 'Stress' in model_name:
            print("Loss_type = Stress")
            added_loss = EuclideanStressLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        elif 'VanillaDistortion' in model_name:
            print("Loss_type = VanillaDistortion")
            NotImplementedError()
            # added_loss = VanillaHyperbolicDistortionLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        elif 'Distortion' in model_name:
            print("Loss_type = Distortion")
            added_loss = EuclideanDistortionLossTermExactMLL(grasps_graph_distances_from_adjacency, loss_scale)
        model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = ExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    mll.load_state_dict(torch.load(model_path))  # load the model from file
    mll.eval()
    mll.model.eval()

    return mll.model


if __name__ == "__main__":
    """
    Tests loading up a distortion model and plotting.
    """
    from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare
    from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d

    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    
    model_name = "hyperbolic_egplvm_prior_dim2_semifull_VanillaDistortion50.0"
    model_path = ROOT_DIR / "final_models" / "distortion_models_for_appendix" / model_name

    model = load_model_with_prior_suppport_poses(model_path)
    print(model)

    #  ---------------- Visualizing -----------------------
    _, shape_poses_names = load_training_data_suppport_poses(model_name.split("_")[4])
    
    # Plot latent variables
    x_latent = model.X.X
    
    # From Lorentz to Poincaré
    x_poincare = lorentz_to_poincare(x_latent).detach().numpy()
    
    # Get colors and legends
    x_colors = [color_function_shape_poses(pose_name) for pose_name in shape_poses_names]
    shape_poses_legend_for_plot = text_function_shape_poses(shape_poses_names)

    # Hyperparameters for magnification:
    model_magnification_path = ROOT_DIR / "final_models" / "distortion_models_for_appendix" / \
                               f"{model_name}_magfac.npz"
    max_magnification = 1.5

    # Plot hyperbolic latent space
    if x_poincare.shape[-1] == 2:
        plot_hyperbolic_gplvm_2d(
            x_poincare,
            x_colors,
            x_legend=None,
            save_path=ROOT_DIR / f"model_viz/{model_name}.png",
            model=model,
            model_magnification_path=model_magnification_path,
            max_magnification=max_magnification
        )
    else:
        plot_hyperbolic_gplvm_3d(x_poincare, x_colors, shape_poses_legend_for_plot, save_path=ROOT_DIR / f"model_viz/{model_name}.png")

    # Plot distances between classes
    # Getting adjaceny matrix and the such
    graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    shape_pose_graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_poses_names)
    v_max = shape_pose_graph_distances.max() + 1
    plot_distance_matrix(shape_pose_graph_distances,
        max_distance=v_max,
        # save_path=ROOT_DIR / "model_viz" / f"distance_matrix_{model_name}.png" 
    )

    # Plot distance matrix on manifold
    distances_latent = lorentz_distance_torch(x_latent, x_latent).detach().numpy()
    plot_distance_matrix(
        distances_latent,
        max_distance=v_max,
        save_path=ROOT_DIR / "model_viz" / f"distance_matrix_{model_name}.png" 
    )

    for name, parameter in model.named_parameters():
        print(name, parameter)
    
