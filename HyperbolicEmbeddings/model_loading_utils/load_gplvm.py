import os
import torch
import gpytorch
from gpytorch.kernels import ScaleKernel, RBFKernel
import gpytorch.priors.torch_priors as torch_priors

from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_initializations import \
    hyperbolic_tangent_pca_initialization, hyperbolic_stress_loss_initialization
from HyperbolicEmbeddings.gplvm.gplvm_models import MapExactGPLVM, BackConstrainedExactGPLVM
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import MapExactHyperbolicGPLVM, \
    BackConstrainedHyperbolicExactGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_exact_marginal_log_likelihood import GPLVMExactMarginalLogLikelihood
from HyperbolicEmbeddings.kernels.kernels_graph import GraphGaussianKernel, GraphMaternKernel
from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL, VanillaHyperbolicDistortionLossTermExactMLL, \
    EuclideanDistortionLossTermExactMLL, EuclideanStressLossTermExactMLL


def load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, model_type, dataset, training_data, adjacency_matrix,
                             indices_in_graph, graph_distances, loss_type, loss_scale, rejection_sampling_kernel=False,
                             outputscale_prior = None, lengthscale_prior = None):

    if "hyperbolic" in model_name:
        # Model definition
        if model_type == 'MAP':
            # Priors
            if outputscale_prior is not None:
                hyperbolic_kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
            else:
                hyperbolic_kernel_outputscale_prior = None
            if lengthscale_prior is not None:
                hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
            else:
                hyperbolic_kernel_lengthscale_prior = None

            # Model
            model = MapExactHyperbolicGPLVM(training_data, latent_dim, X_init=None, batch_params=False,
                                            kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                            kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                            rejection_sampling_kernel=rejection_sampling_kernel)

        elif model_type == 'BC':
            # Prior on latent variable
            if outputscale_prior is not None:
                hyperbolic_kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
            else:
                hyperbolic_kernel_outputscale_prior = None

            if lengthscale_prior is not None:
                hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
            else:
                hyperbolic_kernel_lengthscale_prior = torch_priors.GammaPrior(2.0, 2.0)

            # Back constraints
            data_kernel = ScaleKernel(RBFKernel())
            classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

            if loss_type == 'Stress':
                if dataset == 'grasps':
                    data_kernel.base_kernel.lengthscale = 1.8
                    classes_kernel.base_kernel.lengthscale = 1.5
                    if 'missing_classes' in model_name:
                        classes_kernel.base_kernel.lengthscale = 3.0

                elif dataset == 'support-poses':
                    data_kernel.base_kernel.lengthscale = 0.9
                    classes_kernel.base_kernel.lengthscale = 0.6

                elif dataset == 'augmented-support-poses':
                    data_kernel.base_kernel.lengthscale = 2.0
                    classes_kernel.base_kernel.lengthscale = 0.8
                    if 'missing_classes' in model_name:
                        classes_kernel.base_kernel.lengthscale = 1.3

                data_kernel.outputscale = 2.0
                classes_kernel.outputscale = 1.0

                taxonomy_bc = True

            elif loss_type == 'Zero':
                data_kernel.base_kernel.lengthscale = 1.5  # TODO
                classes_kernel.base_kernel.lengthscale = 1.5
                data_kernel.outputscale = 1.0
                classes_kernel.outputscale = 1.0

                taxonomy_bc = False

            # Model
            model = BackConstrainedHyperbolicExactGPLVM(training_data, latent_dim, indices_in_graph,
                                                        data_kernel=data_kernel, classes_kernel=classes_kernel,
                                                        kernel_lengthscale_prior=hyperbolic_kernel_lengthscale_prior,
                                                        kernel_outputscale_prior=hyperbolic_kernel_outputscale_prior,
                                                        X_init=None, batch_params=False,
                                                        taxonomy_based_back_constraints=taxonomy_bc,
                                                        rejection_sampling_kernel=rejection_sampling_kernel)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
        if loss_type == 'Zero' or None:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
        elif loss_type == 'Stress':
            print("Loss_type = Stress")
            added_loss = HyperbolicStressLossTermExactMLL(graph_distances, loss_scale)
        elif loss_type == 'Distortion':
            print("Loss_type = Distortion")
            added_loss = HyperbolicDistortionLossTermExactMLL(graph_distances, loss_scale)
        elif loss_type == "VanillaDistortion":
            print("Loss_type = VanillaDistortion")
            added_loss = VanillaHyperbolicDistortionLossTermExactMLL(graph_distances, loss_scale)
    else:
        # Model definition
        if model_type == 'MAP':
            # Priors
            if outputscale_prior is not None:
                kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
            else:
                kernel_outputscale_prior = None

            if lengthscale_prior is not None:
                kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
            else:
                kernel_lengthscale_prior = None

            # Model
            model = MapExactGPLVM(training_data, latent_dim, X_init=None, batch_params=False,
                                  kernel_lengthscale_prior=kernel_lengthscale_prior,
                                  kernel_outputscale_prior=kernel_outputscale_prior)
        elif model_type == 'BC':
            # Prior on latent variable
            if outputscale_prior is not None:
                kernel_outputscale_prior = torch_priors.GammaPrior(*outputscale_prior)
            else:
                kernel_outputscale_prior = None

            if lengthscale_prior is not None:
                kernel_lengthscale_prior = torch_priors.GammaPrior(*lengthscale_prior)
            else:
                kernel_lengthscale_prior = None

            # Back constraints
            data_kernel = ScaleKernel(RBFKernel())
            classes_kernel = ScaleKernel(GraphMaternKernel(adjacency_matrix, nu=2.5))

            if loss_type == 'Stress':
                if dataset == 'grasps':
                    data_kernel.base_kernel.lengthscale = 1.8
                    classes_kernel.base_kernel.lengthscale = 1.5

                elif dataset == 'support-poses':
                    data_kernel.base_kernel.lengthscale = 0.9
                    classes_kernel.base_kernel.lengthscale = 0.6

                elif dataset == 'augmented-support-poses':
                    data_kernel.base_kernel.lengthscale = 2.0
                    classes_kernel.base_kernel.lengthscale = 0.8

                data_kernel.outputscale = 2.0
                classes_kernel.outputscale = 1.0

                taxonomy_bc = True

            elif loss_type == 'Zero':
                data_kernel.base_kernel.lengthscale = 1.5  # TODO
                classes_kernel.base_kernel.lengthscale = 1.5
                data_kernel.outputscale = 1.0
                classes_kernel.outputscale = 1.0

                taxonomy_bc = False

            # Kernel
            model = BackConstrainedExactGPLVM(training_data, latent_dim, indices_in_graph,
                                              data_kernel=data_kernel, classes_kernel=classes_kernel,
                                              kernel_lengthscale_prior=kernel_lengthscale_prior,
                                              kernel_outputscale_prior=kernel_outputscale_prior,
                                              X_init=None, batch_params=False,
                                              taxonomy_based_back_constraints=taxonomy_bc)

        # Add an extra loss term for the model (can be seen as an additional prior on the latent variable)
        # loss_type = 'Distortion'  # 'Zero', 'Stress', 'Distortion'
        if loss_type == 'Zero' or None:
            print("Loss_type = Zero")
            added_loss = ZeroAddedLossTermExactMLL()
        elif loss_type == 'Stress':
            print("Loss_type = Stress")
            added_loss = EuclideanStressLossTermExactMLL(graph_distances, loss_scale)
        elif loss_type == 'Distortion':
            print("Loss_type = Distortion")
            added_loss = EuclideanDistortionLossTermExactMLL(graph_distances, loss_scale)
        # elif loss_type == "VanillaDistortion":
        #     print("Loss_type = VanillaDistortion")
        #     added_loss = VanillaHyperbolicDistortionLossTermExactMLL(graph_distances, loss_scale)

    model.add_loss_term(added_loss)

    # Declaring the objective to be optimised along with optimiser
    mll = GPLVMExactMarginalLogLikelihood(model.likelihood, model)

    # Load the model
    mll.train()
    if os.path.isdir(MODEL_PATH / model_name):
        mll.load_state_dict(torch.load(MODEL_PATH / model_name / "model_best_state_dict.pth"))  # load the model from file
    else:
        mll.load_state_dict(torch.load(MODEL_PATH / model_name))  # load the model from file
    mll.eval()

    return mll