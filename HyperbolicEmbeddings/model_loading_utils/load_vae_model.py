from pathlib import Path
from typing import Union

import torch

from HyperbolicEmbeddings.vae_baselines.vanilla_vae import VanillaVAEBaseline
from HyperbolicEmbeddings.vae_baselines.hyperbolic_vae import HyperbolicVAEBaseline


def load_vae(model_name: str, MODEL_PATH: str, latent_dim: int, training_data) -> Union[VanillaVAEBaseline, HyperbolicVAEBaseline]:

    if "hyperbolic" in model_name:
        model_to_load = HyperbolicVAEBaseline
    else:
        model_to_load = VanillaVAEBaseline

    # Model parameters
    kwargs = {"input_dim": training_data.shape[1], "hidden_dim": 22, "latent_dim": latent_dim}

    vae = model_to_load(**kwargs)
    vae.load_state_dict(torch.load(MODEL_PATH / model_name))

    vae.eval()
    return vae
