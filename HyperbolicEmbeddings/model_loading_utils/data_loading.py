from typing import Tuple, List
from pathlib import Path

import torch
import numpy as np

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    shape_poses_graph_distance_mapping, get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, \
    get_reduced_dataset_feet_5poses, get_semifull_dataset, get_indices_shape_poses_graph, NAME_CLASS_MAPPING
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import get_grasp_dataset


def load_training_augmented_data() -> Tuple[torch.Tensor, List[str]]:
    ...

def load_training_data_suppport_poses(dataset: str, leave_out: Tuple[int] = None, pose_training: bool = True) -> Tuple[torch.Tensor, list]:
    """
    Loads the dataset in the model's name and returns it.
    """
    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    DATA_PATH = ROOT_DIR / "data/support_poses"

    # dataset = model_name.split("_")[4]
    poses_path = DATA_PATH / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        pose_data, joint_data, shape_pose_names, _ = parse_xml_pose_shape_data(poses_path)
    elif dataset == 'semifull':
        pose_data, joint_data, shape_pose_names, _ = get_semifull_dataset(poses_path)
    elif dataset == 'reduced':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_2poses(poses_path)
    elif dataset == 'feet5':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_feet_5poses(poses_path)
    elif dataset == 'knees2':
        pose_data, joint_data, shape_pose_names, _ = get_reduced_dataset_knees_2poses(poses_path)
    else:
        raise ValueError(f"Weird dataset: {dataset}")

    if pose_training:
        training_data = pose_data
    else:
        training_data = joint_data

    if leave_out is not None:
        ## Building the reduced dataset, and saving the skipped data.
        # class_to_skip = (1, 1, 0)  # 1F-1H, for example.
        reduced_shape_pose_names = []
        reduced_pose_data = []

        for (pose, pose_name) in zip(training_data, shape_pose_names):
            if NAME_CLASS_MAPPING[pose_name] != leave_out:
                reduced_shape_pose_names.append(pose_name)
                reduced_pose_data.append(pose)
        
        training_data = np.array(reduced_pose_data)
        shape_pose_names = reduced_shape_pose_names
    
    training_data = torch.from_numpy(training_data)
    return training_data, shape_pose_names


def load_skipped_data_suppport_poses(dataset: str, leave_out: Tuple[int]) -> Tuple[torch.Tensor, list]:
    """
    Loads all the data that was skipped from dataset where
    leave_out is the class to skip (e.g. leave_out = (1, 1, 0) for
    1F-1H-0K)
    """
    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    DATA_PATH = ROOT_DIR / "data/support_poses"
    
    # Returns the skipped data.
    poses_path = DATA_PATH / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    if dataset == 'full':
        training_data, training_joints, shape_pose_names, _ = parse_xml_pose_shape_data(poses_path)
    elif dataset == 'semifull':
        training_data, training_joints, shape_pose_names, _ = get_semifull_dataset(poses_path)
    elif dataset == 'reduced':
        training_data, training_joints, shape_pose_names, _ = get_reduced_dataset_2poses(poses_path)
    elif dataset == 'feet5':
        training_data, training_joints, shape_pose_names, _ = get_reduced_dataset_feet_5poses(poses_path)
    elif dataset == 'knees2':
        training_data, training_joints, shape_pose_names, _ = get_reduced_dataset_knees_2poses(poses_path)

    if leave_out is not None:
        skipped_pose_data = []
        skipped_joint_data = []
        skipped_shape_pose_names = [] # this one will be n copies of the same, but we still need it.

        for (pose, joint, pose_name) in zip(training_data, training_joints, shape_pose_names):
            if NAME_CLASS_MAPPING[pose_name] == leave_out:
                skipped_shape_pose_names.append(pose_name)
                skipped_pose_data.append(pose)
                skipped_joint_data.append(joint)

        skipped_pose_data = np.array(skipped_pose_data)
        skipped_joint_data = np.array(skipped_joint_data)

    return torch.from_numpy(skipped_pose_data), torch.from_numpy(skipped_joint_data), skipped_shape_pose_names


def load_training_data_grasps(dataset: str = None, leave_out: Tuple[int] = None) -> Tuple[
    torch.Tensor, list]:
    """
    Loads the dataset in the model's name and returns it.
    """
    ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
    DATA_PATH = ROOT_DIR / "data/hand_grasps"

    hand_joint_data, grasp_names, joint_names = get_grasp_dataset(str(DATA_PATH))

    if leave_out is not None:
        ## Building the reduced dataset, and saving the skipped data.
        # class_to_skip = (1, 1, 0)  # 1F-1H, for example.
        reduced_grasp_names = []
        reduced_hand_joint_data = []

        for (grasp, grasp_name) in zip(hand_joint_data, grasp_names):
            if NAME_CLASS_MAPPING[grasp_name] != leave_out:
                reduced_grasp_names.append(grasp_name)
                reduced_hand_joint_data.append(grasp)

        hand_joint_data = np.array(reduced_hand_joint_data)
        grasp_names = reduced_grasp_names

    hand_joint_data = torch.from_numpy(hand_joint_data)
    return hand_joint_data, grasp_names
