from __future__ import division

from pathlib import Path

import numpy as np
import pandas
import torch
from xml.etree import ElementTree
import os
from collections import Counter

from HyperbolicEmbeddings.graph_utils.graph_functions import get_indices_nodes_graph, nodes_distance_mapping

# (F, H, K)
NAME_CLASS_MAPPING = {
    "LeftFootRightHand": (1, 1, 0),
    "LeftHandLeftKneeRightFootRightHand": (1, 2, 1),
    "LeftFoot": (1, 0, 0),
    "LeftHandRightFootRightHand": (1, 2, 0),
    "LeftKneeRightFoot": (1, 0, 1),
    "LeftHandLeftKneeRightKnee": (0, 1, 2),
    "LeftKneeRightHandRightKnee": (0, 1, 2),
    "LeftHandLeftKnee": (0, 1, 1),
    "RightHandRightKnee": (0, 1, 1),
    "LeftFootLeftHandRightFootRightHand": (2, 2, 0),
    "LeftFootLeftHandRightFoot": (2, 1, 0),
    "LeftFootRightFoot": (2, 0, 0),
    "RightFootRightHand": (1, 1, 0),
    "LeftHandLeftKneeRightHand": (0, 2, 1),
    "LeftKneeRightKnee": (0, 0, 2),
    "LeftFootLeftHand": (1, 1, 0),
    "LeftHandRightFoot": (1, 1, 0),
    "RightKnee": (0, 0, 1),
    "LeftFootRightFootRightHand": (2, 1, 0),
    "RightFoot": (1, 0, 0),
    "LeftFootLeftHandRightHand": (1, 2, 0),
    "LeftFootRightHandRightKnee": (1, 1, 1),
    "LeftFootRightKnee": (1, 0, 1),
    "LeftHandLeftKneeRightFoot": (1, 1, 1),
    "LeftHandLeftKneeRightHandRightKnee": (0, 2, 2),
    "LeftKnee": (0, 0, 1)
}

ordered_shape_pose_names = [
        # 1F
        'LeftFoot', 'RightFoot',
        # 1F Added
        'LeftFootAdded', 'RightFootAdded',
        # 1F-1H
        'LeftFootLeftHand', 'LeftFootRightHand',
        'LeftHandRightFoot', 'RightFootRightHand',
        # 1F-1H Added
        'LeftFootLeftHandAdded', 'LeftFootRightHandAdded',
        'LeftHandRightFootAdded', 'RightFootRightHandAdded',
        # 2F
        'LeftFootRightFoot',
        # 2F Added
        'LeftFootRightFootAdded',
        # 2F-1H
        'LeftFootLeftHandRightFoot',
        'LeftFootRightFootRightHand',
        # 2F-1H Added
        'LeftFootLeftHandRightFootAdded',
        'LeftFootRightFootRightHandAdded',
        # 1F-2H
        'LeftHandRightFootRightHand',
        'LeftFootLeftHandRightHand',
        # 1F-2H Added
        'LeftHandRightFootRightHandAdded',
        'LeftFootLeftHandRightHandAdded',
        # 2F-2H
        'LeftFootLeftHandRightFootRightHand',
        # 2F-2H Added
        'LeftFootLeftHandRightFootRightHandAdded',
        # 1F-1K
        'LeftFootRightKnee', 'LeftKneeRightFoot',
        # 1F-1K added
        'LeftFootRightKneeAdded', 'LeftKneeRightFootAdded',
        # 1F-1H-1K
        'LeftFootRightHandRightKnee', 'LeftHandLeftKneeRightFoot',
        # 1F-1H-1K Added
        'LeftFootRightHandRightKneeAdded', 'LeftHandLeftKneeRightFootAdded',
        # 1F-2H-1K
        'LeftHandLeftKneeRightFootRightHand',
        # 1F-2H-1K Added
        'LeftHandLeftKneeRightFootRightHandAdded',
        # 1K
        'LeftKnee', 'RightKnee',
        # 1K Added
        'LeftKneeAdded', 'RightKneeAdded',
        # 1H-1K
        'LeftHandLeftKnee', 'RightHandRightKnee',
        # 1H-1K Added
        'LeftHandLeftKneeAdded', 'RightHandRightKneeAdded',
        # 2K
        'LeftKneeRightKnee',
        # 2K Added
        'LeftKneeRightKneeAdded',
        # 2H-1K
        'LeftHandLeftKneeRightHand',
        # 2H-1K Added
        'LeftHandLeftKneeRightHandAdded',
        # 1H-2K
        'LeftHandLeftKneeRightKnee', 'LeftKneeRightHandRightKnee',
        # 1H-2K Added
        'LeftHandLeftKneeRightKneeAdded', 'LeftKneeRightHandRightKneeAdded',
        # 2H-2K
        'LeftHandLeftKneeRightHandRightKnee',
        # 2H-2K Added
        'LeftHandLeftKneeRightHandRightKneeAdded',
    ]


def load_support_pose_data(data_folder_path, dataset_type):
    data_folder_path = data_folder_path / "support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset

    # Load data
    if dataset_type == 'full':
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset_type == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset_type == 'reduced':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset_type == 'feet5':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset_type == 'knees2':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)

    # Map pose labels to support pose nodes
    support_pose_labels = shape_poses_label_to_support_pose_labels(shape_pose_names)

    # Adjacency matrix
    graph_file_path = data_folder_path / 'support_poses_closure.csv'
    adjacency_matrix, shape_poses_indices = get_indices_nodes_graph(graph_file_path, support_pose_labels)

    # Distances
    shape_pose_graph_distances = nodes_distance_mapping(graph_file_path, support_pose_labels)

    # Legend
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    # Set training data
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    shape_poses_indices = torch.from_numpy(shape_poses_indices)
    training_data = pose_data
    # training_data = joint_data

    return training_data, adjacency_matrix, shape_pose_graph_distances, shape_pose_names, shape_poses_indices, \
           shape_poses_legend_for_plot, color_function_shape_poses, joint_names


def load_augmented_support_pose_data(data_folder_path, dataset_type):
    # Load data
    data_folder_path = data_folder_path / "support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset

    # Load data
    if dataset_type == 'full':
        pose_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)
    elif dataset_type == 'semifull':
        pose_data, joint_data, shape_pose_names, joint_names = get_semifull_dataset(data_path)
    elif dataset_type == 'reduced':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_2poses(data_path)
    elif dataset_type == 'feet5':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_feet_5poses(data_path)
    elif dataset_type == 'knees2':
        pose_data, joint_data, shape_pose_names, joint_names = get_reduced_dataset_knees_2poses(data_path)

    # Map pose labels to support pose nodes
    support_pose_labels = shape_poses_label_to_augmented_support_pose_labels(shape_pose_names)

    # Adjacency matrix
    graph_file_path = data_folder_path / 'support_poses_augmented_closure.csv'
    adjacency_matrix, shape_poses_indices = get_indices_nodes_graph(graph_file_path, support_pose_labels)

    # Distances
    shape_pose_graph_distances = nodes_distance_mapping(graph_file_path, support_pose_labels)

    # Legend
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    # Augment pose data with contacts
    pose_data = augment_pose_data_with_contacts(pose_data, shape_pose_names)

    # Set training data
    pose_data = torch.from_numpy(pose_data)
    joint_data = torch.from_numpy(joint_data)
    shape_poses_indices = torch.from_numpy(shape_poses_indices)
    # training_data = pose_data
    training_data = joint_data

    return training_data, adjacency_matrix, shape_pose_graph_distances, shape_pose_names, shape_poses_indices, \
           shape_poses_legend_for_plot, color_function_shape_poses, joint_names


def load_missing_augmented_support_pose_data(data_folder_path, dataset_type):
    # Load data
    training_data, _, _, shape_pose_names, _, _, _, _ = \
        load_augmented_support_pose_data(data_folder_path, dataset_type)

    # Load data
    data_folder_path = data_folder_path / "support_poses"
    data_path = data_folder_path / 'keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset

    # Load added data
    if dataset_type == 'full':
        NotImplementedError
    elif dataset_type == 'semifull':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_semifull_dataset(data_path)
    elif dataset_type == 'reduced':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_reduced_dataset_2poses(data_path)
    elif dataset_type == 'feet5':
        added_pose_data, added_joint_data, added_shape_pose_names, added_joint_names = \
            get_missing_data_feet5(data_path)
    elif dataset_type == 'knees2':
        NotImplementedError

    all_shape_pose_names = shape_pose_names + added_shape_pose_names

    # Map pose labels to support pose nodes
    all_support_pose_labels = shape_poses_label_to_augmented_support_pose_labels(all_shape_pose_names)
    added_support_pose_labels = shape_poses_label_to_augmented_support_pose_labels(added_shape_pose_names)

    # Adjacency matrix
    graph_file_path = data_folder_path / 'support_poses_augmented_closure.csv'
    _, added_shape_poses_indices = get_indices_nodes_graph(graph_file_path, added_support_pose_labels)

    # Distances
    all_shape_pose_graph_distances = nodes_distance_mapping(graph_file_path, all_support_pose_labels)

    # Legend
    all_shape_poses_legend_for_plot = text_function_shape_poses(all_shape_pose_names)

    # Augment pose data with contacts
    added_pose_data = augment_pose_data_with_contacts(added_pose_data, added_shape_pose_names)

    # Set training data
    added_pose_data = torch.from_numpy(added_pose_data)
    added_joint_data = torch.from_numpy(added_joint_data)
    added_shape_poses_indices = torch.from_numpy(added_shape_poses_indices)
    # training_data = pose_data
    added_training_data = added_joint_data

    return added_training_data, added_shape_pose_names, added_shape_poses_indices, all_shape_pose_graph_distances, \
           all_shape_poses_legend_for_plot, color_function_shape_poses


def parse_xml_pose_shape_data(path: str, flag_take_all_cords=False, flag_take_new_ee_poses=True):
    """
    Function that parses taxonomy info contained in an XML to python arrays.

    Parameters
    ----------
    path: String with the relative path where the XML file is

    Optional parameters
    -------------------
    flag_take_all_cords: if True, all instances of each subposes are considered.
                         if False, only the first instance is considered.
                         This corresponds to having the 192 data of Andre Langenstein's MA thesis.
    flag_take_new_ee_poses: if True, use the end-effector poses recomputed using Mujoco and the 1m MMM model

    Returns
    -------
    cartesian_data (np.array): Contains float-type array of Cartesian data extracted from the XML file
    pose_names (list): Contains all the names of the shape poses found in the XML file
    """
    # Data holders
    str_cartesian_data = []
    str_joint_data = []
    pose_names = []
    # load trajectory into demonstration
    root = ElementTree.parse(os.path.expanduser(path)).getroot()

    for pose_tag in root.findall('Pose'):
        for subpose_tag in pose_tag.findall('SubPose'):
            if flag_take_all_cords:
                for coordinates in subpose_tag.findall('Coords'):
                    pose_names.append(pose_tag.attrib['name'])
                    str_pose_array = np.array(coordinates.find('TaskSpace').text.replace(' ', ',')[1:-5].split(","))
                    str_cartesian_data.append([float(float_pose) for float_pose in str_pose_array])
                    str_joint_array = np.array(coordinates.find('JointSpace').text.replace(' ', ',')[1:-5].split(","))
                    str_joint_data.append([float(float_joint) for float_joint in str_joint_array])
            else:
                coordinates = subpose_tag.findall('Coords')[0]
                pose_names.append(pose_tag.attrib['name'])
                str_pose_array = np.array(coordinates.find('TaskSpace').text.replace(' ', ',')[1:-5].split(","))
                str_cartesian_data.append([float(float_pose) for float_pose in str_pose_array])
                str_joint_array = np.array(coordinates.find('JointSpace').text.replace(' ', ',')[1:-5].split(","))
                str_joint_data.append([float(float_joint) for float_joint in str_joint_array])

    # Fix joint angles
    old_joint_names = ["BLNx_joint", "BLNy_joint", "BLNz_joint",
                       "BPx_joint", "BPy_joint", "BPz_joint",
                       "BTx_joint", "BTy_joint", "BTz_joint",
                       "BUNx_joint", "BUNy_joint", "BUNz_joint",
                       "LAx_joint", "LAy_joint", "LAz_joint",
                       "LEx_joint", "LEz_joint",
                       "LHx_joint", "LHy_joint", "LHz_joint",
                       "LKx_joint",
                       "LSx_joint", "LSy_joint", "LSz_joint",
                       "LWx_joint", "LWy_joint",
                       "RAx_joint", "RAy_joint", "RAz_joint",
                       "REx_joint", "REz_joint",
                       "RHx_joint", "RHy_joint", "RHz_joint",
                       "RKx_joint",
                       "RSx_joint", "RSy_joint", "RSz_joint",
                       "RWx_joint", "RWy_joint"]

    # new_joint_names = ["BLNx_joint", "BLNy_joint", "BLNz_joint",
    #                    "BPx_joint", "BPy_joint", "BPz_joint",
    #                    "BTx_joint", "BTy_joint", "BTz_joint",
    #                    "BUNx_joint", "BUNy_joint", "BUNz_joint",
    #                    "LAx_joint", "LAy_joint", "LAz_joint",
    #                    "LEx_joint", "LEz_joint",
    #                    "LFx_joint",  # new
    #                    "LHx_joint", "LHy_joint", "LHz_joint",
    #                    "LKx_joint",
    #                    "LMrot_joint",  # new
    #                    "LSx_joint", "LSy_joint", "LSz_joint",
    #                    "LWx_joint", "LWy_joint",
    #                    "RAx_joint", "RAy_joint", "RAz_joint",
    #                    "REx_joint", "REz_joint",
    #                    "RFx_joint",  # new
    #                    "RHx_joint", "RHy_joint", "RHz_joint",
    #                    "RKx_joint",
    #                    "RMrot_joint",  # new
    #                    "RSx_joint", "RSy_joint", "RSz_joint",
    #                    "RWx_joint", "RWy_joint"]

    new_joint_names = ["BLNx_joint", "BLNy_joint", "BLNz_joint",
                       "BPx_joint", "BPy_joint", "BPz_joint",
                       "BTx_joint", "BTy_joint", "BTz_joint",
                       "BUNx_joint", "BUNy_joint", "BUNz_joint",
                       "LAx_joint", "LAy_joint", "LAz_joint",
                       "LEx_joint", "LEz_joint",
                       "LHx_joint", "LHy_joint", "LHz_joint",
                       "LKx_joint",
                       "LSx_joint", "LSy_joint", "LSz_joint",
                       "LWx_joint", "LWy_joint",
                       "LFx_joint",
                       "LMrot_joint",
                       "RAx_joint", "RAy_joint", "RAz_joint",
                       "REx_joint", "REz_joint",
                       "RHx_joint", "RHy_joint", "RHz_joint",
                       "RKx_joint",
                       "RSx_joint", "RSy_joint", "RSz_joint",
                       "RWx_joint", "RWy_joint",
                       "RFx_joint",
                       "RMrot_joint"
                       ]

    common_joint_names = set(old_joint_names).intersection(new_joint_names)
    common_joints_indices = [new_joint_names.index(x) for x in common_joint_names]
    common_joints_indices.sort()
    new_len = len(new_joint_names)

    joint_data = np.zeros((len(str_joint_data), new_len))
    for n in range(len(str_joint_data)):
        joints = np.asarray(str_joint_data[n])
        if joints.shape[0] == new_len:
            joint_data[n] = joints
        else:
            joint_data[n, common_joints_indices] = joints

    cartesian_data = np.asarray(str_cartesian_data)
    cartesian_data /= 1000.  # mm to m

    data_path = Path(__file__).parent.parent.parent.resolve() / "data/support_poses"
    if flag_take_new_ee_poses:
        ee_poses = np.load(data_path / 'recomputed_end_effector_poses_new_mmm.npz')
    else:
        if flag_take_all_cords:
            ee_poses = np.load(data_path / 'recomputed_end_effector_poses_all.npz')
        else:
            ee_poses = np.load(data_path / 'recomputed_end_effector_poses.npz')
    x_leftarm = ee_poses['x_leftarm']
    x_rightarm = ee_poses['x_rightarm']
    x_leftleg = ee_poses['x_leftleg']
    x_rightleg = ee_poses['x_rightleg']

    cartesian_data = np.hstack((x_leftleg[:, :3], x_rightleg[:, :3], x_leftarm[:, :3], x_rightarm[:, :3]))

    return cartesian_data, joint_data, pose_names, new_joint_names


def shape_poses_label_to_support_pose_labels(shape_poses_label):
    """
    This function maps the shape pose names presented in [1] to names of nodes of the whole-body motion taxonomy.
    """
    support_pose_labels = [''] * len(shape_poses_label)

    # For loop to map pose labels to support pose nodes
    for pose_id, pose_label in enumerate(shape_poses_label):
        foot_occurrence = pose_label.count('Foot')
        hand_occurrence = pose_label.count('Hand')
        knee_occurrence = pose_label.count('Knee')

        # Occurrence pairs are written in the order given in Table 3.2 of [1]
        if foot_occurrence == 1 and hand_occurrence == 0 and knee_occurrence == 0:  # 1F
            support_pose_labels[pose_id] = 'f'
        elif foot_occurrence == 1 and hand_occurrence == 1 and knee_occurrence == 0:  # 1F-1H
            support_pose_labels[pose_id] = 'fh'
        elif foot_occurrence == 2 and hand_occurrence == 0 and knee_occurrence == 0:  # 2F
            support_pose_labels[pose_id] = 'f2'
        elif foot_occurrence == 1 and hand_occurrence == 2 and knee_occurrence == 0:  # 1F-2H
            support_pose_labels[pose_id] = 'fh2'
        elif foot_occurrence == 2 and hand_occurrence == 1 and knee_occurrence == 0:  # 2F-1H
            support_pose_labels[pose_id] = 'f2h'
        elif foot_occurrence == 2 and hand_occurrence == 2 and knee_occurrence == 0:  # 2F-2H
            support_pose_labels[pose_id] = 'f2h2'
        elif foot_occurrence == 0 and hand_occurrence == 0 and knee_occurrence == 1:  # 1K
            support_pose_labels[pose_id] = 'k'
        elif foot_occurrence == 0 and hand_occurrence == 1 and knee_occurrence == 1:  # 1K-1H
            support_pose_labels[pose_id] = 'kh'
        elif foot_occurrence == 1 and hand_occurrence == 0 and knee_occurrence == 1:  # 1K-1F
            support_pose_labels[pose_id] = 'kf'
        elif foot_occurrence == 0 and hand_occurrence == 0 and knee_occurrence == 2:  # 2K
            support_pose_labels[pose_id] = 'k2'
        elif foot_occurrence == 0 and hand_occurrence == 2 and knee_occurrence == 1:  # 1K-2H
            support_pose_labels[pose_id] = 'kh2'
        elif foot_occurrence == 1 and hand_occurrence == 1 and knee_occurrence == 1:  # 1F-1K-1H
            support_pose_labels[pose_id] = 'fkh'
        elif foot_occurrence == 0 and hand_occurrence == 1 and knee_occurrence == 2:  # 2K-1H
            support_pose_labels[pose_id] = 'k2h'
        elif foot_occurrence == 1 and hand_occurrence == 2 and knee_occurrence == 1:  # 1F-1K-2H
            support_pose_labels[pose_id] = 'fkh2'
        elif foot_occurrence == 0 and hand_occurrence == 2 and knee_occurrence == 2:  # 2K-2H
            support_pose_labels[pose_id] = 'k2h2'

    return support_pose_labels


def shape_poses_label_to_augmented_support_pose_labels(shape_poses_label):
    """
    This function maps the shape pose names presented in [1] to names of nodes of the augmented whole-body motion
    taxonomy (considering left and right sides of contacts).
    """
    support_pose_labels = [''] * len(shape_poses_label)

    # For loop to map pose labels to support pose nodes
    for pose_id, pose_label in enumerate(shape_poses_label):
        leftfoot_occurrence = pose_label.count('LeftFoot')
        rightfoot_occurrence = pose_label.count('RightFoot')
        foot_occurrence = pose_label.count('Foot')
        lefthand_occurrence = pose_label.count('LeftHand')
        righthand_occurrence = pose_label.count('RightHand')
        hand_occurrence = pose_label.count('Hand')
        leftknee_occurrence = pose_label.count('LeftKnee')
        rightknee_occurrence = pose_label.count('RightKnee')
        knee_occurrence = pose_label.count('Knee')

        # Occurrence pairs are written in the order given in Table 3.2 of [1]
        if leftfoot_occurrence == 1 and rightfoot_occurrence == 0 and hand_occurrence == 0 and knee_occurrence == 0:  # 1F
            support_pose_labels[pose_id] = 'lf'
        elif leftfoot_occurrence == 0 and rightfoot_occurrence == 1 and hand_occurrence == 0 and knee_occurrence == 0:  # 1F
            support_pose_labels[pose_id] = 'rf'

        elif leftfoot_occurrence == 1 and rightfoot_occurrence == 0 and lefthand_occurrence == 1 and righthand_occurrence == 0 and knee_occurrence == 0:  # 1F-1H
            support_pose_labels[pose_id] = 'lflh'
        elif leftfoot_occurrence == 1 and rightfoot_occurrence == 0 and lefthand_occurrence == 0 and righthand_occurrence == 1 and knee_occurrence == 0:  # 1F-1H
            support_pose_labels[pose_id] = 'lfrh'
        elif leftfoot_occurrence == 0 and rightfoot_occurrence == 1 and lefthand_occurrence == 1 and righthand_occurrence == 0 and knee_occurrence == 0:  # 1F-1H
            support_pose_labels[pose_id] = 'rflh'
        elif leftfoot_occurrence == 0 and rightfoot_occurrence == 1 and lefthand_occurrence == 0 and righthand_occurrence == 1 and knee_occurrence == 0:  # 1F-1H
            support_pose_labels[pose_id] = 'rfrh'

        elif foot_occurrence == 2 and hand_occurrence == 0 and knee_occurrence == 0:  # 2F
            support_pose_labels[pose_id] = 'f2'

        elif leftfoot_occurrence == 1 and rightfoot_occurrence == 0 and hand_occurrence == 2 and knee_occurrence == 0:  # 1F-2H
            support_pose_labels[pose_id] = 'lfh2'
        elif leftfoot_occurrence == 0 and rightfoot_occurrence == 1 and hand_occurrence == 2 and knee_occurrence == 0:  # 1F-2H
            support_pose_labels[pose_id] = 'rfh2'

        elif foot_occurrence == 2 and lefthand_occurrence == 1 and righthand_occurrence == 0 and knee_occurrence == 0:  # 2F-1H
            support_pose_labels[pose_id] = 'f2lh'
        elif foot_occurrence == 2 and lefthand_occurrence == 0 and righthand_occurrence == 1 and knee_occurrence == 0:  # 2F-1H
            support_pose_labels[pose_id] = 'f2rh'

        elif foot_occurrence == 2 and hand_occurrence == 2 and knee_occurrence == 0:  # 2F-2H
            support_pose_labels[pose_id] = 'f2h2'

        elif foot_occurrence == 0 and hand_occurrence == 0 and leftknee_occurrence == 1 and rightknee_occurrence == 0:  # 1K
            support_pose_labels[pose_id] = 'lk'
        elif foot_occurrence == 0 and hand_occurrence == 0 and leftknee_occurrence == 0 and rightknee_occurrence == 1:  # 1K
            support_pose_labels[pose_id] = 'rk'

        elif foot_occurrence == 0 and lefthand_occurrence == 1 and righthand_occurrence == 0 and leftknee_occurrence == 1 and rightknee_occurrence == 0:  # 1K-1H
            support_pose_labels[pose_id] = 'lklh'
        elif foot_occurrence == 0 and lefthand_occurrence == 1 and righthand_occurrence == 0 and leftknee_occurrence == 0 and rightknee_occurrence == 1:  # 1K-1H
            support_pose_labels[pose_id] = 'rklh'
        elif foot_occurrence == 0 and lefthand_occurrence == 0 and righthand_occurrence == 1 and leftknee_occurrence == 1 and rightknee_occurrence == 0:  # 1K-1H
            support_pose_labels[pose_id] = 'lkrh'
        elif foot_occurrence == 0 and lefthand_occurrence == 0 and righthand_occurrence == 1 and leftknee_occurrence == 0 and rightknee_occurrence == 1:  # 1K-1H
            support_pose_labels[pose_id] = 'rkrh'

        elif leftfoot_occurrence == 1 and hand_occurrence == 0 and leftknee_occurrence == 0 and rightknee_occurrence == 1:  # 1K-1F
            support_pose_labels[pose_id] = 'rklf'
        elif rightfoot_occurrence == 1 and hand_occurrence == 0 and leftknee_occurrence == 1 and rightknee_occurrence == 0:  # 1K-1F
            support_pose_labels[pose_id] = 'lkrf'

        elif foot_occurrence == 0 and hand_occurrence == 0 and knee_occurrence == 2:  # 2K
            support_pose_labels[pose_id] = 'k2'

        elif foot_occurrence == 0 and hand_occurrence == 2 and leftknee_occurrence == 1 and rightknee_occurrence == 0:  # 1K-2H
            support_pose_labels[pose_id] = 'lkh2'
        elif foot_occurrence == 0 and hand_occurrence == 2 and leftknee_occurrence == 0 and rightknee_occurrence == 1:  # 1K-2H
            support_pose_labels[pose_id] = 'rkh2'

        elif leftfoot_occurrence == 1 and lefthand_occurrence == 1 and righthand_occurrence == 0 and rightknee_occurrence == 1:  # 1F-1K-1H
            support_pose_labels[pose_id] = 'lfrklh'
        elif leftfoot_occurrence == 1 and lefthand_occurrence == 0 and righthand_occurrence == 1 and rightknee_occurrence == 1:  # 1F-1K-1H
            support_pose_labels[pose_id] = 'lfrkrh'
        elif rightfoot_occurrence == 1 and lefthand_occurrence == 1 and righthand_occurrence == 0 and leftknee_occurrence == 1:  # 1F-1K-1H
            support_pose_labels[pose_id] = 'rflklh'
        elif rightfoot_occurrence == 1 and lefthand_occurrence == 0 and righthand_occurrence == 1 and leftknee_occurrence == 1:  # 1F-1K-1H
            support_pose_labels[pose_id] = 'rflkrh'

        elif foot_occurrence == 0 and lefthand_occurrence == 1 and righthand_occurrence == 0 and knee_occurrence == 2:  # 2K-1H
            support_pose_labels[pose_id] = 'k2lh'
        elif foot_occurrence == 0 and lefthand_occurrence == 0 and righthand_occurrence == 1 and knee_occurrence == 2:  # 2K-1H
            support_pose_labels[pose_id] = 'k2rh'

        elif leftfoot_occurrence == 1 and hand_occurrence == 2 and rightknee_occurrence == 1:  # 1F-1K-2H
            support_pose_labels[pose_id] = 'lfrkh2'
        elif rightfoot_occurrence == 1 and hand_occurrence == 2 and leftknee_occurrence == 1:  # 1F-1K-2H
            support_pose_labels[pose_id] = 'rflkh2'

        elif foot_occurrence == 0 and hand_occurrence == 2 and knee_occurrence == 2:  # 2K-2H
            support_pose_labels[pose_id] = 'k2h2'

    return support_pose_labels


def get_taxonomy_colors():
    single_foot_color = "orange"
    double_foot_color = "lightcoral"
    single_knee_color = "skyblue"
    double_knee_color = "lightgreen"
    combined_double_color = "lightsteelblue"

    return single_foot_color, double_foot_color, single_knee_color, double_knee_color, combined_double_color


def color_function_support_poses(support_pose_name):
    single_foot_color, double_foot_color, single_knee_color, double_knee_color, combined_double_color = \
        get_taxonomy_colors()

    if 'f2' in support_pose_name:
        color = double_foot_color
    elif 'k2' in support_pose_name:
        color = double_knee_color
    elif 'fk' in support_pose_name or 'kf' in support_pose_name:
        color = combined_double_color
    elif 'k' in support_pose_name:
        color = single_knee_color
    else:
        color = single_foot_color

    return color


def color_function_contact_shape_poses(shape_pose_name):
    single_foot_color, double_foot_color, single_knee_color, double_knee_color, combined_double_color = \
        get_taxonomy_colors()

    if shape_pose_name.count('Foot') == 2:
        color = double_foot_color
    elif shape_pose_name.count('Knee') == 2:
        color = double_knee_color
    elif shape_pose_name.count('Foot') == 1 and shape_pose_name.count('Knee') == 1:
        color = combined_double_color
    elif shape_pose_name.count('Knee') == 1:
        color = single_knee_color
    else:
        color = single_foot_color

    return color


def color_function_shape_poses(shape_pose_name):
    single_foot_color, double_foot_color, single_knee_color, double_knee_color, combined_double_color = \
        get_taxonomy_colors()

    if shape_pose_name.count('Foot') == 2:
        if shape_pose_name.count('Hand') == 2:
            color = "maroon"
        elif shape_pose_name.count('Hand') == 1:
            color = "rosybrown"
        else:
            color = "lightcoral"

    elif shape_pose_name.count('Knee') == 2:
        if shape_pose_name.count('Hand') == 2:
            color = "navy"
        elif shape_pose_name.count('Hand') == 1:
            color = "royalblue"
        else:
            color = "skyblue"

    elif shape_pose_name.count('Foot') == 1 and shape_pose_name.count('Knee') == 1:
        if shape_pose_name.count('Hand') == 2:
            color = "darkmagenta"
        elif shape_pose_name.count('Hand') == 1:
            color = "mediumorchid"
        else:
            color = "plum"

    elif shape_pose_name.count('Knee') == 1:
        if shape_pose_name.count('Hand') == 2:
            color = "darkgreen"
        elif shape_pose_name.count('Hand') == 1:
            color = "darkcyan"
        else:
            color = "turquoise"
    else:
        if shape_pose_name.count('Hand') == 2:
            color = "crimson"
        elif shape_pose_name.count('Hand') == 1:
            color = "darkorange"
        else:
            color = "gold"

    return color


def text_function_shape_poses(shape_pose_name_list):
    name_list = []
    for shape_name in shape_pose_name_list:
        name_list.append(shape_name.replace('Foot', 'F').replace('Knee', 'K').replace('Hand', 'H').replace('Left', 'L').replace('Right', 'R'))

    return name_list


def get_reduced_dataset_feet_5poses(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    lf_indices = [0, 3, 4, 8, 11]
    lf_lh_indices = [22, 23, 24, 25, 27]
    lfrf_lh_indices = [30, 32, 33, 34, 35]
    lfrf_indices = [42, 44, 46, 48, 54]
    lfrf_rh_indices = [72, 73, 74, 75, 76]
    lf_rh_indices = [87, 89, 90, 91, 92]
    rf_lh_indices = [97, 98, 100, 101, 104]
    rf_indices = [105, 109, 114, 116, 122]
    rf_rh_indices = [124, 126, 129, 130, 131]
    lfrf_lhrh_indices = [136, 137, 139, 141, 142]
    rf_lhrh_indices = [143, 144, 145, 146, 147]
    lf_lhrh_indices = [151, 152, 155, 156, 157]

    feet_indices = lf_indices + lf_lh_indices + lfrf_lh_indices + lfrf_indices +lfrf_rh_indices + lf_rh_indices + \
                   rf_lh_indices + rf_indices + rf_rh_indices + lfrf_lhrh_indices + rf_lhrh_indices + lf_lhrh_indices

    return training_data[feet_indices], joint_data[feet_indices], [shape_pose_names[i] for i in feet_indices], \
           joint_names


def get_reduced_dataset_2poses(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    lf_indices = [0, 8]
    lf_lh_indices = [23, 25]
    lfrf_lh_indices = [32, 34]
    lfrf_indices = [44, 46]
    lfrf_rh_indices = [73, 76]
    lf_rh_indices = [89, 90]
    rf_lh_indices = [97, 101]
    rf_indices = [105, 114]
    rf_rh_indices = [126, 129]
    lfrf_lhrh_indices = [137, 139]
    rf_lhrh_indices = [143, 147]
    lf_lhrh_indices = [151, 155]

    lf_rk_rh_indices = [160, 161]
    lf_rk_indices = [162]
    lk_lh_indices = [165, 169]
    rf_lk_lh_indices = [171, 175]
    lk_lhrh_indices = [176]  # not so clear that rh does not have contact
    lkrk_lh_indices = [177]
    rf_lk_lhrh_indices = [180]
    lkrk_lhrh_indices = [181]
    lk_indices = [182, 183]
    rf_lk_indices = [185, 186]
    lkrk_rh_indices = [188]
    lkrk_indices = [189]
    rk_rh_indices = [190]
    rk_indices = [191]

    feet_indices = lf_indices + lf_lh_indices + lfrf_lh_indices + lfrf_indices +lfrf_rh_indices + lf_rh_indices + \
                   rf_lh_indices + rf_indices + rf_rh_indices + lfrf_lhrh_indices + rf_lhrh_indices + lf_lhrh_indices

    knees_indices = lf_rk_rh_indices + lf_rk_indices + lk_lh_indices + rf_lk_lh_indices + lk_lhrh_indices + \
                    lkrk_lh_indices + rf_lk_lhrh_indices + lkrk_lhrh_indices + lkrk_lhrh_indices + lk_indices + \
                    rf_lk_indices + lkrk_rh_indices + lkrk_indices + rk_rh_indices + rk_indices

    all_indices = feet_indices + knees_indices

    return training_data[all_indices], joint_data[all_indices], [shape_pose_names[i] for i in all_indices], joint_names

def get_missing_data_feet5(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)

    # Missing indices, chosen randomly by hand.
    missing_indices = [
        # LF [0, 3, 4, 8, 11]
        2,
        # 20,
        # LFLH  [22, 23, 24, 25, 27]
        26,
        # 28,
        # LFRF-LH [30, 32, 33, 34, 35]
        31,
        # 40,
        # LFRF [42, 44, 46, 48, 54]
        47,
        # 70,
        # LFRF-RH [72, 73, 74, 75, 76]
        80,
        # 82,
        # LFRH [87, 89, 90, 91, 92]
        88,
        # 95,
        # rf_lh_indices = [97, 98, 100, 101, 104]
        99,
        # 102,
        # rf_indices = [105, 109, 114, 116, 122]
        107,
        # 112,
        # rf_rh_indices = [124, 126, 129, 130, 131]
        125,
        # 133,
        # lfrf_lhrh_indices = [136, 137, 139, 141, 142]
        135,
        # 140,
        # rf_lhrh_indices = [143, 144, 145, 146, 147]
        148,
        # 149,
        # lf_lhrh_indices = [151, 152, 155, 156, 157]
        153,
        # 158
    ]

    added_training_data = training_data[missing_indices]
    added_joint_data = joint_data[missing_indices]
    added_shape_pose_names = [shape_pose_names[idx] for idx in missing_indices]
    added_joint_names = []

    return (
        added_training_data,
        added_joint_data,
        added_shape_pose_names,
        added_joint_names
    )


def get_missing_indices_reduced_dataset_2poses():
    feet_missing_indices = [4, 24, 33, 42, 74, 91, 100, 109, 130, 141, 145, 152]
    knees_missing_indices = [159, 163, 168, 172, 184, 188]
    missing_indices = feet_missing_indices + knees_missing_indices
    return missing_indices


def get_missing_data_reduced_dataset_2poses(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    missing_indices = get_missing_indices_reduced_dataset_2poses()

    return training_data[missing_indices], joint_data[missing_indices], \
           [shape_pose_names[i] for i in missing_indices], joint_names



def get_reduced_dataset_knees_2poses(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    lf_rk_rh_indices = [160, 161]
    lf_rk_indices = [162]
    lk_lh_indices = [165, 169]
    rf_lk_lh_indices = [171, 175]
    lk_lhrh_indices = [176]  # not so clear that rh does not have contact
    lkrk_lh_indices = [177]
    rf_lk_lhrh_indices = [180]
    lkrk_lhrh_indices = [181]
    lk_indices = [182, 183]
    rf_lk_indices = [185, 186]
    lkrk_rh_indices = [188]
    lkrk_indices = [189]
    rk_rh_indices = [190]
    rk_indices = [191]

    knees_indices = lf_rk_rh_indices + lf_rk_indices + lk_lh_indices + rf_lk_lh_indices + lk_lhrh_indices + \
                    lkrk_lh_indices + rf_lk_lhrh_indices + lkrk_lhrh_indices + lkrk_lhrh_indices + lk_indices + \
                    rf_lk_indices + lkrk_rh_indices + lkrk_indices + rk_rh_indices + rk_indices

    return training_data[knees_indices], joint_data[knees_indices], [shape_pose_names[i] for i in knees_indices], \
           joint_names


def get_one_of_each_class_dataset(data_path: Path):
    """
    Returns only one of each class.
    """
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    lf_indices = [0] # 1F
    lf_lh_indices = [23] #1F-1H
    lfrf_lh_indices = [32] #2F-1H
    lfrf_indices = [44] #2F
    # lfrf_rh_indices = [73]
    # lf_rh_indices = [89]
    # rf_lh_indices = [97]
    # rf_indices = [105]
    # rf_rh_indices = [126]
    lfrf_lhrh_indices = [137] #2F-2H
    rf_lhrh_indices = [143] #1F-2H
    # lf_lhrh_indices = [151] 

    lf_rk_rh_indices = [160] # 1F-1K-1H
    lf_rk_indices = [162] # 1F-1K
    lk_lh_indices = [165] # 1K-1H
    # rf_lk_lh_indices = [171]
    lk_lhrh_indices = [176] # 1K-2H
    lkrk_lh_indices = [177] # 2K-1H
    rf_lk_lhrh_indices = [180] # 1F-1K-2H
    lkrk_lhrh_indices = [181] # 2K-2H
    lk_indices = [182] # 1K
    # rf_lk_indices = [185] 
    # lkrk_rh_indices = [188]
    lkrk_indices = [189] #2K
    # rk_rh_indices = [190] 
    # rk_indices = [191]

    feet_indices = lf_indices + lf_lh_indices + lfrf_lh_indices + lfrf_indices \
                   + lfrf_lhrh_indices + rf_lhrh_indices

    knees_indices = lf_rk_rh_indices + lf_rk_indices + lk_lh_indices + lk_lhrh_indices \
                    + lkrk_lh_indices + rf_lk_lhrh_indices + lkrk_lhrh_indices  + lk_indices \
                    + lkrk_indices

    all_indices = feet_indices + knees_indices

    return training_data[all_indices], joint_data[all_indices], [shape_pose_names[i] for i in all_indices], joint_names


def get_semifull_dataset(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    lf_indices = list(range(0, 15))[::2]
    lf_lh_indices = list(range(22, 28))
    lfrf_lh_indices = list(range(30, 36))
    lfrf_indices = list(range(42, 56))[::2]
    lfrf_rh_indices = list(range(72, 80))
    lf_rh_indices = list(range(87, 93))
    rf_lh_indices = list(range(97, 104))
    rf_indices = list(range(105, 121))[::2][:-1]
    rf_rh_indices = list(range(124, 133))[:-2]
    lfrf_lhrh_indices = list(range(135, 143))
    rf_lhrh_indices = list(range(143, 151))[:-1]
    lf_lhrh_indices = list(range(151, 159))[:-1]

    lf_rk_rh_indices = list(range(159, 162))
    lf_rk_indices = list(range(162, 165))
    lk_lh_indices = list(range(165, 170))
    rf_lk_lh_indices = list(range(170, 176))
    lk_lhrh_indices = [176]
    lkrk_lh_indices = list(range(177, 179))
    rf_lk_lhrh_indices = list(range(179, 181))
    lkrk_lhrh_indices = [181]
    lk_indices = [182, 183]
    rf_lk_indices = list(range(184, 188))
    lkrk_rh_indices = [188]
    lkrk_indices = [189]
    rk_rh_indices = [190]
    rk_indices = [191]

    feet_indices = lf_indices + lf_lh_indices + lfrf_lh_indices + lfrf_indices + lfrf_rh_indices + lf_rh_indices + \
                   rf_lh_indices + rf_indices + rf_rh_indices + lfrf_lhrh_indices + rf_lhrh_indices + lf_lhrh_indices

    knees_indices = lf_rk_rh_indices + lf_rk_indices + lk_lh_indices + rf_lk_lh_indices + lk_lhrh_indices + \
                    lkrk_lh_indices + rf_lk_lhrh_indices + lkrk_lhrh_indices + lkrk_lhrh_indices + lk_indices + \
                    rf_lk_indices + lkrk_rh_indices + lkrk_indices + rk_rh_indices + rk_indices

    all_indices = feet_indices + knees_indices

    # Remove "missing indices"
    missing_indices = get_missing_indices_semifull_dataset()
    all_indices = [idx for idx in all_indices if idx not in missing_indices]

    return training_data[all_indices], joint_data[all_indices], [shape_pose_names[i] for i in all_indices], joint_names


def get_missing_indices_semifull_dataset():
    feet_missing_indices = [8, 25, 32, 44, 73, 89, 97, 109, 126, 139, 143, 155]
    knees_missing_indices = [161, 163, 169, 171, 183, 186]
    missing_indices = feet_missing_indices + knees_missing_indices
    return missing_indices


def get_missing_data_semifull_dataset(data_path):
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path,
                                                                                         flag_take_all_cords=False)
    missing_indices = get_missing_indices_semifull_dataset()

    return training_data[missing_indices], joint_data[missing_indices], \
           [shape_pose_names[i] for i in missing_indices], joint_names


def reorder_taxonomy_data(data, shape_pose_names):
    reordered_data = []

    for pose in ordered_shape_pose_names:
        for n in range(len(data)):
            if shape_pose_names[n] == pose:
                reordered_data.append(data[n])

    return reordered_data


def augment_pose_data_with_contacts(pose_data, shape_pose_names):
    nb_shapes = pose_data.shape[0]
    contact_data = -0.1 * np.ones((nb_shapes, 4))  # 1 contact point for each of the 2 feet and hands

    # Check contacts, add them into a contact data matrix following the same order as the poses
    for n in range(nb_shapes):
        if 'LeftHand' in shape_pose_names[n]:
            contact_data[n, 0] = 0.1
        if 'RightHand' in shape_pose_names[n]:
            contact_data[n, 1] = 0.1
        if 'LeftFoot' in shape_pose_names[n]:
            contact_data[n, 2] = 0.1
        if 'RightFoot' in shape_pose_names[n]:
            contact_data[n, 3] = 0.1

    return np.hstack((pose_data, contact_data))


if __name__ == '__main__':
    data_path = '../../data/support_poses/keyPoseShapesXPose.xml'  # Path of XML file including Shape poses dataset
    training_data, joint_data, shape_pose_names, joint_names = parse_xml_pose_shape_data(data_path)  # Getting the data of interest
    training_data2, joint_data2, shape_pose_names2, joint_names2 = parse_xml_pose_shape_data(data_path, flag_take_new_ee_poses=False)  # Getting the data of interest
    print(Counter(shape_pose_names))

    graph_file_path = '../../data/support_poses/support_poses_closure.csv'  # Path of XML file including support poses relations
    # Map pose labels to support pose nodes
    support_pose_labels = shape_poses_label_to_support_pose_labels(shape_pose_names)
    # Compute distances
    shape_pose_graph_distances = nodes_distance_mapping(graph_file_path, support_pose_labels)

    # Reduced datasets
    training_data_r, joint_data_r, shape_pose_names_r, joint_names_r = get_reduced_dataset_2poses(data_path)
    training_data_f, joint_data_f, shape_pose_names_f, joint_names_f = get_reduced_dataset_feet_5poses(data_path)
    training_data_k, joint_data_k, shape_pose_names_k, joint_names_k = get_reduced_dataset_knees_2poses(data_path)
    training_data_s, joint_data_s, shape_pose_names_s, joint_names_s = get_semifull_dataset(data_path)

    np.savez('../../data/semifull', joint_data=joint_data_s, joint_names=joint_names_s)
    np.savez('../../data/feet5', joint_data=joint_data_f, joint_names=joint_names_f)

    print(shape_pose_names_r)
