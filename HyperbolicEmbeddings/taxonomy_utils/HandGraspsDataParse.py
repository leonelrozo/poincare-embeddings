from __future__ import division

from pathlib import Path

import numpy as np
import pandas
import torch
from xml.etree import ElementTree
import os
from operator import itemgetter

from HyperbolicEmbeddings.graph_utils.graph_functions import get_indices_nodes_graph, nodes_distance_mapping

# Hand grasp types as defined in [1]
# [1] F. Stival, et al. "A quantitative taxonomy of human hand grasps". 2019
HAND_GRASPS_NAMES = ["Lateral", "ExtensionType", "Quadpod", "ParallelExtension", "IndexFingerExtension", "Stick",
                     "WritingTripod", "PrismaticFourFingers", "PowerDisk", "LargeDiameter", "MediumWrap",
                     "SmallDiameter", "FixedHook", "Tripod", "PowerSphere", "PrecisionSphere", "ThreeFingersSphere",
                     "PrismaticPinch", "TipPinch", "Ring"]
ordered_hand_grasps_names = ["Lateral", "LateralAdded",
                             "ExtensionType", "ExtensionTypeAdded",
                             "Quadpod", "QuadpodAdded",
                             "ParallelExtension", "ParallelExtensionAdded",
                             "IndexFingerExtension", "IndexFingerExtensionAdded",
                             "Stick", "StickAdded",
                             "WritingTripod", "WritingTripodAdded",
                             "PrismaticFourFingers", "PrismaticFourFingersAdded",
                             "PowerDisk", "PowerDiskAdded",
                             "LargeDiameter", "LargeDiameterAdded",
                             "MediumWrap", "MediumWrapAdded",
                             "SmallDiameter", "SmallDiameterAdded",
                             "FixedHook", "FixedHookAdded",
                             "Tripod", "TripodAdded",
                             "PowerSphere", "PowerSphereAdded",
                             "PrecisionSphere", "PrecisionSphereAdded",
                             "ThreeFingersSphere", "ThreeFingersSphereAdded",
                             "PrismaticPinch", "PrismaticPinchAdded",
                             "TipPinch", "TipPinchAdded",
                             "Ring", "RingAdded"]
# Prismatic Pinch == Palmar Pinch
# ThreeFingersSphere missing in the data


def load_grasps_data(data_folder_path):
    # Load data
    data_folder_path = data_folder_path / "hand_grasps"
    hand_joint_data, grasp_names, joint_names = get_grasp_dataset(str(data_folder_path))
    grasp_names = list(grasp_names)

    # Mapping grasps labels to short labels associated to the tree leaves (for each grasp datapoint)
    hand_grasp_labels = hand_grasp_name_to_grasp_labels(grasp_names)

    # Adjacency matrix
    adjacency_file_path = data_folder_path / 'hand_grasps_closure.csv'
    adjacency_matrix, grasps_indices_in_tree = get_indices_nodes_graph(adjacency_file_path, hand_grasp_labels)

    # Distances
    # graph_file_path = data_folder_path / 'hand_grasps_taxonomy.csv'
    grasps_graph_distances = nodes_distance_mapping(adjacency_file_path, hand_grasp_labels)

    # Legend
    grasps_legend_for_plot = hand_grasp_name_to_grasp_labels(grasp_names)

    # Set training data
    training_data = torch.from_numpy(hand_joint_data)
    grasps_indices_in_tree = torch.from_numpy(grasps_indices_in_tree)

    return training_data, adjacency_matrix, grasps_graph_distances, grasp_names, grasps_indices_in_tree, \
           grasps_legend_for_plot, simple_color_function_grasp, joint_names


def load_missing_grasps_data(data_folder_path):
    # Load data
    training_data, adjacency_matrix, grasps_graph_distances, grasp_names, grasps_indices_in_tree, \
    grasps_legend_for_plot, simple_color_function_grasp, joint_names = load_grasps_data(data_folder_path)

    # Load data from subject 2091 as missing data
    data_folder_path = data_folder_path / "hand_grasps"
    added_hand_joint_data, added_grasp_names, _ = get_grasp_dataset(str(data_folder_path), subject_list=['2091'])
    added_grasp_names = list(added_grasp_names)

    # Mapping grasps labels to short labels associated to the tree leaves (for each grasp datapoint)
    hand_grasp_labels = hand_grasp_name_to_grasp_labels(grasp_names)
    added_hand_grasp_labels = hand_grasp_name_to_grasp_labels(added_grasp_names)

    # Adjacency matrix
    adjacency_file_path = data_folder_path / 'hand_grasps_closure.csv'
    adjacency_matrix, added_grasps_indices_in_tree = get_indices_nodes_graph(adjacency_file_path, added_hand_grasp_labels)

    # Distances
    # graph_file_path = data_folder_path / 'hand_grasps_taxonomy.csv'
    all_grasps_graph_distances = nodes_distance_mapping(adjacency_file_path, hand_grasp_labels + added_hand_grasp_labels)

    # Legend
    all_grasps_legend_for_plot = hand_grasp_name_to_grasp_labels(grasp_names + added_grasp_names)

    # Set training data
    added_data = torch.from_numpy(added_hand_joint_data)
    added_grasps_indices_in_tree = torch.from_numpy(added_grasps_indices_in_tree)

    return added_data, added_grasp_names, added_grasps_indices_in_tree, all_grasps_graph_distances, \
           all_grasps_legend_for_plot, simple_color_function_grasp


def parse_xml_hand_grasps_data(path: str):
    """
    Function that parses taxonomy info contained in an XML to python arrays.

    Parameters
    ----------
    path: String with the relative path where the XML file is

    Optional parameters
    -------------------


    Returns
    -------
    single_hand_joint_data (np.array): A single datapoint representing the hand joint values for the highest ball pos
    hand_joint_data (np.array): Full array of the hand joint values recorded during the whole grasping motion
    ball_motion_data (np.array): Full array of the 3D position of the ball recorded during the graspingg motion
    """
    # Data holders
    hand_joint_data = []
    ball_motion_data = []
    joint_names = []

    # load trajectory into demonstration
    root = ElementTree.parse(os.path.expanduser(path)).getroot()

    # Accessing the node to load joint kinematic data. There are two ways to do this:
    # 1. Via Xpath expression for locating elements in the XML tree as below (it requires to pass specific names)
    # kinematic_node = root.findall(".//*[@name='d_h_2123']/Sensors/Sensor/[@type='Kinematic']")
    # 2. Via the tree indexing using the known index of the desired node (in this case, Motion/Sensors/Sensor/Kinematic
    kinematic_node = root.findall('Motion')[1].findall('Sensors')[0].findall('Sensor')[1]
    # Getting joint names
    for joint_tag in kinematic_node.find('Configuration').findall('Joint'):
        joint_names.append(joint_tag.attrib['name'])
    # Getting the full recorded joint data
    for joint_value in kinematic_node.find('Data').iter('JointPosition'):
        str_joint_data = joint_value.text.replace(' ', ',').split(",")
        hand_joint_data.append(np.array([float(joint_value) for joint_value in str_joint_data]))
    hand_joint_data = np.array(hand_joint_data)

    # Accessing the tree node to load ball motion data.
    ball_node = root.findall('Motion')[0].findall('Sensors')[0].findall('Sensor')[0]
    # Getting the full recorded ball motion data
    for joint_value in ball_node.find('Data').iter('RootPosition'):
        str_ball_motion_data = joint_value.text.replace(' ', ',').split(",")
        ball_motion_data.append(np.array([float(joint_value) for joint_value in str_ball_motion_data]))
    ball_motion_data = np.array(ball_motion_data)

    # Check highest vertical position of the ball to extract a single stable hand grasp configuration
    id_max_z_ball = np.argmax(ball_motion_data[:, 2])
    single_hand_joint_data = hand_joint_data[id_max_z_ball, :]

    return single_hand_joint_data, hand_joint_data, ball_motion_data, joint_names


def hand_grasp_name_to_grasp_labels(hand_grasp_names):
    """
    This function maps the hand grasps names presented in [1] to names of nodes of the grasping taxonomy.
    """
    grasp_labels = [''] * len(hand_grasp_names)

    # For loop to map pose labels to support pose nodes
    for grasp_id, grasp_name in enumerate(hand_grasp_names):
        if grasp_name == 'Lateral':
            grasp_labels[grasp_id] = 'La'
        elif grasp_name == 'ExtensionType':
            grasp_labels[grasp_id] = 'ET'
        elif grasp_name == 'Quadpod':
            grasp_labels[grasp_id] = 'Qu'
        elif grasp_name == 'ParallelExtension':
            grasp_labels[grasp_id] = 'PE'
        elif grasp_name == 'IndexFingerExtension':
            grasp_labels[grasp_id] = 'IE'
        elif grasp_name == 'Stick':
            grasp_labels[grasp_id] = 'St'
        elif grasp_name == 'WritingTripod':
            grasp_labels[grasp_id] = 'WT'
        elif grasp_name == 'PrismaticFourFingers':
            grasp_labels[grasp_id] = 'PF'
        elif grasp_name == 'PowerDisk':
            grasp_labels[grasp_id] = 'PD'
        elif grasp_name == 'LargeDiameter':
            grasp_labels[grasp_id] = 'LD'
        elif grasp_name == 'MediumWrap':
            grasp_labels[grasp_id] = 'MW'
        elif grasp_name == 'SmallDiameter':
            grasp_labels[grasp_id] = 'SD'
        elif grasp_name == 'FixedHook':
            grasp_labels[grasp_id] = 'FH'
        elif grasp_name == 'Tripod':
            grasp_labels[grasp_id] = 'Tr'
        elif grasp_name == 'PowerSphere':
            grasp_labels[grasp_id] = 'PS'
        elif grasp_name == 'PrecisionSphere':
            grasp_labels[grasp_id] = 'RS'
        elif grasp_name == 'ThreeFingersSphere':
            grasp_labels[grasp_id] = 'TS'
        elif grasp_name == 'PrismaticPinch':  # = Palmar pinch
            grasp_labels[grasp_id] = 'PP'
        elif grasp_name == 'TipPinch':
            grasp_labels[grasp_id] = 'TP'
        elif grasp_name == 'Ring':
            grasp_labels[grasp_id] = 'Ri'

    return grasp_labels


def get_grasp_dataset(data_path, subject_list=None):
    if not subject_list:
        subject_list = ['2122', '2123', '2124', '2125', '2177', '2177b', '2177c']

    joint_data = []
    grasp_names = []

    for subject in subject_list:
        for grasp in HAND_GRASPS_NAMES:
            # Path and check if it exists
            path = data_path + '/' + subject + '/' + grasp + '.xml'

            if os.path.isfile(path):
                single_hand_joint_data, hand_joint_data, object_motion_data, joint_names = \
                    parse_xml_hand_grasps_data(path)
                joint_data.append(single_hand_joint_data)
                grasp_names.append(grasp)

    joint_data = np.array(joint_data)

    idx = range(len(grasp_names))
    sorted_idx = sorted(idx, key=lambda x: grasp_names[x])

    grasp_names = itemgetter(*sorted_idx)(grasp_names)
    joint_data = joint_data[sorted_idx]

    return joint_data, grasp_names, joint_names


def simple_color_function_grasp(grasp_name):
    if grasp_name == 'Lateral':
        color = "darkgreen"  # (0.0, 0.39215686274509803, 0.0)
    elif grasp_name == 'ExtensionType':
        color = (0.0, 0.1, 0.0)  # "darkgreen"
    elif grasp_name == 'Quadpod':
        color = (0.0, 0.7, 0.0)  # "darkgreen"
    elif grasp_name == 'ParallelExtension':
        color = "aquamarine"
    elif grasp_name == 'IndexFingerExtension':
        color = "gray"
    elif grasp_name == 'Stick':
        color = (0.8, 0.5, 0.0)  # "darkorange"
    elif grasp_name == 'WritingTripod':
        color = (1.0, 0.4, 0.0)  # "darkorange"  # (1.0, 0.5490196078431373, 0.0)
    elif grasp_name == 'PrismaticFourFingers':
        color = (1.0, 0.7, 0.0)  # "orange"  # (1.0, 0.6470588235294118, 0.0)
    elif grasp_name == 'PowerDisk':
        color = (0.6, 0.3, 0.0)  # "darkorange"
    elif grasp_name == 'LargeDiameter':
        color = "royalblue"
    elif grasp_name == 'MediumWrap':
        color = (0.5, 0.7, 1.0)  # "royalblue"
    elif grasp_name == 'SmallDiameter':
        color = "mediumblue"
    elif grasp_name == 'FixedHook':
        color = (0.0, 0.0, 0.3)
    elif grasp_name == 'Tripod':
        color = (0.8, 0.8, 0.0)  # "gold"
    elif grasp_name == 'PowerSphere':
        color = "gold"  # (1.0, 0.8431372549019608, 0.0)
    elif grasp_name == 'PrecisionSphere':
        color = (1.0, 0.9, 0.7)  # "gold"
    elif grasp_name == 'ThreeFingersSphere':
        color = (0.65, 0.0, 0.1)  # "crimson"
    elif grasp_name == 'PrismaticPinch':  # = Palmar pinch
        color = (1.0, 0.6, 0.6)  # "crimson"
    elif grasp_name == 'TipPinch':
        color = "crimson"
    elif grasp_name == 'Ring':
        color = (0.6, 0.0, 0.)  # "crimson"

    return color


def color_function_grasp(grasp_name):
    if grasp_name == 'Lateral':
        color = "darkgreen"
    elif grasp_name == 'ExtensionType':
        color = "darkcyan"
    elif grasp_name == 'Quadpod':
        color = "turquoise"
    elif grasp_name == 'ParallelExtension':
        color = "aquamarine"
    elif grasp_name == 'IndexFingerExtension':
        color = "gray"
    elif grasp_name == 'Stick':
        color = "darkorange"
    elif grasp_name == 'WritingTripod':
        color = "maroon"
    elif grasp_name == 'PrismaticFourFingers':
        color = "rosybrown"
    elif grasp_name == 'PowerDisk':
        color = "lightcoral"
    elif grasp_name == 'LargeDiameter':
        color = "navy"
    elif grasp_name == 'MediumWrap':
        color = "royalblue"
    elif grasp_name == 'SmallDiameter':
        color = "skyblue"
    elif grasp_name == 'FixedHook':
        color = "lightskyblue"
    elif grasp_name == 'Tripod':
        color = "gold"
    elif grasp_name == 'PowerSphere':
        color = "khaki"
    elif grasp_name == 'PrecisionSphere':
        color = "lemonchiffon"
    elif grasp_name == 'ThreeFingersSphere':
        color = ""
    elif grasp_name == 'PrismaticPinch':  # = Palmar pinch
        color = "darkmagenta"
    elif grasp_name == 'TipPinch':
        color = "mediumorchid"
    elif grasp_name == 'Ring':
        color = "plum"

    return color


if __name__ == '__main__':
    # Path of an XML file with hand grasp data
    data_path = '../../data/hand_grasps/2123/PrecisionSphere.xml'
    grasp_data = parse_xml_hand_grasps_data(data_path)  # Getting the data of interest

    # Mapping grasps labels to short labels associated to the tree leaves (for each grasp datapoint)
    hand_grasp_labels = hand_grasp_name_to_grasp_labels(HAND_GRASPS_NAMES)

    # Path of a CVS file with the hand grasp taxonomy
    graph_file_path = '../../data/hand_grasps/hand_grasps_taxonomy.csv'
    hand_grasps_graph_dist = nodes_distance_mapping(graph_file_path, hand_grasp_labels)

    # Get 1 data per subject and grasp
    data_path = '../../data/hand_grasps'
    joint_data, grasp_names, joint_names = get_grasp_dataset(data_path)
    print(joint_data)
    print(joint_names)

    idx = range(len(grasp_names))
    sorted_idx = sorted(idx, key=lambda x: grasp_names[x])

    sorted_grasp_names = itemgetter(*sorted_idx)(grasp_names)
    sorted_joint_data = itemgetter(*sorted_idx)(joint_data)




