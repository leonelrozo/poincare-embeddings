from __future__ import division

from pathlib import Path

import numpy as np
import pandas as pd
import torch
from xml.etree import ElementTree
import os
from operator import itemgetter
from collections import Counter

from HyperbolicEmbeddings.graph_utils.graph_functions import get_indices_nodes_graph, nodes_distance_mapping

BIMANUAL_MANIPULATION_NAMES = ["unimanual_left", "unimanual_right", "bimanual", "loosely_coupled",
                               "tightly_coupled_asym_l", "tightly_coupled_asym_r", "tightly_coupled_sym"]

ordered_bimanual_names = ["unimanual_left", "unimanual_leftAdded",
                          "unimanual_right", "unimanual_rightAdded",
                          "bimanual", "bimanualAdded",
                          "loosely_coupled", "loosely_coupledAdded",
                          "tightly_coupled_asym_l", "tightly_coupled_asym_lAdded",
                          "tightly_coupled_asym_r", "tightly_coupled_asym_rAdded",
                          "tightly_coupled_sym", "tightly_coupled_symAdded"
                          ]

FINGERS_JOINT_LIMITS = {
    'LeftFingerJoint11y_joint': [-np.pi/4, np.pi/4],
    'LeftFingerJoint11z_joint': [0, np.pi/2],
    'LeftFingerJoint12y_joint': [0, np.pi/2],
    'LeftFingerJoint13y_joint': [0, np.pi/2],
    'LeftFingerJoint21x_joint': [-np.pi/4, np.pi/4],
    'LeftFingerJoint21y_joint': [0, np.pi/2],
    'LeftFingerJoint22y_joint': [0, np.pi/2],
    'LeftFingerJoint23y_joint': [0, np.pi/2],
    'LeftFingerJoint31y_joint': [0, np.pi/2],
    'LeftFingerJoint32y_joint': [0, np.pi/2],
    'LeftFingerJoint33y_joint': [0, np.pi/2],
    'LeftFingerJoint41x_joint': [-np.pi/4, np.pi/4],
    'LeftFingerJoint41y_joint': [0, np.pi/2],
    'LeftFingerJoint42y_joint': [0, np.pi/2],
    'LeftFingerJoint43y_joint': [0, np.pi/2],
    'LeftFingerJoint51x_joint': [-np.pi/4, np.pi/4],
    'LeftFingerJoint51y_joint': [0, np.pi/2],
    'LeftFingerJoint52y_joint': [0, np.pi/2],
    'LeftFingerJoint53y_joint': [0, np.pi/2],
    'RightFingerJoint11y_joint': [-np.pi/4, np.pi/4],
    'RightFingerJoint11z_joint': [0, np.pi/2],
    'RightFingerJoint12y_joint': [0, np.pi/2],
    'RightFingerJoint13y_joint': [0, np.pi/2],
    'RightFingerJoint21x_joint': [-np.pi/4, np.pi/4],
    'RightFingerJoint21y_joint': [0, np.pi/2],
    'RightFingerJoint22y_joint': [0, np.pi/2],
    'RightFingerJoint23y_joint': [0, np.pi/2],
    'RightFingerJoint31y_joint': [0, np.pi/2],
    'RightFingerJoint32y_joint': [0, np.pi/2],
    'RightFingerJoint33y_joint': [0, np.pi/2],
    'RightFingerJoint41x_joint': [-np.pi/4, np.pi/4],
    'RightFingerJoint41y_joint': [0, np.pi/2],
    'RightFingerJoint42y_joint': [0, np.pi/2],
    'RightFingerJoint43y_joint': [0, np.pi/2],
    'RightFingerJoint51x_joint': [-np.pi/4, np.pi/4],
    'RightFingerJoint51y_joint': [0, np.pi/2],
    'RightFingerJoint52y_joint': [0, np.pi/2],
    'RightFingerJoint53y_joint': [0, np.pi/2]
}


def load_bimanual_manipulation_data(data_folder_path):
    # Load data
    data_folder_path = data_folder_path / "bimanual_manipulation"
    joint_data, bimanual_names, joint_names = get_bimanual_manipulation_dataset(str(data_folder_path))

    # Adjacency matrix
    adjacency_file_path = data_folder_path / 'bimanual_manipulation_closure.csv'
    adjacency_matrix, bimanual_indices_in_tree = get_indices_nodes_graph(adjacency_file_path, bimanual_names)
    bimanual_names = list(bimanual_names)

    # Distances
    bimanual_graph_distances = nodes_distance_mapping(adjacency_file_path, bimanual_names)

    # Legend
    bimanual_legend_for_plot = bimanual_names

    # Set training data
    training_data = torch.from_numpy(joint_data)
    bimanual_indices_in_tree = torch.from_numpy(bimanual_indices_in_tree)

    return training_data, adjacency_matrix, bimanual_graph_distances, bimanual_names, bimanual_indices_in_tree, \
           bimanual_legend_for_plot, simple_color_function_bimanual, joint_names


def load_missing_bimanual_manipulation_data(data_folder_path):
    # Load data
    training_data, adjacency_matrix, bimanual_graph_distances, bimanual_names, bimanual_indices_in_tree, \
           bimanual_legend_for_plot, simple_color_function_bimanual, _ = load_bimanual_manipulation_data(data_folder_path)

    # Load data from subject 2091 as missing data
    data_folder_path = data_folder_path / "bimanual_manipulation"
    cut_indices = [
        150,  # loosely coupled
        400,  # tightly coupled asym r
    ]
    peel_indices = [
        45,  # unimanual left
        300,  # loosely coupled
        460,  # tightly coupled asym r
    ]
    roll_indices = [
        # ## 20, 50,  # loosely coupled
        250, 450,  # tightly coupled sym
    ]
    stir_indices = []
    wipe_indices = [
        45,  # unimanual left
    ]

    indices = {
        'Cut': cut_indices,
        'Peel': peel_indices,
        'Roll': roll_indices,
        'Stir': stir_indices,
        'Wipe': wipe_indices
    }
    added_joint_data, added_bimanual_names, joint_names = get_bimanual_manipulation_dataset(str(data_folder_path),
                                                                                            indices=indices)
    added_bimanual_names = list(added_bimanual_names)

    # Adjacency matrix
    adjacency_file_path = data_folder_path / 'bimanual_manipulation_closure.csv'
    adjacency_matrix, added_bimanual_indices_in_tree = get_indices_nodes_graph(adjacency_file_path, added_bimanual_names)

    # Distances
    # graph_file_path = data_folder_path / 'hand_grasps_taxonomy.csv'
    all_bimanual_graph_distances = nodes_distance_mapping(adjacency_file_path, bimanual_names + added_bimanual_names)

    # Legend
    all_bimanual_legend_for_plot = bimanual_names + added_bimanual_names

    # Set training data
    added_data = torch.from_numpy(added_joint_data)
    added_grasps_indices_in_tree = torch.from_numpy(added_bimanual_indices_in_tree)

    return added_data, added_bimanual_names, added_grasps_indices_in_tree, all_bimanual_graph_distances, \
           all_bimanual_legend_for_plot, simple_color_function_bimanual


def get_bimanual_manipulation_dataset(data_path, subject_list=None, motion_list=None, indices=None):
    if not subject_list:
        subject_list = ['1723']
    if not motion_list:
        motion_list = ['Cut', 'Peel', 'Roll', 'Stir', 'Wipe']
        # The motions above for subject 1723 have less issues with fingers joint angle
        # Cut has issues with finger joint angles at the end of the motion -> avoid these data
        # We still fix fingers joints within fingers joint limits before returning the data
    if not indices:
        cut_indices = [
                        200,  # loosely coupled
                        300, 450,  # tightly coupled asym r
                       ]
        peel_indices = [
                        20, 30, 35, 40, 50,  # unimanual left
                        250, 450,   # loosely coupled
                        350, 480, 650,  # tightly coupled asym r
                        ]
        roll_indices = [
                        # ## 20, 50,  # loosely coupled
                        200, 300, 400, 500, 600,  # tightly coupled sym
                        ]
        stir_indices = [
                        150,  # loosely coupled
                        320, 380,  # tightly coupled asym r
                        ]
        wipe_indices = [
                        20, 30, 40, 50, 60,  # unimanual left
                        120,  # loosely coupled
                        200, 240, 280,  # tightly coupled asym r
                        ]
        # tightly coupled asym r : total 10
        # tightly coupled asym l : total 10 (mirrored)
        # unimanual left : total 10
        # unimanual right : total 10 (mirrored)
        # tightly coupled sym: total 5 + 5 (mirrored)
        # loosely coupled: total 5 + 5 (mirrored)

        indices = {
            'Cut': cut_indices,
            'Peel': peel_indices,
            'Roll': roll_indices,
            'Stir': stir_indices,
            'Wipe': wipe_indices
        }

    joint_data = []
    bimanual_names = []

    for subject in subject_list:
        for motion in motion_list:
            # Path and check if it exists
            path = data_path + '/' + subject + '/' + motion + '.xml'

            if os.path.isfile(path):
                # Load data
                motion_joint_data, joint_names, annotations = \
                    parse_xml_bimanual_manipulation_data(path)

                # Select a subset of data
                motion_joint_data = motion_joint_data[indices[motion]]
                annotations = annotations[indices[motion]]

                # Sort joint names
                idx = range(len(joint_names))
                sorted_idx = sorted(idx, key=lambda x: joint_names[x])
                joint_names = list(itemgetter(*sorted_idx)(joint_names))
                motion_joint_data = motion_joint_data[:, sorted_idx]

                # Append data
                joint_data.append(motion_joint_data)
                bimanual_names += list(annotations)

                # Mirror motion
                motion_joint_data_mirror, joint_names_mirror, annotations = mirror_motion(np.copy(motion_joint_data),
                                                                            joint_names.copy(), annotations)
                # Sort mirrored data to follow the same order as joint names
                sorted_idx = sorted(idx, key=lambda x: joint_names_mirror[x])
                joint_names_mirror = list(itemgetter(*sorted_idx)(joint_names_mirror))
                motion_joint_data_mirror = motion_joint_data_mirror[:, sorted_idx]

                # Append mirrored data
                joint_data.append(motion_joint_data_mirror)
                bimanual_names += list(annotations)

    joint_data = np.vstack(joint_data)

    idx = range(len(bimanual_names))
    sorted_idx = sorted(idx, key=lambda x: bimanual_names[x])

    bimanual_names = itemgetter(*sorted_idx)(bimanual_names)
    joint_data = joint_data[sorted_idx]

    # Fix fingers in joint limits
    for finger, limits in FINGERS_JOINT_LIMITS.items():
        joint_data[joint_data[:, joint_names.index(finger)] > limits[1], joint_names.index(finger)] = limits[1]
        joint_data[joint_data[:, joint_names.index(finger)] < limits[0], joint_names.index(finger)] = limits[0]

    return joint_data, bimanual_names, joint_names


def parse_xml_bimanual_manipulation_data(path: str, mirror=False):
    """
    Function that parses taxonomy info contained in an XML to python arrays.

    Parameters
    ----------
    path: String with the relative path where the XML file is

    Optional parameters
    -------------------


    Returns
    -------
    single_hand_joint_data (np.array): A single datapoint representing the hand joint values for the highest ball pos
    joint_data (np.array): Full array of the hand joint values recorded during the whole grasping motion
    ball_motion_data (np.array): Full array of the 3D position of the ball recorded during the graspingg motion
    """
    # load trajectory into demonstration
    root = ElementTree.parse(os.path.expanduser(path)).getroot()

    # Accessing the node to load joint kinematic data. There are two ways to do this:
    # 1. Via Xpath expression for locating elements in the XML tree as below (it requires to pass specific names)
    # kinematic_node = root.findall(".//*[@name='d_h_2123']/Sensors/Sensor/[@type='Kinematic']")
    # 2. Via the tree indexing using the known index of the desired node (in this case, Motion/Sensors/Sensor/Kinematic
    for motion in root.findall('Motion'):
        if motion.attrib['name'] == '1480' or motion.attrib['name'] == '1723':
            motion_node = motion

    # Body joints
    joint_data = []
    joint_names = []
    timesteps = []
    kinematic_node = motion_node.findall('Sensors')[0].findall('Sensor')[1]
    # Getting joint names
    for joint_tag in kinematic_node.find('Configuration').findall('Joint'):
        joint_names.append(joint_tag.attrib['name'])
    # Getting the full recorded joint data
    for measurement in kinematic_node.find('Data').iter('Measurement'):
        timesteps.append(float(measurement.attrib['timestep']))
        str_joint_data = measurement.find('JointPosition').text.replace(' ', ',').split(",")
        joint_data.append(np.array([float(joint_value) for joint_value in str_joint_data]))
    joint_data_body = np.array(joint_data)
    joint_names_body = joint_names
    timesteps_body = np.array(timesteps)

    # Add missing body joints
    joint_names_body += ['LSCy_joint', 'LSCz_joint', 'RSCy_joint', 'RSCz_joint']
    joint_data_body = np.hstack((joint_data_body, np.zeros((joint_data_body.shape[0], 4))))

    # Left hand joints
    joint_data = []
    joint_names = []
    timesteps = []
    kinematic_node = motion_node.findall('Sensors')[0].findall('Sensor')[2]
    # Getting joint names
    for joint_tag in kinematic_node.find('Configuration').findall('Joint'):
        joint_names.append(joint_tag.attrib['name'])
    # Getting the full recorded joint data
    for measurement in kinematic_node.find('Data').iter('Measurement'):
        timesteps.append(float(measurement.attrib['timestep']))
        str_joint_data = measurement.find('JointPosition').text.replace(' ', ',').split(",")
        joint_data.append(np.array([float(joint_value) for joint_value in str_joint_data]))
    joint_data_lhand = np.array(joint_data)
    joint_names_lhand = joint_names
    timesteps_lhand = np.array(timesteps)

    # Right hand joints
    joint_data = []
    joint_names = []
    timesteps = []
    kinematic_node = motion_node.findall('Sensors')[0].findall('Sensor')[3]
    # Getting joint names
    for joint_tag in kinematic_node.find('Configuration').findall('Joint'):
        joint_names.append(joint_tag.attrib['name'])
    # Getting the full recorded joint data
    for measurement in kinematic_node.find('Data').iter('Measurement'):
        timesteps.append(float(measurement.attrib['timestep']))
        str_joint_data = measurement.find('JointPosition').text.replace(' ', ',').split(",")
        joint_data.append(np.array([float(joint_value) for joint_value in str_joint_data]))
    joint_data_rhand = np.array(joint_data)
    joint_names_rhand = joint_names
    timesteps_rhand = np.array(timesteps)

    # TODO joint data are not aligned in time
    # Align data in time
    body_serie = pd.DataFrame(joint_data_body, timesteps_body)
    lhand_serie = pd.DataFrame(joint_data_lhand, timesteps_lhand)
    rhand_serie = pd.DataFrame(joint_data_rhand, timesteps_rhand)
    df = body_serie.join(lhand_serie, how="outer", rsuffix=-1).join(rhand_serie, how="outer", rsuffix=-1)
    # Interpolate for missing frames
    df = df.interpolate(method='polynomial', order=5)
    # Select only body timeframes (more regular than hands)
    init_time = np.max((timesteps_body[0], timesteps_lhand[0], timesteps_rhand[0]))
    end_time = np.min((timesteps_body[-1], timesteps_lhand[-1], timesteps_rhand[-1]))
    df = df.loc[df.index.intersection(pd.Index(timesteps_body))].loc[init_time:end_time]
    # Extract values
    joint_data = df.values
    joint_names = joint_names_body + joint_names_lhand + joint_names_rhand

    # Accessing the tree node to load taxonomy segmentation
    segmentation_node = root.findall('Segment')[0].findall('Segmentation')[2]
    # Getting the annotations
    start_time = []
    end_time = []
    annotations_list = []
    for segment in segmentation_node.iter('Segment'):
        start_time.append(segment.attrib['start'])
        end_time.append(segment.attrib['end'])
        annotations_list.append(segment.find('Annotation').text)
    start_time = np.array(start_time)
    end_time = np.array(end_time)

    # Annotations for each time step
    annotations = np.empty(timesteps_body.shape[0], dtype=object)
    for s in range(len(annotations_list)):
        annotations[np.where((timesteps_body > float(start_time[s])) & (timesteps_body <= float(end_time[s])))] = annotations_list[s]

    if mirror:
        return mirror_motion(joint_data, joint_names, annotations)

    return joint_data, joint_names, annotations


def simple_color_function_bimanual(bimanual_names):
    if bimanual_names == 'unimanual_left':
        color = "darkgreen"
    elif bimanual_names == 'unimanual_right':
        color = "aquamarine"
    elif bimanual_names == 'bimanual':
        color = "royalblue"
    elif bimanual_names == 'loosely_coupled':
        color = "gray"
    elif bimanual_names == 'tightly_coupled_asym_l':
        color = "gold"
    elif bimanual_names == 'tightly_coupled_asym_r':
        color = "darkorange"
    elif bimanual_names == 'tightly_coupled_sym':
        color = "crimson"

    return color


def mirror_motion(joint_data, joint_names, annotations):
    mapping_dict = {
        'LSx_joint': 1,
        'RSx_joint': 1,
        'LSy_joint': -1,
        'RSy_joint': -1,
        'LSz_joint': -1,
        'RSz_joint': -1,
        'LEx_joint': 1,
        'REx_joint': 1,
        'LEz_joint': -1,
        'REz_joint': -1,
        'LWx_joint': 1,
        'RWx_joint': 1,
        'LWy_joint': -1,
        'RWy_joint': -1,
        'LSCz_joint': -1,
        'RSCz_joint': -1,
        'LSCy_joint': -1,
        'RSCy_joint': -1,
        'LAx_joint': 1,
        'RAx_joint': 1,
        'LAy_joint': -1,
        'RAy_joint': -1,
        'LAz_joint': -1,
        'RAz_joint': -1,
        'LHx_joint': 1,
        'RHx_joint': 1,
        'LHy_joint': -1,
        'RHy_joint': -1,
        'LHz_joint': -1,
        'RHz_joint': -1,
        'LKx_joint': 1,
        'RKx_joint': 1,
        'LFx_joint': 1,
        'RFx_joint': 1,
        'LMrot_joint': -1,
        'RMrot_joint': -1,
        'BLNx_joint': 1,
        'BLNy_joint': -1,
        'BLNz_joint': -1,
        'BPx_joint': 1,
        'BPy_joint': -1,
        'BPz_joint': -1,
        'BTx_joint': 1,
        'BTy_joint': -1,
        'BTz_joint': -1,
        'BUNx_joint': 1,
        'BUNy_joint': -1,
        'BUNz_joint': -1,
        'LeftFingerJoint11y_joint': 1,
        'LeftFingerJoint11z_joint': 1,
        'LeftFingerJoint12y_joint': 1,
        'LefttFingerJoint13y_joint': 1,
        'LeftFingerJoint21x_joint': -1,
        'LeftFingerJoint21y_joint': 1,
        'LeftFingerJoint22y_joint': 1,
        'LeftFingerJoint23y_joint': 1,
        'LeftFingerJoint31x_joint': -1,
        'LeftFingerJoint31y_joint': 1,
        'LeftFingerJoint32y_joint': 1,
        'LeftFingerJoint33y_joint': 1,
        'LeftFingerJoint41x_joint': -1,
        'LeftFingerJoint41y_joint': 1,
        'LeftFingerJoint42y_joint': 1,
        'LeftFingerJoint43y_joint': 1,
        'LeftFingerJoint51x_joint': -1,
        'LeftFingerJoint51y_joint': 1,
        'LeftFingerJoint52y_joint': 1,
        'LeftFingerJoint53y_joint': 1,
        'RightFingerJoint11y_joint': 1,
        'RightFingerJoint11z_joint': 1,
        'RightFingerJoint12y_joint': 1,
        'RightFingerJoint13y_joint': 1,
        'RightFingerJoint21x_joint': -1,
        'RightFingerJoint21y_joint': 1,
        'RightFingerJoint22y_joint': 1,
        'RightFingerJoint23y_joint': 1,
        'RightFingerJoint31x_joint': -1,
        'RightFingerJoint31y_joint': 1,
        'RightFingerJoint32y_joint': 1,
        'RightFingerJoint33y_joint': 1,
        'RightFingerJoint41x_joint': -1,
        'RightFingerJoint41y_joint': 1,
        'RightFingerJoint42y_joint': 1,
        'RightFingerJoint43y_joint': 1,
        'RightFingerJoint51x_joint': -1,
        'RightFingerJoint51y_joint': 1,
        'RightFingerJoint52y_joint': 1,
        'RightFingerJoint53y_joint': 1,
    }

    # original_joint_data = joint_data.copy()

    # Modify joint angles
    joint_names_array = np.array(joint_names)
    for joint in mapping_dict:
        joint_data[:, np.where(joint_names_array == joint)] *= mapping_dict[joint]

    # Swap names
    for n in range(len(joint_names)):
        joint = joint_names[n]
        if 'Left' in joint:
            joint_names[n] = joint.replace('Left', 'Right')
        elif 'Right' in joint:
            joint_names[n] = joint.replace('Right', 'Left')
        elif 'L' == joint[0]:
            joint_names[n] = joint.replace('L', 'R')
        elif 'R' == joint[0]:
            joint_names[n] = joint.replace('R', 'L')


    # Annotations
    idx_left = np.where(annotations == "unimanual_left")
    idx_right = np.where(annotations == "unimanual_right")
    annotations[idx_left] = "unimanual_right"
    annotations[idx_right] = "unimanual_left"
    idx_left = np.where(annotations == "tightly_coupled_asym_l")
    idx_right = np.where(annotations == "tightly_coupled_asym_r")
    annotations[idx_left] = "tightly_coupled_asym_r"
    annotations[idx_right] = "tightly_coupled_asym_l"

    return joint_data, joint_names, annotations


if __name__ == '__main__':
    # # Path of an XML file with hand grasp data
    # data_path = [
    #     '../../data/bimanual_manipulation/1480/1480_Cut2_c_0_2cm_01.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Cut2_c_0_2cm_01.xml',
    #     '../../data/bimanual_manipulation/1480/1480_Peel1_cucumber_cb_05.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Peel1_cucumber_cb_04.xml',
    #     '../../data/bimanual_manipulation/1480/1480_Roll1_dough_start_04.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Roll1_dough_start_05.xml',
    #     '../../data/bimanual_manipulation/1480/1480_Stir1_cl_t_right_01.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Stir1_bc_s_right_01.xml',
    #     '../../data/bimanual_manipulation/1480/1480_Wipe1_pl_sp_front_angle_01.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Wipe1_pl_sp_front_angle_01.xml'
    #              ]
    #
    # # The motions below have less issues with fingers joint angles
    # data_path = [
    #     '../../data/bimanual_manipulation/1723/1723_Cut2_c_0_2cm_01.xml',  # Has issues with finger joint angles at the end
    #     '../../data/bimanual_manipulation/1723/1723_Peel1_cucumber_cb_04.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Roll1_dough_start_05.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Stir1_bc_s_right_01.xml',
    #     '../../data/bimanual_manipulation/1723/1723_Wipe1_pl_sp_front_angle_01.xml'
    # ]
    #
    # for path in data_path:
    #     joint_data, joint_names, annotations = parse_xml_bimanual_manipulation_data(path)  # Getting the data of interest
    #     print(annotations)
    #
    #     mir_joint_data, mir_joint_names, mir_annotations = mirror_motion(joint_data, joint_names, annotations)

    # Get dataset
    get_bimanual_manipulation_dataset('../../data/bimanual_manipulation')