"""
This script loads up a trained model and computes the mean
stress of an encoded subset of the data, provided by the user.
"""

from typing import List, Union
from pathlib import Path

import torch
from gpytorch.kernels import Kernel

from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood

from HyperbolicEmbeddings.gplvm.gplvm_optimization import _get_extra_mll_args
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import HyperbolicExactBackConstrainedGPLVM
from HyperbolicEmbeddings.gplvm.gplvm_models import ExactBackConstrainedGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import get_indices_shape_poses_graph, \
    shape_poses_graph_distance_mapping
from HyperbolicEmbeddings.model_loading_utils.data_loading import load_skipped_data_suppport_poses, load_training_data_suppport_poses
from HyperbolicEmbeddings.model_loading_utils.load_model_with_prior import load_model_with_prior_suppport_poses
from HyperbolicEmbeddings.model_loading_utils.load_backconstrained_model import load_backconstrained_model_suppport_poses
from HyperbolicEmbeddings.losses.graph_based_loss import stress


BackConstrainedModel = Union[HyperbolicExactBackConstrainedGPLVM, ExactBackConstrainedGPLVM]

torch.set_default_dtype(torch.float64)

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()
DATA_PATH = ROOT_DIR / "data/support_poses"


def _compute_stress_for_model(
    model: BackConstrainedModel,
    distance_function,
    pose_data: torch.Tensor,
    pose_names: List[str],
    original_pose_names: List[str] = None,
    augmented: bool = False
) -> torch.Tensor:
    # Get indices related to pose names
    if not augmented:
        graph_file_path = DATA_PATH / 'support_poses_closure.csv'
    else:
        graph_file_path = DATA_PATH / 'support_poses_augmented_closure.csv'
    _, shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, pose_names, augmented_taxonomy=augmented)
    shape_pose_indices = torch.from_numpy(shape_pose_indices)

    # Embed
    embeddings = model.X.back_constraint_function(pose_data, shape_pose_indices)

    # Concatenate with the original dataset if provided.
    if original_pose_names is not None:
        data = torch.cat((model.X(), embeddings))
        shape_pose_names = original_pose_names + pose_names
    else:
        data = embeddings
        shape_pose_names = pose_names

    # Compute original graph distances
    graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names,
                                                         augmented_taxonomy=augmented)
    
    # Compute manifold distances
    manifold_distances = distance_function(data, data)

    # Compute stress loss of the embeddings (loss scale = 1)
    return stress(manifold_distances, graph_distances)


def _compute_stress_for_model_with_added_poses(
        model: BackConstrainedModel,
        distance_function,
        added_pose_data: torch.Tensor,
        added_pose_names: List[str],
        original_pose_names: List[str],
        augmented: bool = False
) -> torch.Tensor:
    # Get indices related to pose names
    if not augmented:
        graph_file_path = DATA_PATH / 'support_poses_closure.csv'
    else:
        graph_file_path = DATA_PATH / 'support_poses_augmented_closure.csv'
    _, shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, added_pose_names, augmented_taxonomy=augmented)
    shape_pose_indices = torch.from_numpy(shape_pose_indices)

    # Embed
    embeddings = model.X.back_constraint_function(added_pose_data, shape_pose_indices)

    # Concatenate with the original dataset if provided.
    data = torch.cat((model.X(), embeddings))
    shape_pose_names = original_pose_names + added_pose_names

    # Compute original graph distances
    graph_distances = shape_poses_graph_distance_mapping(graph_file_path, shape_pose_names,
                                                         augmented_taxonomy=augmented)

    # Compute manifold distances
    manifold_distances = distance_function(data, data)

    # Compute stress loss of the embeddings (loss scale = 1)
    distances_diff = graph_distances - manifold_distances

    # Keep only the part of the distances_diff matrix which contains added poses (the rest is set to 0)
    n = added_pose_data.shape[0]
    distances_diff[:-n, :-n] = 0.
    # Keep only the upper diagonal part, the rest is set to zero, and compute the square of the difference
    stress_triu = torch.pow(torch.triu(distances_diff, diagonal=1), 2)

    # Remove the zero values
    stress_vector = torch.reshape(stress_triu, (-1,))
    return stress_vector[torch.nonzero(stress_vector)]


def compute_stress_for_hyperbolic_model(
    model: BackConstrainedModel,
    pose_data: torch.Tensor,
    pose_names: List[str],
    original_pose_names: List[str] = None,
    augmented: bool = False
) -> torch.Tensor:
    return _compute_stress_for_model(model, lorentz_distance_torch, pose_data, pose_names, original_pose_names,
                                     augmented)


def compute_stress_for_euclidean_model(
    model: BackConstrainedModel,
    pose_data: torch.Tensor,
    pose_names: List[str],
    original_pose_names: List[str] = None,
    augmented: bool = False
) -> torch.Tensor:
    return _compute_stress_for_model(model, Kernel().covar_dist, pose_data, pose_names, original_pose_names,
                                     augmented)


def compute_stress_for_hyperbolic_model_with_added_poses(
        model: BackConstrainedModel,
        added_pose_data: torch.Tensor,
        added_pose_names: List[str],
        original_pose_names: List[str],
        augmented: bool = False
) -> torch.Tensor:

    return _compute_stress_for_model_with_added_poses(model, lorentz_distance_torch, added_pose_data,
                                                      added_pose_names, original_pose_names, augmented)


def compute_stress_for_euclidean_model_with_added_poses(
        model: BackConstrainedModel,
        added_pose_data: torch.Tensor,
        added_pose_names: List[str],
        original_pose_names: List[str],
        augmented: bool = False
) -> torch.Tensor:
    return _compute_stress_for_model_with_added_poses(model, Kernel().covar_dist, added_pose_data,
                                                      added_pose_names, original_pose_names, augmented)


def compute_likelihood(model_name):
    FINAL_MODELS_PATH = ROOT_DIR / "final_models"

    # Loading the training data
    if "semifull" in model_name:
        _, pose_names = load_training_data_suppport_poses("semifull")
    elif "feet" in model_name:
        _, pose_names = load_training_data_suppport_poses("feet5")

    if "augmented" in model_name:
        augmented = True
    else:
        augmented = False

    # Computing graph distances
    if not augmented:
        graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    else:
        graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_augmented_closure.csv'
    _, shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, pose_names, augmented_taxonomy=augmented)
    shape_pose_indices = torch.from_numpy(shape_pose_indices)
    graph_distances = shape_poses_graph_distance_mapping(graph_file_path, pose_names, augmented_taxonomy=augmented)

    if "backconstrained" in model_name:
        model = load_backconstrained_model_suppport_poses(FINAL_MODELS_PATH / model_name)
    else:
        model = load_model_with_prior_suppport_poses(FINAL_MODELS_PATH / model_name)

    # Define MLL function
    mll = ExactMarginalLogLikelihood(model.likelihood, model)
    mll.train()

    if hasattr(mll.model.train_inputs, '__call__'):
        # For variational GPLVMs, the training inputs are sampled at each iteration
        # For back-constrained GPLVMs, the training inputs are computed as a function of the training targets
        train_inputs = mll.model.train_inputs()
    else:
        # For exact GPLVMs, the training inputs are fixed
        train_inputs = mll.model.train_inputs[0]

    train_targets = mll.model.train_targets
    output = mll.model(train_inputs)
    # we sum here to support batch mode
    args = [output, train_targets] + _get_extra_mll_args(mll)
    mll_of_model = mll(*args).sum()

    return mll_of_model


if __name__ == "__main__":
    """
    Tests it on one of the added poses example
    """
    from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import get_missing_data_semifull_dataset

    # Loading a model by its name.
    model_name = "euclidean_egplvm_backconstrained_dim2_semifull_Stress0.9"
    model_path = ROOT_DIR / "final_models" / model_name

    model = load_backconstrained_model_suppport_poses(model_path)

    added_pose_data, _, added_shape_pose_names, _ = get_missing_data_semifull_dataset(ROOT_DIR / "data/support_poses" / "keyPoseShapesXPose.xml")
    added_pose_data = torch.from_numpy(added_pose_data)
    
    mean_stress = compute_stress_for_hyperbolic_model(model, added_pose_data, added_shape_pose_names)
    print(f"Mean stress for {model_name}'s missing data: {mean_stress.item()}")

