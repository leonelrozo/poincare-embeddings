from pathlib import Path
import numpy as np
import torch

from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import load_grasps_data, load_missing_grasps_data
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import load_support_pose_data, \
    load_augmented_support_pose_data, load_missing_augmented_support_pose_data
from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import load_bimanual_manipulation_data, \
    load_missing_bimanual_manipulation_data
from HyperbolicEmbeddings.utils.normalization import centering
from HyperbolicEmbeddings.graph_utils.graph_functions import nodes_distance_mapping
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import shape_poses_label_to_support_pose_labels, \
    shape_poses_label_to_augmented_support_pose_labels
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import hand_grasp_name_to_grasp_labels
from HyperbolicEmbeddings.taxonomy_utils.MulticellularRobotDataParse import load_multicellular_robots_data

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def load_taxonomy_data(dataset, dataset_type=None):
    # Load data
    data_folder_path = ROOT_DIR / 'data'

    if dataset == 'grasps':
        training_data, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
        nodes_legend_for_plot, color_function, joint_names = \
            load_grasps_data(data_folder_path)

        max_manifold_distance = 5.0

    elif dataset == 'support-poses':
        training_data, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
        nodes_legend_for_plot, color_function, joint_names = \
            load_support_pose_data(data_folder_path, dataset_type)

        max_manifold_distance = None

    elif dataset == 'augmented-support-poses':
        training_data, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
        nodes_legend_for_plot, color_function, joint_names = \
            load_augmented_support_pose_data(data_folder_path, dataset_type)

        max_manifold_distance = None

    elif dataset == 'bimanual':
        training_data, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
        nodes_legend_for_plot, color_function, joint_names = \
            load_bimanual_manipulation_data(data_folder_path)

        max_manifold_distance = 3.0

    elif dataset == 'multicellular-robots':
        training_data, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
        nodes_legend_for_plot, color_function, joint_names = \
            load_multicellular_robots_data(data_folder_path)

        max_manifold_distance = 3.0

    # Center
    training_data, data_mean = centering(training_data)

    # Rescale distance
    if max_manifold_distance:
        max_graph_distance = np.max(graph_distances.detach().numpy())
        graph_distances = graph_distances / max_graph_distance * max_manifold_distance

    return training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, \
           nodes_legend_for_plot, color_function, max_manifold_distance, joint_names


def load_taxonomy_added_data(dataset, dataset_type=None):
    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = \
        load_taxonomy_data(dataset, dataset_type)

    # Load added data
    data_folder_path = ROOT_DIR / 'data'
    if dataset == 'grasps':
        added_data, added_nodes_names, added_indices_in_graph, all_graph_distances, \
        all_nodes_legend_for_plot, color_function = \
            load_missing_grasps_data(data_folder_path)

    elif dataset == 'support-poses' or dataset == 'augmented-support-poses':
        added_data, added_nodes_names, added_indices_in_graph, all_graph_distances, \
        all_nodes_legend_for_plot, color_function = \
            load_missing_augmented_support_pose_data(data_folder_path, dataset_type)

    elif dataset == 'bimanual':
        added_data, added_nodes_names, added_indices_in_graph, all_graph_distances, \
        all_nodes_legend_for_plot, color_function = \
            load_missing_bimanual_manipulation_data(data_folder_path)

    # Center
    added_data = added_data - data_mean

    # Rescale distance
    if max_manifold_distance:
        max_graph_distance = np.max(graph_distances.detach().numpy())
        graph_distances = graph_distances / max_graph_distance * max_manifold_distance

    # Rescale distance
    if max_manifold_distance:
        max_graph_distance = np.max(all_graph_distances.detach().numpy())
        all_graph_distances = all_graph_distances / max_graph_distance * max_manifold_distance

    return training_data, added_data, data_mean, adjacency_matrix, graph_distances, all_graph_distances, \
           nodes_names, added_nodes_names, indices_in_graph, added_indices_in_graph, \
           nodes_legend_for_plot, all_nodes_legend_for_plot, color_function, max_manifold_distance


def load_taxonomy_added_class(dataset, dataset_type=None, class_to_skip: list = None):
    # Load data
    training_data, data_mean, adjacency_matrix, graph_distances, nodes_names, indices_in_graph, nodes_legend_for_plot, \
    color_function, max_manifold_distance, joint_names = \
        load_taxonomy_data(dataset, dataset_type)

    # Load added data
    data_folder_path = ROOT_DIR / 'data'
    if dataset == 'grasps':
        if class_to_skip is None:
            class_to_skip = ['MediumWrap', 'Stick', 'Ring', 'Quadpod']
        graph_file_path = data_folder_path / 'hand_grasps/hand_grasps_closure.csv'

    elif dataset == 'support-poses':
        if class_to_skip is None:
            class_to_skip = ['LeftFootLeftHand', 'LeftFootRightHand', 'LeftHandRightFoot', 'RightFootRightHand']
        graph_file_path = data_folder_path / 'support_poses/support_poses_closure.csv'

    elif dataset == 'augmented-support-poses':
        if class_to_skip is None:
            class_to_skip = ['LeftFootLeftHand', 'LeftFootRightHand', 'LeftHandRightFoot', 'RightFootRightHand']
        graph_file_path = data_folder_path / 'support_poses/support_poses_augmented_closure.csv'

    elif dataset == 'bimanual':
        if class_to_skip is None:
            class_to_skip = ["tightly_coupled_asym_r"]  # ['unimanual_right']
        graph_file_path = data_folder_path / 'bimanual_manipulation/bimanual_manipulation_closure.csv'

    # Building the reduced dataset, and saving the skipped data.
    reduced_nodes_names = []
    reduced_data = []
    reduced_indices_in_graph = []
    skipped_data = []
    skipped_nodes_names = []
    skipped_indices_in_graph = []
    indices_to_skip = []

    for n in range(training_data.shape[0]):
        data = training_data[n]
        node_name = nodes_names[n]
        index_in_graph = indices_in_graph[n]

        if not (node_name in class_to_skip):
            reduced_nodes_names.append(node_name)
            reduced_data.append(data)
            reduced_indices_in_graph.append(index_in_graph)
        else:
            skipped_data.append(data)
            skipped_nodes_names.append(node_name)
            skipped_indices_in_graph.append(index_in_graph)
            indices_to_skip.append(n)

    reduced_data = torch.vstack(reduced_data)
    skipped_data = torch.vstack(skipped_data)
    reduced_indices_in_graph = torch.vstack(reduced_indices_in_graph)
    skipped_indices_in_graph = torch.vstack(skipped_indices_in_graph)

    # Re-Center
    reduced_data, reduced_data_mean = centering(reduced_data)
    skipped_data = skipped_data - reduced_data_mean

    # Remove rows of graph distances
    reduced_graph_distances = np.delete(graph_distances, indices_to_skip, 0)
    reduced_graph_distances = np.delete(reduced_graph_distances, indices_to_skip, 1)

    # Compute the ordered graph distances for reduced and skipped nodes
    # TODO can we do this independently of the taxonomy
    if dataset == 'grasps':
        all_labels = hand_grasp_name_to_grasp_labels(reduced_nodes_names) \
                     + hand_grasp_name_to_grasp_labels(skipped_nodes_names)
    elif dataset == 'support-poses':
        all_labels = shape_poses_label_to_support_pose_labels(reduced_nodes_names) \
                     + shape_poses_label_to_support_pose_labels(skipped_nodes_names)
    elif dataset == 'augmented-support-poses':
        all_labels = shape_poses_label_to_augmented_support_pose_labels(reduced_nodes_names) + \
                     shape_poses_label_to_augmented_support_pose_labels(skipped_nodes_names)
    elif dataset == 'bimanual':
        all_labels = reduced_nodes_names + skipped_nodes_names

    all_graph_distances = nodes_distance_mapping(graph_file_path, all_labels)
    # Rescale distance
    if max_manifold_distance:
        max_graph_distance = np.max(all_graph_distances.detach().numpy())
        all_graph_distances = all_graph_distances / max_graph_distance * max_manifold_distance

    # Get concatenated variables
    # all_graph_distances = reorder_distance_matrix(graph_distances.detach().numpy(), nodes_names,
    #                                               list(set(reduced_nodes_names + skipped_nodes_names)))
    # all_graph_distances = torch.from_numpy(all_graph_distances)
    # all_nodes_legend_for_plot = reorder_taxonomy_data(nodes_legend_for_plot, nodes_names,
    #                                                   list(set(reduced_nodes_names + skipped_nodes_names)))

    # graph_distances and nodes_legend_for_plot are not ordered as reduced_nodes_names + skipped_nodes_names !!!
    return reduced_data, skipped_data, reduced_data_mean, adjacency_matrix, reduced_graph_distances, \
           all_graph_distances, \
           reduced_nodes_names, skipped_nodes_names, reduced_indices_in_graph, skipped_indices_in_graph, \
           nodes_legend_for_plot, nodes_legend_for_plot, color_function, max_manifold_distance