import torch
from gpytorch.mlls.added_loss_term import AddedLossTerm
from gpytorch.kernels import Kernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch


def stress(distances: torch.Tensor, graph_distances: torch.Tensor) -> torch.Tensor:
    """
    Computes the stress
    """
    distances_diff = graph_distances - distances
    i, j = torch.triu_indices(*distances_diff.shape, offset=1)

    return torch.pow(torch.triu(distances_diff, diagonal=1), 2)[i, j]


class ZeroAddedLossTermExactMLL(AddedLossTerm):
    """
    This class can be used as a template to define an extra loss for an exact marginal likelihood.
    In this case, the latent variable is given as a parameter of the loss function during its call.
    Additional parameters can be passed in the initialization of the loss term.
    """
    def __init__(self):  # param):  # Uncomment to add initial parameters
        super().__init__()
        # self.param = param

    def loss(self, x, **kwargs):
        return 0.0 * torch.sum(x)


class ZeroAddedLossTermApproximateMLL(AddedLossTerm):
    """
    This class can be used as a template to define an extra loss for an approximate marginal likelihood.
    In this case, the latent variable must be updated before the call of the loss function.
    Additional parameters can be passed in the initialization of the loss term.
    """
    def __init__(self, x):
        super().__init__()
        self.x = x

    def loss(self):
        return 0.0 * torch.sum(self.x)


class StressLossTermExactMLL(AddedLossTerm):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the stress loss (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of data points
    loss_scale: Constant to control magnitude of stress loss
    """
    def __init__(self, graph_distance_matrix, distance_function, loss_scale=1.0):
        super().__init__()
        self.graph_distances = graph_distance_matrix  # As this does not change for the optimization, it is a parameter
        self.distance = distance_function
        self.loss_scale = loss_scale

    def loss(self, x, **kwargs):
        """
        Implements stress loss according to Eq. 10 in [1].

        Parameters
        ----------
        x: Optimization variable (i.e. latent variables in GPLVM)
        kwargs: Additional arguments for loss function

        Returns
        -------
        stress_loss: Sum over the squared difference between the graph and manifold distances as defined in [1]
                     The negative stress is return to comply with GPytorch that maximizes added terms.
        """
        distances = self.distance(x, x)
        stress_loss = self.loss_scale * torch.mean(stress(distances, self.graph_distances))

        return - stress_loss


class HyperbolicStressLossTermExactMLL(StressLossTermExactMLL):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the stress loss (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of data points
    loss_scale: Constant to control magnitude of stress loss
    """
    def __init__(self, graph_distance_matrix, loss_scale=1.0):
        super().__init__(graph_distance_matrix, lorentz_distance_torch, loss_scale)


class EuclideanStressLossTermExactMLL(StressLossTermExactMLL):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the stress loss (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of data points
    loss_scale: Constant to control magnitude of stress loss
    """
    def __init__(self, graph_distance_matrix, loss_scale=1.0):

        super().__init__(graph_distance_matrix, Kernel().covar_dist, loss_scale)


class VanillaHyperbolicDistortionLossTermExactMLL(AddedLossTerm):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the distortion loss
    (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of shape poses
    loss_scale: Constant to control magnitude of distortion loss
    distance_ratio_reg: Distance regularizer to avoid division by zero in distance ratio computation
    """
    def __init__(self, graph_distance_matrix, loss_scale=1.0, distance_regularizer=0.1):
        super().__init__()
        self.graph_distances = graph_distance_matrix  # As this does not change for the optimization, it is a parameter
        self.loss_scale = loss_scale
        self.distance_ratio_reg = distance_regularizer

    def loss(self, x, **kwargs):
        """
        Implements distortion loss as given in Eq. 10 of [1]. This is used to optimize latent variables/embeddings that
        are "least-distorting".

        Parameters
        ----------
        x: Optimization variable (i.e. latent variables in GPLVM)
        kwargs: Additional arguments for loss function

        Returns
        -------
        distortion_loss: Sum over the distance ratio as defined in [1]
                         The negative distorsion is return to comply with GPytorch that maximizes added terms.
        """
        manifold_distances = lorentz_distance_torch(x, x)
        upper_triang_index = torch.triu_indices(row=len(x), col=len(x), offset=0)  # Indexes of upper triangular matrix
        # Get upper-triangular components of graph and manifold distance matrices in a vector form
        upper_triang_graph_dist = (self.graph_distances[upper_triang_index[0], upper_triang_index[1]] ** 2)
        upper_triang_manifold_dist = (manifold_distances[upper_triang_index[0], upper_triang_index[1]] ** 2)
        # Compute distance ratio and distortion loss
        distance_ratio = upper_triang_manifold_dist / (upper_triang_graph_dist + self.distance_ratio_reg)
        distortion_loss = self.loss_scale * (torch.sum(torch.abs(distance_ratio - 1.0)) / len(upper_triang_graph_dist))

        return - distortion_loss


class DistortionLossTermExactMLL(AddedLossTerm):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the distortion loss
    (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of shape poses
    loss_scale: Constant to control magnitude of distortion loss
    distance_ratio_reg: Distance regularizer to avoid division by zero in distance ratio computation
    """

    def __init__(self, graph_distance_matrix, distance_function, loss_scale=1.0, distance_regularizer=0.1):
        super().__init__()
        self.graph_distances = graph_distance_matrix  # As this does not change for the optimization, it is a parameter
        self.loss_scale = loss_scale
        self.distance_ratio_reg = distance_regularizer
        self.distance = distance_function

    def loss(self, x, **kwargs):
        """
        Implements distortion loss as given in Eq. 10 of [1]. This is used to optimize latent variables/embeddings that
        are "least-distorting".

        Parameters
        ----------
        x: Optimization variable (i.e. latent variables in GPLVM)
        kwargs: Additional arguments for loss function

        Returns
        -------
        distortion_loss: Sum over the distance ratio as defined in [1]
                         The negative distorsion is return to comply with GPytorch that maximizes added terms.
        """
        manifold_distances = self.distance(x, x)
        upper_triang_index = torch.triu_indices(row=len(x), col=len(x), offset=1)  # Indexes of upper triangular matrix
        # Get upper-triangular components of graph and manifold distance matrices in a vector form
        upper_triang_graph_dist = (self.graph_distances[upper_triang_index[0], upper_triang_index[1]] ** 2)
        upper_triang_manifold_dist = (manifold_distances[upper_triang_index[0], upper_triang_index[1]] ** 2)

        same_class_mask = upper_triang_graph_dist == 0.0

        # Making nodes of the same class go to manifold distance 0.
        if len(upper_triang_manifold_dist[same_class_mask]) > 0:
            same_class_error = (upper_triang_manifold_dist[same_class_mask]).mean()
        else:
            same_class_error = 0.0

        # Different class error
        different_class_distance_ratio = upper_triang_manifold_dist[same_class_mask == False] / (
        upper_triang_graph_dist[same_class_mask == False])
        different_class_error = (torch.abs(different_class_distance_ratio - 1.0)).mean()

        # Final error
        distortion_loss = self.loss_scale * (0.01 * same_class_error + 10.0 * different_class_error)

        # # Compute distance ratio and distortion loss
        # distance_ratio = upper_triang_manifold_dist / (upper_triang_graph_dist + self.distance_ratio_reg)
        # distortion_loss = self.loss_scale * (torch.sum(torch.abs(distance_ratio - 1.0)) / len(upper_triang_graph_dist))

        print(f"Avg. distance ratio geodesics and graph: {different_class_distance_ratio.mean()}")

        return - distortion_loss


class HyperbolicDistortionLossTermExactMLL(DistortionLossTermExactMLL):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the distortion loss
    (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of shape poses
    loss_scale: Constant to control magnitude of distortion loss
    distance_ratio_reg: Distance regularizer to avoid division by zero in distance ratio computation
    """
    def __init__(self, graph_distance_matrix, loss_scale=1.0, distance_regularizer=0.1):
        super().__init__(graph_distance_matrix, lorentz_distance_torch, loss_scale, distance_regularizer)


class EuclideanDistortionLossTermExactMLL(DistortionLossTermExactMLL):
    """
    Class to compute cost regularizer (coming from a prior on latent variables) based on the distortion loss
    (Eq. 10 in [1])

    [1] C. Cruceru. "Computationally Tractable Riemannian Manifolds for Graph Embeddings." AAAI, 2021

    Parameters
    ----------
    graph_distances: Graph distance matrix for all pairs of shape poses
    loss_scale: Constant to control magnitude of distortion loss
    distance_ratio_reg: Distance regularizer to avoid division by zero in distance ratio computation
    """
    def __init__(self, graph_distance_matrix, loss_scale=1.0, distance_regularizer=0.1):
        super().__init__(graph_distance_matrix, Kernel().covar_dist, loss_scale, distance_regularizer)

