import numpy as np
import torch
from scipy.signal import savgol_filter
from mayavi import mlab
import matplotlib.colors as pltc
from typing import Union, Tuple
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import exp_map
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import log_map, parallel_transport_mu0, inverse_parallel_transport_mu0
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare


def Rodrigues_rotation_matrix(q,theta):
    """
    This is used to rotate the vector in the space, given the axis of rotation and the angle of rotation. Here we are
    only interested in finding the Rotation matrix.

    K is the cross-product matrix for vector q. It is a skew-symmetric matrix. If q=[q0,q1,q2], then K is defined as:
    K = [0  ,-q2, q1],
        [q2 , 0 ,-q0],
        [-q1, q0, 0]]

    The rotation matrix through an angle /theta counterclockwise about the axis q is:
    R = I + (sin(theta))*K + (1-cos(theta))*K^2

    Parameters
    ----------
    :param q: The axis of rotation.
    :param theta: The angle of rotation.

    Returns
    -------
    :return: The Rotational Matrix R that rotates a vector along the axis q at the angle theta in the counter-clockwise
             direction.
    """
    # Cross Product Matrix
    K = np.asarray([[0, -q[2], q[1]],
                   [q[2], 0, -q[0]],
                   [-q[1], q[0], 0]])

    # Rotational Matrix
    dim_M = q.shape[0]
    R = np.eye(dim_M) + np.sin(theta)*K + (1-np.cos(theta))*K.dot(K)
    return R


def axis_angle(q):
    """
    The axis-angle representation comprises of a  unit vector 'axis' that describes the axis of rotation about which
    the vector q rotates by an angle around this axis by the right-hand rule.

    axis = (e x q)/||e x q||
    angle = arccos(q.e)

    x - Cross-Product
    . - Dot Product

    Parameters
    ----------
    :param q: The vector q whose axis-angle representation is to be found.

    Returns
    -------
    :return: The axis and angle for vector q.
    """
    # Origin Vector
    e = np.asarray([0,0,1])

    # Threshold for Norm
    norm_threshold = 1e-6

    # Axis-angle represntation
    cross_product = np.cross(e,q)
    norm_cross_product = np.linalg.norm(cross_product)
    if norm_cross_product < norm_threshold:
        axis = e
        angle = 0
    else:
        axis = cross_product/norm_cross_product
        angle = np.arccos(np.maximum(np.minimum(np.dot(q, e), 1.), -1.))
    return axis,angle


def draw_arrow_mayavi(x, y, z, u, v, w, r=0.025, color=(0, 0, 0), line_width=2., n_elems=30):
    phi = np.linspace(0, 2 * np.pi, n_elems)

    base = np.stack((x, y, z))
    diff_base_tip = np.stack((u, v, w))
    tip = np.stack((u, v, w)) + base
    # Rotation of the cone
    # 90°
    axis = np.array([0, 1, 0])
    R = Rodrigues_rotation_matrix(axis, np.pi / 2.)
    # arrow direction
    axis, ang = axis_angle(diff_base_tip / np.linalg.norm(diff_base_tip))
    R = Rodrigues_rotation_matrix(axis, ang).dot(R)

    # Points of the cone
    xyz = np.vstack((r*2 * np.ones(n_elems), r * np.sin(phi), r / np.sqrt(2) * np.cos(phi)))
    xyz = R.dot(xyz)

    xcone = np.vstack((np.zeros(n_elems), xyz[0])) + tip[0]
    ycone = np.vstack((np.zeros(n_elems), xyz[1])) + tip[1]
    zcone = np.vstack((np.zeros(n_elems), xyz[2])) + tip[2]

    # Plot vector
    vector = np.stack((base, tip))
    mlab.plot3d(vector[:, 0], vector[:, 1], vector[:, 2], color=color, line_width=line_width,
                tube_radius=None,)
    # Plot tip
    mlab.mesh(xcone, ycone, zcone, color=color)


def plot_sphere_tangent_plane(base, l_vert=1):
    """
    Plots tangent plane of a point lying on the sphere manifold
    Based on the function of riepybdlib (https://gitlab.martijnzeestraten.nl/martijn/riepybdlib)

    Parameters
    ----------
    :param base: base point of the tangent space

    Optional parameters
    -------------------
    :param l_vert: length/width of the displayed plane

    Returns
    -------
    :return: -
    """
    # Tangent axis at 0 rotation:
    T0 = np.array([[1, 0], [0, 1], [0, 0]])

    # Rotation matrix with respect to zero:
    axis, ang = axis_angle(base)
    R = Rodrigues_rotation_matrix(axis, -ang)

    # Tangent axis in new plane:
    T = R.T.dot(T0)

    # Compute vertices of tangent plane at g
    hl = 0.5 * l_vert
    X = [[hl, hl],  # p0
         [hl, -hl],  # p1
         [-hl, hl],  # p2
         [-hl, -hl]]  # p3
    X = np.array(X).T
    points = (T.dot(X).T).T
    psurf = points.reshape((-1, 2, 2))

    # Plot contours of the tangent space
    contour = points[:, [0, 1, 3, 2, 0]]

    return psurf, contour


def plot_ellipsoid(color: Tuple = (0.7, 0.7, 0.7), opacity: float = 0.8, radius: float = 0.99, n_elems: int = 100,
                figure=None, scalars=None, colormap=None, offset: np.ndarray = np.array([0., 0., 0.])):
    """
    Plots a sphere

    Optional parameters
    -------------------
    :param color: color of the surface
    :param opacity: transparency index
    :param radius: sphere radius
    :param n_elems: number of points in the surface
    :param figure: mayavi figure handle
    :param base_point: center of the sphere

    Returns
    -------
    :return: -
    """
    u = np.linspace(0, 2 * np.pi, n_elems)
    v = np.linspace(0, np.pi, n_elems)

    x = radius * np.outer(np.cos(u), np.sin(v))
    y = radius * np.outer(np.sin(u), np.sin(v))
    z = radius * 0.6 * np.outer(np.ones(np.size(u)), np.cos(v))

    if scalars is not None:
        mlab.mesh(x + offset[0], y + offset[1], z + offset[2], figure=figure, opacity=opacity,
                  scalars=scalars, colormap=colormap)
    elif colormap is not None:
        mlab.mesh(x + offset[0], y + offset[1], z + offset[2], figure=figure, opacity=opacity, colormap=colormap)
    else:
        mlab.mesh(x + offset[0], y + offset[1], z + offset[2], figure=figure, color=color, opacity=opacity)


def plot_hyperboloid_mayavi(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id1a = 125
    id1b = 70
    id2 = 375
    id2a = 385
    id2b = 365
    y1 = np.array([x[id1, id1], y[id1, id1], z[id1, id1]])
    y1a = np.array([x[id1a, id1a], y[id1a, id1a], z[id1a, id1a]])
    y1b = np.array([x[id1b, id1b], y[id1b, id1b], z[id1b, id1b]])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y2a = np.array([x[id2a, id2a], y[id2a, id2a], z[id2a, id2a]])
    y2b = np.array([x[id2b, id2b], y[id2b, id2b], z[id2b, id2b]])
    y1_lorentz = np.array([z[id1, id1], x[id1, id1], y[id1, id1]])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))

    # Projection of one point in the tangent space of the other
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))

    # Geodesic from y1 to y2
    geodesic_pts = 20
    geodesic = np.array([exp_map(y2_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., geodesic_pts)])
    poincare_geodesic = lorentz_to_poincare(torch.tensor(geodesic)).numpy()

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('turquoise'), scale_factor=0.1)
    mlab.points3d(y1a[0], y1a[1], y1a[2], color=pltc.to_rgb('turquoise'), scale_factor=0.1)
    mlab.points3d(y1b[0], y1b[1], y1b[2], color=pltc.to_rgb('turquoise'), scale_factor=0.1)
    mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('mediumorchid'), scale_factor=0.1)
    mlab.points3d(y2a[0], y2a[1], y2a[2], color=pltc.to_rgb('mediumorchid'), scale_factor=0.1)
    mlab.points3d(y2b[0], y2b[1], y2b[2], color=pltc.to_rgb('mediumorchid'), scale_factor=0.1)

    # Plot geodesic
    # mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=pltc.to_rgb('darkgreen'), line_width=3.5, tube_radius=None)
    mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=clr_red, line_width=3.5,
                tube_radius=None)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([2*y1[0], 2*y1[1], -2*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=3.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], y2_tgt[1], y2_tgt[2], y2_tgt[0], r=0.03,
                      color=pltc.to_rgb('orange'), line_width=3.)

    # Plot Poincare unit circle
    theta = np.linspace(0, 2 * np.pi, 150)
    radius = 1.5
    a = radius * np.cos(theta)
    b = radius * np.sin(theta)
    c = np.zeros(150)
    mlab.plot3d(a, b, c, color=clr_blue, line_width=3.5, tube_radius=None)
    # Plot data on the Poincare model
    mlab.points3d(y1_poincare[0], y1_poincare[1], y1_poincare[2], color=pltc.to_rgb('turquoise'), scale_factor=0.1)
    mlab.points3d(y2_poincare[0], y2_poincare[1], y2_poincare[2], color=pltc.to_rgb('mediumorchid'), scale_factor=0.1)
    # Plot projection lines
    y1_proj = np.stack((y1, y1_poincare))
    y2_proj = np.stack((y2, y2_poincare))
    mlab.plot3d(y1_proj[:, 0], y1_proj[:, 1], y1_proj[:, 2], color=(0.7, 0.7, 0.7), line_width=1.5, tube_radius=None)
    mlab.plot3d(y2_proj[:, 0], y2_proj[:, 1], y2_proj[:, 2], color=(0.7, 0.7, 0.7), line_width=1.5, tube_radius=None)
    # Plot geodesic
    # mlab.plot3d(poincare_geodesic[:, 0], poincare_geodesic[:, 1], np.zeros(geodesic_pts), color=pltc.to_rgb('darkgreen'),
    #             line_width=3.5, tube_radius=None)
    mlab.plot3d(poincare_geodesic[:, 0], poincare_geodesic[:, 1], np.zeros(geodesic_pts),
                color=clr_red,
                line_width=3.5, tube_radius=None)

    mlab.view(0, 170)
    mlab.show()


def plot_hyperbolic_explog_maps(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id1a = 125
    id1b = 70
    id2 = 375
    id2a = 385
    id2b = 365
    y1 = np.array([x[id1, id1], y[id1, id1], z[id1, id1]])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y1_lorentz = np.array([z[id1, id1], x[id1, id1], y[id1, id1]])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))

    # Projection of one point in the tangent space of the other
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))

    # Geodesic from y1 to y2
    geodesic_pts = 20
    geodesic = np.array([exp_map(y2_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., geodesic_pts)])
    poincare_geodesic = lorentz_to_poincare(torch.tensor(geodesic)).numpy()

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('black'), scale_factor=0.1)
    mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('black'), scale_factor=0.1)

    # Plot geodesic
    # mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=pltc.to_rgb('darkgreen'), line_width=3.5, tube_radius=None)
    mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=clr_red, line_width=3.5,
                tube_radius=None)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([2*y1[0], 2*y1[1], -2*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=3.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], y2_tgt[1], y2_tgt[2], y2_tgt[0], r=0.03,
                      color=pltc.to_rgb('orange'), line_width=3.)

    mlab.view(0, 170)
    mlab.show()


def plot_hyperbolic_prl_trsp(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id2 = 375
    y1 = np.array([x[id1, id1], y[id1, id1], z[id1, id1]])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y1_lorentz = np.array([z[id1, id1], x[id1, id1], y[id1, id1]])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y3_lorentz = np.array([z[id2, id2], y[id2, id2], x[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))

    # Projection of one point in the tangent space of the other
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))
    y3_tgt = log_map(torch.tensor(y3_lorentz), torch.tensor(y1_lorentz))
    y3_tgt_mu0 = inverse_parallel_transport_mu0(torch.tensor(y3_tgt), torch.tensor(y1_lorentz))
    y3_tgt_prltrsp = parallel_transport_mu0(y3_tgt_mu0, torch.tensor(y2_lorentz))

    # Geodesic from y1 to y2
    geodesic_pts = 20
    geodesic = np.array([exp_map(y2_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., geodesic_pts)])
    poincare_geodesic = lorentz_to_poincare(torch.tensor(geodesic)).numpy()

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('black'), scale_factor=0.1)
    mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('black'), scale_factor=0.1)

    # Plot geodesic
    # mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=pltc.to_rgb('darkgreen'), line_width=3.5, tube_radius=None)
    mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=clr_red, line_width=3.5,
                tube_radius=None)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([1.5*y1[0], 1.5*y1[1], -1.5*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=3.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], y3_tgt[1], y3_tgt[2], y3_tgt[0], r=0.03,
                      color=pltc.to_rgb('orange'), line_width=3.)

    normal_vec = np.array([1.5*y2[0], 1.5*y2[1], -1.5*y2[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=3.0)
    mlab.mesh(psurf[0]+y2[0], psurf[1]+y2[1], psurf[2]+y2[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y2[0], contour[1]+y2[1], contour[2]+y2[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y2[0], y2[1], y2[2], y3_tgt_prltrsp[1], y3_tgt_prltrsp[2], y3_tgt_prltrsp[0], r=0.03,
                      color=pltc.to_rgb('orange'), line_width=3.)

    mlab.view(0, 170)
    mlab.show()


def plot_hyperbolic_tangent_projection(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id1a = 125
    id1b = 70
    id2 = 375
    id2a = 385
    id2b = 365
    y1 = np.array([x[id1, id1], y[id1, id1], z[id1, id1]])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y1_lorentz = np.array([z[id1, id1], x[id1, id1], y[id1, id1]])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))

    # Vector in ambiant space 
    v = y2_tgt - torch.tensor([0.3, 0.3, -0.5])

    # Tangent space projection  
    metric = torch.eye(3)
    metric[0, 0] = -1
    w = torch.mm((metric + torch.mm(torch.tensor(y1_lorentz)[:,None], torch.tensor(y1_lorentz)[None])), v[:, None])[:, 0]

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('black'), scale_factor=0.1)
    # mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('black'), scale_factor=0.1)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([2*y1[0], 2*y1[1], -2*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=3.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot ambiant vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], v[1], v[2], v[0], r=0.03,
                      color=pltc.to_rgb('dodgerblue'), line_width=3.)
    # Plot tangent vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], w[1], w[2], w[0], r=0.03,
                      color=pltc.to_rgb('orange'), line_width=3.)

    mlab.view(0, 170)
    mlab.show()



def plot_hyperbolic_distribution(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id2 = 375
    y1 = np.array([0.0, 0.0, 1.0])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y1_lorentz = np.array([1.0, 0.0, 0.0])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y3_lorentz = np.array([z[id2, id2], y[id2, id2], x[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))

    # Projection of one point in the tangent space of the other
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))
    y3_tgt = log_map(torch.tensor(y3_lorentz), torch.tensor(y1_lorentz))
    y3_tgt = y3_tgt / 2 - y2_tgt / 5
    y3_tgt_prltrsp = parallel_transport_mu0(y3_tgt, torch.tensor(y2_lorentz))
    sample = exp_map(y3_tgt_prltrsp, torch.tensor(y2_lorentz))

    # Geodesic from y1 to y2
    geodesic_pts = 20
    geodesic = np.array([exp_map(y2_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., geodesic_pts)])
    poincare_geodesic = lorentz_to_poincare(torch.tensor(geodesic)).numpy()

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('black'), scale_factor=0.1)
    mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('dodgerblue'), scale_factor=0.1)
    mlab.points3d(sample[1], sample[2], sample[0], color=pltc.to_rgb('crimson'), scale_factor=0.1)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([1.5*y1[0], 1.5*y1[1], -1.5*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=2.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y1[0], y1[1], y1[2], y3_tgt[1], y3_tgt[2], y3_tgt[0], r=0.03,
                      color=pltc.to_rgb('black'), line_width=3.)

    normal_vec = np.array([1.5*y2[0], 1.5*y2[1], -1.5*y2[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=1.5)
    mlab.mesh(psurf[0]+y2[0], psurf[1]+y2[1], psurf[2]+y2[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y2[0], contour[1]+y2[1], contour[2]+y2[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    draw_arrow_mayavi(y2[0], y2[1], y2[2], y3_tgt_prltrsp[1], y3_tgt_prltrsp[2], y3_tgt_prltrsp[0], r=0.03,
                      color=pltc.to_rgb('dodgerblue'), line_width=3.)

    mlab.view(0, 170)
    mlab.show()


def plot_hyperbolic_pullback(origin=0.0, height=1.5, num_pts=500, surface_unique_color=(0.7, 0.7, 0.7), mesh_alpha=0.5):
    """
    Plots a one-sheet hyperboloid that represents the Lorentz model for the hyperbolic manifold
    More info at https://bjlkeng.github.io/posts/hyperbolic-geometry-and-poincare-embeddings/

    :param origin: Origin of the hyperboloid
    :param height: Height of the hyperboloid
    :param num_pts: Points used to generate meshgrid
    :param surface_unique_color: Surface color in [R,G,B] format
    :param mesh_alpha: Opacity value for mesh

    """
    # Colors
    clr_blue = (0., 0.35, 0.6)
    clr_red = (0.8013, 0.2165, 0.2899)

    # Plot hyperboloid
    u = np.linspace(origin, height, num_pts)
    v = np.linspace(0, 2 * np.pi, num_pts)
    [t, theta] = np.meshgrid(u, v)

    a = 1
    b = 1
    c = 1

    x = a * np.sinh(t) * np.cos(theta)
    y = b * np.sinh(t) * np.sin(theta)
    z = c * np.cosh(t)

    # Plot hyperbolid using Cartesian coordinates
    # u = np.arange(-5, 5, 0.25)
    # v = np.arange(-5, 5, 0.25)
    # x, y = np.meshgrid(u, v)
    # z = np.sqrt(4. * (x ** 2 + y ** 2) / 1. + 1)

    # Create data
    id1 = 100
    id1a = 125
    id1b = 70
    id2 = 375
    id2a = 385
    id2b = 365
    y1 = np.array([x[id1, id1], y[id1, id1], z[id1, id1]])
    y1a = np.array([x[id1a, id1a], y[id1a, id1a], z[id1a, id1a]])
    y1b = np.array([x[id1b, id1b], y[id1b, id1b], z[id1b, id1b]])
    y2 = np.array([x[id2, id2], y[id2, id2], z[id2, id2]])
    y2a = np.array([x[id2a, id2a], y[id2a, id2a], z[id2a, id2a]])
    y2b = np.array([x[id2b, id2b], y[id2b, id2b], z[id2b, id2b]])
    y1_lorentz = np.array([z[id1, id1], x[id1, id1], y[id1, id1]])
    y2_lorentz = np.array([z[id2, id2], x[id2, id2], y[id2, id2]])
    y1_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y1_lorentz)).numpy(), np.zeros(1)))
    y2_poincare = np.hstack((lorentz_to_poincare(torch.tensor(y2_lorentz)).numpy(), np.zeros(1)))

    # Projection of one point in the tangent space of the other
    y2_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y1_lorentz))

    # Geodesic from y1 to y2
    geodesic_pts = 20
    geodesic = np.array([exp_map(y2_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., geodesic_pts)])
    poincare_geodesic = lorentz_to_poincare(torch.tensor(geodesic)).numpy()

    # Generate an artificial pullback geodesics and data points
    y3_lorentz = (y1_lorentz + y2_lorentz) / 2
    y3_lorentz[1] += 0.5
    y3_lorentz[0] = np.sqrt(1 + y3_lorentz[1]**2  + y3_lorentz[2]**2)
    y13_tgt = log_map(torch.tensor(y3_lorentz), torch.tensor(y1_lorentz))
    y32_tgt = log_map(torch.tensor(y2_lorentz), torch.tensor(y3_lorentz))
    pullback_geodesic = np.array([ (y1_lorentz + (y3_lorentz - y1_lorentz)*t) for t in np.linspace(0., 1., int(geodesic_pts/2))] + [(y3_lorentz + (y2_lorentz - y3_lorentz)*t) for t in np.linspace(0., 1., int(geodesic_pts/2))])
    pullback_geodesic = savgol_filter(pullback_geodesic.T, 10, 3).T
    pullback_geodesic[:, 0] = np.sqrt(1 + pullback_geodesic[:, 1]**2  + pullback_geodesic[:, 2]**2)
    # pullback_geodesic = np.array([exp_map(y13_tgt * t, y1_lorentz).numpy() for t in np.linspace(0., 1., int(geodesic_pts/2))] + [exp_map(y32_tgt * t, y3_lorentz).numpy() for t in np.linspace(0., 1., int(geodesic_pts/2))])

    data = np.vstack([pullback_geodesic, pullback_geodesic])
    data += 0.1 * np.random.rand(*data.shape)
    data[:, 0] = np.sqrt(1 + data[:, 1]**2  + data[:, 2]**2)

    mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    mlab.clf()
    mlab.mesh(x, y, z, color=surface_unique_color, opacity=mesh_alpha)

    # Plot data on the Lorentz model
    mlab.points3d(y1[0], y1[1], y1[2], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.points3d(y1a[0], y1a[1], y1a[2], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.points3d(y1b[0], y1b[1], y1b[2], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.points3d(y2[0], y2[1], y2[2], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.points3d(y2a[0], y2a[1], y2a[2], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.points3d(y2b[0], y2b[1], y2b[2], color=pltc.to_rgb('orange'), scale_factor=0.07)

    # Plot geodesic
    # mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=pltc.to_rgb('darkgreen'), line_width=3.5, tube_radius=None)
    mlab.plot3d(geodesic[:, 1], geodesic[:, 2], geodesic[:, 0], color=(0.4, 0.4, 0.4), line_width=5.0,
                tube_radius=None)
    
    # Plot pullback geodesic
    for n in range(data.shape[0]):
        mlab.points3d(data[n, 1], data[n, 2], data[n, 0], color=pltc.to_rgb('orange'), scale_factor=0.07)
    mlab.plot3d(pullback_geodesic[:, 1], pullback_geodesic[:, 2], pullback_geodesic[:, 0], color=(0., 0., 0.), line_width=5.5,
                tube_radius=None)

    # Plot tangent space
    # Tangent plane equation
    # x = np.linspace(-1.5, 1.5, 30) + y1[0]
    # y = np.linspace(-1.5, 1.5, 30) + y1[1]
    # x, y = np.meshgrid(x, y)
    # z = (y1[0] * (x - y1[0]) + y1[1] * (y - y1[1]) + y1[2]**2) / y1[2]
    # mlab.mesh(x, y, z, color=(0.9, 0.9, 0.9), opacity=0.5)
    # Plot contours of the tangent space
    normal_vec = np.array([2*y1[0], 2*y1[1], -2*y1[2]])
    normal_vec /= np.linalg.norm(normal_vec)
    psurf, contour = plot_sphere_tangent_plane(normal_vec, l_vert=1.0)
    mlab.mesh(psurf[0]+y1[0], psurf[1]+y1[1], psurf[2]+y1[2], color=(0.9, 0.9, 0.9), opacity=0.3)
    mlab.plot3d(contour[0]+y1[0], contour[1]+y1[1], contour[2]+y1[2], color=(0, 0, 0), line_width=2., tube_radius=None)
    # Plot tangent vector
    # draw_arrow_mayavi(y1[0], y1[1], y1[2], y2_tgt[1], y2_tgt[2], y2_tgt[0], r=0.03,
    #                   color=pltc.to_rgb('orange'), line_width=3.)

    # Plot Euclidean metric
    plot_ellipsoid(color=pltc.to_rgb('skyblue'), offset=y1, radius=0.2)
    # Plot pullback metric
    theta = np.linspace(0, 2 * np.pi, 150)
    radius = 0.2
    a = radius * np.cos(theta)
    b = radius * np.sin(theta)
    c = np.zeros(150)
    # Tangent axis at 0 rotation:
    metric = np.vstack([a,b,c])

    # Rotation matrix with respect to zero:
    axis, ang = axis_angle(normal_vec)
    R = Rodrigues_rotation_matrix(axis, -ang)

    # Tangent axis in new plane:
    metric = R.T.dot(metric) + y1[:, None]

    mlab.plot3d(metric[0], metric[1], metric[2], color=pltc.to_rgb('navy'), line_width=7.5, tube_radius=None)

    # # Plot Poincare unit circle
    # theta = np.linspace(0, 2 * np.pi, 150)
    # radius = 1.5
    # a = radius * np.cos(theta)
    # b = radius * np.sin(theta)
    # c = np.zeros(150)
    # mlab.plot3d(a, b, c, color=clr_blue, line_width=3.5, tube_radius=None)
    # # Plot data on the Poincare model
    # mlab.points3d(y1_poincare[0], y1_poincare[1], y1_poincare[2], color=pltc.to_rgb('turquoise'), scale_factor=0.1)
    # mlab.points3d(y2_poincare[0], y2_poincare[1], y2_poincare[2], color=pltc.to_rgb('mediumorchid'), scale_factor=0.1)
    # # Plot projection lines
    # y1_proj = np.stack((y1, y1_poincare))
    # y2_proj = np.stack((y2, y2_poincare))
    # mlab.plot3d(y1_proj[:, 0], y1_proj[:, 1], y1_proj[:, 2], color=(0.7, 0.7, 0.7), line_width=1.5, tube_radius=None)
    # mlab.plot3d(y2_proj[:, 0], y2_proj[:, 1], y2_proj[:, 2], color=(0.7, 0.7, 0.7), line_width=1.5, tube_radius=None)
    # # Plot geodesic
    # # mlab.plot3d(poincare_geodesic[:, 0], poincare_geodesic[:, 1], np.zeros(geodesic_pts), color=pltc.to_rgb('darkgreen'),
    # #             line_width=3.5, tube_radius=None)
    # mlab.plot3d(poincare_geodesic[:, 0], poincare_geodesic[:, 1], np.zeros(geodesic_pts),
    #             color=clr_red,
    #             line_width=3.5, tube_radius=None)

    mlab.view(0, 170)
    mlab.show()


if __name__ == "__main__":
    # plot_hyperboloid_mayavi()
    # plot_hyperbolic_explog_maps()
    # plot_hyperbolic_prl_trsp()
    # plot_hyperbolic_tangent_projection()
    # plot_hyperbolic_distribution()
    plot_hyperbolic_pullback()
