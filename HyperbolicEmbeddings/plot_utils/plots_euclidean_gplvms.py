import numpy as np
from scipy.interpolate import interp2d
import torch

from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm
import matplotlib.colors as pltc
from mayavi import mlab
from itertools import product
from gpytorch.likelihoods import Likelihood
from gpytorch.kernels import Kernel
from typing import Callable, Tuple
from HyperbolicEmbeddings.gplvm.gplvm_predict_pullback_metric import predict_pullback_metric
from HyperbolicEmbeddings.utils.gp_utils import Prediction

from HyperbolicEmbeddings.vae_baselines.vanilla_vae import VanillaVAEBaseline


def plot_euclidean_gplvm_2d(x_latent, x_colors, x_legend=None, save_path=None, show=True,
                            geodesics=None, geodesics_colors=None, model=None, fig=None, marker=None, alpha=None):
    if fig is None:
        fig = plt.figure(figsize=(8, 8))
        ax = plt.gca()
        close = True
    else:
        ax = fig.gca()
        close = False

    # Plot points
    if marker == "*":
        s = 400
    else:
        s = 140
    for n in range(x_latent.shape[0]):

        plt.scatter(
            x_latent[n, 0],
            x_latent[n, 1],
            c=x_colors[n],
            edgecolors="black",
            s=s,
            zorder=2,
            linewidths=2.0,
            marker=marker,
            alpha=alpha,
        )
        if x_legend:
            plt.text(
                x_latent[n, 0], x_latent[n, 1], x_legend[n], fontdict={"fontsize": 20}
            )
    xmin, xmax, ymin, ymax = plt.axis()

    diff_axes = (xmax - xmin) - (ymax - ymin)
    if diff_axes < 0:
        xmax += -diff_axes / 2.0
        xmin -= -diff_axes / 2.0
    elif diff_axes > 0:
        ymax += diff_axes / 2.0
        ymin -= diff_axes / 2.0

    # Plot magnification factor
    if model:
        if isinstance(model, VanillaVAEBaseline):
            X, Y, factor = get_magnification_factor_vanilla_VAE(
                model, 200, xmin, xmax, ymin, ymax
            )
            plt.pcolormesh(X, Y, factor, cmap="Greys")
        else:
            X, Y, factor = get_magnification_factor_euclidean(
                model, 200, xmin, xmax, ymin, ymax
            )
            # smoothed_factor = interp2d(X, Y, factor, kind='cubic')
            plt.pcolormesh(X, Y, factor, cmap="Greys")
            # plt.pcolormesh(X, Y, factor, cmap='bone_r')

    # Plot geodesics
    if geodesics:
        for g in range(len(geodesics)):
            geodesic = geodesics[g]
            if geodesics_colors:
                # Set colormap
                color_set = set(geodesics_colors[g])
                color_idx = list(range(len(color_set)))
                color_dict = dict(zip(color_set, color_idx))
                cmap = ListedColormap(color_set)
                norm_list = list(np.array(color_idx) - 0.5) + [color_idx[-1] + 0.5]
                norm = BoundaryNorm(norm_list, cmap.N)

                # Get int values for colors
                colors = np.array([color_dict[c] for c in geodesics_colors[g]])

                # Draw line
                points = geodesic[:, None]
                segments = np.concatenate([points[:-1], points[1:]], axis=1)
                lc = LineCollection(segments, cmap=cmap, norm=norm, zorder=1)
                lc.set_array(colors)
                lc.set_linewidth(4)
                line = ax.add_collection(lc)

            else:
                plt.plot(geodesic[:, 0], geodesic[:, 1], color="black")

    plt.axis('equal')
    plt.xlim([xmin, xmax])
    plt.ylim([ymin, ymax])
    plt.axis("off")

    if save_path is not None:
        fig.savefig(save_path)

    if show:
        plt.show()

    if close:
        plt.close()


def plot_euclidean_gplvm_3d(
    x_latent,
    x_colors,
    x_legend=None,
    geodesics=None,
    save_path=None,
    show=True,
    geodesics_colors=None,
    fig=None,
    marker=None,
    opacity=1.0,
):
    if fig is None:
        fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
        # mlab.points3d(0, 0, 0, color=pltc.to_rgb('black'), scale_factor=0.05)  # Plot center

    # Plot points
    if marker is None:
        marker = "sphere"
    if marker != "sphere":
        line_width = 5.0
        scale_factor = 0.2
    else:
        line_width = 2.0
        scale_factor = 0.15
    for n in range(x_latent.shape[0]):
        mlab.points3d(
            x_latent[n, 0],
            x_latent[n, 1],
            x_latent[n, 2],
            color=pltc.to_rgb(x_colors[n]),
            scale_factor=scale_factor,
            mode=marker,
            opacity=opacity,
            line_width=line_width,
        )
        if marker == "2dcross":
            mlab.points3d(
                x_latent[n, 0],
                x_latent[n, 1],
                x_latent[n, 2],
                color=pltc.to_rgb("black"),
                scale_factor=scale_factor + 0.02,
                mode="2dthick_cross",
                opacity=opacity,
                line_width=line_width,
            )
        if x_legend:
            mlab.text3d(
                x_latent[n, 0], x_latent[n, 1], x_latent[n, 2], x_legend[n], scale=0.03
            )

    # Plot geodesics
    if geodesics:
        for g in range(len(geodesics)):
            geodesic = geodesics[g]
            if geodesics_colors:
                color_set = geodesics_colors[g]

                for i in range(geodesic.shape[0] - 1):
                    mlab.plot3d(
                        geodesic[i : i + 2, 0],
                        geodesic[i : i + 2, 1],
                        geodesic[i : i + 2, 2],
                        color=pltc.to_rgb(color_set[i]),
                        line_width=4.5,
                        tube_radius=None,
                    )

            else:
                mlab.plot3d(
                    geodesic[:, 0],
                    geodesic[:, 1],
                    geodesic[:, 2],
                    color=pltc.to_rgb("black"),
                    line_width=4.5,
                    tube_radius=None,
                )

    if save_path is not None:
        mlab.savefig(save_path)

    if show:
        mlab.show()

    return fig


def plot_euclidean_gplvm_2d_added_data(x_latent, x_colors, added_x_latent, added_x_colors, alpha: float = 0.2,
                                       added_alpha: float = 1.0, model=None, save_path=None):
    # Build the figure
    fig = plt.figure(figsize=(8, 8))

    # Plotting the original data
    # Hyperparameters and weird stuff for magnification factor.
    plot_euclidean_gplvm_2d(
        x_latent,
        x_colors,
        x_legend=None,
        fig=fig,
        marker="o",
        alpha=alpha,
        model=model,
        show=False,
    )

    # Plotting the added data
    plot_euclidean_gplvm_2d(
        added_x_latent,
        added_x_colors,
        x_legend=None,
        fig=fig,
        marker="*",
        alpha=added_alpha,
        show=False,
    )

    if save_path is not None:
        fig.savefig(save_path)

    return fig


def plot_euclidean_gplvm_3d_added_data(x_latent, x_colors, added_x_latent, added_x_colors, alpha: float = 0.2,
                                       added_alpha: float = 1.0):
    # Plotting the original data
    # Hyperparameters and weird stuff for magnification factor.
    fig = plot_euclidean_gplvm_3d(
        x_latent,
        x_colors,
        x_legend=None,
        marker='sphere',
        opacity=alpha,
        show=False,
    )

    # Plotting the added data
    plot_euclidean_gplvm_3d(
        added_x_latent,
        added_x_colors,
        x_legend=None,
        fig=fig,
        marker='2dcross',
        opacity=added_alpha,
    )

    return fig


def get_magnification_factor_euclidean(
    model, num_pts, xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0
):
    x = np.linspace(xmin, xmax, num_pts)
    y = np.linspace(ymin, ymax, num_pts)
    X, Y = np.meshgrid(x, y)
    data = np.vstack((X.reshape(num_pts**2), Y.reshape(num_pts**2))).T
    posterior = model(torch.from_numpy(data))
    variance = posterior.variance.T.detach().numpy()

    factor = np.sum(variance, 1)

    return X, Y, factor.reshape(num_pts, num_pts)


def get_magnification_factor_vanilla_VAE(
    model: VanillaVAEBaseline, num_pts, xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0
):
    x = np.linspace(xmin, xmax, num_pts)
    y = np.linspace(ymin, ymax, num_pts)
    X, Y = np.meshgrid(x, y)
    data = np.vstack((X.reshape(num_pts**2), Y.reshape(num_pts**2))).T
    variance = (model.decode(torch.from_numpy(data)).scale ** 2).detach().numpy()
    factor = np.sum(variance, 1)

    return X, Y, factor.reshape(num_pts, num_pts)


def sample_2d_grid(x: torch.Tensor, grid_size: int = 128, padding_percent=0.05) -> Tuple[torch.Tensor,
                                                                                         Tuple[float, float]]:
    """
    computes the smalles grid that contains all the latent points in X plus 5% padding.

    Parameters
    -
    X: torch.shape([N, 2])  N latent points

    Returns
    -
    X_grid: torch.shape([grid_size*grid_size, 2]) regularly sampled latent points in a square grid
    X_grid = [
        x_1,             x_2,           , ... , x_grid_size, \\
        x_(grid_size+1), x_(grid_size+2), ... , x_(2*grid_size),
        ...
    ]
    limits: [lower_bound, upper_bound]
    """
    min_in_latent = x.min().item()
    max_in_latent = x.max().item()
    padding = (max_in_latent - min_in_latent) * padding_percent
    limits = (min_in_latent - padding, max_in_latent + padding)
    return torch.Tensor([[x, y] for x, y in product(torch.linspace(*limits, grid_size),
                                                    reversed(torch.linspace(*limits, grid_size)))]), limits


def _from_tensor_to_image(values: torch.Tensor, limits: Tuple[float], grid_size: int) -> np.ndarray:
    """
    Grabs a tensor of size (grid_size*grid_size) and transforms
    it into an image of size (grid_size, grid_size) according to
    the limits provided.

    Parameters
    ----------

    - values (torch.Tensor of shape (grid_size ** 2,)): the tensor
      to convert into an image.

    - limits (Tuple[float]): the limits of the gridsize (min, max).

    - grid_size (int): the size of the grid/image.

    Returns
    -------

    - image (np.ndarray of shape (grid_size, grid_size)): the image
      version of the values provided, assuming they are in the same
      order as the grid below.

    """
    grid = torch.Tensor([[x, y] for x, y in product(torch.linspace(*limits, grid_size),
                                                    reversed(torch.linspace(*limits, grid_size)))])
    z_to_values_map = {(z1.item(), z2.item()): values[i].item() for i, (z1, z2) in enumerate(grid)}
    image_of_values = np.zeros((grid_size, grid_size))
    for j, x in enumerate(torch.linspace(*limits, grid_size)):
        for i, y in enumerate(reversed(torch.linspace(*limits, grid_size))):
            image_of_values[i, j] = z_to_values_map[x.item(), y.item()]
    return image_of_values


def plot_gplvm_pullback_metric(x_latent: torch.Tensor, training_data: torch.Tensor, likelihood: Likelihood,
                               kernel: Kernel, x_colors, x_legend=None, save_path=None, show=True,
                               geodesics=None, geodesics_colors=None, fig=None, marker=None, alpha=None,
                               grid_size: int = 128):

    if fig is None:
        fig = plt.figure(figsize=(8, 8))
        ax = plt.gca()
        close = True
    else:
        ax = fig.gca()
        close = False

    # Plot points
    if marker == "*":
        s = 400
    else:
        s = 140
    for n in range(x_latent.shape[0]):
        plt.scatter(x_latent[n, 0], x_latent[n, 1], c=x_colors[n], edgecolors="black", s=s, zorder=2, linewidths=2.0,
                    marker=marker, alpha=alpha)
        if x_legend:
            plt.text(x_latent[n, 0], x_latent[n, 1], x_legend[n], fontdict={"fontsize": 20})

    # xmin, xmax, ymin, ymax = plt.axis()
    #
    # diff_axes = (xmax - xmin) - (ymax - ymin)
    # if diff_axes < 0:
    #     xmax += -diff_axes / 2.0
    #     xmin -= -diff_axes / 2.0
    # elif diff_axes > 0:
    #     ymax += diff_axes / 2.0
    #     ymin -= diff_axes / 2.0

    def get_volume(X_grid: torch.Tensor):
        pullback_metric = predict_pullback_metric(X_grid, x_latent, training_data, likelihood, kernel)
        volumes = torch.Tensor([G.det().sqrt().item() for G in pullback_metric])
        return volumes.numpy()

    # def get_variance(x_grid_vals: torch.Tensor):
        # prediction = predict_eval_inputs(x_grid_vals)
        # return (prediction.stddev**2).sum(axis=1)

    # Plot metric volume
    x_grid, limits = sample_2d_grid(x_latent, grid_size)
    img_tensor = get_volume(x_grid)
    img = _from_tensor_to_image(img_tensor, limits, grid_size)

    plot_ = ax.imshow(img, extent=[*limits, *limits], interpolation="bicubic")
    cbar = plt.colorbar(plot_, ax=ax, fraction=0.046, pad=0.04)
    cbar.ax.tick_params(labelsize=24)

    # Plot geodesics if any
    if geodesics:
        if isinstance(geodesics, list):
            for g in range(len(geodesics)):
                geodesic = geodesics[g]
                if geodesics_colors:
                    # Set colormap
                    color_set = set(geodesics_colors[g])
                    color_idx = list(range(len(color_set)))
                    color_dict = dict(zip(color_set, color_idx))
                    cmap = ListedColormap(color_set)
                    norm_list = list(np.array(color_idx) - 0.5) + [color_idx[-1] + 0.5]
                    norm = BoundaryNorm(norm_list, cmap.N)

                    # Get int values for colors
                    colors = np.array([color_dict[c] for c in geodesics_colors[g]])

                    # Draw line
                    points = geodesic[:, None]
                    segments = np.concatenate([points[:-1], points[1:]], axis=1)
                    lc = LineCollection(segments, cmap=cmap, norm=norm, zorder=1)
                    lc.set_array(colors)
                    lc.set_linewidth(4)
                    line = ax.add_collection(lc)

                else:
                    plt.plot(geodesic.squeeze()[:, 0], geodesic.squeeze()[:, 1], color="black")

        else:
            plt.plot(geodesics.squeeze()[:, 0], geodesics.squeeze()[:, 1], color=[0.8, 0.8, 0.8], linewidth=2)

    plt.axis('equal')
    # plt.xlim([xmin, xmax])
    # plt.ylim([ymin, ymax])
    plt.axis("off")

    if save_path is not None:
        fig.savefig(save_path)

    if show:
        plt.show()

