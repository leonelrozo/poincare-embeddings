import numpy as np
import sklearn.datasets
import matplotlib.pyplot as plt
import umap

'''
    Simple example showing a uMap dimensionality reduction from a high-dim hyperbolic space (Poincare ball) to a 
    bi-dimensional hyperbolic space (Lorentz hyperboloid). 
    The resulting low-dimensional data in the Lorentz model is then transformed to be visualized in the Poincare circle.
'''

if __name__ == '__main__':
    # Load PenDigits dataset (these are originally in Euclidean space but "assumed" to be in the Poincare ball"
    digits = sklearn.datasets.load_digits()

    # uMAP dimensionality reduction from Poincare ball to Lorentz hyperboloid.
    hyperbolic_mapper = umap.UMAP(metric='poincare', output_metric='hyperboloid', random_state=42).fit(digits.data)

    # The code lines below are for visualization purposes
    # View the x and y coordinates of the Lorentz hyperboloid
    # plt.scatter(hyperbolic_mapper.embedding_.T[0], hyperbolic_mapper.embedding_.T[1], c=digits.target, cmap='Spectral')
    # plt.show()

    #  Solve for the z coordinate and view the data lying on a hyperboloid in 3D space
    x = hyperbolic_mapper.embedding_[:, 0]
    y = hyperbolic_mapper.embedding_[:, 1]
    z = np.sqrt(1 + np.sum(hyperbolic_mapper.embedding_ ** 2, axis=1))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x, y, z, c=digits.target, cmap='Spectral')
    ax.view_init(35, 80)
    plt.show()

    # Map the data into the Poincare model in 2D.
    disk_x = x / (1 + z)
    disk_y = y / (1 + z)

    # Visualize the data in a Poincare disk
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(disk_x, disk_y, c=digits.target, cmap='Spectral')
    boundary = plt.Circle((0, 0), 1, fc='none', ec='k')
    ax.add_artist(boundary)
    ax.axis('off')
    plt.show()
