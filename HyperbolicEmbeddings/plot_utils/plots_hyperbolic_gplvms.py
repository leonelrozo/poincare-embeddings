import numpy as np
import torch
import os
from os import path
import math

from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm
import matplotlib.colors as pltc
from mayavi import mlab

from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import poincare_to_lorentz
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import reorder_taxonomy_data, color_function_shape_poses
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import simple_color_function_grasp
from HyperbolicEmbeddings.taxonomy_utils.BimanualManipulationDataParse import simple_color_function_bimanual
from HyperbolicEmbeddings.vae_baselines.hyperbolic_vae import HyperbolicVAEBaseline


def plot_hyperbolic_gplvm_2d(x_poincare, x_colors, x_legend=None, save_path=None, show=True, geodesics=None,
                             geodesics_colors=None, model=None, model_magnification_path=None, max_magnification=None,
                             fig=None, marker=None, alpha=None):
    if fig is None:
        fig = plt.figure(figsize=(8, 8))
        ax = plt.gca()
        close = True
    else:
        close = False
        ax = fig.gca()

    # Plot Poincaré disk
    circle = plt.Circle(np.array([0, 0]), radius=1., color='black', fill=False, linewidth=2., zorder=3)
    ax.add_patch(circle)

    # Plot points
    if marker == "*":
        s = 400
    else:
        s = 140
    for n in range(x_poincare.shape[0]):
        plt.scatter(x_poincare[n, 0], x_poincare[n, 1], c=x_colors[n], edgecolors='black', s=s, linewidths=2.,
                    zorder=2, alpha=alpha, marker=marker)
        if x_legend:
            plt.text(x_poincare[n, 0], x_poincare[n, 1], x_legend[n], fontdict={"fontsize": 10})

    # Plot magnification factor
    if model:
        if model_magnification_path and path.exists(model_magnification_path):
            magnification = np.load(model_magnification_path)
            X = magnification['X']
            Y = magnification['Y']
            factor = magnification['factor']
        else:
            if isinstance(model, HyperbolicVAEBaseline):
                X, Y, factor = get_magnification_factor_hyperbolic_vae(model, 200, model_magnification_path)
            else:
                X, Y, factor = get_magnification_factor_poincare(model, 200, model_magnification_path)

        if max_magnification:
            factor[factor > max_magnification] = max_magnification
            # Correct for numerical imprecisions
            factor[np.where((factor > 0.) & (X**2 + Y**2 > 0.92))] = max_magnification
            # factor[0, factor[0] > 0.0] = max_magnification
            # factor[-1, factor[-1] > 0.0] = max_magnification
            # factor[factor[-1] > 0.0, 0] = max_magnification
            # factor[factor[-1] > 0.0, -1] = max_magnification

        plt.pcolormesh(X, Y, factor, cmap='Greys')
        # plt.pcolormesh(X, Y, factor, cmap='bone_r')

    # Plot geodesics
    if geodesics:
        for g in range(len(geodesics)):
            geodesic = geodesics[g]
            if geodesics_colors:
                # Set colormap
                color_set = set(geodesics_colors[g])
                color_idx = list(range(len(color_set)))
                color_dict = dict(zip(color_set, color_idx))
                cmap = ListedColormap(color_set)
                norm_list = list(np.array(color_idx) - 0.5) + [color_idx[-1] + 0.5]
                norm = BoundaryNorm(norm_list, cmap.N)

                # Get int values for colors
                colors = np.array([color_dict[c] for c in geodesics_colors[g]])

                # Draw line
                points = geodesic[:, None]
                segments = np.concatenate([points[:-1], points[1:]], axis=1)
                lc = LineCollection(segments, cmap=cmap, norm=norm, zorder=1)
                lc.set_array(colors)
                lc.set_linewidth(4)
                line = ax.add_collection(lc)

            else:
                plt.plot(geodesic[:, 0], geodesic[:, 1], color='black')

    plt.axis('off')

    if save_path is not None:
        fig.savefig(save_path)

    if show:
        plt.show()
    
    if close:
        plt.close()
    

def plot_hyperbolic_gplvm_3d(x_poincare, x_colors, x_legend=None, save_path=None, show=True, geodesics=None,
                             geodesics_colors=None, fig=None, marker=None, opacity=1.0):

    if fig is None:
        # Mayavi plot of the Poincare ball
        fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
        num_pts = 200
        u = np.linspace(0, 2 * np.pi, num_pts)
        v = np.linspace(0, np.pi, num_pts)
        x = 1 * np.outer(np.cos(u), np.sin(v))
        y = 1 * np.outer(np.sin(u), np.sin(v))
        z = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
        mlab.clf()
        mlab.mesh(x, y, z, color=(0.7, 0.7, 0.7), opacity=0.3)
        # mlab.points3d(0, 0, 0, color=pltc.to_rgb('black'), scale_factor=0.05)  # Plot center

    # Plot points
    if marker is None:
        marker = 'sphere'
    if marker != 'sphere':
        line_width = 5.
        scale_factor = 0.12
    else:
        line_width = 2.
        scale_factor = 0.07
    for n in range(x_poincare.shape[0]):
        mlab.points3d(x_poincare[n, 0], x_poincare[n, 1], x_poincare[n, 2],
                      color=pltc.to_rgb(x_colors[n]), scale_factor=scale_factor, mode=marker, opacity=opacity,
                      line_width=line_width)
        if marker == '2dcross':
            mlab.points3d(x_poincare[n, 0], x_poincare[n, 1], x_poincare[n, 2],
                          color=pltc.to_rgb('black'), scale_factor=scale_factor+0.02, mode='2dthick_cross', opacity=opacity, line_width=line_width)
        if x_legend:
            mlab.text3d(x_poincare[n, 0], x_poincare[n, 1], x_poincare[n, 2], x_legend[n],
                        scale=0.03)

    # Plot geodesics
    if geodesics:
        for g in range(len(geodesics)):
            geodesic = geodesics[g]
            if geodesics_colors:
                color_set = geodesics_colors[g]

                for i in range(geodesic.shape[0]-1):
                    mlab.plot3d(geodesic[i:i+2, 0], geodesic[i:i+2, 1], geodesic[i:i+2, 2],
                                color=pltc.to_rgb(color_set[i]), line_width=4.5, tube_radius=None)

            else:
                mlab.plot3d(geodesic[:, 0], geodesic[:, 1], geodesic[:, 2], color=pltc.to_rgb('black'),
                            line_width=4.5, tube_radius=None)

    if save_path is not None:
        # from tvtk.api import tvtk
        # e = tvtk.GL2PSExporter()
        mlab.savefig(str(save_path), size=(400, 400))

    if show:
        mlab.show()
    
    # mlab.close()
    return fig


def plot_hyperbolic_gplvm_2d_added_data(x_poincare, x_colors, added_x_poincare, added_x_colors, alpha: float = 0.2,
                                        added_alpha: float = 1.0, model=None, model_magnification_path=None,
                                        max_magnification=None, save_path=None):
    # Build the figure
    fig = plt.figure(figsize=(8, 8))

    # Plotting the original data
    # Hyperparameters and weird stuff for magnification factor.
    plot_hyperbolic_gplvm_2d(
        x_poincare,
        x_colors,
        x_legend=None,
        fig=fig,
        marker="o",
        alpha=alpha,
        show=False,
        model=model,
        model_magnification_path=model_magnification_path,
        max_magnification=max_magnification
    )

    # Plotting the added data
    plot_hyperbolic_gplvm_2d(
        added_x_poincare,
        added_x_colors,
        x_legend=None,
        fig=fig,
        marker="*",
        alpha=added_alpha,
        show=False
    )

    if save_path is not None:
        fig.savefig(save_path)

    return fig


def plot_hyperbolic_gplvm_3d_added_data(x_poincare, x_colors, added_x_poincare, added_x_colors, alpha: float = 0.2,
                                        added_alpha: float = 1.0):
    # Plotting the original data
    # Hyperparameters and weird stuff for magnification factor.
    fig = plot_hyperbolic_gplvm_3d(
        x_poincare,
        x_colors,
        x_legend=None,
        marker='sphere',
        opacity=alpha,
        show=False,
    )

    # Plotting the added data
    plot_hyperbolic_gplvm_3d(
        added_x_poincare,
        added_x_colors,
        x_legend=None,
        fig=fig,
        marker='2dcross',
        opacity=added_alpha,
        show=True
    )

    return fig


def get_magnification_factor_hyperbolic_vae(model, num_pts, model_path):
    x = np.linspace(-0.99, 0.99, num_pts)
    y = np.linspace(-0.99, 0.99, num_pts)
    X, Y = np.meshgrid(x, y)
    data = np.vstack((X.reshape(num_pts ** 2), Y.reshape(num_pts ** 2))).T
    norm_data = np.linalg.norm(data, axis=1)
    idx = np.argwhere(norm_data < 1.0)[:, 0]

    data_lorentz = poincare_to_lorentz(torch.from_numpy(data[idx]))
    decoded_distribution = model.decode(data_lorentz)
    variance = (decoded_distribution.scale ** 2).detach().numpy()

    factor = np.zeros(num_pts**2)
    factor[idx] = np.sum(variance, 1)
    factor = factor.reshape(num_pts, num_pts)

    return X, Y, factor.reshape(num_pts, num_pts)


def get_magnification_factor_poincare(model, num_pts, model_path):
    # r = np.linspace(0., 0.99, num_pts)
    # theta = np.linspace(0., 2*np.pi - 0.01, num_pts)
    # X = r[None] * np.cos(theta[:, None])
    # Y = r[None] * np.sin(theta[:, None])
    x = np.linspace(-0.99, 0.99, num_pts)
    y = np.linspace(-0.99, 0.99, num_pts)
    X, Y = np.meshgrid(x, y)
    data = np.vstack((X.reshape(num_pts ** 2), Y.reshape(num_pts ** 2))).T
    norm_data = np.linalg.norm(data, axis=1)
    idx = np.argwhere(norm_data < 1.0)[:, 0]

    data_lorentz = poincare_to_lorentz(torch.from_numpy(data[idx]))

    variance = []
    # posterior = model(data_lorentz)
    # variance = posterior.variance.T.detach().numpy()
    n = 1
    n_data_batch = 100
    while n < math.ceil(idx.shape[0]/n_data_batch):
        print(n)
        posterior = model(data_lorentz[(n-1)*n_data_batch:n*n_data_batch])
        variance.append(posterior.variance.T.detach().numpy())
        del posterior  # to clear the memory
        n += 1
    posterior = model(data_lorentz[(n - 1) * n_data_batch:])
    variance.append(posterior.variance.T.detach().numpy())
    variance = np.vstack(variance)

    factor = np.zeros(num_pts**2)
    factor[idx] = np.sum(variance, 1)
    factor = factor.reshape(num_pts, num_pts)

    np.savez(model_path, X=X, Y=Y, factor=factor)

    return X, Y, factor


def plot_legend_2d(nodes_names, legend, color_function, save_path=None):
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "Helvetica"
    })

    N = len(nodes_names)
    # shape_pose_names = reorder_taxonomy_data(shape_pose_names, shape_pose_names)
    # legend = reorder_taxonomy_data(legend, shape_pose_names)

    x_colors = []
    for n in range(N):
        x_colors.append(color_function(nodes_names[n]))

    x = np.linspace(1, N, N)

    fig = plt.figure(figsize=(20, 2))
    ax = plt.gca()
    for n in range(N):
        plt.scatter(x[n], 0.0, c=x_colors[n], edgecolors='black', s=180, linewidths=2.,
                    zorder=2)
        plt.text(x[n] + 0.2, -0.005, legend[n], fontdict={"fontsize": 20})
    # plt.legend(legend, ncol=N)

    plt.axis('off')
    if save_path is not None:
        fig.savefig(save_path)

    plt.show()


def plot_legend_3d(nodes_names, legend, color_function, save_path=None):
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "Helvetica"
    })

    N = len(nodes_names)
    # shape_pose_names = reorder_taxonomy_data(shape_pose_names, shape_pose_names)
    # legend = reorder_taxonomy_data(legend, shape_pose_names)

    x_colors = []
    for n in range(N):
        x_colors.append(color_function(nodes_names[n]))

    x = 0.1 * np.linspace(1, N, N)

    fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(700, 700))
    for n in range(N):
        mlab.points3d(x[n], 0.0, 0.0,
                      color=pltc.to_rgb(x_colors[n]), scale_factor=0.07, mode='sphere', opacity=1.0,
                      line_width=2.0)
    mlab.show()


if __name__ == '__main__':
    CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
    FIGURE_PATH = os.path.join(CURRENT_DIR, '../../images/')

    # Legend for semifull
    shape_pose_names = [
        'Foot', 'FootHand', 'FootFoot', 'FootHandFoot', 'HandFootHand', 'FootHandFootHand', 'FootKnee',  'FootHandKnee',
        'HandKneeFootHand', 'Knee', 'HandKnee', 'KneeKnee', 'HandKneeHand', 'HandKneeKnee', 'HandKneeHandKnee',
    ]
    legend = [
        r'$\mathsf{F}$', r'$\mathsf{FH}$', r'$\mathsf{F}_2$', r'$\mathsf{F}_2\mathsf{H}$', r'$\mathsf{FH}_2$',
        r'$\mathsf{F}_2\mathsf{H}_2$', r'$\mathsf{FK}$', r'$\mathsf{FKH}$', r'$\mathsf{FKH}_2$',
        r'$\mathsf{K}$', r'$\mathsf{KH}$', r'$\mathsf{K}_2$', r'$\mathsf{KH}_2$', r'$\mathsf{K}_2\mathsf{H}$',
        r'$\mathsf{K}_2\mathsf{H}_2$',
    ]
    # plot_legend_2d(shape_pose_names, legend, color_function_shape_poses, FIGURE_PATH + 'legend_semifull.png')
    # plot_legend_3d(shape_pose_names, legend, color_function_shape_poses)

    # # Legend for feet
    # shape_pose_names = [
    #     'Foot', 'FootHand', 'FootFoot', 'FootHandFoot', 'HandFootHand', 'FootHandFootHand',
    # ]
    # legend = [
    #     r'$\mathsf{F}^l$, $\mathsf{F}^r$',
    #     r'$\mathsf{F}^l\mathsf{H}^l$, $\mathsf{F}^l\mathsf{H}^r$, $\mathsf{F}^r\mathsf{H}^l$, $\mathsf{F}^r\mathsf{H}^r$',
    #     r'$\mathsf{F}_2$',
    #     r'$\mathsf{F}_2\mathsf{H}^l$, $\mathsf{F}_2\mathsf{H}^r$',
    #     r'$\mathsf{F}^l\mathsf{H}_2$, $\mathsf{F}^r\mathsf{H}_2$',
    #     r'$\mathsf{F}_2\mathsf{H}_2$'
    # ]
    # plot_legend_2d(shape_pose_names, legend, color_function_shape_poses, FIGURE_PATH + 'legend_feet.png')

    # Legend for grasps
    grasps_names = [
        "Lateral", "ExtensionType", "Quadpod", "ParallelExtension", "IndexFingerExtension", "Stick",
        "WritingTripod", "PrismaticFourFingers", "PowerDisk", "LargeDiameter", "MediumWrap",
        "SmallDiameter", "FixedHook", "Tripod", "PowerSphere", "PrecisionSphere", "ThreeFingersSphere",
        "PrismaticPinch", "TipPinch", "Ring"
    ]
    legend = [
        r'$\mathsf{La}$', r'$\mathsf{ET}$', r'$\mathsf{Qu}$', r'$\mathsf{PE}$', r'$\mathsf{IE}$', r'$\mathsf{St}$',
        r'$\mathsf{WT}$', r'$\mathsf{PF}$', r'$\mathsf{PD}$', r'$\mathsf{LD}$', r'$\mathsf{MW}$',
        r'$\mathsf{SD}$', r'$\mathsf{FH}$', r'$\mathsf{Tr}$', r'$\mathsf{PS}$', r'$\mathsf{RS}$', r'$\mathsf{TS}$',
        r'$\mathsf{PP}$', r'$\mathsf{TP}$', r'$\mathsf{Ri}$'
    ]
    # plot_legend_2d(grasps_names, legend, simple_color_function_grasp, FIGURE_PATH + 'legend_grasps.png')
    plot_legend_3d(grasps_names, legend, simple_color_function_grasp)

    # Legend for grasps
    bimanual_names = [
        "unimanual_left", "unimanual_right", "bimanual", "loosely_coupled",
        "tightly_coupled_asym_l", "tightly_coupled_asym_r", "tightly_coupled_sym"
    ]
    legend = [
        r'$\mathsf{U}_{\mathsf{left}}$', r'$\mathsf{U}_{\mathsf{right}}$', r'$\mathsf{B}$', r'$\mathsf{LC}$',
        r'$\mathsf{TCA}_{\mathsf{left}}$', r'$\mathsf{TCA}_{\mathsf{right}}$', r'$\mathsf{TCS}$'
    ]
    # plot_legend_2d(bimanual_names, legend, simple_color_function_bimanual, FIGURE_PATH + 'legend_bimanual.png')
    # plot_legend_3d(bimanual_names, legend, simple_color_function_bimanual)