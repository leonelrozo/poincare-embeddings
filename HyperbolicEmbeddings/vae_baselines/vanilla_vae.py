"""
Here we specify the vanilla VAE baseline model.
It is just a 2-layer-encoder with symmetric decoder,
with the same latent space size.
"""
from typing import List, Tuple

import torch
import torch.nn as nn

from torch.distributions import Normal, kl_divergence

from HyperbolicEmbeddings.losses.graph_based_loss import (
    ZeroAddedLossTermExactMLL,
    EuclideanStressLossTermExactMLL,
)

torch.set_default_dtype(torch.float64)


class VanillaVAEBaseline(torch.nn.Module):
    def __init__(
        self,
        input_dim: int = 12,
        latent_dim: int = 2,
        hidden_dim: int = 6,
        loss_type: str = "Zero",
        graph_distance_matrix: torch.Tensor = None,
        loss_scale: float = 0.1,
    ) -> None:
        super().__init__()
        self.input_dim = input_dim
        self.latent_dim = latent_dim
        self.hidden_dim = hidden_dim
        self.loss_type = loss_type
        self.loss_scale = loss_scale

        if loss_type == "Zero":
            self.added_loss = ZeroAddedLossTermExactMLL()
        elif loss_type == "Stress":
            assert (
                graph_distance_matrix is not None
            ), "You need to provide the graph distance matrix as a kwarg."
            self.added_loss = EuclideanStressLossTermExactMLL(
                graph_distance_matrix=graph_distance_matrix, loss_scale=loss_scale
            )

        self.encoder = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.Tanh(),
        )
        self.encoder_mu = nn.Sequential(nn.Linear(hidden_dim, latent_dim))
        self.encoder_log_var = nn.Sequential(
            nn.Linear(hidden_dim, latent_dim),
        )

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim, hidden_dim),
            nn.Tanh(),
        )
        self.decoder_mu = nn.Sequential(nn.Linear(hidden_dim, input_dim))
        self.decoder_log_var = nn.Sequential(nn.Linear(hidden_dim, input_dim))

        self.p_z = Normal(torch.zeros(latent_dim), torch.ones(latent_dim))

    def encode(self, pose_data: torch.Tensor) -> Normal:
        """Returns a normal distribution over latent codes q(z|x)"""
        hidden = self.encoder(pose_data)
        mu, log_var = self.encoder_mu(hidden), self.encoder_log_var(hidden)

        return Normal(mu, torch.exp(0.5 * log_var))

    def decode(self, latent_codes: torch.Tensor) -> Normal:
        """Returns a Normal distribution over reconstructions p(x|z)"""
        hidden = self.decoder(latent_codes)
        mu, log_var = self.decoder_mu(hidden), self.decoder_log_var(hidden)

        return Normal(mu, torch.exp(0.5 * log_var))

    def forward(self, pose_data: torch.Tensor) -> Tuple[Normal, Normal, torch.Tensor]:
        """
        Returns q(z|x) and p(x|z).
        """
        q_z_given_x = self.encode(pose_data)
        latent_codes = q_z_given_x.rsample()
        p_x_given_z = self.decode(latent_codes)

        return [q_z_given_x, p_x_given_z, latent_codes]

    def elbo_loss(self, pose_data: torch.Tensor) -> List[torch.Tensor]:
        """Computes the elbo loss of a given input against the prior p(z)"""
        q_z_given_x, p_x_given_z, latent_codes = self.forward(pose_data)

        rec_loss = -p_x_given_z.log_prob(pose_data).sum(dim=-1)
        kld_loss = kl_divergence(q_z_given_x, self.p_z).sum(dim=-1)

        added_loss = -self.added_loss.loss(latent_codes)

        return (
            (rec_loss + kld_loss + added_loss).mean(),
            rec_loss.mean(),
            kld_loss.mean(),
            added_loss,
        )
