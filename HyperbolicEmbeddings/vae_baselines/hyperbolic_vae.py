"""
This implementation of a Hyperbolic VAE
is taken and adapted from:

https://github.com/joeybose/HyperbolicNF/blob/master/models/hyperbolic_vae.py

Including most of the mathematical tools they use. We use our
LorentzWrappedNormal implementation (which is adapted from Bose's)
"""

from typing import List, Tuple

import torch
import torch.nn as nn

from torch.distributions import Normal, kl_divergence

from HyperbolicEmbeddings.losses.graph_based_loss import (
    ZeroAddedLossTermExactMLL,
    HyperbolicStressLossTermExactMLL,
)
from HyperbolicEmbeddings.hyperbolic_distributions.hyperbolic_wrapped_normal import (
    LorentzWrappedNormal,
)
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import (
    exp_map_mu0,
    log_map_mu0,
)
from HyperbolicEmbeddings.hyperbolic_manifold.math_ops_hyperbolic import (
    expand_proj_dims,
)

torch.set_default_dtype(torch.float64)


class HyperbolicVAEBaseline(torch.nn.Module):
    def __init__(
        self,
        input_dim: int = 12,
        latent_dim: int = 2,
        hidden_dim: int = 6,
        loss_type: str = "Zero",
        graph_distance_matrix: torch.Tensor = None,
        loss_scale: float = 0.1,
    ) -> None:
        super().__init__()
        self.input_dim = input_dim
        self.latent_dim = latent_dim
        self.hidden_dim = hidden_dim
        self.loss_type = loss_type
        self.loss_scale = loss_scale

        if loss_type == "Zero":
            self.added_loss = ZeroAddedLossTermExactMLL()
        elif loss_type == "Stress":
            assert (
                graph_distance_matrix is not None
            ), "You need to provide the graph distance matrix as a kwarg."
            self.added_loss = HyperbolicStressLossTermExactMLL(
                graph_distance_matrix=graph_distance_matrix, loss_scale=loss_scale
            )

        self.encoder = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.Tanh(),
        )
        self.encoder_mu = nn.Sequential(nn.Linear(hidden_dim, latent_dim))
        self.encoder_log_var = nn.Sequential(
            nn.Linear(hidden_dim, latent_dim),
        )

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim + 1, hidden_dim),
            nn.Tanh(),
        )
        self.decoder_mu = nn.Sequential(nn.Linear(hidden_dim, input_dim))
        self.decoder_log_var = nn.Sequential(nn.Linear(hidden_dim, input_dim))

        self.p_z = LorentzWrappedNormal(
            expand_proj_dims(torch.zeros(latent_dim)), torch.ones(latent_dim)
        )

    def encode(self, pose_data: torch.Tensor) -> LorentzWrappedNormal:
        """
        Returns a hyperbolic normal distribution (in our case, a wrapped
        Lorentz Normal) over latent codes q(z|x) = HN(enc_mu(x), enc_log_var(x))
        """
        hidden = self.encoder(pose_data)
        mu, log_var = self.encoder_mu(hidden), self.encoder_log_var(hidden)

        # We need to exp the mu, move it from tangent to the manifold.
        mu_h = exp_map_mu0(expand_proj_dims(mu))
        return LorentzWrappedNormal(mu_h, torch.exp(0.5 * log_var))

    def decode(self, latent_codes: torch.Tensor) -> Normal:
        """
        Returns a Normal distribution over reconstructions p(x|z)
        where the z are actually living in the hyperbolic latent
        space. This means that we need to do a log map first.
        """
        latent_codes_in_tangent = log_map_mu0(latent_codes)
        hidden = self.decoder(latent_codes_in_tangent)
        mu, log_var = self.decoder_mu(hidden), self.decoder_log_var(hidden)

        return Normal(mu, torch.exp(0.5 * log_var))

    def forward(self, pose_data: torch.Tensor) -> Tuple[Normal, Normal, torch.Tensor]:
        """
        Returns q(z|x) and p(x|z).
        """
        q_z_given_x = self.encode(pose_data)
        latent_codes = q_z_given_x.rsample()
        p_x_given_z = self.decode(latent_codes)

        return [q_z_given_x, p_x_given_z, latent_codes]

    def elbo_loss(self, pose_data: torch.Tensor) -> List[torch.Tensor]:
        """Computes the elbo loss of a given input against the prior p(z)"""
        q_z_given_x, p_x_given_z, latent_codes = self.forward(pose_data)

        rec_loss = -p_x_given_z.log_prob(pose_data).sum(dim=-1)
        kld_loss = kl_divergence(q_z_given_x, self.p_z).sum(dim=-1)

        added_loss = -self.added_loss.loss(latent_codes)

        return (
            (rec_loss + kld_loss + added_loss).mean(),
            rec_loss.mean(),
            kld_loss.mean(),
            added_loss,
        )
