from pathlib import Path
from typing import Tuple, Any, Optional, Dict, List
import numpy as np
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import torch.distributions
from torch.distributions import Normal
from torch import Tensor
import matplotlib.pyplot as plt

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()

# The code of this file was originally implemented in https://github.com/joeybose/HyperbolicNF/tree/master/data.


class ToDefaultTensor(transforms.Lambda):

    def __init__(self) -> None:
        super().__init__(lambda x: x.to(torch.get_default_dtype()))


class EuclideanUniform(torch.distributions.Uniform):

    def log_prob(self, value: Tensor) -> Tensor:
        return super().log_prob(value).sum(dim=-1)
    

class ImageDynamicBinarization:

    def __init__(self, train: bool, invert: bool = False) -> None:
        self.uniform = EuclideanUniform(0, 1)
        self.train = train
        self.invert = invert

    def __call__(self, x: torch.Tensor) -> torch.Tensor:
        x = x.reshape((-1,))  # Reshape per element, not batched yet.
        if self.invert:
            x = 1 - x
        if self.train:
            x = x > self.uniform.sample(x.shape)  # dynamic binarization
        else:
            x = x > 0.5  # fixed binarization for eval
        x = x.to(torch.get_default_dtype())
        return x
    

class MnistDataset:
    def __init__(self, batch_size: int) -> None:
        self.batch_size = batch_size
        self._in_dim = 784
        self._img_dims = (-1, 1, 28, 28)
        self.data_folder = ROOT_DIR / 'data/'

    @property
    def img_dims(self) -> Optional[Tuple[int, ...]]:
        return self._img_dims

    @property
    def in_dim(self) -> int:
        return self._in_dim

    def _get_dataset(self, train: bool, download: bool, transform: Any) -> torch.utils.data.Dataset:
        return datasets.MNIST(self.data_folder, train=train, download=download, transform=transform)

    def _load_mnist(self, train: bool, download: bool) -> DataLoader:
        # transformation = transforms.Compose(
            # [transforms.ToTensor(),
             # ToDefaultTensor(),
             # transforms.Lambda(ImageDynamicBinarization(train=train))])
        transformation = transforms.Compose(
            [transforms.ToTensor(),
            #  ToDefaultTensor(),
             ImageDynamicBinarization(train=train)])
        return DataLoader(dataset=self._get_dataset(train, download, transform=transformation),
                          batch_size=self.batch_size,
                          num_workers=8,
                          pin_memory=True,
                          shuffle=train)
    
    def get_dynamic_binary_mnist_subset(self, train: bool, data_per_class: int = None, classes: List = None)-> torch.utils.data.Dataset:
        transformation = transforms.Compose(
            [transforms.ToTensor(),
             ToDefaultTensor(),
             ImageDynamicBinarization(train=train)])
        dataset = self._get_dataset(train=train, download=True, transform=transformation)

        # Initialize a dictionary to store 100 samples from each class
        if classes is None:
            classes = list(range(10))
        samples_per_class = {c: [] for c in classes}

        # Loop through the dataset and extract 100 samples for each class
        for image, label in dataset:
            if label in samples_per_class:
                if len(samples_per_class[label]) < data_per_class:
                    samples_per_class[label].append(image)
            if all(len(samples_per_class[c]) == data_per_class for c in classes):
                break

        # Convert the lists to torch tensors
        for label in samples_per_class:
            samples_per_class[label] = torch.stack(samples_per_class[label])

        # Display the shape of the extracted data
        # for label in range(10):
        #     print(f"Class {label}: {samples_per_class[label].shape}")

        # Stack all class tensors into a single tensor
        all_samples = torch.cat([samples_per_class[label] for label in classes], dim=0)

        all_labels = torch.tensor(np.hstack([[label] * data_per_class for label in classes]))

        # Plot the 25 first samples of 3
        # plt.figure(figsize=(10, 10))
        # for i in range(25):
        #     plt.subplot(5, 5, i + 1)
        #     plt.imshow(all_samples[300 + i].reshape(28,28), cmap='gray')
        #     plt.axis('off')
        # plt.show()

        return all_samples, all_labels
    
    def create_loaders(self) -> Tuple[DataLoader, DataLoader]:
        train_loader = self._load_mnist(train=True, download=False)
        test_loader = self._load_mnist(train=False, download=False)
        return train_loader, test_loader

    def reconstruction_loss(self, x_mb_: torch.Tensor, x_mb: torch.Tensor) -> torch.Tensor:
        return F.binary_cross_entropy_with_logits(x_mb_, x_mb, reduction="none")
    
    def metrics(self, x_mb_: torch.Tensor, mode: str = "train") -> Dict[str, float]:
        return {}
    

def mnist_color_function(label):
    if label == 0:
        color = "darkgreen"  
    elif label == 1:
        color = (0.0, 0.1, 0.0)  
    elif label == 2:
        color = "aquamarine"
    elif label == 3:
        color = "gray"
    elif label == 4:
        color = (1.0, 0.4, 0.0)  
    elif label == 5:
        color = (1.0, 0.7, 0.0)  
    elif label == 6:
        color = (0.5, 0.7, 1.0)  
    elif label == 7:
        color = "mediumblue"
    elif label == 8:
        color = "gold" 
    elif label == 9:
        color = "crimson"

    return color


class BinaryDiffusionProcessDataset(torch.utils.data.Dataset):
    """
    Implementation of a synthetic dataset by hierarchical diffusion.

    Args:
        dim: dimension of the input sample
        depth: depth of the tree; the root corresponds to the depth 0
        number_of_children: Number of children of each node in the tree
        number_of_siblings: Number of noisy observations obtained from the nodes of the tree
        sigma_children: noise
        param: integer by which :math:`\\sigma_{\\text{children}}` is divided at each deeper level of the tree
    """

    def __init__(self,
                 dim: int,
                 depth: int,
                 number_of_children: int = 2,
                 sigma_children: float = 1,
                 param: int = 1,
                 number_of_siblings: int = 1,
                 factor_sibling: float = 10) -> None:
        self.dim = int(dim)
        self.root = np.zeros(self.dim)
        self.depth = int(depth)
        self.sigma_children = sigma_children
        self.factor_sibling = factor_sibling
        self.param = param
        self.number_of_children = int(number_of_children)
        self.number_of_siblings = int(number_of_siblings)

        self.origin_data, self.origin_labels, self.data, self.labels = self.bst()

        # Normalise data (0 mean, 1 std)
        self.data -= np.mean(self.data, axis=0, keepdims=True)
        self.data /= np.std(self.data, axis=0, keepdims=True)

    def __len__(self) -> int:
        """
        this method returns the total number of samples/nodes
        """
        return len(self.data)

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        Generates one sample
        """
        data, labels = self.data[idx], self.labels[idx]
        return torch.Tensor(data), torch.Tensor(labels)

    def get_children(self,
                     parent_value: np.ndarray,
                     parent_label: np.ndarray,
                     current_depth: int,
                     offspring: bool = True) -> List[Tuple[float, np.ndarray]]:
        """
        :param 1d-array parent_value
        :param 1d-array parent_label
        :param int current_depth
        :param  Boolean offspring: if True the parent node gives birth to number_of_children nodes
                                    if False the parent node gives birth to number_of_siblings noisy observations
        :return: list of 2-tuples containing the value and label of each child of a parent node
        :rtype: list of length number_of_children
        """
        if offspring:
            number_of_children = self.number_of_children
            sigma = self.sigma_children / (self.param**current_depth)
        else:
            number_of_children = self.number_of_siblings
            sigma = self.sigma_children / (self.factor_sibling * (self.param**current_depth))
        children = []
        for i in range(number_of_children):
            child_value = parent_value + np.random.randn(self.dim) * np.sqrt(sigma)
            child_label = np.copy(parent_label)
            if offspring:
                child_label[current_depth] = i + 1
            else:
                child_label[current_depth] = -i - 1
            children.append((child_value, child_label))
        return children

    def bst(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """
        This method generates all the nodes of a level before going to the next level
        """
        queue = [(self.root, np.zeros(self.depth + 1), 0)]
        visited = []
        labels_visited = []
        values_clones = []
        labels_clones = []
        while len(queue) > 0:
            current_node, current_label, current_depth = queue.pop(0)
            visited.append(current_node)
            labels_visited.append(current_label)
            if current_depth < self.depth:
                children = self.get_children(current_node, current_label, current_depth)
                for child in children:
                    queue.append((child[0], child[1], current_depth + 1))
            if current_depth <= self.depth:
                clones = self.get_children(current_node, current_label, current_depth, False)
                for clone in clones:
                    values_clones.append(clone[0])
                    labels_clones.append(clone[1])
        length = int((self.number_of_children**(self.depth + 1) - 1) / (self.number_of_children - 1))
        images = np.concatenate([i for i in visited]).reshape(length, self.dim)
        labels_visited = np.concatenate([i for i in labels_visited]).reshape(length, self.depth + 1)[:, :self.depth]
        values_clones = np.concatenate([i for i in values_clones]).reshape(self.number_of_siblings * length, self.dim)
        labels_clones = np.concatenate([i for i in labels_clones]).reshape(self.number_of_siblings * length,
                                                                           self.depth + 1)
        return images, labels_visited, values_clones, labels_clones
    

class BdpDataset:
    def __init__(self, batch_size: int, *args: Any, **kwargs: Any) -> None:
        self.batch_size = batch_size
        self.in_dim = 50
        self.img_dims = None

    def _load_synth(self, dataset: BinaryDiffusionProcessDataset, train: bool = True) -> DataLoader:
        return DataLoader(dataset=dataset, batch_size=self.batch_size, num_workers=8, pin_memory=True, shuffle=train)

    def get_binary_diffusion_subset(self, nb_data):
        dataset = BinaryDiffusionProcessDataset(self.in_dim,
                                                5,
                                                number_of_children=2,
                                                sigma_children=1,
                                                param=1,
                                                number_of_siblings=5,
                                                factor_sibling=10)
        
        return dataset

    def create_loaders(self) -> Tuple[DataLoader, DataLoader]:
        dataset = BinaryDiffusionProcessDataset(self.in_dim,
                                                5,
                                                number_of_children=2,
                                                sigma_children=1,
                                                param=1,
                                                number_of_siblings=5,
                                                factor_sibling=10)
        n_train = int(len(dataset) * 0.7)
        n_test = len(dataset) - n_train
        train_dataset, test_dataset = torch.utils.data.random_split(dataset, [n_train, n_test])
        train_loader = self._load_synth(train_dataset, train=True)
        test_loader = self._load_synth(test_dataset, train=False)
        return train_loader, test_loader

    def reconstruction_loss(self, x_mb_: torch.Tensor, x_mb: torch.Tensor) -> torch.Tensor:
        return -Normal(x_mb_, torch.ones_like(x_mb_)).log_prob(x_mb)
