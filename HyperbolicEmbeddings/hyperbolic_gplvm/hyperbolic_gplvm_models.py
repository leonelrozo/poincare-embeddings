import numpy as np
import torch
import warnings

from gpytorch.models.gplvm.latent_variable import LatentVariable, MAPLatentVariable, PointLatentVariable
from gpytorch.means import ZeroMean
from gpytorch.likelihoods import GaussianLikelihood
from gpytorch.priors.prior import Prior
from gpytorch.kernels import ScaleKernel, Kernel

from geoopt.manifolds import Lorentz
from geoopt import ManifoldParameter

import geometric_kernels.torch
from geometric_kernels.spaces import Hyperbolic
from geometric_kernels.kernels.feature_map import MaternFeatureMapKernel
from geometric_kernels.feature_maps.rejection_sampling import RejectionSamplingFeatureMapHyperbolic
from geometric_kernels.frontends.gpytorch import GPyTorchGeometricKernel

from HyperbolicEmbeddings.gplvm.gplvm_models import ExactGPLVM
from HyperbolicEmbeddings.kernels.kernels_hyperbolic import HyperbolicRiemannianGaussianKernel
from HyperbolicEmbeddings.hyperbolic_distributions.hyperbolic_wrapped_normal import LorentzWrappedNormal, \
    LorentzWrappedNormalPrior
from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_latent_variable import HyperbolicBackConstraintsLatentVariable, \
    HyperbolicTaxonomyBackConstraintsLatentVariable
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import log_map_mu0


class HyperbolicExactGPLVM(ExactGPLVM):
    """
    This class implements a Gaussian Process Hyperbolic Latent Variable Model (GPHLVM) class.
    The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
    The latent variables are inferred as point estimates for latent X.

    The Lorentz model of the hyperbolic manifold is used for all computations.

    Attributes
    ----------
    self.X: hyperbolic latent variables, an instance of gpytorch.models.gplvm.PointLatentVariable or
            gpytorch.models.gplvm.MAPLatentVariable
    self.train_targets: observations in the original space
    self.likelihood: likelihood of the GPs defining the generative mapping,
                     an instance of gpytorch.likelihoods.GaussianLikelihood
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
    self.batch_shape: dimension of the Euclidean observations space R^D or torch.Size(), used as batch dimension
    self.added_loss: additional loss added to the log posterior during MAP estimation

    """
    def __init__(self, X: LatentVariable, data: torch.Tensor, latent_dim: int,
                 kernel_lengthscale_prior: Prior = None, kernel_outputscale_prior: Prior = None, batch_params=True, 
                 rejection_sampling_kernel=False):
        """
        Initialization.

        Parameters
        ----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space

        Optional parameters
        -------------------
        :param kernel_lengthscale_prior: prior on the lengthscale parameter of the hyperbolic kernel
        :param kernel_outputscale_prior: prior on the scale parameter of the hyperbolic kernel
        :param batch_params: batch dimension, either dimension of the Euclidean observations space R^D or torch.Size()
        :rejection_sampling_kernel: if True and latent_dim=2, use the rejection sampling kernel from geometric_kernels
        """
        data_dim = data.shape[1]
        n = data.shape[0]
        if batch_params:
            self.batch_shape = torch.Size([data_dim])
        else:
            self.batch_shape = torch.Size()
        self.n = n

        # Gaussian likelihood
        likelihood = GaussianLikelihood(batch_shape=self.batch_shape)
        # likelihood = MultitaskGaussianLikelihood(data_dim, has_global_noise=False)

        # Model parameters
        # Kernel (acting on latent dimensions)
        mean_module = ZeroMean(batch_shape=self.batch_shape)
        covar_module = ScaleKernel(HyperbolicRiemannianGaussianKernel(latent_dim, nb_points_integral=3000,
                                                                      lengthscale_prior=kernel_lengthscale_prior,
                                                                      batch_shape=self.batch_shape),
                                   outputscale_prior=kernel_outputscale_prior)
        
        if rejection_sampling_kernel and latent_dim==2:
            print('Using rejection sampling hyperbolic kernel')
            hyperboloid = Hyperbolic(dim=latent_dim)
            feature_map_rs = RejectionSamplingFeatureMapHyperbolic(hyperboloid, num_random_phases=3000, shifted_laplacian=False)
            kernel_rs = MaternFeatureMapKernel(hyperboloid, feature_map_rs, torch.Generator())  # torch.Generator('cuda' if cuda else None)
            base_kernel = GPyTorchGeometricKernel(kernel_rs)
            base_kernel.nu = torch.inf
            covar_module = ScaleKernel(base_kernel=base_kernel, outputscale_prior=kernel_outputscale_prior)
            # base_kernel.raw_nu_constraint = Positive()

        # if latent_dim == 2:
        #     if k2_single_sum_computation:
        #         nb_points_int_phi_l = 1000
        #         nb_points_int_heat = 1000
        #     else:
        #         nb_points_int_phi_l = 50
        #         nb_points_int_heat = 50
        #     self.covar_module = ScaleKernel(HyperbolicD2RiemannianGaussianKernel(latent_dim,
        #                                                                          single_sum_computation=
        #                                                                          k2_single_sum_computation,
        #                                                                          nb_points_int_phi_l=
        #                                                                          nb_points_int_phi_l,
        #                                                                          nb_points_int_heat=nb_points_int_heat,
        #                                                                          lengthscale_prior=
        #                                                                          kernel_lengthscale_prior,
        #                                                                          batch_shape=self.batch_shape),
        #                                     outputscale_prior=kernel_outputscale_prior)
        # elif latent_dim == 3:
        #     self.covar_module = ScaleKernel(HyperbolicRiemannianMillsonGaussianKernel(latent_dim,
        #                                                                               lengthscale_prior=
        #                                                                               kernel_lengthscale_prior,
        #                                                                               batch_shape=self.batch_shape
        #                                                                               ),
        #                                     outputscale_prior=kernel_outputscale_prior)
        # else:
        #     raise NotImplementedError

        super().__init__(X, data.T, likelihood, mean_module, covar_module)

        # Initialize lengthscale and outputscale to mean of priors
        if kernel_lengthscale_prior:
            self.covar_module.base_kernel.lengthscale = kernel_lengthscale_prior.mean
        if kernel_outputscale_prior:
            self.covar_module.outputscale = kernel_outputscale_prior.mean


class PointExactHyperbolicGPLVM(HyperbolicExactGPLVM):
    """
    This class implements a Gaussian Process Hyperbolic Latent Variable Model (GPHLVM) class.
    The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
    The latent variables are inferred as point estimates for latent X.

    Attributes
    ----------
    self.X: hyperbolic latent variables, an instance of gpytorch.models.gplvm.PointLatentVariable
    self.train_targets: observations in the original space
    self.likelihood: likelihood of the GPs defining the generative mapping,
                     an instance of gpytorch.likelihoods.GaussianLikelihood
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
    self.batch_shape: dimension of the Euclidean observations space R^D or torch.Size(), used as batch dimension
    self.added_loss: additional loss added to the log posterior during MAP estimation


    """
    def __init__(self, data: torch.Tensor, latent_dim: int, X_init: torch.Tensor = None, **kwargs):
        """
        Initialization.

        Parameters
        ----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space

        Optional parameters
        -------------------
        :param X_init: initial latent variables, if None, initialized randomly
        """
        n = data.shape[0]

        # Initialise X with PCA or randn
        # Define a distribution to sample initial X and initial inducing inputs
        sample_mean = torch.zeros(latent_dim + 1)  # Mean on the Lorentz model -> Q+1
        sample_mean[0] = 1.
        sample_scale = 0.1 * torch.ones(latent_dim)  # On the tangent space of the mean on the Lorentz model -> Q
        sample_distribution = LorentzWrappedNormal(sample_mean, sample_scale)

        # Initialise X
        if X_init is None:
            print('No initialization given, the latent variables are initialized randomly.')
            X_init = torch.nn.Parameter(sample_distribution.sample((1, n)).squeeze())

        # Point estimate latent variable
        X = PointLatentVariable(n, latent_dim, X_init)
        # Replace torch Parameters which are on the hyperbolic space by ManifoldParameters
        if not isinstance(X.X, ManifoldParameter):
            X.X = ManifoldParameter(X.X, manifold=Lorentz())  # Latent points

        super().__init__(X, data, latent_dim, **kwargs)


class MapExactHyperbolicGPLVM(HyperbolicExactGPLVM):
    """
    This class implements a Hyperbolic Gaussian Process Latent Variable Model (GPHLVM) class.
    The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
    The latent variables are inferred using MAP Inference.

    Attributes
    ----------
    self.X: hyperbolic latent variables, an instance of gpytorch.models.gplvm.MAPLatentVariable
    self.train_targets: observations in the original space
    self.likelihood: likelihood of the GPs defining the generative mapping,
                     an instance of gpytorch.likelihoods.GaussianLikelihood
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
    :param batch_params: batch dimension, if True dimension of the Euclidean observations space R^D otherwise torch.Size()
    self.added_loss: additional loss added to the log posterior during MAP estimation


    """
    def __init__(self,  data: torch.Tensor, latent_dim: int, X_init: torch.Tensor = None, latent_prior: Prior = None,
                 **kwargs):
        """
        Initialization.

        Parameters
        ----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space

        Optional parameters
        -------------------
        :param X_init: initial latent variables, if None, initialized randomly
        :param map_latent_variable: if True, use a MAP latent variable, otherwise use a point latent variable
        :param latent_prior: prior on the latent variable
        """
        data_dim = data.shape[1]
        n = data.shape[0]

        # Define prior for X
        if latent_prior is None:
            X_prior_mean = torch.zeros(n, latent_dim + 1)  # Mean on the Lorentz model -> N x Q+1
            X_prior_mean[:, 0] = 1.
            X_prior_scale = 2.0 * torch.ones(n, latent_dim)  # On the tangent space of the mean on the Lorentz model -> N x Q
            prior_x = LorentzWrappedNormalPrior(X_prior_mean, X_prior_scale)
        else:
            prior_x = latent_prior

        # Define a distribution to sample initial X and initial inducing inputs
        sample_mean = torch.zeros(latent_dim + 1)  # Mean on the Lorentz model -> Q+1
        sample_mean[0] = 1.
        sample_scale = 0.1 * torch.ones(latent_dim)  # On the tangent space of the mean on the Lorentz model -> Q
        sample_distribution = LorentzWrappedNormal(sample_mean, sample_scale)

        # Initialise X
        if X_init is None:
            print('No initialization given, the latent variables are initialized randomly.')
            X_init = torch.nn.Parameter(sample_distribution.sample((1, n)).squeeze())

        # MAP latent variable
        X = MAPLatentVariable(n, latent_dim, X_init, prior_x)

        # Replace torch Parameters which are on the hyperbolic space by ManifoldParameters
        if not isinstance(X.X, ManifoldParameter):
            X.X = ManifoldParameter(X.X, manifold=Lorentz())  # Latent points

        super().__init__(X, data, latent_dim, **kwargs)


class BackConstrainedHyperbolicExactGPLVM(HyperbolicExactGPLVM):
    """
    This class implements a back-constrained Bayesian Gaussian Process Hyperbolic Latent Variable Model (GPHLVM) class.
    The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
    The latent variables belong to the hyperbolic manifold and is inferred as a function of the data and of their given
    classes (relevant when hyperbolic data are instances of a taxonomy).

    The Lorentz model of the hyperbolic manifold is used for all computations.

    Attributes
    ----------
    self.X: hyperbolic latent variables, instance of HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_latent_variable.
            HyperbolicBackConstraintsLatentVariable or HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_latent_variable.
            HyperbolicTaxonomyBackConstraintsLatentVariable
    self.train_targets: observations in the original space
    self.likelihood: likelihood of the GPs defining the generative mapping,
                     an instance of gpytorch.likelihoods.GaussianLikelihood
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
    self.batch_shape: dimension of the Euclidean observations space R^D, used as batch dimension
    self.added_loss: additional loss added to the log posterior during MAP estimation

    Methods
    -------
    self.forward(self, x)
    self.train_inputs(self)
    self._get_batch_idx(self)
    self.add_loss_term(added_loss)

    """
    def __init__(self, data: torch.Tensor, latent_dim: int, hyperbolic_class_idx: torch.Tensor,
                 X_init: torch.Tensor = None, taxonomy_based_back_constraints: bool = True,
                 data_kernel: Kernel = None, classes_kernel: Kernel = None, weight_prior: Prior = None, **kwargs):
        """
        Initialization.

        Parameters
        ----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space
        :param class_idx: indices of data classes

        Optional parameters
        -------------------
        :param X_init: initial latent variables, if None, initialized randomly
        :param data_kernel: kernel defining the relationship between the observations for the back constraints
        :param classes_kernel: kernel defining the relationship between the data classes for the back constraints
        :param weight_prior: prior for the weight parameter of the back constraints
        """
        data_dim = data.shape[1]
        n = data.shape[0]

        # Initialise X
        if X_init is None:
            print('No initialization given, the latent variables are initialized randomly.')
            weight_init = torch.nn.Parameter(0.01 * torch.randn(n, latent_dim))
        else:
            X_init_tangent = log_map_mu0(X_init)[:, 1:]
            # Compute least squares weights
            if taxonomy_based_back_constraints:
                kernel_backconstraints = (data_kernel(data) * classes_kernel(hyperbolic_class_idx)).evaluate()
            else:
                kernel_backconstraints = data_kernel(data).evaluate()
            regularization = 1e-6 * torch.eye(n)
            weight_init = torch.matmul(torch.inverse(kernel_backconstraints + regularization), X_init_tangent)

        # Back constrained latent variables
        if taxonomy_based_back_constraints:
            X = HyperbolicTaxonomyBackConstraintsLatentVariable(n, latent_dim, data,
                                                                hyperbolic_class_idx, classes_kernel, data_kernel,
                                                                weight_prior, weight_init)
        else:
            X = HyperbolicBackConstraintsLatentVariable(n, latent_dim, data, data_kernel, weight_prior,
                                                        weight_init)

        super().__init__(X, data, latent_dim, **kwargs)



# class OldHyperbolicExactGPLVM(ExactGP):
#     """
#     This class implements a Bayesian Gaussian Process Latent Variable Model (GPLVM) class.
#     The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
#     The latent variables belong to the hyperbolic manifold and are inferred as:
#     1. Point estimates for latent X when prior_x = None
#     2. MAP Inference for X when prior_x is not None and inference == 'map'
#
#     The Lorentz model of the hyperbolic manifold is used for all computations.
#
#     Attributes
#     ----------
#     self.X: hyperbolic latent variables, an instance of gpytorch.models.gplvm.PointLatentVariable or
#             gpytorch.models.gplvm.MAPLatentVariable
#     self.train_targets: observations in the original space
#     self.likelihood: likelihood of the GPs defining the generative mapping,
#                      an instance of gpytorch.likelihoods.GaussianLikelihood
#     self.mean_module: mean of the GPs defining the generative mapping
#     self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
#     self.batch_shape: dimension of the Euclidean observations space R^D, used as batch dimension
#     self.added_loss: additional loss added to the log posterior during MAP estimation
#
#     Methods
#     -------
#     self.forward(self, x)
#     self.sample_latent_variable(self)
#     self._get_batch_idx(self)
#     self.add_loss_term(added_loss)
#     """
#     def __init__(self, data: torch.Tensor, latent_dim: int, pca: bool = False, map_latent_variable: bool = True,
#                  latent_prior: Prior = None, kernel_lengthscale_prior: Prior = None,
#                  kernel_outputscale_prior: Prior = None, k2_single_sum_computation=True):
#         """
#         Parameters
#         ----------
#         :param data: observations  [n x dimension]
#         :param latent_dim: dimension of the latent space
#
#         Optional parameters
#         -------------------
#         :param pca: if True, initialize the latent variables using PCA, otherwise random
#         :param map_latent_variable: if True, use a MAP latent variable, otherwise use a point latent variable
#         :param latent_prior: prior on the latent variable
#         :param kernel_lengthscale_prior: prior on the lengthscale parameter of the hyperbolic kernel
#         :param kernel_outputscale_prior: prior on the scale parameter of the hyperbolic kernel
#         :param k2_single_sum_computation: if True, uses single sum approximation instead of double sum when latent_dim=2
#         """
#         data_dim = data.shape[1]
#         n = data.shape[0]
#         self.batch_shape = torch.Size([data_dim])
#
#         # Define prior for X
#         if latent_prior is None:
#             X_prior_mean = torch.zeros(n, latent_dim + 1)  # Mean on the Lorentz model -> N x Q+1
#             X_prior_mean[:, 0] = 1.
#             X_prior_scale = 2.0 * torch.ones(n, latent_dim)  # On the tangent space of the mean on the Lorentz model -> N x Q
#             prior_x = LorentzWrappedNormalPrior(X_prior_mean, X_prior_scale)
#         else:
#             prior_x = latent_prior
#
#         # Define a distribution to sample initial X and initial inducing inputs
#         sample_mean = torch.zeros(latent_dim + 1)  # Mean on the Lorentz model -> Q+1
#         sample_mean[0] = 1.
#         sample_scale = 0.1 * torch.ones(latent_dim)  # On the tangent space of the mean on the Lorentz model -> Q
#         sample_distribution = LorentzWrappedNormal(sample_mean, sample_scale)
#
#         # Initialise X with PCA or randn
#         if pca == True:
#             X_init = tangent_pca_initialization(data, latent_dim)  # Initialise X via PCA
#         else:
#             X_init = torch.nn.Parameter(sample_distribution.sample((1, n)).squeeze())
#             # X_init = torch.nn.Parameter(prior_x.sample().squeeze())
#
#         if map_latent_variable:
#             # MAP latent variable
#             X = MAPLatentVariable(n, latent_dim, X_init, prior_x)
#         else:
#             # Point estimate latent variable
#             X = PointLatentVariable(n, latent_dim, X_init)
#
#         # Replace torch Parameters which are on the hyperbolic space by ManifoldParameters
#         X.X = ManifoldParameter(X.X, manifold=Lorentz())  # Latent points  TODO directly in file (?)
#
#         # Gaussian likelihood
#         likelihood = GaussianLikelihood(batch_shape=self.batch_shape)
#
#         super().__init__(X.X, data.T, likelihood)
#
#         # Latent variable and dimension
#         self.X = X
#         self.n = n
#
#         # Model parameters
#         # Kernel (acting on latent dimensions)
#         self.mean_module = ZeroMean(batch_shape=self.batch_shape)
#         self.covar_module = ScaleKernel(HyperbolicRiemannianGaussianKernel(latent_dim, nb_points_integral=1000,
#                                                                            lengthscale_prior=
#                                                                            kernel_lengthscale_prior,
#                                                                            batch_shape=self.batch_shape
#                                                                            ),
#                                         outputscale_prior=kernel_outputscale_prior)
#
#         # if latent_dim == 2:
#         #     if k2_single_sum_computation:
#         #         nb_points_int_phi_l = 1000
#         #         nb_points_int_heat = 1000
#         #     else:
#         #         nb_points_int_phi_l = 50
#         #         nb_points_int_heat = 50
#         #     self.covar_module = ScaleKernel(HyperbolicD2RiemannianGaussianKernel(latent_dim,
#         #                                                                          single_sum_computation=
#         #                                                                          k2_single_sum_computation,
#         #                                                                          nb_points_int_phi_l=
#         #                                                                          nb_points_int_phi_l,
#         #                                                                          nb_points_int_heat=nb_points_int_heat,
#         #                                                                          lengthscale_prior=
#         #                                                                          kernel_lengthscale_prior,
#         #                                                                          batch_shape=self.batch_shape),
#         #                                     outputscale_prior=kernel_outputscale_prior)
#         # elif latent_dim == 3:
#         #     self.covar_module = ScaleKernel(HyperbolicRiemannianMillsonGaussianKernel(latent_dim,
#         #                                                                               lengthscale_prior=
#         #                                                                               kernel_lengthscale_prior,
#         #                                                                               batch_shape=self.batch_shape
#         #                                                                               ),
#         #                                     outputscale_prior=kernel_outputscale_prior)
#         # else:
#         #     raise NotImplementedError
#
#         # Initialize lengthscale and outputscale to mean of priors
#         if kernel_lengthscale_prior:
#             self.covar_module.base_kernel.lengthscale = kernel_lengthscale_prior.mean
#         if kernel_outputscale_prior:
#             self.covar_module.outputscale = kernel_outputscale_prior.mean
#
#         # Initialize an added loss term
#         self.added_loss = None
#
#     def forward(self, x):
#         """
#         In prior mode (self.train()): returns the prior of the GPLVM on the observation corresponding to the
#         latent variable(s) x.
#         In posterior mode (self.eval()): returns the posterior of the GPLVM on the observation corresponding to the
#         latent variable(s) x.
#
#         Parameters
#         ----------
#         :param x: point(s) in the latent space [n x latent dimension]
#
#         Return
#         ------
#         :return prior or posterior of the GPLVM at x.
#         """
#         mean_x = self.mean_module(x)
#         covar_x = self.covar_module(x)
#         dist = MultivariateNormal(mean_x, covar_x)
#
#         if self.added_loss:
#             self.update_added_loss_term("added_loss", self.added_loss)
#
#         return dist
#
#     def _get_batch_idx(self, batch_size):
#         valid_indices = np.arange(self.n)
#         batch_indices = np.random.choice(valid_indices, size=batch_size, replace=False)
#         return np.sort(batch_indices)
#
#     def sample_latent_variable(self):
#         """
#         Samples the latent variable.
#         """
#         sample = self.X()
#         return sample
#
#     def add_loss_term(self, added_loss: AddedLossTerm):
#         """
#         Register an added loss term.
#
#         Parameters
#         ----------
#         :param added_loss: additional loss added to the log posterior during MAP estimation
#         """
#         self.added_loss = added_loss
#         self.register_added_loss_term("added_loss")
#
#
# class OldHyperbolicExactBackConstrainedGPLVM(GP):
#     """
#     This class implements a Back-constrained Bayesian Gaussian Process Latent Variable Model (GPLVM) class.
#     The model is estimated exactly as opposed to variational models, which use inducing points for their estimation.
#     The latent variables belong to the hyperbolic manifold and is inferred as a function of the data and of their given
#     classes (relevant when hyperbolic data are instances of a taxonomy).
#
#     The Lorentz model of the hyperbolic manifold is used for all computations.
#
#     Attributes
#     ----------
#     self.X: hyperbolic latent variables, instance of HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_latent_variable.
#             HyperbolicBackConstraintsLatentVariable or HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_latent_variable.
#             HyperbolicTaxonomyBackConstraintsLatentVariable
#     self.train_targets: observations in the original space
#     self.likelihood: likelihood of the GPs defining the generative mapping,
#                      an instance of gpytorch.likelihoods.GaussianLikelihood
#     self.mean_module: mean of the GPs defining the generative mapping
#     self.kernel_module: hyperbolic kernel of the GPs defining the generative mapping
#     self.batch_shape: dimension of the Euclidean observations space R^D, used as batch dimension
#     self.added_loss: additional loss added to the log posterior during MAP estimation
#
#     Methods
#     -------
#     self.forward(self, x)
#     self.train_inputs(self)
#     self._get_batch_idx(self)
#     self.add_loss_term(added_loss)
#
#     """
#     def __init__(self, data: torch.Tensor, latent_dim: int, hyperbolic_class_idx: torch.Tensor, pca: bool = False,
#                  kernel_lengthscale_prior: Prior = None, kernel_outputscale_prior: Prior = None,
#                  k2_single_sum_computation: bool = True, taxonomy_based_back_constraints: bool = True,
#                  data_kernel: Kernel = None, classes_kernel: Kernel = None, weight_prior: Prior = None):
#         """
#         Initialization.
#
#         Parameters
#         ----------
#         :param data: observations  [n x dimension]
#         :param latent_dim: dimension of the latent space
#         :param hyperbolic_class_idx: indices of data classes
#
#         Optional parameters
#         -------------------
#         :param kernel_lengthscale_prior: prior on the lengthscale parameter of the hyperbolic kernel
#         :param kernel_outputscale_prior: prior on the outputscale parameter of the hyperbolic kernel
#         :param k2_single_sum_computation: if True, uses single sum approximation instead of double sum when latent_dim=2
#         :param data_kernel: kernel defining the relationship between the observations for the back constraints
#         :param classes_kernel: kernel defining the relationship between the data classes for the back constraints
#         :param weight_prior: prior for the weight parameter of the back constraints
#         """
#         data_dim = data.shape[1]
#         self.n = data.shape[0]
#         self.batch_shape = torch.Size([data_dim])
#         self.train_targets = data.T
#
#         super().__init__()
#
#         # Gaussian likelihood
#         self.likelihood = GaussianLikelihood(batch_shape=self.batch_shape)
#
#         # Initialise X with PCA or randn
#         if pca == True:
#             # Initialise X via PCA
#             X_init = tangent_pca_initialization(data, latent_dim)
#             X_init_tangent = log_map_mu0(X_init)[:, 1:]
#             # Compute least squares weights
#             if taxonomy_based_back_constraints:
#                 kernel_backconstraints = (data_kernel(data) * classes_kernel(hyperbolic_class_idx)).evaluate()
#             else:
#                 kernel_backconstraints = data_kernel(data).evaluate()
#             regularization = 1e-6 * torch.eye(self.n)
#             weight_init = torch.matmul(torch.inverse(kernel_backconstraints + regularization), X_init_tangent)
#         else:
#             weight_init = torch.nn.Parameter(0.01 * torch.randn(self.n, latent_dim))
#
#         # Back constrained latent variables
#         if taxonomy_based_back_constraints:
#             self.X = HyperbolicTaxonomyBackConstraintsLatentVariable(self.n, latent_dim, data,
#                                                                      hyperbolic_class_idx, classes_kernel, data_kernel,
#                                                                      weight_prior, weight_init)
#         else:
#             self.X = HyperbolicBackConstraintsLatentVariable(self.n, latent_dim, data, data_kernel, weight_prior,
#                                                              weight_init)
#
#         # Model parameters
#         # Kernel (acting on latent dimensions)
#         self.mean_module = ZeroMean(batch_shape=self.batch_shape)
#         self.covar_module = ScaleKernel(HyperbolicRiemannianGaussianKernel(latent_dim, nb_points_integral=1000,
#                                                                            lengthscale_prior=
#                                                                            kernel_lengthscale_prior,
#                                                                            batch_shape=self.batch_shape
#                                                                            ),
#                                         outputscale_prior=kernel_outputscale_prior)
#         # if latent_dim == 2:
#         #     if k2_single_sum_computation:
#         #         nb_points_int_phi_l = 1000
#         #         nb_points_int_heat = 1000
#         #     else:
#         #         nb_points_int_phi_l = 50
#         #         nb_points_int_heat = 50
#         #     self.covar_module = ScaleKernel(HyperbolicD2RiemannianGaussianKernel(latent_dim,
#         #                                                                          single_sum_computation=
#         #                                                                          k2_single_sum_computation,
#         #                                                                          nb_points_int_phi_l=
#         #                                                                          nb_points_int_phi_l,
#         #                                                                          nb_points_int_heat=nb_points_int_heat,
#         #                                                                          lengthscale_prior=
#         #                                                                          kernel_lengthscale_prior,
#         #                                                                          batch_shape=self.batch_shape),
#         #                                     outputscale_prior=kernel_outputscale_prior)
#         # elif latent_dim == 3:
#         #     self.covar_module = ScaleKernel(HyperbolicRiemannianMillsonGaussianKernel(latent_dim,
#         #                                                                               lengthscale_prior=
#         #                                                                               kernel_lengthscale_prior,
#         #                                                                               batch_shape=self.batch_shape
#         #                                                                               ),
#         #                                     outputscale_prior=kernel_outputscale_prior)
#         # else:
#         #     raise NotImplementedError
#
#         # Initialize lengthscale and outputscale to mean of priors
#         if kernel_lengthscale_prior:
#             self.covar_module.base_kernel.lengthscale = kernel_lengthscale_prior.mean
#         if kernel_outputscale_prior:
#             self.covar_module.outputscale = kernel_outputscale_prior.mean
#
#         # Initialize an added loss term
#         self.added_loss = None
#
#         # Initialize the prediction strategy
#         self.prediction_strategy = None
#
#     def forward(self, x):
#         """
#         In prior mode (self.train()): returns the prior of the GPLVM on the observation corresponding to the
#         latent variable(s) x.
#         In posterior mode (self.eval()): returns the posterior of the GPLVM on the observation corresponding to the
#         latent variable(s) x.
#
#         Parameters
#         ----------
#         :param x: point(s) in the latent space [n x latent dimension]
#
#         Return
#         ------
#         :return prior or posterior of the GPLVM at x.
#         """
#         mean_x = self.mean_module(x)
#         covar_x = self.covar_module(x)
#         dist = MultivariateNormal(mean_x, covar_x)
#
#         if self.added_loss:
#             self.update_added_loss_term("added_loss", self.added_loss)
#
#         return dist
#
#     def train_inputs(self):
#         """
#         Get the latent variables used the back-constraints mapping.
#         """
#         return self.X()
#
#     def _get_batch_idx(self, batch_size):
#         valid_indices = np.arange(self.n)
#         batch_indices = np.random.choice(valid_indices, size=batch_size, replace=False)
#         return np.sort(batch_indices)
#
#     def add_loss_term(self, added_loss: AddedLossTerm):
#         """
#         Register an added loss term.
#
#         Parameters
#         ----------
#         :param added_loss: additional loss added to the log posterior during MAP estimation
#         """
#         self.added_loss = added_loss
#         self.register_added_loss_term("added_loss")
#
#     def __call__(self, *args, **kwargs):
#         """
#         This function has been adapted from the class gpytorch.models.exact_gp.ExactGP to handle non-constant inputs
#         computed as functions.
#         """
#         train_inputs = [self.train_inputs()] if self.train_inputs() is not None else []
#         inputs = [i.unsqueeze(-1) if i.ndimension() == 1 else i for i in args]
#
#         # Training mode: optimizing
#         if self.training:
#             if self.train_inputs() is None:
#                 raise RuntimeError(
#                     "train_inputs, train_targets cannot be None in training mode. "
#                     "Call .eval() for prior predictions, or call .set_train_data() to add training data."
#                 )
#             if settings.debug.on():
#                 if not all(torch.equal(train_input, input) for train_input, input in zip(train_inputs, inputs)):
#                     raise RuntimeError("You must train on the training inputs!")
#             res = super().__call__(*inputs, **kwargs)
#             return res
#
#         # Prior mode
#         elif settings.prior_mode.on() or self.train_inputs() is None or self.train_targets is None:
#             full_inputs = args
#             full_output = super(OldHyperbolicExactBackConstrainedGPLVM, self).__call__(*full_inputs, **kwargs)
#             if settings.debug().on():
#                 if not isinstance(full_output, MultivariateNormal):
#                     raise RuntimeError("HyperbolicExactBackConstrainedGPLVM.forward must return a MultivariateNormal")
#             return full_output
#
#         # Posterior mode
#         else:
#             if settings.debug.on():
#                 if all(torch.equal(train_input, input) for train_input, input in zip(train_inputs, inputs)):
#                     warnings.warn(
#                         "The input matches the stored training data. Did you forget to call model.train()?",
#                         GPInputWarning,
#                     )
#
#             # Get the terms that only depend on training data
#             if self.prediction_strategy is None:
#                 train_output = super().__call__(*train_inputs, **kwargs)
#
#                 # Create the prediction strategy for
#                 self.prediction_strategy = prediction_strategy(
#                     train_inputs=train_inputs,
#                     train_prior_dist=train_output,
#                     train_labels=self.train_targets,
#                     likelihood=self.likelihood,
#                 )
#
#             # Concatenate the input to the training input
#             full_inputs = []
#             batch_shape = train_inputs[0].shape[:-2]
#             for train_input, input in zip(train_inputs, inputs):
#                 # Make sure the batch shapes agree for training/test data
#                 if batch_shape != train_input.shape[:-2]:
#                     batch_shape = _mul_broadcast_shape(batch_shape, train_input.shape[:-2])
#                     train_input = train_input.expand(*batch_shape, *train_input.shape[-2:])
#                 if batch_shape != input.shape[:-2]:
#                     batch_shape = _mul_broadcast_shape(batch_shape, input.shape[:-2])
#                     train_input = train_input.expand(*batch_shape, *train_input.shape[-2:])
#                     input = input.expand(*batch_shape, *input.shape[-2:])
#                 full_inputs.append(torch.cat([train_input, input], dim=-2))
#
#             # Get the joint distribution for training/test data
#             full_output = super(OldHyperbolicExactBackConstrainedGPLVM, self).__call__(*full_inputs, **kwargs)
#             if settings.debug().on():
#                 if not isinstance(full_output, MultivariateNormal):
#                     raise RuntimeError("HyperbolicExactBackConstrainedGPLVM.forward must return a MultivariateNormal")
#             full_mean, full_covar = full_output.loc, full_output.lazy_covariance_matrix
#
#             # Determine the shape of the joint distribution
#             batch_shape = full_output.batch_shape
#             joint_shape = full_output.event_shape
#             tasks_shape = joint_shape[1:]  # For multitask learning
#             test_shape = torch.Size([joint_shape[0] - self.prediction_strategy.train_shape[0], *tasks_shape])
#
#             # Make the prediction
#             with settings._use_eval_tolerance():
#                 predictive_mean, predictive_covar = self.prediction_strategy.exact_prediction(full_mean, full_covar)
#
#             # Reshape predictive mean to match the appropriate event shape
#             predictive_mean = predictive_mean.view(*batch_shape, *test_shape).contiguous()
#             return full_output.__class__(predictive_mean, predictive_covar)

