
import torch
from stochman.discretized_manifold import DiscretizedManifold
from stochman.manifold import Manifold


class DiscreteGPLVMManifoldWrapper(DiscretizedManifold):
    def __init__(self, grid, metric_function):
        super().__init__()
        self.grid = grid
        self.metric_function = metric_function

    def fit(self, batch_size: int = 4):
        return super().fit(self, self.grid, batch_size=batch_size)

    def metric(self, c: torch.Tensor, return_deriv=False):
        return self.metric_function(c)


class GPLVMManifoldWrapper(Manifold):
    def __init__(self, metric_function):
        super().__init__()
        self.metric_function = metric_function

    def metric(self, c: torch.Tensor, return_deriv=False):
        return self.metric_function(c)
