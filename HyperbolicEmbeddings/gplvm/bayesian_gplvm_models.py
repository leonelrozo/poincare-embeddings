import numpy as np
import torch

from gpytorch.models.gplvm.latent_variable import VariationalLatentVariable, MAPLatentVariable
from gpytorch.models.gplvm.bayesian_gplvm import BayesianGPLVM
from gpytorch.means import ZeroMean
from gpytorch.priors import NormalPrior
from gpytorch.variational import VariationalStrategy
from gpytorch.variational import CholeskyVariationalDistribution
from gpytorch.kernels import ScaleKernel, RBFKernel
from gpytorch.distributions import MultivariateNormal
from gpytorch.mlls.added_loss_term import AddedLossTerm

from HyperbolicEmbeddings.gplvm.gplvm_initializations import pca_initialization


class MapBayesianGPLVM(BayesianGPLVM):
    """
    This class implements a variational Bayesian Gaussian Process Latent Variable Model (GPLVM) class.
    The latent variables is obtained via MAP inference and endowed with a Gaussian prior.
    This class was adapted from GpyTorch: https://github.com/cornellius-gp/gpytorch/tree/master/examples/045_GPLVM.

    Attributes
    ----------
    self.X: latent variables, an instance of gpytorch.models.gplvm.MAPLatentVariable
    self.variational_strategy: The strategy that determines how the model marginalizes over the variational distribution
                          (over inducing points) to produce the approximate posterior distribution (over data),
                          an instance of gpytorch.variational._VariationalStrategy
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: kernel of the GPs defining the generative mapping
    self.batch_shape: dimension of the Euclidean observations space R^D, used as batch dimension
    self.inducing_input: inducing inputs in the latent space R^Q
    self.added_loss: additional loss added to the log posterior during MAP estimation

    Methods
    -------
    self.forward(self, x)
    self.train_inputs(self)
    self._get_batch_idx(self)

    """
    def __init__(self, data, latent_dim, n_inducing, X_init: torch.Tensor = None,):
        """
        Initialization.

        Parameters
        -----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space
        :param n_inducing: number of inducing variables

        Optional parameters
        -------------------
        :param X_init: initial latent variables, if None, initialized randomly
        """
        data_dim = data.shape[1]
        n = data.shape[0]
        self.batch_shape = torch.Size([data_dim])

        # Define prior for X
        X_prior_mean = torch.zeros(n, latent_dim)  # shape: N x Q
        prior_x = NormalPrior(X_prior_mean, torch.ones_like(X_prior_mean))

        # Initialise X
        if X_init is None:
            print('No initialization given, the latent variables are initialized randomly.')
            X_init = torch.nn.Parameter(torch.randn(n, latent_dim))
        else:
            X_init = torch.nn.Parameter(X_init)

        # MAP latent variable (b)
        X = MAPLatentVariable(n, latent_dim, X_init, prior_x)

        # Locations Z_{d} corresponding to u_{d}, they can be randomly initialized or
        # regularly placed with shape (D x n_inducing x latent_dim).
        self.inducing_inputs = torch.randn(data_dim, n_inducing, latent_dim)

        # Sparse Variational Formulation (inducing variables initialised as randn)
        q_u = CholeskyVariationalDistribution(n_inducing, batch_shape=self.batch_shape)
        q_f = VariationalStrategy(self, self.inducing_inputs, q_u, learn_inducing_locations=True)

        super().__init__(X, q_f)

        # Kernel (acting on latent dimensions)
        # TODO add batch shape if we want one kernel per dimension. Hyperbolic does not consider ard_num_dims.
        self.mean_module = ZeroMean(ard_num_dims=latent_dim)
        self.covar_module = ScaleKernel(RBFKernel(ard_num_dims=latent_dim))

        self.train_targets = data.T

        # Initialize an added loss term
        self.added_loss = None

    def forward(self, x):
        """
        In prior mode (self.train()): returns the prior of the GPLVM on the observation corresponding to the
        latent variable(s) x.
        In posterior mode (self.eval()): returns the posterior of the GPLVM on the observation corresponding to the
        latent variable(s) x.

        Parameters
        ----------
        :param x: point(s) in the latent space [n x latent dimension]

        Return
        ------
        :return prior or posterior of the GPLVM at x.
        """
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        dist = MultivariateNormal(mean_x, covar_x)
        return dist

    def _get_batch_idx(self, batch_size):
        valid_indices = np.arange(self.n)
        batch_indices = np.random.choice(valid_indices, size=batch_size, replace=False)
        return np.sort(batch_indices)

    def train_inputs(self):
        """
        Returns a sample from the latent variable distribution.
        """
        return self.sample_latent_variable()

    def add_loss_term(self, added_loss: AddedLossTerm):
        """
        Register an added loss term.

        Parameters
        ----------
        :param added_loss: additional loss added to the log posterior during MAP estimation
        """
        self.added_loss = added_loss
        self.register_added_loss_term("added_loss")


class VariationalBayesianGPLVM(BayesianGPLVM):
    """
    This class implements a variational Bayesian Gaussian Process Latent Variable Model (GPLVM) class.
    The latent variables follow a Gaussian variational distribution q(X).
    This class was adapted from GpyTorch: https://github.com/cornellius-gp/gpytorch/tree/master/examples/045_GPLVM.

    Attributes
    ----------
    self.X: latent variables, an instance of gpytorch.models.gplvm.VariationalLatentVariable
    self.variational_strategy: The strategy that determines how the model marginalizes over the variational distribution
                          (over inducing points) to produce the approximate posterior distribution (over data),
                          an instance of gpytorch.variational._VariationalStrategy
    self.train_targets: observations [dim x nb_data]
    self.mean_module: mean of the GPs defining the generative mapping
    self.kernel_module: kernel of the GPs defining the generative mapping
    self.batch_shape: dimension of the Euclidean observations space R^D, used as batch dimension
    self.inducing_input: inducing inputs in the latent space R^Q
    self.added_loss: additional loss added to the log posterior during MAP estimation

    Methods
    -------
    self.forward(self, x)
    self.train_inputs(self)
    self._get_batch_idx(self)

    """
    def __init__(self, data, latent_dim, n_inducing, X_init: torch.Tensor = None,):
        """
        Initialization.

        Parameters
        ----------
        :param data: observations  [n x dimension]
        :param latent_dim: dimension of the latent space
        :param n_inducing: number of inducing variables
        :param X_init: initial latent variables, if None, initialized randomly
        """
        data_dim = data.shape[1]
        n = data.shape[0]
        self.batch_shape = torch.Size([data_dim])
        self.train_targets = data.T

        # Locations Z_{d} corresponding to u_{d}, they can be randomly initialized or
        # regularly placed with shape (D x n_inducing x latent_dim).
        self.inducing_inputs = torch.randn(data_dim, n_inducing, latent_dim)

        # Sparse Variational Formulation (inducing variables initialised as randn)
        q_u = CholeskyVariationalDistribution(n_inducing, batch_shape=self.batch_shape)
        q_f = VariationalStrategy(self, self.inducing_inputs, q_u, learn_inducing_locations=True)

        # Define prior for X
        X_prior_mean = torch.zeros(n, latent_dim)  # shape: N x Q
        prior_x = NormalPrior(X_prior_mean, torch.ones_like(X_prior_mean))

        # Initialise X
        if X_init is None:
            print('No initialization given, the latent variables are initialized randomly.')
            X_init = torch.nn.Parameter(torch.randn(n, latent_dim))
        else:
            X_init = torch.nn.Parameter(X_init)

        # LatentVariable
        X = VariationalLatentVariable(n, data_dim, latent_dim, X_init, prior_x)

        super().__init__(X, q_f)

        # Kernel (acting on latent dimensions)
        # TODO add batch shape if we want one kernel per dimension. Hyperbolic does not consider ard_num_dims.
        self.mean_module = ZeroMean(ard_num_dims=latent_dim)
        self.covar_module = ScaleKernel(RBFKernel(ard_num_dims=latent_dim))

        # Initialize an added loss term
        self.added_loss = None

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        dist = MultivariateNormal(mean_x, covar_x)
        return dist

    def _get_batch_idx(self, batch_size):
        valid_indices = np.arange(self.n)
        batch_indices = np.random.choice(valid_indices, size=batch_size, replace=False)
        return np.sort(batch_indices)

    def train_inputs(self):
        """
        Returns a sample from the latent variable distribution.
        """
        return self.sample_latent_variable()

    def add_loss_term(self, added_loss: AddedLossTerm):
        """
        Register an added loss term.

        Parameters
        ----------
        :param added_loss: additional loss added to the log posterior during MAP estimation
        """
        self.added_loss = added_loss
        self.register_added_loss_term("added_loss")