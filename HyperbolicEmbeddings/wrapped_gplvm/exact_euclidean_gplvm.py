"""
This implementation of an exact Euclidean GPLVM
uses our Wrapped GPLVM, but forces the manifold to
be Euclidean and the multitask kernel to have diagonal
task covariance.

Implementing it this way saves us from having to
re-implement pullback predictions.

TODO: If I want to be less lazy, I could in theory
implement this using GPyTorch's own RBFGrad kernel.
This would be a better comparison, since in the
current implementation we actually have a non-identity
correlation between tasks (i.e. it is still diagonal,
but there are values in the diagonal that aren't 
necessarily one).
"""

import torch

import gpytorch
from gpytorch.likelihoods import MultitaskGaussianLikelihood

from geomstats.geometry.euclidean import Euclidean

from HyperbolicEmbeddings.wrapped_gplvm.wrapped_gplvm import WrappedGPLVM

torch.set_default_dtype(torch.float64)


class ExactEuclideanGPLVM(WrappedGPLVM):
    """
    An implementation of an exact GPLVM on the Euclidean
    manifold of the dimension given by the training targets.

    In its current implementation, it is just a Wrapper around
    our WrappedGPLVM (hehe), creating internally an instance
    of an Euclidean manifold and a constant basepoint function
    on the origin.

    By default, it uses a multitask kernel with rank 0 (i.e.
    a diagonal one) and a zero mean.

    Attributes and methods can be found in the documentation
    for WrappedGPLVM.
    """

    def __init__(
        self,
        latent_dim: int,
        training_targets: torch.Tensor,
        likelihood: MultitaskGaussianLikelihood,
        kernel: gpytorch.kernels.Kernel = None,
        mean: gpytorch.priors.Prior = None,
        initial_latent_variables: torch.nn.Parameter = None,
        batch_independent: bool = True,
        initialization: str = "pca",
    ):
        """
        Constructs the ExactEuclideanGPLVM.

        Parameters
        ----------
        - latent_dim (type: int): The dimension of the latent space.

        - training_targets (type: torch.Tensor[float64]): the data we want to train
          on, assumed to be in some Euclidean space.

        - likelihood (type: gpytorch.likelihoods.MultitaskGaussianLikelihood):
          the likelihood of the GP in the whole ambient manifold.

        - kernel (type: gpytorch.kernels.Kernel, optional): The kernel of the GP.
          By default, it is a MultitaskKernel(ScaleKernel(RBFKernel)) with 0 rank
          (i.e. a diagonal task covariance).

        - mean (type: gpytorch.means.Mean, optional): The prior of the GP. By default,
          it is a ZeroMean.

        - initial_latent_variables (type: torch.Tensor, optional): a custom
          initialization for the latent variables. If provided, it overrides
          the initialization kwarg below.

        - batch_independent (type: bool, optional): determines whether we have a
          multitask (False) or batch independent (True) set-up for the GPLVM.
          By default, it is True

        - initialization (type: str): One of ["random", "pca"]. By default, we have
          pca initialization. However, if initial_latent_variables is provided, we
          will use those of course.
        """
        _, ambient_dim = training_targets.shape
        manifold = Euclidean(ambient_dim)

        basepoint_function = lambda _: torch.zeros((ambient_dim,))

        if kernel is None:
            if batch_independent:
                # Default kernel: ScaleKernel(RBFKernel()) w. batch independence.
                kernel = gpytorch.kernels.MultitaskKernel(
                    gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel(ard_num_dims=latent_dim)
                    ),
                    num_tasks=ambient_dim,
                    rank=0,  # i.e. diagonal task covariance.
                )
            else:
                kernel = gpytorch.kernels.MultitaskKernel(
                    gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel(ard_num_dims=latent_dim)
                    ),
                    num_tasks=ambient_dim,
                    rank=ambient_dim,
                )

        if mean is None:
            # Default tangent mean: Constant and B.Indep.
            mean = gpytorch.means.MultitaskMean(
                gpytorch.means.ZeroMean(ard_num_dims=latent_dim),
                num_tasks=ambient_dim,
            ).type(torch.float64)

        super().__init__(
            latent_dim,
            manifold,
            basepoint_function,
            training_targets,
            likelihood,
            kernel,
            mean,
            initial_latent_variables,
            False,
            initialization,
        )
