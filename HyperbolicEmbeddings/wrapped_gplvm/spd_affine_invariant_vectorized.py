import torch

from geomstats.geometry.spd_matrices import SPDMetricAffine


class SPDMetricAffineVectorized(SPDMetricAffine):
    """
    A Wrapper around the affine metric for the SPD Manifold
    provided by geomstats. In our framework, we are expecting
    tangent vectors to be 6-dimensional, but geomstats expects
    a 3x3 symmetric matrix. This wrapper handles that disconnect
    by re-implementing log and exp.

    This vectorization comes with considering the tangent space
    at SPD (i.e. the symmetric matrices) as R6 after taking the
    canonical basis. This connection between symmetric matrices
    and tangent vectors is taken care of by the local methods
    _matrix_from_tangent_vector and _tangent_vec_from_matrix
    """

    def __init__(self, n: int, power_affine=1):
        super().__init__(n, power_affine)
        self.tangent_space_dim = (n * (n - 1)) // 2 + n
        self.ambient_space_dim = n**2

    def exp(self, tangent_vec: torch.Tensor, base_point: torch.Tensor, **kwargs):
        """
        Computes the exponential map of the SPD manifold, taking tangent vectors
        as vectors of coordinates k=n(n+1)/2 coordinates. Returns an nxn SPD matrix.

        Parameters:
            - tangent_vec (torch.Tensor of shape (b, k)): the coordinates
              of a tangent vector.
            - base_point (torch.Tensor of shape (b, n, n) or (b, n**2)): the
              basepoints on the manifold from which the tangent vectors are
              "shot" using the exponential.

        Returns:
            - exp (torch.Tensor of shape (b, n, n)): the exponential of
              the tangent vectors at the given basepoints.
        """
        if len(base_point.shape) == 2 and base_point.shape[1] == self.n**2:
            base_point = base_point.view(-1, self.n, self.n)
        elif len(base_point.shape) == 1 and base_point.shape[0] == self.n**2:
            base_point = base_point.view(self.n, self.n)

        # At this point, we are sure that base_point is a matrix or
        # batch of matrices.

        tangent_matrix = self._matrix_from_tangent_vec(tangent_vec)

        return super().exp(tangent_matrix, base_point, **kwargs)

    def log(self, point: torch.Tensor, base_point: torch.Tensor, **kwargs):
        """
        Computes the logarithm map for the SPD manifold, returning a
        tangent vector of shape k = n(n+1)/2 + n. This is the tangent vector
        that is needed to get from the basepoints to the points using
        the exponential map.

        Parameters:
            - point (torch.Tensor of shape (b, n, n) or (b, n**2)): the points
              we want to "lift" to the tangent space.
            - base_point (torch.Tensor of shape (b, n, n) or (b, n**2)): the
              basepoints that are used to define the tangent spaces we want to
              lift to.

        Returns:
            - tangent_vectors (torch.Tensor of shape (b, k)): the tangent vectors
              that "shoot" to the points starting at the base_points.
        """
        if len(base_point.shape) == 2 and base_point.shape[1] == self.n**2:
            base_point = base_point.view(-1, self.n, self.n)

        if len(point.shape) == 2 and point.shape[1] == self.n**2:
            point = point.view(-1, self.n, self.n)

        tangent_matrix = super().log(point, base_point, **kwargs)

        return self._tangent_vec_from_matrix(tangent_matrix)

    def _matrix_from_tangent_vec(self, tangent_vec: torch.Tensor) -> torch.Tensor:
        """
        Grabs an k = n(n+1)/2 tangent vector and returns a symmetric matrix
        by filling the diagonal and upper-triangular part with the vector itself,
        and then forces the lower-triangular part to be symmetric.

        Parameters:
            - tangent_vec (torch.Tensor of shape (b, k))

        Returns:
            - tangent_matrix (torch.Tensor of shape (b, n, n))
        """
        if len(tangent_vec.shape) == 1:
            batched = False
            tangent_vec = tangent_vec.unsqueeze(0)
        elif len(tangent_vec.shape) == 2:
            batched = True
        else:
            raise ValueError("Expecting vectors of shape (n,) or (b, n).")

        i, j = torch.triu_indices(self.n, self.n)
        b, _ = tangent_vec.shape
        tangent_matrices = torch.zeros((b, self.n, self.n), dtype=torch.float64)
        tangent_matrices[:, i, j] = tangent_vec
        tangent_matrices.transpose(1, 2)[:, i, j] = tangent_vec

        if batched:
            return tangent_matrices
        else:
            return tangent_matrices.squeeze(0)

    def _tangent_vec_from_matrix(self, tangent_matrix: torch.Tensor) -> torch.Tensor:
        """
        Returns the n(n+1)/2-sized vector from a symmetric matrix by considering
        only the diagonal and upper-triangular part.

        Parameters:
            - tangent_matrix (torch.Tensor of shape (b, n, n))

        Returns:
            - tangent_vector (torch.Tensor of shape (b, k) where k = n(n+1)/2).
        """
        if len(tangent_matrix.shape) == 2:
            batched = False
            tangent_matrix = tangent_matrix.unsqueeze(0)
        elif len(tangent_matrix.shape) == 3:
            batched = True
        else:
            raise ValueError("Expecting matrices of shape (n,n) or (b, n, n).")

        i, j = torch.triu_indices(self.n, self.n)
        tangent_vec = tangent_matrix[:, i, j].flatten(start_dim=1)

        if batched:
            return tangent_vec
        else:
            return tangent_vec.squeeze(0)

    def tangent_space_basis_as_matrices(self) -> torch.Tensor:
        """
        SPDs are parallelizable, and the basis is given
        by the coordinates of the diagonal and upper-triangular
        part of the symmetric matrix.
        """
        i_s, j_s = torch.triu_indices(self.n, self.n)
        basis_matrices = [
            torch.zeros(self.n, self.n) for _ in range(self.tangent_space_dim)
        ]

        # triu indices
        for k, i, j in zip(range(self.tangent_space_dim), i_s, j_s):
            basis_matrices[k][i, j] = 1.0
            basis_matrices[k][j, i] = 1.0

        return torch.cat([bm.unsqueeze(0) for bm in basis_matrices])

    def metric_matrix(self, base_point: torch.Tensor) -> torch.Tensor:
        """
        Computes the metric matrix by evaluating the inner product
        of the basis tangent vectors at the given basepoint,
        assuming g_p_ij = g_p(e_i, e_j).

        Parameters
        ----------

        - base_point (torch.Tensor of shape (n, n), (n**2,) (b, n, n) or
          (b, n**2)) the matrix basepoint in which to evaluate the
          inner product.
        """
        # Making sure that base_point is batched
        if len(base_point.shape) == 1:
            # Making sure it's a single vector of size n**2
            assert base_point.shape[0] == self.n**2
            batched = False
            base_point = base_point.view(1, self.n, self.n)
        elif len(base_point.shape) == 2:
            # We have two cases, it's either (n, n) or (b, n**2)
            if base_point.shape[0] == base_point.shape[1] == self.n:
                print("Warning: assuming what was given is a single matrix.")
                batched = False
                base_point = base_point.unsqueeze(0)
            elif base_point.shape[1] == self.n**2:
                batched = True
                base_point = base_point.view(-1, self.n, self.n)
            else:
                raise ValueError(
                    "base_point's shape is of length 2 but it isn't "
                    f"(b, {self.n **2}) nor ({self.n}, {self.n})"
                )
        else:
            # Can only be (b, n, n)
            assert base_point.shape[-1] == base_point.shape[-2] == self.n
            batched = True

        # At this point, base_point is batched.
        metric_matrix = torch.zeros(
            (len(base_point), self.tangent_space_dim, self.tangent_space_dim)
        )

        basis = self.tangent_space_basis_as_matrices()

        i_s, j_s = torch.triu_indices(self.n, self.n)
        for i, j in zip(i_s, j_s):
            metric_matrix[:, i, j] = self.inner_product(basis[i], basis[j], base_point)
            if i != j:
                metric_matrix[:, j, i] = self.inner_product(
                    basis[i], basis[j], base_point
                )

        if not batched:
            metric_matrix = metric_matrix.squeeze(0)

        return metric_matrix


if __name__ == "__main__":
    spd_3 = SPDMetricAffineVectorized(3)
    metric_matrix = spd_3.metric_matrix(
        torch.eye(3).unsqueeze(0).repeat_interleave(100, dim=0)
    )
    print(metric_matrix)
