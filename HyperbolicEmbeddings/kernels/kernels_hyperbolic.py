import time
import warnings
import numpy as np
import torch
import gpytorch
import time

from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch, lorentz_to_poincare

# if torch.cuda.is_available():
#     device = torch.cuda.current_device()
# else:
#     device = 'cpu'
device = 'cpu'


class HyperbolicRiemannianGaussianKernel(gpytorch.kernels.Kernel):
    """
    Instances of this class represent a Gaussian (RBF) covariance matrix between input points on the hyperbolic
    manifold.

    Attributes
    ----------
    self.dim, dimension of the hyperbolic H^n on which the data handled by the kernel are living
    self.nb_points_integral, number of points used to compute the integral for the heat kernel

    Methods
    -------
    forward(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)
    forward_dim1(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)
    forward_dim2(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)
    forward_dim3(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)

    Static methods
    --------------
    """
    def __init__(self, dim, nb_points_integral=1000, **kwargs):
        """
        Initialisation.

        Parameters
        ----------
        :param dim: dimension of the hyperbolic H^n on which the data handled by the kernel are living

        Optional parameters
        -------------------
        :param nb_points_integral: number of points used to compute the integral for the heat kernel
        :param kwargs: additional arguments
        """
        self.has_lengthscale = True
        super(HyperbolicRiemannianGaussianKernel, self).__init__(has_lengthscale=True, ard_num_dims=None, **kwargs)

        # Dimension of hyperbolic manifold (data dimension = self.dim + 1)
        self.dim = dim

        # Number of points for the integral computation
        self.nb_points_integral = nb_points_integral

    def forward_dim1(self, x1, x2):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold H^1

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """
        # Compute hyperbolic distance
        distance = lorentz_distance_torch(x1, x2)

        # Kernel
        kernel = torch.exp(- torch.pow(distance, 2) / (2 * self.lengthscale ** 2))

        return kernel

    def inner_product(self, x1, x2, diag=False):
        """
        Computes the inner product between points in the hyperbolic manifold (Poincaré ball representation) as
        <z, b> = \frac{1}{2}\log\frac{1-|z|^2}{|z-b|^2}

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the matrix, or just the diagonal? If True, we must have `x1 == x2`

        Returns
        -------
        :return: inner product matrix between x1 and x2
        """
        if diag is False:
            # Expand dimensions to compute all vector-vector distances
            x1 = x1.unsqueeze(-2)
            x2 = x2.unsqueeze(-3)

            # Repeat x and y data along -2 and -3 dimensions to have b1 x ... x ndata_x x ndata_y x dim arrays
            x1 = torch.cat(x2.shape[-2] * [x1], dim=-2)
            x2 = torch.cat(x1.shape[-3] * [x2], dim=-3)

        # Difference between x1 and x2
        diff_x = x1 - x2

        # Inner product
        inner_product = 0.5 * torch.log((1. - torch.norm(x1, dim=-1)**2) / torch.norm(diff_x, dim=-1)**2)

        return inner_product

    def forward_dim2(self, x1, x2):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold H^2 following:

        p(z, w, t) ~ C \sum_{m} \rho_m \tanh(\pi \rho_m) \frac{1}{N}
                        \sum_{n} e^{(i 2 \rho_m + 1) \langle z, b_n \rangle} \overline{e^{(i 2 \rho_m + 1) \langle w,
        with b_n ~ U(\mathbb{S}^1) and \rho_m = |\tilde{\rho_m}|, \tilde{\rho_m} ~ N(0, 1/2t)
        and t = lengthscale^2 / 2

        In this case, the inner sum is approximated with a single sample b_n.
        Note that more samples of \rho_m (~1000) are required to achieve a similar approximation as for the
        forward_double_sum function.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """

        # Lorentz to Poincare
        x1_p = lorentz_to_poincare(x1)
        x2_p = lorentz_to_poincare(x2)

        # Sample b uniformly on S1
        angle_samples = 2. * torch.pi * torch.rand(self.nb_points_integral)
        bns = torch.cat((torch.cos(angle_samples)[:, None], torch.sin(angle_samples)[:, None]), -1).to(device)

        # Sample p - Getting samples from truncated normal distribution
        pms = torch.abs(torch.randn(self.nb_points_integral, requires_grad=False, device=device)
                        * (1. / self.lengthscale)).squeeze().to(device)

        # Compute phi_l coefficient and factor of the sum over pms
        phi_l_coefficient = (2j * pms + 1.).unsqueeze(-2).unsqueeze(-2)
        pms_factor = (pms * torch.tanh(torch.pi * pms)).unsqueeze(-2).unsqueeze(-2)

        # ## Batch computation
        # Compute inner product and expand dimensions to compute all vector-vector operations
        inner_product_x1 = self.inner_product(x1_p, bns).unsqueeze(-2)
        inner_product_x2 = self.inner_product(x2_p, bns).unsqueeze(-3)
        # Repeat data along -2 and -3 dimensions to have b1 x ... x ndata_x x ndata_y x dim arrays
        inner_product_x1 = torch.cat(inner_product_x2.shape[-2] * [inner_product_x1], dim=-2)
        inner_product_x2 = torch.cat(inner_product_x1.shape[-3] * [inner_product_x2], dim=-3)

        # Compute terms of the inner sum over b: we here use one sample on the circle for each element of the outer sum
        exponential_x1p = torch.exp(phi_l_coefficient * inner_product_x1)
        exponential_x2p = torch.exp(phi_l_coefficient * inner_product_x2)
        phi_l = exponential_x1p * torch.conj(exponential_x2p)

        # Compute heat kernel ~ outer sum over p
        outer_sum = torch.sum(pms_factor * phi_l, -1) / self.nb_points_integral

        # With normalization factor
        phi_l_normal = exponential_x1p[..., 0, 0, :] * torch.conj(exponential_x1p[..., 0, 0, :])
        normalization_factor = 1. / (torch.sum(pms_factor[..., 0, 0, :] * phi_l_normal, -1) / self.nb_points_integral)
        normalization_factor = normalization_factor.unsqueeze(-1).unsqueeze(-1)

        kernel = normalization_factor * outer_sum

        # Kernel
        return kernel.real

    def forward_dim3(self, x1, x2):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold H^1

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """
        # Compute hyperbolic distance
        distance = lorentz_distance_torch(x1, x2)

        # Kernel (simplifying non-distance-related terms)
        kernel = torch.exp(- torch.pow(distance, 2) / (2 * self.lengthscale ** 2))
        # kernel = kernel * distance / torch.sinh(distance)  # Adding 1e-8 avoids numerical issues around d=0
        kernel = kernel * (distance+1e-8) / torch.sinh(distance+1e-8)  # Adding 1e-8 avoids numerical issues around d=0
        return kernel

    def forward(self, x1, x2, diag=False, **params):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the whole distance matrix, or just the diagonal? If True, we must have `x1 == x2`
        :param params: additional parameters

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """

        # If dimension is 1, 2, or 3, compute the kernel directly
        if self.dim == 1:
            kernel = self.forward_dim1(x1, x2)
        elif self.dim == 2:
            # start = time.time()
            kernel = self.forward_dim2(x1, x2)
            # print(time.time()-start)
        elif self.dim == 3:
            kernel = self.forward_dim3(x1, x2)
        else:
            raise NotImplementedError

        # Handle diagonal case
        # TODO We should do this more efficiently
        if diag:
            kernel = torch.diagonal(kernel, 0, -2, -1)

        # Kernel
        return kernel


class HyperbolicRiemannianMillsonGaussianKernel(gpytorch.kernels.Kernel):
    """
    Instances of this class represent a Gaussian (RBF) covariance matrix between input points on the hyperbolic manifold
    using Millson's formula.

    Attributes
    ----------
    self.dim, dimension of the hyperbolic H^n on which the data handled by the kernel are living
    self.nb_points_integral, number of points used to compute the integral for the heat kernel

    Methods
    -------
    link_function(cosh_distance, b)
    forward(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)

    Static methods
    --------------
    """
    def __init__(self, dim, nb_points_integral=1000, **kwargs):
        """
        Initialisation.

        Parameters
        ----------
        :param dim: dimension of the hyperbolic H^n on which the data handled by the kernel are living

        Optional parameters
        -------------------
        :param nb_points_integral: number of points used to compute the integral for the heat kernel
        :param kwargs: additional arguments
        """
        self.has_lengthscale = True
        super(HyperbolicRiemannianMillsonGaussianKernel, self).__init__(has_lengthscale=True, ard_num_dims=None, **kwargs)

        # Dimension of hyperbolic manifold (data dimension = self.dim + 1)
        self.dim = dim

        # Number of points for the integral computation
        self.nb_points_integral = nb_points_integral

    def forward_dim1(self, distance):
        # Kernel
        kernel = torch.exp(- torch.pow(distance, 2) / (2 * self.lengthscale ** 2))

        return kernel

    def forward_dim2_montecarlo(self, distance):
        # Draw samples
        # The samples should be drawn according to N(0, lengthscale**2/2)
        # pms = torch.abs(torch.normal(torch.zeros(self.nb_points_integral), self.lengthscale ))
        pms = torch.abs(torch.randn(self.nb_points_integral) * self.lengthscale).squeeze()

        # Repeat data along last dimension for computing the integral approximation
        ext_distance = torch.cat(self.nb_points_integral * [distance.unsqueeze(-1)], dim=-1)
        cosh_distance = torch.cat(self.nb_points_integral * [torch.cosh(distance).unsqueeze(-1)], dim=-1)

        # Compute integral using Monte Carlo samples
        cosh_term = (pms + ext_distance) / torch.sqrt(torch.cosh(pms + ext_distance) - cosh_distance)
        exp_term = torch.exp(-(2. * ext_distance * pms + ext_distance**2) / (2. * self.lengthscale**2))
        kernel = torch.sum(exp_term * cosh_term, dim=-1) / self.nb_points_integral

        # Normalizing constant
        # normalizing_cst = np.sqrt(2.) * torch.exp(-self.lengthscale**2 / 8.) / (2. * torch.pi * self.lengthscale**2)
        normalizing_cst = torch.sum(pms / torch.sqrt(torch.cosh(pms) - 1.0), dim=-1) / self.nb_points_integral

        # return kernel
        return kernel / normalizing_cst

    def forward_dim2(self, distance):
        # Evaluate integral part
        s_vals = torch.linspace(1e-2, 100., self.nb_points_integral).to(device) + distance.unsqueeze(-1)
        integral_vals = s_vals * torch.exp(-s_vals**2/(2*self.lengthscale**2)) \
                        / torch.sqrt(torch.cosh(s_vals) - torch.cosh(distance.unsqueeze(-1)))

        # Kernel integral part
        kernel = torch.trapz(integral_vals, s_vals, dim=-1)

        # Normalizing constant
        s_vals_normalizing = torch.linspace(1e-2, 100., self.nb_points_integral).to(device)
        integral_vals_normalizing = s_vals_normalizing * \
                                    torch.exp(-s_vals_normalizing ** 2 / (2 * self.lengthscale ** 2)) \
                                    / torch.sqrt(torch.cosh(s_vals_normalizing) - 1.)
        normalizing_cst = torch.trapz(integral_vals_normalizing, s_vals_normalizing, dim=-1)

        # return kernel
        return kernel / normalizing_cst

    def forward_dim3(self, distance):
        # Kernel (simplifying non-distance-related terms)
        kernel = torch.exp(- torch.pow(distance, 2) / (2 * self.lengthscale ** 2))
        # kernel = kernel * distance / torch.sinh(distance)  # Adding 1e-8 avoids numerical issues around d=0
        kernel = kernel * (distance+1e-8) / torch.sinh(distance+1e-8)  # Adding 1e-8 avoids numerical issues around d=0
        return kernel

    def forward(self, x1, x2, diag=False, **params):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the whole distance matrix, or just the diagonal? If True, we must have `x1 == x2`
        :param params: additional parameters

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """
        # Compute hyperbolic distance
        distance = lorentz_distance_torch(x1, x2, diag=diag)

        # If dimension is 1, 2, or 3, compute the kernel directly
        if self.dim == 1:
            kernel = self.forward_dim1(distance)
        elif self.dim == 2:
            kernel = self.forward_dim2(distance)
            # kernel = self.forward_dim2_montecarlo(distance)
        elif self.dim == 3:
            kernel = self.forward_dim3(distance)
        else:
            if self.dim % 2 == 0:  # even dimension > 2
                # Number of necessary derivatives
                nb_derivatives = int((self.dim - 2) / 2)
                # Approximate distance smaller than a constant
                min_distance = 1e-6
                distance[distance < min_distance] = min_distance
                distance.requires_grad = True
                # Compute sinh of hyperbolic distance
                sinhdistance = torch.sinh(distance)

                # Compute derivative(s)
                derivative_part = self.forward_dim2(distance)
                for i in range(nb_derivatives):
                    gradient = torch.autograd.grad(derivative_part, distance,
                                                   grad_outputs=torch.ones_like(distance), create_graph=True)[0]
                    derivative_part = gradient / sinhdistance

                # Normalizing term
                normalizing_distance = min_distance * torch.ones((1, 1), dtype=distance.dtype, requires_grad=True)
                derivative_part_normalizing = self.forward_dim2(normalizing_distance)
                for i in range(nb_derivatives):
                    gradient_normalizing = torch.autograd.grad(derivative_part_normalizing,
                                                               normalizing_distance,
                                                               grad_outputs=torch.ones_like(normalizing_distance),
                                                               create_graph=True)[0]
                    derivative_part_normalizing = gradient_normalizing / torch.sinh(normalizing_distance)

                kernel = -derivative_part #/ torch.pow(sinhdistance, nb_derivatives)
                normalizing_cst = -derivative_part_normalizing #/ torch.pow(torch.sinh(normalizing_distance), nb_derivatives)
                kernel = kernel / normalizing_cst

            else:  # odd dimension > 3
                # Number of necessary derivatives
                nb_derivatives = int((self.dim - 3) / 2)
                # Approximate distance smaller than a constant
                min_distance = 1e-6
                distance[distance < min_distance] = min_distance
                distance.requires_grad = True
                # Compute sinh of hyperbolic distance
                sinhdistance = torch.sinh(distance)

                # Compute derivative(s)
                derivative_part = self.forward_dim3(distance)
                for i in range(nb_derivatives):
                    gradient = torch.autograd.grad(derivative_part, distance,
                                                   grad_outputs=torch.ones_like(distance), create_graph=True)[0]
                    derivative_part = gradient / sinhdistance

                # Normalizing term
                normalizing_distance = min_distance * torch.ones((1, 1), dtype=distance.dtype, requires_grad=True)
                derivative_part_normalizing = self.forward_dim3(normalizing_distance)
                for i in range(nb_derivatives):
                    gradient_normalizing = torch.autograd.grad(derivative_part_normalizing,
                                                                      normalizing_distance,
                                                                      grad_outputs=torch.ones_like(normalizing_distance),
                                                                      create_graph=True)[0]
                    derivative_part_normalizing = gradient_normalizing / torch.sinh(normalizing_distance)

                kernel = -derivative_part #/ torch.pow(sinhdistance, nb_derivatives)
                normalizing_cst = -derivative_part_normalizing #/ torch.pow(torch.sinh(normalizing_distance), nb_derivatives)

                kernel = kernel / normalizing_cst

        # Kernel
        return kernel


class HyperbolicD2RiemannianGaussianKernel(gpytorch.kernels.Kernel):
    """
    Instances of this class represent a Gaussian (RBF) covariance matrix between input points on the hyperbolic manifold
    using an integral over \phi_\lambda functions.

    Attributes
    ----------
    self.dim, dimension of the hyperbolic H^n on which the data handled by the kernel are living
    self.nb_points_int_phi_lambda, number of points used to compute the integral of the \phi_\lambda functions
    self.nb_points_int_heat, number of points used to compute the integral for the heat kernel

    Methods
    -------
    link_function(cosh_distance, b)
    forward(point1_in_hyperbolic, point2_in_hyperbolic, diagonal_matrix_flag=False, **params)

    Static methods
    --------------
    """
    def __init__(self, dim, nb_points_int_phi_l=50, nb_points_int_heat=200, single_sum_computation=True, **kwargs):
        """
        Initialisation.

        Parameters
        ----------
        :param dim: dimension of the hyperbolic H^n on which the data handled by the kernel are living

        Optional parameters
        -------------------
        :param nb_points_int_phi_lambda: number of points used to compute the integral of the \phi_\lambda functions
        :param nb_points_int_heat: number of points used to compute the integral for the heat kernel
        :param single_sum_computation: if True, the single sum approximation is used instead of the double sum one
        :param kwargs: additional arguments
        """
        self.has_lengthscale = True
        super(HyperbolicD2RiemannianGaussianKernel, self).__init__(has_lengthscale=True, ard_num_dims=None, **kwargs)

        # Dimension of hyperbolic manifold (data dimension = self.dim + 1)
        if dim != 2:
            raise ValueError('The HyperbolicD2RiemannianGaussianKernel is only for dimension 2!')
        self.dim = dim

        # Number of points for the integral computation
        self.nb_points_int_phi_l = nb_points_int_phi_l
        self.nb_points_int_heat = nb_points_int_heat

        # Computation type
        self.single_sum_computation = single_sum_computation
        if single_sum_computation:
            if self.nb_points_int_phi_l != self.nb_points_int_heat:
                self.nb_points_int_heat = self.nb_points_int_phi_l
                warnings.warn("Number of points for phi_l and heat kernel sum set equals for single sum computation.")

    def inner_product(self, x1, x2, diag=False):
        """
        Computes the inner product between points in the hyperbolic manifold (Poincaré ball representation) as
        <z, b> = \frac{1}{2}\log\frac{1-|z|^2}{|z-b|^2}

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the matrix, or just the diagonal? If True, we must have `x1 == x2`

        Returns
        -------
        :return: inner product matrix between x1 and x2
        """
        if diag is False:
            # Expand dimensions to compute all vector-vector distances
            x1 = x1.unsqueeze(-2)
            x2 = x2.unsqueeze(-3)

            # Repeat x and y data along -2 and -3 dimensions to have b1 x ... x ndata_x x ndata_y x dim arrays
            x1 = torch.cat(x2.shape[-2] * [x1], dim=-2)
            x2 = torch.cat(x1.shape[-3] * [x2], dim=-3)

        # Difference between x1 and x2
        diff_x = x1 - x2

        # Inner product
        inner_product = 0.5 * torch.log((1. - torch.norm(x1, dim=-1)**2) / torch.norm(diff_x, dim=-1)**2)

        return inner_product

    def forward_double_sum(self, x1, x2, diag=False, **params):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold following:

        p(z, w, t) ~ C \sum_{m} \rho_m \tanh(\pi \rho_m) \frac{1}{N}
                        \sum_{n} e^{(i 2 \rho_m + 1) \langle z, b_n \rangle} \overline{e^{(i 2 \rho_m + 1) \langle w,
        with b_n ~ U(\mathbb{S}^1) and \rho_m = |\tilde{\rho_m}|, \tilde{\rho_m} ~ N(0, 1/2t)
        and t = lengthscale^2 / 2

        In this case, both sums are explicitely computed by sampling b_n and \rho_m.
        50 samples for b_n with 200 for \rho_m usually give a good approximation of the kernel.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the whole kernel matrix, or just the diagonal? If True, we must have `x1 == x2`
        :param params: additional parameters

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """

        # Lorentz to Poincare
        x1_p = lorentz_to_poincare(x1)
        x2_p = lorentz_to_poincare(x2)

        # Sample b uniformly on S1
        angle_samples = torch.linspace(0., 2. * torch.pi * (self.nb_points_int_phi_l-1)/self.nb_points_int_phi_l,
                                       self.nb_points_int_phi_l)
        bns = torch.cat((torch.cos(angle_samples)[:, None], torch.sin(angle_samples)[:, None]), -1).to(device)

        # Sample p - Getting samples from truncated normal distribution
        pms = torch.abs(torch.randn(self.nb_points_int_heat, requires_grad=False, device=device)
                        * (1./self.lengthscale)).squeeze().to(device)
        # pms = torch.abs(torch.normal(torch.zeros((1, self.nb_points_int_heat)), 1./self.lengthscale))

        # Compute phi_l coefficient and factor of the sum over pms
        phi_l_coefficient = (2j * pms + 1.)
        pms_factor = pms * torch.tanh(torch.pi * pms)

        # start = time.time()
        # Compute inner product and expand dimensions to compute all vector-vector operations
        inner_product_x1 = self.inner_product(x1_p, bns).unsqueeze(-2)
        inner_product_x2 = self.inner_product(x2_p, bns).unsqueeze(-3)
        # Repeat data along -2 and -3 dimensions to have b1 x ... x ndata_x x ndata_y x dim arrays
        inner_product_x1 = torch.cat(inner_product_x2.shape[-2] * [inner_product_x1], dim=-2)
        inner_product_x2 = torch.cat(inner_product_x1.shape[-3] * [inner_product_x2], dim=-3)

        # # ## Batch computation
        # # start = time.time()
        # # Repeat data along last dimension for computing the integral approximation (outer sum)
        # inner_product_x1_b = torch.cat(self.nb_points_int_heat * [inner_product_x1.unsqueeze(-1)], dim=-1)
        # inner_product_x2_b = torch.cat(self.nb_points_int_heat * [inner_product_x2.unsqueeze(-1)], dim=-1)
        #
        # # Compute terms of the inner sum over b
        # exponential_x1p = torch.exp(phi_l_coefficient * inner_product_x1_b)
        # exponential_x2p = torch.exp(phi_l_coefficient * inner_product_x2_b)
        # inner_term = exponential_x1p * torch.conj(exponential_x2p)
        #
        # # Compute phi_lambda ~ inner sum over b
        # phi_l = torch.sum(inner_term, -2) / self.nb_points_int_phi_l
        #
        # # Compute heat kernel ~ outer sum over p
        # outer_sum = torch.sum(pms_factor * phi_l, -1) / self.nb_points_int_heat
        # # print("Batch: ", time.time() - start)

        # ## Loop computation (faster when data dimension is bigger)
        # start = time.time()
        batch_shape = list(self.batch_shape)
        kernel_shape = list(inner_product_x1.shape[:-1])[-2:]  # TODO there is probably a more elegant way to do this
        shape = batch_shape + kernel_shape
        outer_sum = torch.complex(torch.zeros(shape, dtype=x1.dtype), torch.zeros(shape, dtype=x1.dtype)).to(device)

        # Unsqueeze coefficients and factors for batch cases
        # [(...,) + (None,) * len(inner_product_shape)]
        phi_l_coefficient = phi_l_coefficient.unsqueeze(-2)
        for i in range(len(kernel_shape)):
            phi_l_coefficient = phi_l_coefficient.unsqueeze(-2)
            pms_factor = pms_factor.unsqueeze(-2)

        # Compute pms_factor * phi_l for each pms
        for i in range(self.nb_points_int_heat):
            exponential_x1p = torch.exp(phi_l_coefficient[..., i] * inner_product_x1)
            exponential_x2p = torch.exp(phi_l_coefficient[..., i] * inner_product_x2)
            outer_sum += pms_factor[..., i] * torch.sum(exponential_x1p * torch.conj(exponential_x2p), -1)

        outer_sum /= self.nb_points_int_phi_l * self.nb_points_int_heat
        # print("Loop: ", time.time() - start)

        # With normalization factor
        # normalization_factor = torch.exp(-self.lengthscale**2 / 8.0) / (4. * np.sqrt(np.pi / 2.) * self.lengthscale)
        normalization_factor = 1. / (torch.sum(pms_factor, -1) / self.nb_points_int_heat)  # phi_l = 1 when x1=x2
        kernel = normalization_factor * outer_sum

        # Sanity check
        if torch.max(torch.abs(kernel.imag)) > 5e-2:
            warnings.warn("Kernel maximum imaginary part above 1e-2!")

        # Kernel
        return kernel.real

    def forward_single_sum(self, x1, x2, diag=False, **params):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold following:

        p(z, w, t) ~ C \sum_{m} \rho_m \tanh(\pi \rho_m) \frac{1}{N}
                        \sum_{n} e^{(i 2 \rho_m + 1) \langle z, b_n \rangle} \overline{e^{(i 2 \rho_m + 1) \langle w,
        with b_n ~ U(\mathbb{S}^1) and \rho_m = |\tilde{\rho_m}|, \tilde{\rho_m} ~ N(0, 1/2t)
        and t = lengthscale^2 / 2

        In this case, the inner sum is approximated with a single sample b_n.
        Note that more samples of \rho_m (~1000) are required to achieve a similar approximation as for the
        forward_double_sum function.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the whole kernel matrix, or just the diagonal? If True, we must have `x1 == x2`
        :param params: additional parameters

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """

        # Lorentz to Poincare
        x1_p = lorentz_to_poincare(x1)
        x2_p = lorentz_to_poincare(x2)

        # Sample b uniformly on S1
        angle_samples = 2. * torch.pi * torch.rand(self.nb_points_int_phi_l)
        bns = torch.cat((torch.cos(angle_samples)[:, None], torch.sin(angle_samples)[:, None]), -1).to(device)

        # Sample p - Getting samples from truncated normal distribution
        pms = torch.abs(torch.randn(self.nb_points_int_heat, requires_grad=False, device=device)
                        * (1./self.lengthscale)).squeeze().to(device)
        # pms = torch.abs(torch.normal(torch.zeros((1, self.nb_points_int_heat)), 1./self.lengthscale))

        # Compute phi_l coefficient and factor of the sum over pms
        phi_l_coefficient = (2j * pms + 1.).unsqueeze(-2).unsqueeze(-2)
        pms_factor = (pms * torch.tanh(torch.pi * pms)).unsqueeze(-2).unsqueeze(-2)

        # ## Batch computation
        # start = time.time()
        # Compute inner product and expand dimensions to compute all vector-vector operations
        inner_product_x1 = self.inner_product(x1_p, bns).unsqueeze(-2)
        inner_product_x2 = self.inner_product(x2_p, bns).unsqueeze(-3)
        # Repeat data along -2 and -3 dimensions to have b1 x ... x ndata_x x ndata_y x dim arrays
        inner_product_x1 = torch.cat(inner_product_x2.shape[-2] * [inner_product_x1], dim=-2)
        inner_product_x2 = torch.cat(inner_product_x1.shape[-3] * [inner_product_x2], dim=-3)

        # Compute terms of the inner sum over b: we here use one sample on the circle for each element of the outer sum
        exponential_x1p = torch.exp(phi_l_coefficient * inner_product_x1)
        exponential_x2p = torch.exp(phi_l_coefficient * inner_product_x2)
        phi_l = exponential_x1p * torch.conj(exponential_x2p)

        # Compute heat kernel ~ outer sum over p
        outer_sum = torch.sum(pms_factor * phi_l, -1) / self.nb_points_int_heat
        # print("Batch: ", time.time() - start)

        # With normalization factor
        # normalization_factor = 1. / (torch.sum(pms_factor, -1) / self.nb_points_int_heat)  # phi_l = 1 when x1=x2,
        # not precise when using a single inner sample
        phi_l_normal = exponential_x1p[..., 0, 0, :] * torch.conj(exponential_x1p[..., 0, 0, :])
        normalization_factor = 1. / (torch.sum(pms_factor[..., 0, 0, :] * phi_l_normal, -1) / self.nb_points_int_heat)
        normalization_factor = normalization_factor.unsqueeze(-1).unsqueeze(-1)

        kernel = normalization_factor * outer_sum

        # Sanity check
        if torch.max(torch.abs(kernel.imag)) > 5e-2:
            warnings.warn("Kernel maximum imaginary part above 1e-2!")

        # Kernel
        return kernel.real

    def forward(self, x1, x2, diag=False, **params):
        """
        Computes the Gaussian kernel matrix between inputs x1 and x2 belonging to a hyperbolic manifold.

        Parameters
        ----------
        :param x1: input points on the hyperbolic manifold
        :param x2: input points on the hyperbolic manifold

        Optional parameters
        -------------------
        :param diag: Should we return the whole kernel matrix, or just the diagonal? If True, we must have `x1 == x2`
        :param params: additional parameters

        Returns
        -------
        :return: kernel matrix between x1 and x2
        """

        if self.single_sum_computation:
            return self.forward_single_sum(x1, x2, diag, **params)
        else:
            return self.forward_double_sum(x1, x2, diag, **params)

        # self.nb_points_int_heat = 1000
        # self.nb_points_int_phi_l = 1000
        # k1 = self.forward_single_sum(x1, x2, diag, **params)
        # self.nb_points_int_heat = 50
        # self.nb_points_int_phi_l = 50
        # k2 = self.forward_double_sum(x1, x2, diag, **params)
        # return k2


if __name__ == "__main__":
    import random
    import matplotlib.pyplot as plt

    plt.rcParams['text.usetex'] = True  # use Latex font for plots
    plt.rcParams['text.latex.preamble'] = r'\usepackage{bm}'

    device = 'cpu'

    seed = 1234
    # Set numpy and pytorch seeds
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # Define the dimension
    dim_data = 3
    dim_hyperbolic = dim_data - 1  # We are on H^d embedded in R^d+1

    # Generate data
    data = np.array([[7.14142843, -5., -5.],
                     [5.2308406, -3.63054358, -3.63054358],
                     [3.84582629, -2.62586936, -2.62586936],
                     [2.8472249, -1.88503178, -1.88503178],
                     [2.13470104, -1.33359449, -1.33359449],
                     [1.63666324, -0.91615134, -0.91615134],
                     [1.30307067, -0.59075933, -0.59075933],
                     [1.10040533, -0.32472441, -0.32472441],
                     [1.00830422, -0.09131647, -0.09131647],
                     [1.0175134, 0.13291636, 0.13291636],
                     [1.12895818, 0.37050409, 0.37050409],
                     [1.35383606, 0.64531856, 0.64531856],
                     [1.71474186, 0.98497199, 0.98497199],
                     [2.24793793, 1.4235914, 1.4235914],
                     [3.00699763, 2.00524746, 2.00524746],
                     [4.06818817, 2.78838259, 2.78838259],
                     [5.53813365, 3.85168303, 3.85168303],
                     [7.56452819, 5.30198485, 5.30198485],
                     [10.35097565, 7.28500847, 7.28500847],
                     [14.17744688, 10., 10.]])

    x1 = torch.from_numpy(data).to(device)
    x2 = torch.from_numpy(data[-1]).to(device)
    x2 = x1[-1][None]

    lengthscale = 1.5

    # RBF kernel
    rbfm_kernel = HyperbolicRiemannianMillsonGaussianKernel(dim_hyperbolic, nb_points_integral=1000)
    rbfm_kernel.lengthscale = lengthscale
    rbfm_kernel.to(device)
    K_rbfm = rbfm_kernel.forward(x1, x2)
    K_rbfm_np = K_rbfm.cpu().detach().numpy()
    # K_rbfm_np = K_rbfm_np / K_rbfm_np[-1]

    # RBF kernel for dimension 2
    # rbf2_kernel = HyperbolicD2RiemannianGaussianKernel(dim_hyperbolic, single_sum_computation=False,
    #                                                    nb_points_int_phi_l=50, nb_points_int_heat=50)
    # rbf2_kernel = HyperbolicD2RiemannianGaussianKernel(dim_hyperbolic, single_sum_computation=True,
    #                                                    nb_points_int_phi_l=1000, nb_points_int_heat=1000)
    rbf2_kernel = HyperbolicRiemannianGaussianKernel(dim_hyperbolic, nb_points_integral=1000)
    rbf2_kernel.lengthscale = lengthscale
    rbf2_kernel.to(device)
    K_rbf2 = rbf2_kernel.forward(x1, x2)
    K_rbf2_np = K_rbf2.cpu().detach().numpy()
    K_rbf2_np = K_rbf2_np / K_rbf2_np[-1]

    # Distance between x1 and x2
    distance = lorentz_distance_torch(x1, x2)
    distance_np = distance.cpu().detach().numpy()

    # Plot kernel value in function of the distance
    plt.figure(figsize=(12, 6))
    ax = plt.gca()
    plt.plot(distance_np, K_rbf2_np, color='red', linewidth=1)
    plt.plot(distance_np, K_rbfm_np, color='navy', linewidth=1.5)
    ax.tick_params(labelsize=22)
    ax.set_xlabel(r'$d_{\mathcal{H}}(\bm{x}_1,\bm{x}_2)$', fontsize=30)
    ax.set_ylabel(r'$k(\bm{x}_1,\bm{x}_2)$', fontsize=30)
    ax.legend([r'RBF 2', r'RBF Millson'], fontsize=24)
    plt.show()
