"""
This script analyses the mean stresses for all
the models in our set-up. We want to compare:

1. For the added poses experiment, how different are
   the mean stresses for the held-out poses for
   the backconstrained models:

    a) semifull: Hyperbolic and Euclidean.

2. For the held-out-classes experiment, how different
   are the mean stresses for the held-out data for the
   backconstrained models:

    a) semifull:
        i) leaving out the class 1F-1H-0K
        ii) leaving out the class 0F-1H-1K
        iii) leave out the class 1F-0H-0H
"""
from itertools import product
from pathlib import Path
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt

import torch
from gpytorch.kernels import Kernel

from HyperbolicEmbeddings.taxonomy_utils.data_loaders import load_taxonomy_data, load_taxonomy_added_data, \
    load_taxonomy_added_class
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch
from HyperbolicEmbeddings.losses.graph_based_loss import stress
from HyperbolicEmbeddings.model_loading_utils.load_gplvm import load_trained_gplvm_model
from HyperbolicEmbeddings.model_loading_utils.load_vae_model import load_vae
from HyperbolicEmbeddings.gplvm.gplvm_optimization import _get_extra_mll_args

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def compute_stress(algorithm, manifold, latent_dim, model_type, dataset, dataset_type, training_data, adjacency_matrix,
                   indices_in_graph, graph_distances, loss_type, loss_scale, max_manifold_distance,
                   added_data=None, added_indices_in_graph=None, added_classes=False, pullback_metric=False):
    # Paths
    MODEL_PATH = ROOT_DIR / (dataset + '_saved_models_' + algorithm)
    MODEL_PATH.mkdir(exist_ok=True)
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")
    VIZ_DIR.mkdir(exist_ok=True)

    # Model name
    if model_type is not None:
        model_name = manifold + '_' + algorithm + '_' + model_type + '_dim' + str(latent_dim) + '_' + dataset + '_'
    else:
        model_name = manifold + '_' + algorithm + '_dim' + str(latent_dim) + '_' + dataset + '_'
    if dataset_type is not None:
        model_name += dataset_type + '_'
    model_name += loss_type
    if loss_type != 'Zero':
        model_name += str(loss_scale)
    if max_manifold_distance:
        model_name += '_maxdist' + str(max_manifold_distance)
    if pullback_metric:
        model_name += '_pullback'
    if added_classes:
        model_name += '_missing_classes'

    # Loading the model
    if algorithm == "gplvm":
        mll = load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, model_type, dataset, training_data,
                                       adjacency_matrix, indices_in_graph, graph_distances, loss_type, loss_scale)
        model = mll.model
    elif algorithm == "vae":
        model = load_vae(model_name, MODEL_PATH, latent_dim, training_data)
    else:
        raise NotImplementedError

    if "vae" in model_name:
        # q_z_given_x, _, _ = model.forward(training_data)
        # latent_embeddings = q_z_given_x.mean
        latent_embeddings = model.encode(training_data).loc
        reconstructed_training_data = model.decode(latent_embeddings).loc
    else:
        latent_embeddings = model.X()
        reconstructed_training_data = model(latent_embeddings).mean.T

    if added_data is not None:
        # Compute latent variable corresponding to all added data
        added_latent_embeddings = mll.model.X.back_constraint_function(added_data, added_indices_in_graph)
        # Augmented latent variables
        latent_embeddings = torch.vstack((latent_embeddings, added_latent_embeddings))

    if "hyperbolic" in model_name:
        manifold_distances = lorentz_distance_torch(latent_embeddings, latent_embeddings)
    elif pullback_metric:
        manifold_distances = np.load(MODEL_PATH / (model_name + '_geodesic_distances.npz'))['geodesic_distances']
    elif "euclidean" in model_name:
        manifold_distances = Kernel().covar_dist(latent_embeddings, latent_embeddings)
    else:
        raise NotImplementedError

    # Compute stress
    stress_of_model = stress(manifold_distances, graph_distances)

    # if not loss_type == 'Zero':
    #     stress2 = model.added_loss.loss(model.X()).detach().numpy() / model.added_loss.loss_scale
    #     print(stress2)

    # Compute reconstruction error
    reconstruction_error = torch.abs(reconstructed_training_data - training_data)

    # Compute model likelihood
    if added_data is None:
        if "vae" in model_name:
            likelihood = 0.0
        else:
            # We reload the model with a zero stress loss in all cases to compute the likelihood without the additional stress loss
            # TODO this could be made more elegantly
            mll = load_trained_gplvm_model(model_name, MODEL_PATH, latent_dim, model_type, dataset, training_data,
                                           adjacency_matrix, indices_in_graph, graph_distances, loss_type, loss_scale=0.0)
            model = mll.model
            # Compute the likelihood
            mll.train()
            if hasattr(mll.model.train_inputs, '__call__'):
                # For variational GPLVMs, the training inputs are sampled at each iteration
                # For back-constrained GPLVMs, the training inputs are computed as a function of the training targets
                train_inputs = mll.model.train_inputs()
            else:
                # For exact GPLVMs, the training inputs are fixed
                train_inputs = mll.model.train_inputs[0]

            train_targets = mll.model.train_targets
            output = mll.model(train_inputs)
            # we sum here to support batch mode
            args = [output, train_targets] + _get_extra_mll_args(mll)
            # likelihood = mll.loglikelihood(*args).sum()
            likelihood = mll(*args).sum()
            mll.eval()

    else:
        likelihood = 0.0

    return stress_of_model, reconstruction_error, likelihood


def get_table_of_mean_stresses_for_gplvm(stress_map, stress_bc, latent_dim, dataset, dataset_type=None):
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(
            ["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"])
    )

    table = pd.DataFrame(
        [["None"] * 3] * len(multi_indices), index=multi_index, columns=["Stress", "Error", "Likelihood"]
    )

    training_data, _, adjacency_matrix, graph_distances, _, indices_in_graph, _, _, max_manifold_distance, _ = \
        load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Analysing the vanilla model's stress:
    print("=" * 50)
    print("VANILLA MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'MAP',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Zero', 0.0,
                                                                       max_manifold_distance)
    table.loc[("Euclidean", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Euclidean", "Vanilla"), "Error"] = (
        f"{reconstruction_error.mean().item():.2f}"
        + r"\pm"
        + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Euclidean", "Vanilla"), "Likelihood"] = (
            f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'MAP',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Zero', 0.0,
                                                                       max_manifold_distance)
    table.loc[("Hyperbolic", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Vanilla"), "Error"] = (
            f"{reconstruction_error.mean().item():.2f}"
            + r"\pm"
            + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Vanilla"), "Likelihood"] = (
        f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the Stress-trained models' stress:
    print("=" * 50)
    print("STRESS MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'MAP',
                                                                        dataset, dataset_type, training_data,
                                                                        adjacency_matrix, indices_in_graph,
                                                                        graph_distances, 'Stress', stress_map,
                                                                        max_manifold_distance)

    table.loc[("Euclidean", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Euclidean", "Stress"), "Error"] = (
        f"{reconstruction_error.mean().item():.2f}"
        + r"\pm"
        + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Euclidean", "Stress"), "Likelihood"] = (
            f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'MAP',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Stress', stress_map,
                                                                       max_manifold_distance)
    table.loc[("Hyperbolic", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Stress"), "Error"] = (
            f"{reconstruction_error.mean().item():.2f}"
            + r"\pm"
            + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Stress"), "Likelihood"] = (
        f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the BC+Stress model's stress
    print("=" * 50)
    print("BACKCONSTRAINED MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'BC',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Stress', stress_bc,
                                                                       max_manifold_distance)
    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Euclidean", "BC+Stress"), "Error"] = (
        f"{reconstruction_error.mean().item():.2f}"
        + r"\pm"
        + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Euclidean", "BC+Stress"), "Likelihood"] = (
            f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'BC',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Stress', stress_bc,
                                                                       max_manifold_distance)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "BC+Stress"), "Error"] = (
            f"{reconstruction_error.mean().item():.2f}"
            + r"\pm"
            + f"{reconstruction_error.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "BC+Stress"), "Likelihood"] = (
        f"{likelihood.item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_gplvm_added_data(stress_bc, latent_dim, dataset, dataset_type=None):
    multi_indices = [
        (space, reg)
        for space, reg in product(
            ["Euclidean", "Hyperbolic"], ["BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["BC+Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # Loading data and missing data
    training_data, added_data, data_mean, adjacency_matrix, graph_distances, all_graph_distances, \
    nodes_names, added_nodes_names, indices_in_graph, added_indices_in_graph, \
    nodes_legend_for_plot, all_nodes_legend_for_plot, color_function, max_manifold_distance = \
        load_taxonomy_added_data(dataset, dataset_type)

    print("BACKCONSTRAINED MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'BC',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       all_graph_distances, 'Stress', stress_bc,
                                                                       max_manifold_distance, added_data,
                                                                       added_indices_in_graph)
    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
            f"{stress_of_model.mean().item():.2f}"
            + r"$\pm$"
            + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'BC',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       all_graph_distances, 'Stress', stress_bc,
                                                                       max_manifold_distance, added_data,
                                                                       added_indices_in_graph)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
            f"{stress_of_model.mean().item():.2f}"
            + r"$\pm$"
            + f"{stress_of_model.std().item():.2f}"
    )

    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc, latent_dim, dataset, dataset_type=None):
    multi_indices = [
        (space, reg)
        for space, reg in product(
            ["Euclidean", "Hyperbolic"], ["BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["BC+Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # Loading data and missing data
    reduced_data, skipped_data, data_mean, adjacency_matrix, reduced_graph_distances, all_graph_distances, \
    reduced_nodes_names, skipped_nodes_names, reduced_indices_in_graph, skipped_indices_in_graph, \
    nodes_legend_for_plot, all_nodes_legend_for_plot, color_function, max_manifold_distance = \
        load_taxonomy_added_class(dataset, dataset_type)

    # all_data = torch.vstack((x_latent, added_x_latent))
    # skipped_nodes_names = [pose_name + "Added" for pose_name in skipped_nodes_names]
    # all_nodes_names = reduced_nodes_names + skipped_nodes_names
    # if dataset == 'grasps':
    #     ordered_nodes_name = ordered_hand_grasps_names
    # elif dataset == 'support-poses':
    #     pass
    # elif dataset == 'augmented-support-poses':
    #     ordered_nodes_name = ordered_shape_pose_names
    # elif dataset == 'bimanual':
    #     ordered_nodes_name = ordered_bimanual_names
    # data_ordered = torch.vstack(reorder_taxonomy_data(all_x_latent, all_nodes_names, ordered_nodes_name))
    # all_nodes_names_ordered = reorder_taxonomy_data(all_nodes_names, all_nodes_names, ordered_nodes_name)
    # graph_distances_ordered = reorder_distance_matrix(all_graph_distances, all_nodes_names, ordered_nodes_name)

    print("BACKCONSTRAINED MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'BC',
                                                                       dataset, dataset_type, reduced_data,
                                                                       adjacency_matrix, reduced_indices_in_graph,
                                                                       all_graph_distances, 'Stress',
                                                                       stress_bc, max_manifold_distance,
                                                                       skipped_data, skipped_indices_in_graph,
                                                                       added_classes=True)

    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
            f"{stress_of_model.mean().item():.2f}"
            + r"$\pm$"
            + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'BC',
                                                                       dataset, dataset_type, reduced_data,
                                                                       adjacency_matrix, reduced_indices_in_graph,
                                                                       all_graph_distances, 'Stress',
                                                                       stress_bc, max_manifold_distance,
                                                                       skipped_data, skipped_indices_in_graph,
                                                                       added_classes=True)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
            f"{stress_of_model.mean().item():.2f}"
            + r"$\pm$"
            + f"{stress_of_model.std().item():.2f}"
    )

    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_vaes(stress_eucl, stress_hyp, latent_dim, dataset, dataset_type=None):
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(
            ["Euclidean", "Hyperbolic"], ["Vanilla", "Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["Vanilla", "Stress"])
    )

    table = pd.DataFrame(
        [["None"] * 2] * len(multi_indices), index=multi_index, columns=["Stress", "Error"]
    )

    training_data, _, adjacency_matrix, graph_distances, _, indices_in_graph, _, _, max_manifold_distance, _ = \
        load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Analysing the vanilla model's stress:
    print("=" * 50)
    print("VANILLA MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('vae', 'euclidean', latent_dim, None,
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Zero', 0.0,
                                                                       max_manifold_distance)
    table.loc[("Euclidean", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Euclidean", "Vanilla"), "Error"] = (
        f"{reconstruction_error.mean().item():.2f}"
        + r"\pm"
        + f"{reconstruction_error.std().item():.2f}"
    )

    stress_of_model, reconstruction_error, likelihood = compute_stress('vae', 'hyperbolic', latent_dim, None,
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Zero', 0.0,
                                                                       max_manifold_distance)
    table.loc[("Hyperbolic", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Vanilla"), "Error"] = (
            f"{reconstruction_error.mean().item():.2f}"
            + r"\pm"
            + f"{reconstruction_error.std().item():.2f}"
    )

    # Analysing the Stress-trained models' stress:
    print("=" * 50)
    print("STRESS MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('vae', 'euclidean', latent_dim, None,
                                                                        dataset, dataset_type, training_data,
                                                                        adjacency_matrix, indices_in_graph,
                                                                        graph_distances, 'Stress', stress_eucl,
                                                                        max_manifold_distance)

    table.loc[("Euclidean", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Euclidean", "Stress"), "Error"] = (
        f"{reconstruction_error.mean().item():.2f}"
        + r"\pm"
        + f"{reconstruction_error.std().item():.2f}"
    )

    stress_of_model, reconstruction_error, likelihood = compute_stress('vae', 'hyperbolic', latent_dim, None,
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Stress', stress_hyp,
                                                                       max_manifold_distance)
    table.loc[("Hyperbolic", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"\pm"
        + f"{stress_of_model.std().item():.2f}"
    )
    table.loc[("Hyperbolic", "Stress"), "Error"] = (
            f"{reconstruction_error.mean().item():.2f}"
            + r"\pm"
            + f"{reconstruction_error.std().item():.2f}"
    )

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_pullback_gplvm(latent_dim, dataset, dataset_type=None):
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """

    training_data, _, adjacency_matrix, graph_distances, _, indices_in_graph, _, _, max_manifold_distance, _ = \
        load_taxonomy_data(dataset, dataset_type=dataset_type)

    # Analysing the vanilla model's stress:
    print("=" * 50)
    print("PULLBACK MODELS")
    stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'MAP',
                                                                       dataset, dataset_type, training_data,
                                                                       adjacency_matrix, indices_in_graph,
                                                                       graph_distances, 'Zero', 0.0,
                                                                       max_manifold_distance, pullback_metric=True)

    print(f"{stress_of_model.mean().item():.2f}" + r"\pm" + f"{stress_of_model.std().item():.2f}")


def get_figure_stress_likelihood_error_gplvms(stress_scale_list, latent_dim, dataset, dataset_type=None):
    """
    This function plots the stress, likelihood, and reconstruction error as a function of the stress loss scale
    """

    training_data, _, adjacency_matrix, graph_distances, _, indices_in_graph, _, _, max_manifold_distance, _ = \
        load_taxonomy_data(dataset, dataset_type=dataset_type)

    euclidean_stress = []
    euclidean_likelihood = []
    euclidean_reconstruction_error = []
    hyperbolic_stress = []
    hyperbolic_likelihood = []
    hyperbolic_reconstruction_error = []

    for loss_scale in stress_scale_list:
        stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'euclidean', latent_dim, 'MAP',
                                                                           dataset, dataset_type, training_data,
                                                                           adjacency_matrix, indices_in_graph,
                                                                           graph_distances, 'Stress', loss_scale,
                                                                           max_manifold_distance)
        euclidean_stress.append(stress_of_model.mean().item())
        euclidean_likelihood.append(likelihood.item())
        euclidean_reconstruction_error.append(reconstruction_error.mean().item())

        stress_of_model, reconstruction_error, likelihood = compute_stress('gplvm', 'hyperbolic', latent_dim, 'MAP',
                                                                           dataset, dataset_type, training_data,
                                                                           adjacency_matrix, indices_in_graph,
                                                                           graph_distances, 'Stress', loss_scale,
                                                                           max_manifold_distance)
        hyperbolic_stress.append(stress_of_model.mean().item())
        hyperbolic_likelihood.append(likelihood.item())
        hyperbolic_reconstruction_error.append(reconstruction_error.mean().item())

    stress_scale = np.array(stress_scale_list)
    euclidean_stress = np.array(euclidean_stress)
    euclidean_likelihood = np.array(euclidean_likelihood)
    euclidean_reconstruction_error = np.array(euclidean_reconstruction_error)
    hyperbolic_stress = np.array(hyperbolic_stress)
    hyperbolic_likelihood = np.array(hyperbolic_likelihood)
    hyperbolic_reconstruction_error = np.array(hyperbolic_reconstruction_error)

    CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
    ROOT_DIR = Path(CURRENT_DIR).parent.parent.resolve()
    VIZ_DIR = ROOT_DIR / (dataset + "_model_viz_final")

    fig, ax1 = plt.subplots(figsize=(12, 6))
    color = 'dodgerblue'
    ax1.set_xlabel('$\gamma$', fontsize=30)
    ax1.set_ylabel('$\ell_{stress}$', color=color, fontsize=30)
    ax1.plot(stress_scale, hyperbolic_stress, color=color, linewidth=2)
    ax1.plot(stress_scale, euclidean_stress, color=color, linestyle='--', linewidth=2)
    ax1.axvline(x=6000, color='black', linestyle='-', linewidth=2)  # TODO this is for grasps 3
    ax1.axhline(y=0.03, color='navy', linestyle='-', linewidth=2)  # TODO this is for grasps 3
    ax1.axhline(y=0.13, color='navy', linestyle='--', linewidth=2)  # TODO this is for grasps 3
    # plt.xscale('log')
    plt.yscale('log')
    ax1.tick_params(axis='x', labelsize=18)
    ax1.tick_params(axis='y', labelcolor=color, labelsize=18)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.tick_params(labelsize=18)
    color = 'crimson'
    ax2.set_ylabel('$-\ell_{MAP}$', color=color, fontsize=26)  # we already handled the x-label with ax1
    ax2.plot(stress_scale, -hyperbolic_likelihood, color=color, linewidth=2)
    ax2.plot(stress_scale, -euclidean_likelihood, color=color, linestyle='--', linewidth=2)

    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig(VIZ_DIR / f"stress_vs_LL_gplvm_MAP_{dataset}_{str(latent_dim)}.png")


if __name__ == "__main__":
    # Augmented support poses
    # ####
    # get_table_of_mean_stresses_for_gplvm(stress_map=7000.0, stress_bc=5000.0, latent_dim=2, dataset='augmented-support-poses', dataset_type='semifull')
    # get_table_of_mean_stresses_for_gplvm(stress_map=10000.0, stress_bc=8000.0, latent_dim=3, dataset='augmented-support-poses', dataset_type='semifull')

    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=5000.0, latent_dim=2, dataset='augmented-support-poses', dataset_type='semifull')
    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=8000.0, latent_dim=3, dataset='augmented-support-poses', dataset_type='semifull')

    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=5000.0, latent_dim=2, dataset='augmented-support-poses', dataset_type='semifull')
    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=8000.0, latent_dim=3, dataset='augmented-support-poses', dataset_type='semifull')

    # get_table_of_mean_stresses_for_vaes(50.0, 200.0, 2, dataset='augmented-support-poses', dataset_type='semifull')
    # get_table_of_mean_stresses_for_vaes(50.0, 200.0, 3, dataset='augmented-support-poses', dataset_type='semifull')

    # get_table_of_mean_stresses_for_pullback_gplvm(2, dataset='augmented-support-poses', dataset_type='semifull')

    # ####
    # Grasps
    # ####
    # get_table_of_mean_stresses_for_gplvm(stress_map=5500.0, stress_bc=2000.0, latent_dim=2, dataset='grasps', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm(stress_map=6000.0, stress_bc=3000.0, latent_dim=3, dataset='grasps', dataset_type=None)

    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=2000.0, latent_dim=2, dataset='grasps', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=3000.0, latent_dim=3, dataset='grasps', dataset_type=None)

    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=2000.0, latent_dim=2, dataset='grasps', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=3000.0, latent_dim=3, dataset='grasps', dataset_type=None)

    # get_table_of_mean_stresses_for_vaes(50.0, 200.0, 2, dataset='grasps', dataset_type=None)
    # get_table_of_mean_stresses_for_vaes(100.0, 400.0, 3, dataset='grasps', dataset_type=None)

    # get_table_of_mean_stresses_for_pullback_gplvm(2, dataset='grasps', dataset_type=None)

    stress_scale_list = [0.0, 100.0, 200.0, 500.0, 1000.0, 2000.0, 5000.0, 6000.0, 10000.0, 12000.0, 15000.0, 20000.0]
    get_figure_stress_likelihood_error_gplvms(stress_scale_list, latent_dim=3, dataset='grasps', dataset_type=None)

    # ####
    # Bimanual manipulation
    # ####
    # get_table_of_mean_stresses_for_gplvm(stress_map=1500.0, stress_bc=1000.0, latent_dim=2, dataset='bimanual', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm(stress_map=6000.0, stress_bc=1200.0, latent_dim=3, dataset='bimanual', dataset_type=None)

    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=1000.0, latent_dim=2, dataset='bimanual', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm_added_data(stress_bc=1200.0, latent_dim=3, dataset='bimanual', dataset_type=None)

    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=1000.0, latent_dim=2, dataset='bimanual', dataset_type=None)
    # get_table_of_mean_stresses_for_gplvm_added_classes(stress_bc=1200.0, latent_dim=3, dataset='bimanual', dataset_type=None)

    # get_table_of_mean_stresses_for_vaes(50.0, 120.0, 2, dataset='bimanual', dataset_type=None)
    # get_table_of_mean_stresses_for_vaes(150.0, 300.0, 3, dataset='bimanual', dataset_type=None)

    # get_table_of_mean_stresses_for_pullback_gplvm(2, dataset='bimanual', dataset_type=None)


