"""
This script analyses the mean stresses for all
the models in our set-up. We want to compare:

1. For the added poses experiment, how different are
   the mean stresses for the held-out poses for
   the backconstrained models:

    a) semifull: Hyperbolic and Euclidean. 

2. For the held-out-classes experiment, how different
   are the mean stresses for the held-out data for the
   backconstrained models:

    a) semifull:
        i) leaving out the class 1F-1H-0K
        ii) leaving out the class 0F-1H-1K
        iii) leave out the class 1F-0H-0H
"""
from itertools import product
from pathlib import Path
import pandas as pd

import torch
from gpytorch.kernels import Kernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch
from HyperbolicEmbeddings.losses.graph_based_loss import stress

from HyperbolicEmbeddings.model_loading_utils.load_model_with_prior import load_model_with_prior_grasps
from HyperbolicEmbeddings.model_loading_utils.load_backconstrained_model import load_backconstrained_model_grasps
from HyperbolicEmbeddings.model_loading_utils.load_vae_model import load_vae
from HyperbolicEmbeddings.taxonomy_utils.HandGraspsDataParse import hand_grasps_distance_mapping
from HyperbolicEmbeddings.model_loading_utils.data_loading import load_training_data_grasps

from examples.taxonomy_augmented_support_poses.vae.vanilla_vae_augmented_support_poses import \
    load_data as load_vae_augmented_training_data

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def compute_stress(model_name):
    FINAL_MODELS_PATH = ROOT_DIR / "grasps_saved_models"

    # Loading the training data
    if "vae" in model_name:
        _, pose_data, _, pose_names = load_vae_augmented_training_data("feet5")  # TODO adapt !!
    else:
        grasp_data, grasp_names = load_training_data_grasps()

    # Loading the model
    if "backconstrained" in model_name:
        model = load_backconstrained_model_grasps(FINAL_MODELS_PATH / model_name)
    elif "vae" in model_name:
        model = load_vae(model_name)
    else:
        model = load_model_with_prior_grasps(FINAL_MODELS_PATH / model_name)

    if "vae" in model_name:
        q_z_given_x, _, _ = model.forward(grasp_data)
        latent_embeddings = q_z_given_x.mean
    else:
        latent_embeddings = model.X()

    if "euclidean" in model_name:
        manifold_distances = Kernel().covar_dist(latent_embeddings, latent_embeddings)
    else:
        manifold_distances = lorentz_distance_torch(latent_embeddings, latent_embeddings)

    graph_distances = model.added_loss.graph_distances

    stress2 = model.added_loss.loss(model.X()).detach().numpy() / model.added_loss.loss_scale
    print(stress2)
    stress_of_model = stress(manifold_distances, graph_distances)

    return stress_of_model


def compute_stress_in_vaes():
    pass


# def analysis_added_poses_experiment():
#     # ---------------- Analyzing augmented feet5 ------------------
#     # Loading missing data for feet5
#     (
#         added_pose_data,
#         added_joint_data,
#         added_shape_pose_names,
#         _,
#     ) = get_missing_data_feet5(ROOT_DIR / "data/support_poses" / "keyPoseShapesXPose.xml")
#     added_pose_data = torch.from_numpy(added_pose_data)
#     added_joint_data = torch.from_numpy(added_joint_data)
#
#     _, original_pose_names = load_training_data_suppport_poses("feet5", pose_training=True)
#     # Euclidean model
#     model_name = (
#         "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
#     )
#     model_path = ROOT_DIR / "final_models" / model_name
#
#     model = load_backconstrained_model_suppport_poses(model_path)
#     mean_stress = compute_stress_for_euclidean_model_with_added_poses(
#         model,
#         added_joint_data,
#         added_shape_pose_names,
#         original_pose_names=original_pose_names,
#         augmented=True,
#     )
#     print(
#         f"Mean stress for {model_name}'s missing data: {mean_stress.mean().item()} pm {mean_stress.std().item()}"
#     )
#
#     # Hyperbolic.
#     model_name = (
#         "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
#     )
#     model_path = ROOT_DIR / "final_models" / model_name
#
#     model = load_backconstrained_model_suppport_poses(model_path)
#     mean_stress = compute_stress_for_hyperbolic_model_with_added_poses(
#         model,
#         added_joint_data,
#         added_shape_pose_names,
#         original_pose_names=original_pose_names,
#         augmented=True,
#     )
#     print(
#         f"Mean stress for {model_name}'s missing data: {mean_stress.mean().item()} pm {mean_stress.std().item()}"
#     )
#
#
# def analysis_added_classes_experiment():
#     # =================== Analysing augmented feet5 =====================
#
#     # ---------- skipping 1F1H0K --------------
#     added_pose_data, added_joint_data, added_shape_pose_names = load_skipped_data_suppport_poses(
#         "feet5", (1, 1, 0)
#     )
#     training_data, original_pose_names = load_training_data_suppport_poses(
#         "feet5", leave_out=(1, 1, 0), pose_training=False
#     )
#
#     model_name = "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5_leaves_out_1F_1H_0K"
#     model_path = (
#         ROOT_DIR / "final_models" / "leave_one_class_out_experiment" / model_name
#     )
#
#     model = load_backconstrained_model_suppport_poses(
#         model_path,
#         leave_out=(1, 1, 0),
#         training_data=training_data,
#         shape_pose_names=original_pose_names,
#     )
#     stress = compute_stress_for_euclidean_model_with_added_poses(
#         model,
#         added_joint_data,
#         added_shape_pose_names,
#         original_pose_names=original_pose_names,
#         augmented=True,
#     )
#     # print(stress)
#     mean_stress = stress.mean()
#     std_stress = stress.std()
#     print(
#         f"Mean stress for {model_name}'s missing data: {mean_stress.item()} +/- {std_stress.item()}"
#     )
#
#     model_name = "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5_leaves_out_1F_1H_0K"
#     model_path = (
#         ROOT_DIR / "final_models" / "leave_one_class_out_experiment" / model_name
#     )
#
#     model = load_backconstrained_model_suppport_poses(
#         model_path,
#         leave_out=(1, 1, 0),
#         training_data=training_data,
#         shape_pose_names=original_pose_names,
#     )
#     stress = compute_stress_for_hyperbolic_model_with_added_poses(
#         model,
#         added_joint_data,
#         added_shape_pose_names,
#         original_pose_names=original_pose_names,
#         augmented=True,
#     )
#     # print(stress)
#     mean_stress = stress.mean()
#     std_stress = stress.std()
#     print(
#         f"Mean stress for {model_name}'s missing data: {mean_stress.item()} +/- {std_stress.item()}"
#     )
#
#     pass


def get_table_of_mean_stresses_for_models_2d_grasps(stress_prior, stress_bc):
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(
            # ["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"]  # TODO put back
            ["Euclidean", "Hyperbolic"], ["Stress", "BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        # (["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"])  # TODO put back
        (["Euclidean", "Hyperbolic"], ["Stress", "BC+Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # # Analysing the vanilla model's stress:  # TODO add back
    # print("=" * 50)
    # print("VANILLA MODELS")
    # model_name = "euclidean_egplvm_prior_dim2_grasps_maxdist5.0_Zero0.0"
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Euclidean", "Vanilla"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Vanilla Euclidean",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })
    #
    # model_name = "hyperbolic_egplvm_prior_dim2_grasps_maxdist5.0_Zero0.0"
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Hyperbolic", "Vanilla"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Vanilla Hyperbolic",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })

    # Analysing the Stress-trained models' stress:
    print("=" * 50)
    print("STRESS MODELS")
    model_name = "euclidean_egplvm_prior_dim2_grasps_maxdist5.0_Stress" + str(stress_prior)
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_prior_dim2_grasps_maxdist5.0_Stress" + str(stress_prior)
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the BC+Stress model's stress
    print("=" * 50)
    print("BACKCONSTRAINED MODELS")
    model_name = (
        "euclidean_egplvm_backconstrained_dim2_grasps_maxdist5.0_Stress" + str(stress_bc)
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = (
        "hyperbolic_egplvm_backconstrained_dim2_grasps_maxdist5.0_Stress" + str(stress_bc)
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_models_3d_grasps(stress_prior, stress_bc):
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(
            # ["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"]  # TODO put back
            ["Euclidean", "Hyperbolic"], ["BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        # (["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"])  # TODO put back
        (["Euclidean", "Hyperbolic"], ["BC+Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # # Analysing the vanilla model's stress:  # TODO add back
    # print("=" * 50)
    # print("VANILLA MODELS")
    # model_name = "euclidean_egplvm_prior_dim2_grasps_maxdist5.0_Zero0.0"
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Euclidean", "Vanilla"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Vanilla Euclidean",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })
    #
    # model_name = "hyperbolic_egplvm_prior_dim2_grasps_maxdist5.0_Zero0.0"
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Hyperbolic", "Vanilla"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Vanilla Hyperbolic",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })

    # # Analysing the Stress-trained models' stress:
    # print("=" * 50)
    # print("STRESS MODELS")
    # model_name = "euclidean_egplvm_prior_dim3_grasps_maxdist5.0_Stress" + str(stress_prior)
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Euclidean", "Stress"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Stress Euclidean",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })
    #
    # model_name = "hyperbolic_egplvm_prior_dim3_grasps_maxdist5.0_Stress" + str(stress_prior)
    # stress_of_model = compute_stress(model_name)
    # table.loc[("Hyperbolic", "Stress"), "Stress"] = (
    #     f"{stress_of_model.mean().item():.2f}"
    #     + r"$\pm$"
    #     + f"{stress_of_model.std().item():.2f}"
    # )
    # # table_rows.append({
    # #     "Model": "Stress Hyperbolic",
    # #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # # })
    #
    # Analysing the BC+Stress model's stress
    print("=" * 50)
    print("BACKCONSTRAINED MODELS")
    model_name = (
        "euclidean_egplvm_backconstrained_dim3_grasps_maxdist5.0_Stress" + str(stress_bc)
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = (
        "hyperbolic_egplvm_backconstrained_dim3_grasps_maxdist5.0_Stress" + str(stress_bc)
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))

# def get_table_of_mean_stresses_for_vae_baselines_augmented():
#     # table_rows = []
#
#     multi_indices = [
#         (space, reg)
#         for space, reg in product(["Euclidean", "Hyperbolic"], ["No reg.", "Stress"])
#     ]
#     multi_index = pd.MultiIndex.from_product(
#         (["Euclidean", "Hyperbolic"], ["No reg.", "Stress"])
#     )
#
#     table = pd.DataFrame(
#         ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
#     )
#
#     # Analysing the vanilla model's stress:
#     print("=" * 50)
#     print("VANILLA MODELS")
#     model_name = "vanilla_vae_augmented_joints_Zero0.0"
#     stress_of_model = compute_stress(model_name)
#     table.loc[("Euclidean", "No reg."), "Stress"] = (
#         f"{stress_of_model.mean().item():.2f}"
#         + r"$\pm$"
#         + f"{stress_of_model.std().item():.2f}"
#     )
#     # table_rows.append({
#     #     "Model": "Vanilla Euclidean",
#     #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
#     # })
#
#     model_name = "hyperbolic_vae_augmented_Zero0.0"
#     stress_of_model = compute_stress(model_name)
#     table.loc[("Hyperbolic", "No reg."), "Stress"] = (
#         f"{stress_of_model.mean().item():.2f}"
#         + r"$\pm$"
#         + f"{stress_of_model.std().item():.2f}"
#     )
#     # table_rows.append({
#     #     "Model": "Vanilla Hyperbolic",
#     #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
#     # })
#
#     # Analysing the Stress-trained models' stress:
#     print("=" * 50)
#     print("STRESS MODELS")
#     model_name = "vanilla_vae_augmented_joints_Stress1.5"
#     stress_of_model = compute_stress(model_name)
#     table.loc[("Euclidean", "Stress"), "Stress"] = (
#         f"{stress_of_model.mean().item():.2f}"
#         + r"$\pm$"
#         + f"{stress_of_model.std().item():.2f}"
#     )
#     # table_rows.append({
#     #     "Model": "Stress Euclidean",
#     #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
#     # })
#
#     model_name = "hyperbolic_vae_augmented_joints_Stress1.5"
#     stress_of_model = compute_stress(model_name)
#     table.loc[("Hyperbolic", "Stress"), "Stress"] = (
#         f"{stress_of_model.mean().item():.2f}"
#         + r"$\pm$"
#         + f"{stress_of_model.std().item():.2f}"
#     )
#     # table_rows.append({
#     #     "Model": "Stress Hyperbolic",
#     #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
#     # })
#
#     # df = pd.DataFrame(table_rows)
#     # print(df.to_latex(escape=False))
#     print(table.to_latex(escape=False))


if __name__ == "__main__":
    get_table_of_mean_stresses_for_models_2d_grasps(stress_prior=6000.0, stress_bc=1000)
    # stress_bc= 3, 4, 5, 6, 8
    # stress_bc= 0.3, 0.4, 0.5, 0.6, 0.8

    # get_table_of_mean_stresses_for_models_3d_grasps(stress_prior=6.0, stress_bc=0.8)


    # get_table_of_mean_stresses_for_vae_baselines_augmented()
    # analysis_added_poses_experiment()
    # analysis_added_classes_experiment()
