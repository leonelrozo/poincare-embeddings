"""
This script analyses the mean likelihoods for all
the models in our set-up. We want to compare:

1. For the added poses experiment, how different are
   the mean likelihoods for the held-out poses for
   the backconstrained models:

    a) semifull: Hyperbolic and Euclidean. 

2. For the held-out-classes experiment, how different
   are the mean likelihoods for the held-out data for the
   backconstrained models:

    a) semifull:
        i) leaving out the class 1F-1H-0K
        ii) leaving out the class 0F-1H-1K
        iii) leave out the class 1F-0H-0H
"""
from itertools import product
from pathlib import Path
import pandas as pd

from HyperbolicEmbeddings.taxonomy_utils.compute_metrics_for_support_poses_models import compute_likelihood

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def get_table_of_mean_likelihoods_for_models_3d_feet_augmented():
    """
    This function gets the likelihood numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg) for space, reg in product(["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"])
    ]
    multi_index = pd.MultiIndex.from_product((["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"]))

    table = pd.DataFrame(["None"] * len(multi_indices), index=multi_index, columns=["Stress"])

    # Analysing the vanilla model's likelihood:
    print("=" * 50)
    print("VANILLA MODELS")
    model_name = "euclidean_egplvm_augmented_joints_prior_dim3_feet5_Zero0.0"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Euclidean",
               "Vanilla"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "Vanilla Euclidean",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_augmented_joints_prior_dim3_feet5_Zero0.0"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Hyperbolic",
               "Vanilla"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "Vanilla Hyperbolic",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    # Analysing the Stress-trained models' likelihood:
    print("=" * 50)
    print("STRESS MODELS")
    model_name = "euclidean_egplvm_augmented_joints_prior_dim3_feet5_Stress5.0"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Euclidean",
               "Stress"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "Stress Euclidean",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_augmented_joints_prior_dim3_feet5_Stress5.0"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Hyperbolic",
               "Stress"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "Stress Hyperbolic",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    # Analysing the BC+Stress model's likelihood
    print("=" * 50)
    print("BACKCONSTRAINED MODELS")
    model_name = "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Euclidean",
               "BC+Stress"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    mll_of_model = compute_likelihood(model_name)
    table.loc[("Hyperbolic",
               "BC+Stress"), "MLL"] = f"{mll_of_model.item():.2f}"
    # table_rows.append({
    #     "Model": "BC+Stress Hyperbolic",
    #     "Stress": f"{mll_of_model.mean().item():.2f}" + r"$\pm$" + f"{mll_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


if __name__ == "__main__":
    get_table_of_mean_likelihoods_for_models_3d_feet_augmented()
