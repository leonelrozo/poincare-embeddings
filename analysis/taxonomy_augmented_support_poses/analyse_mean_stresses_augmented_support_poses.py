"""
This script analyses the mean stresses for all
the models in our set-up. We want to compare:

1. For the added poses experiment, how different are
   the mean stresses for the held-out poses for
   the backconstrained models:

    a) semifull: Hyperbolic and Euclidean. 

2. For the held-out-classes experiment, how different
   are the mean stresses for the held-out data for the
   backconstrained models:

    a) semifull:
        i) leaving out the class 1F-1H-0K
        ii) leaving out the class 0F-1H-1K
        iii) leave out the class 1F-0H-0H
"""
from itertools import product
from pathlib import Path
import pandas as pd

import torch
from gpytorch.kernels import Kernel
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch
from HyperbolicEmbeddings.losses.graph_based_loss import stress
from HyperbolicEmbeddings.taxonomy_utils.compute_metrics_for_support_poses_models import \
    compute_stress_for_hyperbolic_model_with_added_poses, \
    compute_stress_for_euclidean_model_with_added_poses

from HyperbolicEmbeddings.model_loading_utils.data_loading import (
    load_skipped_data_suppport_poses,
    load_training_data_suppport_poses,
)
from HyperbolicEmbeddings.model_loading_utils.load_model_with_prior import load_model_with_prior_suppport_poses
from HyperbolicEmbeddings.model_loading_utils.load_backconstrained_model import load_backconstrained_model_suppport_poses
from HyperbolicEmbeddings.model_loading_utils.load_vae_model import load_vae
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import get_indices_shape_poses_graph, \
    get_missing_data_feet5, shape_poses_graph_distance_mapping

from examples.taxonomy_augmented_support_poses.vae.vanilla_vae_augmented_support_poses import \
    load_data as load_vae_augmented_training_data

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def compute_stress(model_name):
    FINAL_MODELS_PATH = ROOT_DIR / "final_models"

    # Loading the training data
    if "vae" in model_name and "augmented" in model_name:
        _, pose_data, _, pose_names = load_vae_augmented_training_data("feet5")
    elif "semifull" in model_name or "vae" in model_name:
        pose_data, pose_names = load_training_data_suppport_poses("semifull")
    elif "feet" in model_name:
        _, pose_names = load_training_data_suppport_poses("feet5")

    if "augmented" in model_name:
        augmented = True
    else:
        augmented = False

    # Computing graph distances
    if not augmented:
        graph_file_path = ROOT_DIR / "data/support_poses" / "support_poses_closure.csv"
    else:
        graph_file_path = ROOT_DIR / "data/support_poses" / "support_poses_augmented_closure.csv"
    _, shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, pose_names, augmented_taxonomy=augmented)
    shape_pose_indices = torch.from_numpy(shape_pose_indices)
    graph_distances = shape_poses_graph_distance_mapping(graph_file_path, pose_names, augmented_taxonomy=augmented)

    if "backconstrained" in model_name:
        model = load_backconstrained_model_suppport_poses(FINAL_MODELS_PATH / model_name)
    elif "vae" in model_name:
        model = load_vae(model_name)
    else:
        model = load_model_with_prior_suppport_poses(FINAL_MODELS_PATH / model_name)

    if "vae" in model_name:
        q_z_given_x, _, _ = model.forward(pose_data)
        latent_embeddings = q_z_given_x.mean
    else:
        latent_embeddings = model.X()

    if "euclidean" in model_name:
        manifold_distances = Kernel().covar_dist(latent_embeddings, latent_embeddings)
    else:
        manifold_distances = lorentz_distance_torch(
            latent_embeddings, latent_embeddings
        )

    stress_of_model = stress(manifold_distances, graph_distances)

    return stress_of_model


def compute_stress_in_vaes():
    pass


def analysis_added_poses_experiment():
    # ---------------- Analyzing augmented feet5 ------------------
    # Loading missing data for feet5
    (
        added_pose_data,
        added_joint_data,
        added_shape_pose_names,
        _,
    ) = get_missing_data_feet5(ROOT_DIR / "data/support_poses" / "keyPoseShapesXPose.xml")
    added_pose_data = torch.from_numpy(added_pose_data)
    added_joint_data = torch.from_numpy(added_joint_data)

    _, original_pose_names = load_training_data_suppport_poses("feet5", pose_training=True)
    # Euclidean model
    model_name = (
        "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    )
    model_path = ROOT_DIR / "final_models" / model_name

    model = load_backconstrained_model_suppport_poses(model_path)
    mean_stress = compute_stress_for_euclidean_model_with_added_poses(
        model,
        added_joint_data,
        added_shape_pose_names,
        original_pose_names=original_pose_names,
        augmented=True,
    )
    print(
        f"Mean stress for {model_name}'s missing data: {mean_stress.mean().item()} pm {mean_stress.std().item()}"
    )

    # Hyperbolic.
    model_name = (
        "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    )
    model_path = ROOT_DIR / "final_models" / model_name

    model = load_backconstrained_model_suppport_poses(model_path)
    mean_stress = compute_stress_for_hyperbolic_model_with_added_poses(
        model,
        added_joint_data,
        added_shape_pose_names,
        original_pose_names=original_pose_names,
        augmented=True,
    )
    print(
        f"Mean stress for {model_name}'s missing data: {mean_stress.mean().item()} pm {mean_stress.std().item()}"
    )


def analysis_added_classes_experiment():
    # =================== Analysing augmented feet5 =====================

    # ---------- skipping 1F1H0K --------------
    added_pose_data, added_joint_data, added_shape_pose_names = load_skipped_data_suppport_poses(
        "feet5", (1, 1, 0)
    )
    training_data, original_pose_names = load_training_data_suppport_poses(
        "feet5", leave_out=(1, 1, 0), pose_training=False
    )

    model_name = "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5_leaves_out_1F_1H_0K"
    model_path = (
        ROOT_DIR / "final_models" / "leave_one_class_out_experiment" / model_name
    )

    model = load_backconstrained_model_suppport_poses(
        model_path,
        leave_out=(1, 1, 0),
        training_data=training_data,
        shape_pose_names=original_pose_names,
    )
    stress = compute_stress_for_euclidean_model_with_added_poses(
        model,
        added_joint_data,
        added_shape_pose_names,
        original_pose_names=original_pose_names,
        augmented=True,
    )
    # print(stress)
    mean_stress = stress.mean()
    std_stress = stress.std()
    print(
        f"Mean stress for {model_name}'s missing data: {mean_stress.item()} +/- {std_stress.item()}"
    )

    model_name = "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5_leaves_out_1F_1H_0K"
    model_path = (
        ROOT_DIR / "final_models" / "leave_one_class_out_experiment" / model_name
    )

    model = load_backconstrained_model_suppport_poses(
        model_path,
        leave_out=(1, 1, 0),
        training_data=training_data,
        shape_pose_names=original_pose_names,
    )
    stress = compute_stress_for_hyperbolic_model_with_added_poses(
        model,
        added_joint_data,
        added_shape_pose_names,
        original_pose_names=original_pose_names,
        augmented=True,
    )
    # print(stress)
    mean_stress = stress.mean()
    std_stress = stress.std()
    print(
        f"Mean stress for {model_name}'s missing data: {mean_stress.item()} +/- {std_stress.item()}"
    )

    pass


def get_table_of_mean_stresses_for_models_3d_feet_augmented():
    """
    This function gets the stress numbers that are mentioned in Appendix G.3
    and put in table [TODO: add ref].
    """
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(
            ["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"]
        )
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["Vanilla", "Stress", "BC+Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # Analysing the vanilla model's stress:
    print("=" * 50)
    print("VANILLA MODELS")
    model_name = "euclidean_egplvm_augmented_joints_prior_dim3_feet5_Zero0.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_augmented_joints_prior_dim3_feet5_Zero0.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "Vanilla"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the Stress-trained models' stress:
    print("=" * 50)
    print("STRESS MODELS")
    model_name = "euclidean_egplvm_augmented_joints_prior_dim3_feet5_Stress5.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_egplvm_augmented_joints_prior_dim3_feet5_Stress5.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the BC+Stress model's stress
    print("=" * 50)
    print("BACKCONSTRAINED MODELS")
    model_name = (
        "euclidean_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = (
        "hyperbolic_egplvm_augmented_joints_backconstrained_dim3_feet5_Stress1.5"
    )
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "BC+Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "BC+Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


def get_table_of_mean_stresses_for_vae_baselines_augmented():
    # table_rows = []

    multi_indices = [
        (space, reg)
        for space, reg in product(["Euclidean", "Hyperbolic"], ["No reg.", "Stress"])
    ]
    multi_index = pd.MultiIndex.from_product(
        (["Euclidean", "Hyperbolic"], ["No reg.", "Stress"])
    )

    table = pd.DataFrame(
        ["None"] * len(multi_indices), index=multi_index, columns=["Stress"]
    )

    # Analysing the vanilla model's stress:
    print("=" * 50)
    print("VANILLA MODELS")
    model_name = "vanilla_vae_augmented_joints_Zero0.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "No reg."), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_vae_augmented_Zero0.0"
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "No reg."), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Vanilla Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # Analysing the Stress-trained models' stress:
    print("=" * 50)
    print("STRESS MODELS")
    model_name = "vanilla_vae_augmented_joints_Stress1.5"
    stress_of_model = compute_stress(model_name)
    table.loc[("Euclidean", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Euclidean",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    model_name = "hyperbolic_vae_augmented_joints_Stress1.5"
    stress_of_model = compute_stress(model_name)
    table.loc[("Hyperbolic", "Stress"), "Stress"] = (
        f"{stress_of_model.mean().item():.2f}"
        + r"$\pm$"
        + f"{stress_of_model.std().item():.2f}"
    )
    # table_rows.append({
    #     "Model": "Stress Hyperbolic",
    #     "Stress": f"{stress_of_model.mean().item():.2f}" + r"$\pm$" + f"{stress_of_model.std().item():.2f}"
    # })

    # df = pd.DataFrame(table_rows)
    # print(df.to_latex(escape=False))
    print(table.to_latex(escape=False))


if __name__ == "__main__":
    get_table_of_mean_stresses_for_models_3d_feet_augmented()
    get_table_of_mean_stresses_for_vae_baselines_augmented()
    analysis_added_poses_experiment()
    analysis_added_classes_experiment()
