"""
This script makes the main plots of Sec. 5.2. (the one about added poses and classes)
"""

from pathlib import Path
from typing import List

import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.figure import Figure
from mayavi import mlab
from gpytorch.kernels import Kernel

from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_distance_torch, lorentz_to_poincare
from HyperbolicEmbeddings.plot_utils.plot_general import plot_distance_matrix
from HyperbolicEmbeddings.plot_utils.plots_euclidean_gplvms import plot_euclidean_gplvm_2d, plot_euclidean_gplvm_3d
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d

from HyperbolicEmbeddings.model_loading_utils.data_loading import load_skipped_data_suppport_poses, load_training_data_suppport_poses
from HyperbolicEmbeddings.model_loading_utils.load_backconstrained_model_suppport_poses import load_backconstrained_model_suppport_poses
from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import color_function_shape_poses, \
    get_indices_shape_poses_graph, get_missing_data_reduced_dataset_2poses, get_missing_data_semifull_dataset, \
    reorder_taxonomy_data, shape_poses_graph_distance_mapping, get_missing_data_feet5

ROOT_DIR = Path(__file__).parent.parent.parent.resolve()


def _inner_plotting_function_hyperbolic2d(
    original_embeddings: torch.Tensor,
    original_pose_names: List[str],
    added_embeddings: torch.Tensor,
    added_shape_pose_names: List[str],
    original_alpha: float = 0.2,
    added_alpha: float = 1.0,
    model=None,
    model_magnification_path=None,
    max_magnification=None
) -> Figure:
    # Build the figure
    fig = plt.figure(figsize=(8, 8))
    
    # Plotting the original data
    original_colors = [color_function_shape_poses(pose_name) for pose_name in original_pose_names]
    original_marker = "o"
    
    # Hyperparameters and weird stuff for magnification factor.
    plot_hyperbolic_gplvm_2d(
        lorentz_to_poincare(original_embeddings).detach().numpy(),
        original_colors,
        x_legend=None,
        fig=fig,
        marker=original_marker,
        alpha=original_alpha,
        show=False,
        model=model,
        model_magnification_path=model_magnification_path,
        max_magnification=max_magnification
    )

    # Plotting the added data
    added_colors = [color_function_shape_poses(pose_name) for pose_name in added_shape_pose_names]
    added_marker = "*"

    plot_hyperbolic_gplvm_2d(
        lorentz_to_poincare(added_embeddings).detach().numpy(),
        added_colors,
        x_legend=None,
        fig=fig,
        marker=added_marker,
        alpha=added_alpha,
        show=False
    )

    return fig

def _inner_plotting_function_euclidean2d(
    original_embeddings: torch.Tensor,
    original_pose_names: List[str],
    added_embeddings: torch.Tensor,
    added_shape_pose_names: List[str],
    original_alpha: float = 0.2,
    added_alpha: float = 1.0,
    model=None,
) -> Figure:
    # Build the figure
    fig = plt.figure(figsize=(8, 8))
    
    # Plotting the original data
    original_colors = [color_function_shape_poses(pose_name) for pose_name in original_pose_names]
    original_marker = "o"
    
    # Hyperparameters and weird stuff for magnification factor.
    plot_euclidean_gplvm_2d(
        original_embeddings.detach().numpy(),
        original_colors,
        x_legend=None,
        fig=fig,
        marker=original_marker,
        alpha=original_alpha,
        model=model
    )

    # Plotting the added data
    added_colors = [color_function_shape_poses(pose_name) for pose_name in added_shape_pose_names]
    added_marker = "*"

    plot_euclidean_gplvm_2d(
        added_embeddings.detach().numpy(),
        added_colors,
        x_legend=None,
        fig=fig,
        marker=added_marker,
        alpha=added_alpha,
    )

    return fig


def _inner_plotting_function_hyperbolic3d(
        original_embeddings: torch.Tensor,
        original_pose_names: List[str],
        added_embeddings: torch.Tensor,
        added_shape_pose_names: List[str],
        original_alpha: float = 0.2,
        added_alpha: float = 1.0
) -> Figure:
    # Plotting the original data
    original_colors = [color_function_shape_poses(pose_name) for pose_name in original_pose_names]
    original_marker = 'sphere'

    # Hyperparameters and weird stuff for magnification factor.
    fig = plot_hyperbolic_gplvm_3d(
        lorentz_to_poincare(original_embeddings).detach().numpy(),
        original_colors,
        x_legend=None,
        marker=original_marker,
        opacity=original_alpha,
        show=False,
    )

    # Plotting the added data
    added_colors = [color_function_shape_poses(pose_name) for pose_name in added_shape_pose_names]
    added_marker = '2dcross'

    plot_hyperbolic_gplvm_3d(
        lorentz_to_poincare(added_embeddings).detach().numpy(),
        added_colors,
        x_legend=None,
        fig=fig,
        marker=added_marker,
        opacity=added_alpha,
        show=True
    )

    return fig


def _inner_plotting_function_euclidean3d(
        original_embeddings: torch.Tensor,
        original_pose_names: List[str],
        added_embeddings: torch.Tensor,
        added_shape_pose_names: List[str],
        original_alpha: float = 0.2,
        added_alpha: float = 1.0,
) -> Figure:
    # Plotting the original data
    original_colors = [color_function_shape_poses(pose_name) for pose_name in original_pose_names]
    original_marker = 'sphere'

    # Hyperparameters and weird stuff for magnification factor.
    fig = plot_euclidean_gplvm_3d(
        original_embeddings.detach().numpy(),
        original_colors,
        x_legend=None,
        marker=original_marker,
        opacity=original_alpha,
        show=False,
    )

    # Plotting the added data
    added_colors = [color_function_shape_poses(pose_name) for pose_name in added_shape_pose_names]
    added_marker = '2dcross'

    plot_euclidean_gplvm_3d(
        added_embeddings.detach().numpy(),
        added_colors,
        x_legend=None,
        fig=fig,
        marker=added_marker,
        opacity=added_alpha,
    )

    return fig


def _inner_plot_distance_matrix(
    original_embeddings: torch.Tensor,
    original_pose_names: List[str],
    added_embeddings: torch.Tensor,
    added_shape_pose_names: List[str],
    model_name: str,
    save_path: Path,
    augmented_taxonomy=False,
) -> Figure:

    # Adds "Added" to class names to differentiate them
    # when painting.
    added_shape_pose_names = [
        pose_name + "Added" for pose_name in added_shape_pose_names
    ]

    all_embeddings = torch.cat((original_embeddings, added_embeddings))
    all_shape_pose_names = original_pose_names + added_shape_pose_names

    ordered_embeddings = torch.vstack(reorder_taxonomy_data(all_embeddings, all_shape_pose_names))
    ordered_pose_names = reorder_taxonomy_data(all_shape_pose_names, all_shape_pose_names)

    # Computing the distances
    if "hyperbolic" in model_name:
        distances_latent = lorentz_distance_torch(ordered_embeddings, ordered_embeddings).detach().numpy()
    else:
        distances_latent = Kernel().covar_dist(ordered_embeddings, ordered_embeddings).detach().numpy()

    if not augmented_taxonomy:
        graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    else:
        graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_augmented_closure.csv'
    graph_distances = shape_poses_graph_distance_mapping(
        graph_file_path,
        list(map(lambda s: s.replace("Added", ""), ordered_pose_names)),
        augmented_taxonomy=augmented_taxonomy
    )
    
    max_distance = np.max(graph_distances.detach().numpy()) + 1.0
    ordered_colors = [
        color_function_shape_poses(pose_name.replace("Added", "")) for pose_name in ordered_pose_names
    ]
    box_colors = [
        "red" if "Added" in pose_name else "black" for pose_name in ordered_pose_names
    ]

    plot_distance_matrix(
        distances_latent,
        max_distance=max_distance,
        x_colors=ordered_colors,
        box_colors=box_colors,
        show=True,
        save_path=save_path
    )


def added_poses_plot(model_name: str):
    """
    This function saves the plot with added poses in model_viz/added_poses_{model_name}.png
    """
    # Get the original pose names for colors and labels
    dataset = model_name.split("_")[4]
    _, original_pose_names = load_training_data_suppport_poses(dataset)

    # Load the Hyperbolic backconstrained_model.
    model_path = ROOT_DIR / "final_models" / model_name
    model = load_backconstrained_model_suppport_poses(model_path)
    
    # Load the missing data for the semifull dataset 
    pose_data_path = ROOT_DIR / "data/support_poses" / "keyPoseShapesXPose.xml"
    if dataset == "semifull":
        added_pose_data, _, added_shape_pose_names, _ = get_missing_data_semifull_dataset(pose_data_path)
    elif dataset == "feet5":
        added_pose_data, _, added_shape_pose_names, _ = get_missing_data_feet5(pose_data_path)
    else:
        raise ValueError(f"Weird dataset: {dataset}. We only support semifull and feet 5")

    added_pose_data = torch.from_numpy(added_pose_data)
    graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    _, added_shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, added_shape_pose_names)
    added_shape_pose_indices = torch.from_numpy(added_shape_pose_indices)
    
    # Embed the missing data
    added_embeddings = model.X.back_constraint_function(added_pose_data, added_shape_pose_indices)

    # Get the original embeddins
    original_embeddings = model.X()

    # Plotting the figure
    
    ## Weird hyperparameters
    if "backconstrained" in model_name:
        max_magnification = 1.5
    elif "Stress" in model_name:
        max_magnification = 0.5
    elif "Zero" in model_name:
        max_magnification = 1.5
    
    model_magnification_path = ROOT_DIR / "final_models" / f"{model_name}_magfac.npz"

    if "hyperbolic" in model_name:
        if 'dim2' in model_name:
            fig = _inner_plotting_function_hyperbolic2d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names,
                model=model,
                model_magnification_path=model_magnification_path,
                max_magnification=max_magnification
            )
        elif 'dim3' in model_name:
            fig = _inner_plotting_function_hyperbolic3d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names
            )
        else:
            raise ValueError(f"Weird model dimension, not 2 or 3: {model_name}")
    elif "euclidean" in model_name:
        if 'dim2' in model_name:
            fig = _inner_plotting_function_euclidean2d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names,
                model=model
            )
        elif 'dim3' in model_name:
            fig = _inner_plotting_function_euclidean3d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names
            )
        else:
            raise ValueError(f"Weird model dimension, not 2 or 3: {model_name}")
    else:
        raise ValueError(f"Weird model name, no hyperbolic or euclidean: {model_name}")

    if 'dim2' in model_name:
        # Saving
        (ROOT_DIR / "model_viz").mkdir(exist_ok=True)
        fig.savefig(ROOT_DIR / "model_viz" / f"added_poses_{model_name}.png")

    # Second figure: the distance matrix
    _inner_plot_distance_matrix(
        original_embeddings,
        original_pose_names,
        added_embeddings,
        added_shape_pose_names,
        model_name,
        ROOT_DIR / "model_viz" / f"distance_matrix_for_added_poses_{model_name}_with_highlighting.png"
    )

    plt.show()


def added_classes_plot(model_name):
    """
    This plot gets the added classes visualization for Sec. 5.2.
    """

    # Load the model with left-out data
    model_path = ROOT_DIR / "final_models" / "leave_one_class_out_experiment" / model_name
    model = load_backconstrained_model_suppport_poses(model_path, leave_out=(1, 1, 0))


    # Get the original embeddings
    dataset = model_name.split("_")[4]
    _, original_pose_names = load_training_data_suppport_poses(dataset, leave_out=(1, 1, 0))
    original_embeddings = model.X()

    
    # Embed the missing data
    added_class_data, added_shape_pose_names = load_skipped_data_suppport_poses(dataset, leave_out=(1, 1, 0))
    
    graph_file_path = ROOT_DIR / "data/support_poses" / 'support_poses_closure.csv'
    _, added_shape_pose_indices = get_indices_shape_poses_graph(graph_file_path, added_shape_pose_names)
    added_shape_pose_indices = torch.from_numpy(added_shape_pose_indices)
    
    added_embeddings = model.X.back_constraint_function(added_class_data, added_shape_pose_indices)

    # Plot the rest.
    
    ## Weird hyperparameters
    if "backconstrained" in model_name:
        max_magnification = 1.5
    elif "Stress" in model_name:
        max_magnification = 0.5
    elif "Zero" in model_name:
        max_magnification = 1.5
    
    model_magnification_path = ROOT_DIR / "final_models" / f"{model_name}_magfac.npz"

    if "hyperbolic" in model_name:
        if 'dim2' in model_name:
            fig = _inner_plotting_function_hyperbolic2d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names,
                model=model,
                model_magnification_path=model_magnification_path,
                max_magnification=max_magnification
            )
        elif 'dim3' in model_name:
            fig = _inner_plotting_function_hyperbolic3d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names
            )
        else:
            raise ValueError(f"Weird model dimension, not 2 or 3: {model_name}")
    elif "euclidean" in model_name:
        if 'dim2' in model_name:
            fig = _inner_plotting_function_euclidean2d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names,
                model=model
            )
        elif 'dim3' in model_name:
            fig = _inner_plotting_function_euclidean3d(
                original_embeddings,
                original_pose_names,
                added_embeddings,
                added_shape_pose_names
            )
        else:
            raise ValueError(f"Weird model dimension, not 2 or 3: {model_name}")
    else:
        raise ValueError(f"Weird model name, no hyperbolic or euclidean: {model_name}")

    if 'dim2' in model_name:
        # Save it
        (ROOT_DIR / "model_viz").mkdir(exist_ok=True)
        fig.savefig(ROOT_DIR / "model_viz" / f"added_classes_{model_name}.png")

    # Second figure: the distance matrix
    _inner_plot_distance_matrix(
        original_embeddings,
        original_pose_names,
        added_embeddings,
        added_shape_pose_names,
        model_name,
        ROOT_DIR / "model_viz" / f"distance_matrix_for_added_classes_{model_name}_with_highlighting.png"
    )

    plt.show()

if __name__ == "__main__":
    model_names_for_poses = [
        # "hyperbolic_egplvm_backconstrained_dim2_semifull_Stress1.3",
        # "euclidean_egplvm_backconstrained_dim2_semifull_Stress1.3",
        "hyperbolic_egplvm_backconstrained_dim2_feet5_Stress0.7",
        "euclidean_egplvm_backconstrained_dim2_feet5_Stress0.7"
        "hyperbolic_egplvm_backconstrained_dim3_semifull_Stress1.5",
        "euclidean_egplvm_backconstrained_dim3_semifull_Stress1.5"
    ]
    for model_name in model_names_for_poses:
        added_poses_plot(model_name)
    

    model_names_for_classes = [
        # "hyperbolic_egplvm_backconstrained_dim2_semifull_Stress1.3_leaves_out_1F_1H_0K",
        # "euclidean_egplvm_backconstrained_dim2_semifull_Stress1.3_leaves_out_1F_1H_0K"
        "hyperbolic_egplvm_backconstrained_dim2_feet5_Stress0.7_leaves_out_1F_1H_0K",
        "euclidean_egplvm_backconstrained_dim2_feet5_Stress0.7_leaves_out_1F_1H_0K"
        # "euclidean_egplvm_backconstrained_dim2_semifull_Stress1.3_leaves_out_1F_1H_0K",
        "hyperbolic_egplvm_backconstrained_dim3_semifull_Stress1.5_leaves_out_1F_1H_0K",
        "euclidean_egplvm_backconstrained_dim3_semifull_Stress1.5_leaves_out_1F_1H_0K"
    ]
    for model_name in model_names_for_classes:
        added_classes_plot(model_name)
