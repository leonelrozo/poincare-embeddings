"""
This script contains examples on how to load
and inspect a model's embeddings.
"""
from pathlib import Path
import os

import torch

import gpytorch

from HyperbolicEmbeddings.hyperbolic_gplvm.hyperbolic_gplvm_models import HyperbolicExactGPLVM
from HyperbolicEmbeddings.hyperbolic_manifold.lorentz_functions_torch import lorentz_to_poincare
from HyperbolicEmbeddings.plot_utils.plots_hyperbolic_gplvms import plot_hyperbolic_gplvm_2d, plot_hyperbolic_gplvm_3d

from HyperbolicEmbeddings.taxonomy_utils.PoseShapeDataParse import parse_xml_pose_shape_data, \
    get_reduced_dataset_2poses, get_reduced_dataset_knees_2poses, get_reduced_dataset_feet_5poses, \
    get_one_of_each_class_dataset, get_semifull_dataset, shape_poses_graph_distance_mapping, \
    color_function_shape_poses, text_function_shape_poses

from HyperbolicEmbeddings.losses.graph_based_loss import ZeroAddedLossTermExactMLL, HyperbolicStressLossTermExactMLL, \
    HyperbolicDistortionLossTermExactMLL

from HyperbolicEmbeddings.model_loading_utils.load_hyperbolic_model import load_hyperbolic_model, load_dataset

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = Path(CURRENT_DIR).parent.parent.resolve()

if __name__ == "__main__":
    model_name = "hyperbolic_egplvm_prior_dim3_semifull_Zero_0.0_seed_73"
    model = load_hyperbolic_model(model_name)

    _, _, shape_pose_names, _ = load_dataset("semifull")
    shape_poses_legend_for_plot = text_function_shape_poses(shape_pose_names)

    sample = model.X.X
    sample = lorentz_to_poincare(sample).detach().numpy()
    sample_colors = [color_function_shape_poses(pose_name) for pose_name in shape_pose_names]

    plot_hyperbolic_gplvm_3d(sample, sample_colors, shape_poses_legend_for_plot)
