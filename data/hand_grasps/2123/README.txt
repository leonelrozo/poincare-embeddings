1. Data for the hand motion:
	* Following the line: <Motion name="d_h_2123" synchronized="true">
	* In the hand motion, the kinematic data (joints) follow the line: <Sensor type="Kinematic" version="1.0">

2. Data for the object motion: 
	* Following the line: <Motion name="[ObjectName]" type="object" synchronized="true">
	* In the object motion, the position of the object is given after the line: <Sensor type="ModelPose" version="1.0">
